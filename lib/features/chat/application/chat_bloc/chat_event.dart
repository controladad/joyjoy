part of 'chat_bloc.dart';

@freezed
class ChatEvent with _$ChatEvent {
  const factory ChatEvent.initial() = _Initial;
  const factory ChatEvent.updateCredit() = _UpdateCredit;
  const factory ChatEvent.sendMessage(String message) = _SendMessage;
  const factory ChatEvent.clearMessages() = _ClearMessages;
  const factory ChatEvent.messageLiked(String id) = _MessageLiked;
  const factory ChatEvent.messageDisliked(String id) = _MessageDisliked;
}
