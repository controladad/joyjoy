import 'package:dartz/dartz.dart';
import 'package:kt_dart/kt.dart';
import 'package:motivation_flutter/common/common_models.dart';
import 'package:motivation_flutter/core/failures.dart';
import 'package:motivation_flutter/features/profile/domain/models/collection_model.dart';

import 'package:motivation_flutter/features/profile/domain/models/profile_models.dart';
import 'package:motivation_flutter/features/profile/domain/models/reminder_model.dart';
import 'package:motivation_flutter/features/profile/domain/models/update_model.dart';
import 'package:motivation_flutter/features/profile/sub_features/select_author_category/select_author_category_bloc/select_author_category_bloc.dart';
import 'package:motivation_flutter/features/profile/sub_features/select_author_category/select_author_category_args.dart';
import 'package:motivation_flutter/features/quotes/domain/models/quotes_model.dart';
import 'package:motivation_flutter/features/quotes/domain/models/saved_quote_union.dart';
import 'package:motivation_flutter/features/quotes/domain/models/sub_category_model.dart';

abstract class IProfileRepository {
  Stream<void> get cacheUpdated;
  Future<Either<GeneralFailure, UpdateModel>> get hasUpdate;
  Future<void> clearAllCacheDebug();

  Future<Either<GeneralFailure, ProfileModel>> get getProfile;
  Future<Either<GeneralFailure, ProfileContentsModel>> get getOnlineContents;

  Future<Either<GeneralFailure, KtList<CollectionModel>>> get getCollections;
  Future<Either<GeneralFailure, KtList<SubCategoryModel>>> getCategoriesByIds(
    KtList<String> ids,
  );

  Future<Either<GeneralFailure, Unit>> changeUserName(String newName);

  Future<Either<GeneralFailure, KtList<QuoteModel>>> getQuotesByDestination({
    required SavedQuoteType destination,
  });

  Future<Either<GeneralFailure, LazyLoadResponseModel<AuthorOrCategoryUnion>>>
      getAllAuthorsOrCategoriesLazyLoad({
    required AuthorOrCategoryEnum type,
    required Option<KtList<String>> ids,
    required Option<String> searchQuery,
    required int page,
    required int limit,
  });

  Future<Either<GeneralFailure, KtList<CollectionModel>>> addOrUpdateCollection(
    CollectionModel collection,
  );
  Future<Either<GeneralFailure, KtList<ReminderModel>>> get getReminders;

  Future<Either<GeneralFailure, KtList<ReminderModel>>> removeReminder(
    String reminderId,
  );

  Future<Either<GeneralFailure, ReminderModel>> addOrUpdateReminder(
    ReminderModel reminder,
  );

  Future<Either<GeneralFailure, KtList<CollectionModel>>> deleteCollection(
    String id,
  );

  Future<Either<GeneralFailure, KtList<QuoteModel>>> removeQuote({
    required SavedQuoteType type,
    required String quoteId,
  });

  Future<Either<GeneralFailure, Unit>> updatePreferrers(
    AuthorOrCategoryEnum type,
    KtList<String> id,
  );
}
