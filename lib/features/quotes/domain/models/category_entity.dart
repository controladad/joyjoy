import 'package:hive/hive.dart';
import 'package:kt_dart/kt.dart';
import 'package:motivation_flutter/features/quotes/domain/models/category_model.dart';
import 'package:motivation_flutter/features/quotes/domain/models/sub_category_entity.dart';

part 'category_entity.g.dart';

@HiveType(typeId: 6)
class CategoryEntity extends HiveObject {
  @HiveField(0)
  String id;
  @HiveField(1)
  String name;
  @HiveField(2)
  List<SubCategoryEntity> subCategories;

  CategoryEntity({
    required this.id,
    required this.name,
    required this.subCategories,
  });

  CategoryEntity copyWith({
    String? id,
    String? name,
    List<SubCategoryEntity>? subCategories,
  }) {
    return CategoryEntity(
      id: id ?? this.id,
      name: name ?? this.name,
      subCategories: subCategories ?? this.subCategories,
    );
  }

  factory CategoryEntity.fromDomain(CategoryModel model) {
    return CategoryEntity(
      id: model.id,
      name: model.name,
      subCategories: model.subCategories
          .map((item) => SubCategoryEntity.fromDomain(item))
          .asList(),
    );
  }

  CategoryModel toDomain() {
    return CategoryModel(
      id: id,
      name: name,
      subCategories: subCategories.map((e) => e.toDomain()).toList().kt,
    );
  }
}
