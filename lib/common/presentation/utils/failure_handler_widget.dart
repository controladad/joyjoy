import 'package:dartz/dartz.dart';
import 'package:motivation_flutter/common/presentation/buttons.dart';
// import 'package:motivation_flutter/common/presentation/buttons.dart';
import 'package:motivation_flutter/common/presentation/common_imports.dart';
import 'package:motivation_flutter/core/failures.dart';
import 'package:store_redirect/store_redirect.dart';

class FailureHandlerWidget<T> extends StatelessWidget {
  final Option<Either<GeneralFailure, T>> data;
  final Widget Function(T data) child;
  final Function() onRetry;
  const FailureHandlerWidget({
    super.key,
    required this.data,
    required this.child,
    required this.onRetry,
  });

  @override
  Widget build(BuildContext context) {
    late String image;
    late String title;
    late String subtitle;
    String buttonLabel = context.translate.tryAgain;
    GeneralFailure? failure;
    T? model;

    if (data.isNone()) {
      return const Center(
        child: CircularProgressIndicator(
          color: AppTheme.primary_dark,
        ),
      );
    }

    data.fold(
      () => null,
      (a) => a.fold(
        (l) {
          failure = l;
          return l.map(
            noNetwork: (e) {
              image = Images.connectionErrorImage;
              title = context.translate.oops;
              subtitle = context.translate.looksLikeWeHitASnagConnecting;
            },
            authentication: (e) {
              image = Images.authenticationErrorImage;
              title = context.translate.authenticationError;
              subtitle = context.translate.authenticationErrorPleaseLoginAgain;
            },
            underMaintenance: (e) {
              buttonLabel = context.translate.checkWebsite;
              image = Images.underMaintenanceErrorImage;
              title = context.translate.underMaintenance;
              subtitle = context.translate.wereTakingAQuickBreakWellBeBackSoon;
            },
            forceUpdate: (e) {
              image = Images.forceUpdate;
              title = context.translate.whoops;
              subtitle = context.translate.please_update;
            },
            unexpected: (e) {
              print(e.message);
              image = Images.underMaintenanceErrorImage;
              title = context.translate.oops;
              subtitle = context.translate.looksLikeWeHitASnagConnecting;
            },
            localException: (e) {
              image = Images.underMaintenanceErrorImage;
              title = context.translate.oops;
              subtitle = context.translate.looksLikeWeHitASnagConnecting;
            },
          );
        },
        (r) {
          model = r;
        },
      ),
    );

    if (failure != null) {
      return failure!.maybeMap(
        forceUpdate: (e) => Column(
          children: [
            const Spacer(flex: 3),
            ImageWidget(image: image, height: 354, width: 227),
            const Spacer(),
            SText(
              context.translate.whoops,
              style: TStyle.HeadlineSmall,
            ),
            const SBox(height: 12),
            SText(
              context.translate.please_update,
              style: TStyle.BodyLarge,
            ),
            const SBox(height: 16),
            CustomFilledButton(
              label: context.translate.update,
              onTap: () async {
                StoreRedirect.redirect(
                  iOSAppId: Constants.appStoreId,
                );
              },
            ),
            const SBox(height: 48),
          ],
        ),
        orElse: () => Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Spacer(),
            ImageWidget(image: image, height: 200, width: 340),
            const SizedBox(height: 32),
            Padding(
              padding: pad(50, 0, 50, 0),
              child: Column(
                children: [
                  SText(
                    title,
                    style: TStyle.TitleLarge,
                    textAlign: TextAlign.center,
                  ),
                  const SizedBox(height: 10),
                  SText(
                    subtitle,
                    style: TStyle.BodyMedium,
                    textAlign: TextAlign.center,
                  ),
                ],
              ),
            ),
            const Spacer(),
            data.fold(
              () => Container(),
              (a) => a.fold(
                (l) => l.maybeMap(
                  orElse: () => CustomFilledButton(
                    label: context.translate.tryAgain,
                    onTap: onRetry,
                  ),
                  underMaintenance: (e) => CustomOutlinedButton(
                    label: buttonLabel,
                    onTap: () {},
                    leftIcon: Padding(
                      padding: pad(5, 0, 5, 0),
                      child: const ImageWidget(
                        image: Images.earth,
                        height: 20,
                        width: 20,
                      ),
                    ),
                  ),
                ),
                (r) => Container(),
              ),
            ),
            const SizedBox(height: 32),
          ],
        ),
      );
    } else {
      return child(model as T);
    }
  }
}
