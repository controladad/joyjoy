import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:kt_dart/kt.dart';
import 'package:motivation_flutter/features/sub_auth/domain/models/paywall_model.dart';

part 'paywall_dto.freezed.dart';
part 'paywall_dto.g.dart';

@freezed
class PayWallDto with _$PayWallDto {
  @JsonSerializable(fieldRename: FieldRename.snake)
  const factory PayWallDto({
    required List<Plan> plans,
    @JsonKey(name: 'FAQs') required List<Faq> faQs,
  }) = _PayWallDto;

  factory PayWallDto.fromJson(Map<String, dynamic> json) =>
      _$PayWallDtoFromJson(json);

  const PayWallDto._();

  PaywallModel toDomain({
    required String userName,
    required PaywallTrialScheduleModel schedule,
    required KtList<PayWallOptionDefinitionModel> definitions,
    required KtList<PayWallComparisonTableItemModel> tableItems,
  }) {
    return PaywallModel(
      userName: userName,
      definitions: definitions,
      reviews: emptyList(),
      schedule: schedule,
      tableItems: tableItems,
      plans: plans.kt.map((e) => e.toDomain()),
      questions: faQs.kt.map((e) => e.toDomain()),
    );
  }
}

@freezed
class Faq with _$Faq {
  @JsonSerializable(fieldRename: FieldRename.snake)
  const factory Faq({
    required int id,
    required String question,
    required String answer,
  }) = _Faq;

  factory Faq.fromJson(Map<String, dynamic> json) => _$FaqFromJson(json);
  const Faq._();
  QuestionAndAnswerModel toDomain() {
    return QuestionAndAnswerModel(
      id: id.toString(),
      question: question,
      answer: answer,
    );
  }
}

@freezed
class Plan with _$Plan {
  @JsonSerializable(fieldRename: FieldRename.snake)
  const factory Plan({
    required int id,
    required String shortDescription,
    required String typeTitle,
    required String? typeShortDescription,
    required int durationValue,
    required String durationUnit,
    required String? token,
    required String oldPrice,
    required String? newPrice,
    required String? percentPrice,
    required String revenueCatProductId,
    required String identifier,
    required String offeringIdentifier,
    required int? trialDurationValue,
    required String? trialDurationUnit,
  }) = _Plan;

  const Plan._();
  factory Plan.fromJson(Map<String, dynamic> json) => _$PlanFromJson(json);
  PayWallSubscriptionPlanModel toDomain() {
    return PayWallSubscriptionPlanModel(
      productId: revenueCatProductId,
      identifier: identifier,
      id: id.toString(),
      title: shortDescription,
      duration: durationValue,
      durationType: durationUnit,
      price: double.tryParse(oldPrice)!,
      discountedPrice: double.tryParse(newPrice ?? ''),
      discount: double.tryParse(percentPrice ?? ''),
      subTitle: typeTitle,
      trialModel: trialDurationValue != null
          ? PlanTrialModel(
              duration: trialDurationValue!,
              unit: trialDurationUnit!,
            )
          : null,
    );
  }
}
