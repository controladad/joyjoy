import 'package:dartz/dartz.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:motivation_flutter/core/failures.dart';
import 'package:motivation_flutter/features/force_update/domain/update_info_model.dart';
import 'package:motivation_flutter/features/force_update/domain/update_info_repository.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:package_info_plus/package_info_plus.dart';

part 'update_info_event.dart';
part 'update_info_state.dart';
part 'update_info_bloc.freezed.dart';

@injectable
class UpdateInfoBloc extends Bloc<UpdateInfoEvent, UpdateInfoState> {
  final UpdateInfoRepository updateInfoRepository;
  UpdateInfoBloc(this.updateInfoRepository, UpdateInfoState init)
      : super(init) {
    on<UpdateInfoEvent>(
      (event, emit) async {
        emit(state.copyWith(updateInfoModel: none()));

        final PackageInfo packageInfo = await PackageInfo.fromPlatform();

        final String appVersion = packageInfo.version;

        late Either<GeneralFailure, UpdateInfoModel> result;
        result = await updateInfoRepository.getUpdateInfo(
          appVersion,
        );
        emit(state.copyWith(updateInfoModel: some(result)));
      },
    );
  }
}
