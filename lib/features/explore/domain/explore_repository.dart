import 'package:dartz/dartz.dart';
import 'package:kt_dart/kt.dart';
import 'package:motivation_flutter/common/common_models.dart';
import 'package:motivation_flutter/core/failures.dart';
import 'package:motivation_flutter/features/explore/domain/models/explore_model.dart';
import 'package:motivation_flutter/features/explore/domain/models/search_models.dart';

abstract class IExploreRepository {
  Future<Either<GeneralFailure, ExploreModel>> get getExplore;
  Future<Either<GeneralFailure, SearchModel>> instanceSearchQuery(String query);
  Future<Either<GeneralFailure, LazyLoadResponseModel<SearchResultUnion>>>
      fullSearchQuery({
    required String query,
    required SearchResultUnion firstItem,
    required int page,
  });
  Future<Either<GeneralFailure, KtList<String>>> get getSearchHistories;
  Future<Either<GeneralFailure, KtList<String>>> addNewSearchHistory(
    String query,
  );
  Future<Either<GeneralFailure, KtList<String>>> removeSearchHistories();
}
