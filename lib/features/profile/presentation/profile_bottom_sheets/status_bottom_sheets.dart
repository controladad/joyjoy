import 'package:motivation_flutter/common/presentation/common_presentation.dart';
import 'package:motivation_flutter/common/presentation/utils/custom_bottom_sheets.dart';
import 'package:motivation_flutter/features/profile/presentation/profile_bottom_sheets/delete_account_dialog.dart';

void showStatusBottomSheet(BuildContext context) {
  showCustomBottomSheet(
    context: context,
    onClose: () {},
    child: _StatusBottomSheet(),
  );
}

class _StatusBottomSheet extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: pad(16, 0, 16, 0),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Padding(
            padding: pad(0, 8, 0, 26),
            child: Row(
              children: [
                const CommonBackArrowButton(),
                SText(
                  context.translate.status.capitalize(),
                  style: TStyle.TitleLarge,
                ),
              ],
            ),
          ),
          Container(
            padding: pad(16, 0, 16, 0),
            height: 58,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15),
              border: Border.all(
                width: .25,
                color: AppTheme.outline_variant_dark,
              ),
            ),
            child: Row(
              children: [
                SText(
                  context.translate.active.capitalize(),
                  style: TStyle.TitleLarge,
                ),
                const Spacer(),
                const ImageWidget(
                  image: AppIcons.checkMark,
                  height: 40,
                  width: 40,
                ),
              ],
            ),
          ),
          const SBox(height: 12),
          TextButton(
            onPressed: () => showDeleteAccountDialog(context),
            child: SText(
              context.translate.deleteAccount.toUpperCase(),
              style: TStyle.LabelButton,
              color: AppTheme.secondary_container_dark,
            ),
          ),
          SText(
            context.translate.permanentlyDeleteYourAccount,
            style: TStyle.LabelSmall,
            color: AppTheme.white,
            textAlign: TextAlign.center,
            maxWidth: 310,
          ),
          const SBox(height: 32),
        ],
      ),
    );
  }
}
