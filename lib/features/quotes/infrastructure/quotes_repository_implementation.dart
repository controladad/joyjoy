import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:kt_dart/collection.dart';
import 'package:motivation_flutter/core/failures.dart';
import 'package:motivation_flutter/features/profile/domain/models/collection_model.dart';

import 'package:motivation_flutter/features/quotes/domain/models/quotes_screen_type.dart';
import 'package:motivation_flutter/features/quotes/domain/models/quotes_model.dart';
import 'package:motivation_flutter/features/quotes/domain/models/saved_quote_union.dart';

import 'package:motivation_flutter/features/quotes/domain/quotes_repository.dart';

import 'package:motivation_flutter/common/presentation/common_imports.dart';
import 'package:motivation_flutter/features/quotes/infrastructure/quotes_data_source.dart';
import 'package:motivation_flutter/shared/domain/shared_cache_props_model.dart';
import 'package:motivation_flutter/shared/infrastructure/shared_data_source.dart';

@Injectable(as: IQuotesRepository)
class QuotesRepositoryImplementation implements IQuotesRepository {
  final ISharedDataSource sharedDataSource;
  final IQuotesDataSource quotesDataSource;

  QuotesRepositoryImplementation(this.sharedDataSource, this.quotesDataSource);

  @override
  Stream<SharedCachePropsModel?> get cacheUpdated {
    return sharedDataSource.cacheUpdated;
  }

  @override
  Future<Either<GeneralFailure, Unit>> addQuoteToCollection({
    required CollectionModel collection,
    required QuoteModel quote,
  }) async {
    final result = await tryCacheBlock(
      () => sharedDataSource.toggleSavedQuote(
        type: SavedQuoteType.collection(collection),
        quoteId: quote.id,
      ),
    );

    return result.fold((l) => left(l), (r) => right(unit));
  }

  @override
  Future<void> deleteAllQuotes() async {
    return sharedDataSource.deleteAllQuotes();
  }

  @override
  Future<Either<GeneralFailure, KtList<QuoteModel>>> getQuotes(
    QuotesScreenType type,
  ) async {
    return tryCacheBlock(() async {
      return type.map(
        feeds: (e) => sharedDataSource.getNewQuotesByFavorites(),
        category: (e) => sharedDataSource.getQuotesByCategoryOrAuthor(
          authorId: none(),
          categoryId: some(e.categoryId),
        ),
        author: (e) => sharedDataSource.getQuotesByCategoryOrAuthor(
          authorId: some(e.authorId),
          categoryId: none(),
        ),
      );
    });
  }

  @override
  Future<Either<GeneralFailure, SharedCachePropsModel>> getCacheData() async {
    return tryCacheBlock(() async {
      final cacheData = await sharedDataSource.getCacheData;

      return cacheData;
    });
  }

  @override
  Future<Either<GeneralFailure, Unit>> reportQuote({
    required String quoteId,
    required String reportText,
  }) {
    return tryCacheBlock(
      () async {
        await quotesDataSource.reportQuote(
          quoteId: quoteId,
          reportText: reportText,
        );
        return unit;
      },
    );
  }

  @override
  Future<Either<GeneralFailure, Unit>> deleteQuote(String quoteId) {
    return tryCacheBlock(
      () async {
        await sharedDataSource.deleteQuote(quoteId);
        return unit;
      },
    );
  }

  @override
  Future<Either<GeneralFailure, Unit>> toggleQuoteLike(QuoteModel quote) async {
    final result = await tryCacheBlock(
      () => sharedDataSource.toggleSavedQuote(
        type: const SavedQuoteType.liked(),
        quoteId: quote.id,
      ),
    );

    return result.fold((l) => left(l), (r) => right(unit));
  }

  @override
  Future<void> changeFontRandomly() async {
    const fonts = [
      'Bosan',
      'avenir_next_condensed',
      'averia_gruesa_libre_regular',
      'bebas_neue_regular',
      'bokor',
      'bree_serif',
      'chilanka',
    ];

    tryCacheBlock(() async {
      final theme = await sharedDataSource.getSelectedTheme;
      final currentFontIndex = fonts.indexWhere(
        (element) => element == theme.fontFamily,
      );

      final nextFonts = fonts[(currentFontIndex + 1) % fonts.length];
      final newTheme = theme.copyWith(fontFamily: nextFonts);
      return sharedDataSource.addOrUpdateThemes(newTheme);
    });
  }

  @override
  Future<Either<GeneralFailure, String>> get getUserName {
    return tryCacheBlock(() async {
      final cache = await sharedDataSource.getCacheData;
      return cache.account.name.toNullable() ?? '';
    });
  }
}
