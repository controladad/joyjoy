import 'package:freezed_annotation/freezed_annotation.dart';

part 'review_model.freezed.dart';

@freezed
class PaywallReviewModel with _$PaywallReviewModel {
  const factory PaywallReviewModel({
    required String id,
    required String name,
    required String review,
    required String marketName,
    required int stars,
  }) = _PaywallReviewModel;
}
