import 'package:motivation_flutter/common/presentation/common_imports.dart';

class CommonLoader extends StatelessWidget {
  final double? size;
  const CommonLoader({super.key, this.size});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: size,
      width: size,
      child: const Center(child: CircularProgressIndicator()),
    );
  }
}
