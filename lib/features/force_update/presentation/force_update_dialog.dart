import 'package:dartz/dartz.dart';
import 'package:kt_dart/kt.dart';
import 'package:motivation_flutter/common/presentation/common_imports.dart';
import 'package:motivation_flutter/common/presentation/utils/failure_handler_widget.dart';
import 'package:motivation_flutter/core/failures.dart';
import 'package:motivation_flutter/features/profile/domain/models/update_model.dart';
import 'package:store_redirect/store_redirect.dart';

@RoutePage()
class ForceUpdateScreen extends StatelessWidget {
  const ForceUpdateScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return CommonBackgroundContainer(
      child: FailureHandlerWidget(
        data: some(left(const GeneralFailure.forceUpdate(Constants.storeLink))),
        onRetry: () {},
        child: (_) => Container(),
      ),
    );
  }
}

class ForceUpdateDialog extends StatelessWidget {
  final Option<KtList<UpdateHistoryModel>> changeLog;
  const ForceUpdateDialog({required this.changeLog});

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      backgroundColor: AppTheme.surface_dark.withOpacity(.9),
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(28.0)),
      ),
      content: SizedBox(
        width: 330,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            const ImageWidget(
              image: Images.forceUpdateDialog,
              height: 120,
              width: 220,
            ),
            const SBox(height: 30),
            SText(
              context.translate.fresh_update,
              style: TStyle.HeadlineSmall,
            ),
            const SBox(height: 15),
            changeLog.fold(
              () => SText(
                context.translate.fresh_update_description,
                style: TStyle.BodySmall,
              ),
              (a) => Flexible(
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: a.toListOfWidget(
                      (item) => Padding(
                        padding: pad(0, 0, 0, 5),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            '- V${item.version} :'
                                .widget(style: TStyle.BodyMedium),
                            Padding(
                              padding: pad(10, 0, 10, 0),
                              child: item.description
                                  .widget(style: TStyle.BodySmall),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ),
            const SBox(height: 45),
            Row(
              children: <Widget>[
                Expanded(child: Container()),
                GestureDetector(
                  onTap: () {
                    Navigator.of(context).pop();
                  },
                  child: SText(
                    context.translate.dismiss.toUpperCase(),
                    style: TStyle.LabelButton,
                    color: AppTheme.secondary_dark,
                  ),
                ),
                Container(width: 20),
                GestureDetector(
                  onTap: () {
                    StoreRedirect.redirect(iOSAppId: Constants.appStoreId);
                  },
                  child: SText(
                    context.translate.update.toUpperCase(),
                    style: TStyle.LabelButton,
                    color: AppTheme.primary_dark,
                  ),
                ),
              ],
            ),
            Container(height: 20),
          ],
        ),
      ),
    );
  }
}
