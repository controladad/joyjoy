import 'package:freezed_annotation/freezed_annotation.dart';

import 'package:motivation_flutter/features/onboarding/domain/models/onboarding_models.dart';

part 'onboarding_dto.freezed.dart';
part 'onboarding_dto.g.dart';

@freezed
class OnboardingDto with _$OnboardingDto {
  factory OnboardingDto({
    @JsonKey(name: 'id') required String id,
  }) = _OnboardingDto;

  factory OnboardingDto.fromJson(Map<String, dynamic> json) =>
      _$OnboardingDtoFromJson(json);
  const OnboardingDto._();

  OnboardingModel toDomain() {
    throw UnimplementedError();
  }
}
