import 'package:injectable/injectable.dart';
import 'package:motivation_flutter/utils/router/app_router.dart';

@module
abstract class AppRouterModule {
  @singleton
  AppRouter get appRouter => AppRouter();
}
