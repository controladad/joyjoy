import 'package:motivation_flutter/common/presentation/buttons.dart';
import 'package:motivation_flutter/common/presentation/common_presentation.dart';
import 'package:motivation_flutter/common/presentation/utils/custom_bottom_sheets.dart';
import 'package:motivation_flutter/features/sub_auth/application/paywall_bloc/paywall_cubit.dart';
import 'package:motivation_flutter/features/sub_auth/domain/models/paywall_model.dart';

Future<void> showBuySubscriptionBottomSheets({
  required PaywallCubit bloc,
  required BuildContext context,
  required PayWallSubscriptionPlanModel plan,
  required Function() onDone,
}) {
  return showCustomBottomSheet(
    context: context,
    onClose: () {},
    child: _Widget(
      plan: plan,
      bloc: bloc,
      onDone: onDone,
    ),
  );
}

class _Widget extends StatelessWidget {
  final PaywallCubit bloc;
  final Function() onDone;
  final PayWallSubscriptionPlanModel plan;

  const _Widget({required this.plan, required this.bloc, required this.onDone});

  @override
  Widget build(BuildContext context) {
    final actualPrice = plan.discountedPrice ?? plan.price;
    final oldPrice = plan.price;
    final discount = plan.discount;
    final bool hasDiscount = discount != null;

    return BlocProvider.value(
      value: bloc,
      child: Builder(
        builder: (context) {
          final state = context.watch<PaywallCubit>().state;
          final isLoading = state.currentSubscription.isNone();
          return Padding(
            padding: pad(16, 0, 16, 0),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                SText(
                  context.translate.you_have_chosen,
                  style: TStyle.TitleLarge,
                ),
                const SBox(height: 16),
                Container(
                  padding: pad(16, 8, 16, 8),
                  decoration: const BoxDecoration(
                    border: Border(
                      bottom: BorderSide(
                        color: AppTheme.outline_variant_dark,
                        width: .5,
                      ),
                    ),
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SText(
                            context.translate.total.capitalize(),
                            style: TStyle.BodyLarge,
                          ),
                          SText(
                            '${context.translate.for_word} ${plan.duration} ${plan.durationType}',
                            style: TStyle.LabelMedium,
                            color: AppTheme.outline_dark,
                          ),
                        ],
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Row(
                            children: [
                              if (hasDiscount)
                                SText(
                                  '\$${oldPrice.toStringAsFixed(2)}',
                                  style: TStyle.BodySmall,
                                  color: AppTheme.error,
                                  decoration: TextDecoration.lineThrough,
                                  decorationColor: AppTheme.error,
                                ),
                              const SBox(width: 4),
                              SText(
                                '\$${actualPrice.toStringAsFixed(2)}',
                                style: TStyle.TitleMedium,
                              ),
                            ],
                          ),
                          if (hasDiscount)
                            SText(
                              '${context.translate.you_save.capitalize()} $discount%',
                              style: TStyle.BodySmall,
                              color: AppTheme.primary_60,
                            )
                        ],
                      ),
                    ],
                  ),
                ),
                const SBox(height: 16),
                CustomFilledButton(
                  isLoading: isLoading,
                  label: context.translate.goPremium,
                  subtitle: plan.trialModel != null
                      ? '${context.translate.and_a} ${plan.trialModel!.duration}-${plan.trialModel!.unit} ${context.translate.trial}'
                      : null,
                  onTap: () async {
                    await bloc.changePlan(plan);
                    if (context.mounted) {
                      Navigator.pop(context);
                      onDone();
                    }
                  },
                ),
                const SBox(height: 10),
                if (plan.trialModel != null)
                  SText(
                    context.translate.don_worry_we_will_remind_you_trial,
                    style: TStyle.LabelMedium,
                    textAlign: TextAlign.center,
                  ),
                const SBox(height: 32),
              ],
            ),
          );
        },
      ),
    );
  }
}
