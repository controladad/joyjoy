import 'dart:io';

import 'package:android_id/android_id.dart';
import 'package:device_info_plus/device_info_plus.dart';
import 'package:injectable/injectable.dart';

@module
abstract class DeviceInfoService {
  @Named('deviceId')
  @singleton
  @preResolve
  Future<String> get deviceId async {
    if (Platform.isIOS) {
      final iosPlugin = await DeviceInfoPlugin().iosInfo;
      return iosPlugin.identifierForVendor ?? '';
    } else if (Platform.isAndroid) {
      final androidId = await const AndroidId().getId();
      return androidId ?? '';
    } else {
      return '';
    }
  }
}
