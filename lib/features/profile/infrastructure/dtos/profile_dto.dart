// ignore_for_file: unnecessary_parenthesis

import 'package:dartz/dartz.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:kt_dart/collection.dart';
import 'package:motivation_flutter/common/presentation/utils/common_functions.dart';

import 'package:motivation_flutter/features/profile/domain/models/profile_models.dart';
import 'package:motivation_flutter/features/profile/domain/models/update_model.dart';

part 'profile_dto.freezed.dart';
part 'profile_dto.g.dart';

@freezed
class ProfileDto with _$ProfileDto {
  factory ProfileDto({
    @JsonKey(name: 'id') required String id,
  }) = _ProfileDto;

  factory ProfileDto.fromJson(Map<String, dynamic> json) =>
      _$ProfileDtoFromJson(json);
  const ProfileDto._();

  ProfileModel toDomain() {
    throw UnimplementedError();
  }
}

@freezed
class HomeDto with _$HomeDto {
  @JsonSerializable(fieldRename: FieldRename.snake)
  factory HomeDto({
    List<BannerDto>? banners,
  }) = _HomeDto;
  const HomeDto._();
  factory HomeDto.fromJson(Map<String, dynamic> json) =>
      _$HomeDtoFromJson(json);
}

@freezed
class BannerDto with _$BannerDto {
  @JsonSerializable(fieldRename: FieldRename.snake)
  factory BannerDto({
    required int id,
    required String image,
    DateTime? dateFrom,
    DateTime? dateTo,
    required int sortOrder,
    String? link,
  }) = _BannerDto;
  const BannerDto._();
  factory BannerDto.fromJson(Map<String, dynamic> json) =>
      _$BannerDtoFromJson(json);

  ProfileBannerModel get toDomain {
    late BannerLinkUnion theLink;
    if (link == null) {
      theLink = const BannerLinkUnion.noAction();
    } else {
      final linkParts = link!.split('.');
      if (linkParts.length > 1 && linkParts[0] == 'in_app') {
        switch (linkParts[1]) {
          case 'chat':
            theLink = const BannerLinkUnion.inApp(InAppSection.Chat);

          case 'login':
            theLink = const BannerLinkUnion.inApp(InAppSection.Login);
        }
      } else {
        theLink = BannerLinkUnion.link(link!);
      }
    }

    return ProfileBannerModel(
      id: id.toString(),
      image: image,
      dateFrom: dateFrom.toOption(),
      dateTo: dateTo.toOption(),
      sortOrder: sortOrder,
      link: theLink,
    );
  }
}

@freezed
class UpdateDto with _$UpdateDto {
  @JsonSerializable(fieldRename: FieldRename.snake)
  factory UpdateDto({
    bool? updateAvailable,
    bool? forceUpdate,
    String? googlePlayLink,
    String? appStoreLink,
    String? version,
    List<UpdateHistoryDto>? history,
  }) = _UpdateDto;
  const UpdateDto._();
  factory UpdateDto.fromJson(Map<String, dynamic> json) =>
      _$UpdateDtoFromJson(json);
  UpdateModel toDomain() {
    return UpdateModel(
      updateAvailable: updateAvailable ?? false,
      forceUpdate: forceUpdate ?? false,
      version: version ?? '',
      appStoreLink: appStoreLink.toOption(),
      googlePlayLink: googlePlayLink.toOption(),
      changeLog: (history?.map((e) => e.toDomain()).toList().kt).toOption(),
    );
  }
}

@freezed
class UpdateHistoryDto with _$UpdateHistoryDto {
  @JsonSerializable(fieldRename: FieldRename.snake)
  factory UpdateHistoryDto({
    required String version,
    required String description,
  }) = _UpdateHistoryDto;
  const UpdateHistoryDto._();
  factory UpdateHistoryDto.fromJson(Map<String, dynamic> json) =>
      _$UpdateHistoryDtoFromJson(json);
  UpdateHistoryModel toDomain() {
    return UpdateHistoryModel(version: version, description: description);
  }
}
