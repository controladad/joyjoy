import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:kt_dart/kt.dart';

part 'common_models.freezed.dart';

@freezed
class LazyLoadResponseModel<T> with _$LazyLoadResponseModel<T> {
  const factory LazyLoadResponseModel({
    required KtList<T> results,
    required int total,
  }) = _LazyLoadResponseModel;
}
