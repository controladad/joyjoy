import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:kt_dart/collection.dart';
import 'package:motivation_flutter/features/onboarding/domain/models/onboarding_model.dart';

import 'package:motivation_flutter/features/onboarding/infrastructure/onboarding_api_services.dart';
import 'package:motivation_flutter/features/quotes/domain/models/author_overview_model.dart';
import 'package:motivation_flutter/features/quotes/domain/models/sub_category_model.dart';
import 'package:motivation_flutter/features/sub_auth/domain/models/review_model.dart';
import 'package:motivation_flutter/shared/infrastructure/dtos/author_dto.dart';
import 'package:motivation_flutter/shared/infrastructure/dtos/sub_category_dto.dart';

abstract class IOnboardingApiDataSource {
  Future<OnboardingModel> get getData;
}

@Injectable(as: IOnboardingApiDataSource)
class OnboardingApiDataSourceImplementation
    implements IOnboardingApiDataSource {
  final IOnboardingApiService _apiService;

  OnboardingApiDataSourceImplementation(this._apiService);

  @override
  Future<OnboardingModel> get getData async {
    final categoriesResponse = await _apiService.getCategories();
    final authorsResponse = await _apiService.getAuthors();

    final categoriesRawData = categoriesResponse.data as Map<String, dynamic>;
    final authorsRawData = authorsResponse.data as Map<String, dynamic>;

    final List<SubCategoryModel> categories = [];
    final List<AuthorModel> authors = [];

    for (final categoryData in categoriesRawData['data'] as List<dynamic>) {
      final data = categoryData as Map<String, dynamic>;
      final dto = SubCategoryDto.fromJson(data);
      categories.add(dto.toDomain());
      if (categories.length >= 12) break;
    }
    for (final authorData in authorsRawData['data'] as List<dynamic>) {
      final data = authorData as Map<String, dynamic>;
      final dto = AuthorDto.fromJson(data);
      authors.add(dto.toDomain());
    }

    return OnboardingModel(
      id: 'onboarding',
      account: none(),
      categories: categories.kt,
      authors: authors.kt,
      //TODO : should get from api
      reviews: KtList.from([
        const PaywallReviewModel(
          id: '0',
          name: 'akbar',
          review:
              'This app is apart of my day...literally.😁 I have it set mornings, afternoon & late evening to give me that little pep talk throughout the day. I am a female business owner, and there',
          marketName: 'apple',
          stars: 4,
        ),
        const PaywallReviewModel(
          id: '1',
          name: 'ali',
          review:
              'This app is apart of my day...literally.😁 I have it set mornings, afternoon & late evening to give me that little pep talk throughout the day. I am a female business owner, and there',
          marketName: 'playStore',
          stars: 3,
        ),
      ]),
    );
  }
}
