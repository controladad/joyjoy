import 'package:dots_indicator/dots_indicator.dart';
import 'package:flutter_swiper_null_safety/flutter_swiper_null_safety.dart';
import 'package:kt_dart/kt.dart';
import 'package:motivation_flutter/common/presentation/buttons.dart';
import 'package:motivation_flutter/common/presentation/common_imports.dart';
import 'package:motivation_flutter/common/presentation/common_presentation.dart';
import 'package:motivation_flutter/common/presentation/utils/failure_handler_widget.dart';
import 'package:motivation_flutter/features/onboarding/application/onboarding_bloc/onboarding_bloc.dart';
import 'package:motivation_flutter/features/onboarding/domain/models/onboarding_models.dart';
import 'package:motivation_flutter/features/sub_auth/domain/models/review_model.dart';

class OnboardingReviewsScreen extends StatelessWidget {
  final Function() onSkip;
  const OnboardingReviewsScreen({
    super.key,
    required this.onSkip,
  });

  @override
  Widget build(BuildContext context) {
    return Builder(
      builder: (context) {
        final state = context.watch<OnboardingBloc>().state;
        return Scaffold(
          body: CommonBackgroundContainer(
            child: SafeArea(
              child: FailureHandlerWidget<OnboardingModel>(
                data: state.onboardingData,
                onRetry: () {},
                child: (r) => _Body(
                  reviews: r.reviews,
                  onSkip: onSkip,
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}

class _Body extends StatefulWidget {
  final Function() onSkip;
  final KtList<PaywallReviewModel> reviews;
  const _Body({required this.reviews, required this.onSkip});

  @override
  State<_Body> createState() => _BodyState();
}

class _BodyState extends State<_Body> {
  int activeIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const SBox(height: 30),
        SText(
          context.translate.ourUserReviews.toTitleCase(),
          style: TStyle.HeadlineSmall,
        ),
        const SBox(height: 30),
        Stack(
          alignment: Alignment.center,
          children: [
            const Align(
              alignment: Alignment.centerRight,
              child: ImageWidget(
                image: Images.reviewBoardRight,
                height: 450,
                width: 270,
              ),
            ),
            Positioned(
              bottom: 65,
              child: SBox(
                height: 200,
                width: 365,
                child: Swiper(
                  onIndexChanged: (value) {
                    setState(() {
                      activeIndex = value;
                    });
                  },
                  itemBuilder: (context, index) {
                    final review = widget.reviews[index];
                    return _SwiperItemWidget(review: review);
                  },
                  itemCount: widget.reviews.size,
                ),
              ),
            ),
            const IgnorePointer(
              child: Align(
                alignment: Alignment.centerLeft,
                child: ImageWidget(
                  image: Images.reviewBoardLeft,
                  height: 450,
                  width: 300,
                ),
              ),
            ),
            Positioned(
              bottom: 10,
              child: DotsIndicator(
                dotsCount: widget.reviews.size,
                position: activeIndex,
                decorator: const DotsDecorator(
                  activeColor: AppTheme.primary_80,
                  color: AppTheme.outline_dark,
                ),
              ),
            )
          ],
        ),
        const Spacer(),
        CustomFilledButton(
          label: context.translate.skip,
          onTap: widget.onSkip,
        ),
        const SBox(height: 30),
      ],
    );
  }
}

class _SwiperItemWidget extends StatelessWidget {
  const _SwiperItemWidget({
    required this.review,
  });

  final PaywallReviewModel review;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: SContainer(
        height: 200,
        width: 345,
        padding: pad(40, 24, 20, 20),
        decoration: BoxDecoration(
          gradient: AppGradients.primaryDark,
          borderRadius: const BorderRadius.all(Radius.circular(2)),
        ),
        child: Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Flexible(
                child: SText(
                  review.review,
                  maxLines: 7,
                  style: TStyle.BodyMedium,
                  overflow: TextOverflow.ellipsis,
                  color: AppTheme.on_surface_light,
                ),
              ),
              SText(
                '- ${review.marketName.toTitleCase()}',
                style: TStyle.BodyMedium,
                color: AppTheme.on_surface_light,
              )
            ],
          ),
        ),
      ),
    );
  }
}
