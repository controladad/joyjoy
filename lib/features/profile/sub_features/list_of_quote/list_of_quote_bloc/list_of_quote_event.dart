part of 'list_of_quote_bloc.dart';

@freezed
class ListOfQuoteEvent with _$ListOfQuoteEvent {
  const factory ListOfQuoteEvent.getFirstPage(SavedQuoteType destination) =
      _GetFirstPage;
  const factory ListOfQuoteEvent.refresh() = _Refresh;
  const factory ListOfQuoteEvent.removeQuote(String id) = _Remove;
  const factory ListOfQuoteEvent.search(String searchQuery) = _Search;
}
