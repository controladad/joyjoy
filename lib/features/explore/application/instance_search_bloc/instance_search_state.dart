part of 'instance_search_bloc.dart';

@injectable
@freezed
class InstanceSearchState with _$InstanceSearchState {
  const factory InstanceSearchState({
    required Option<Either<GeneralFailure, SearchModel>> instanceSearchData,
    required KtList<SearchResultUnion> instanceSearchResults,
    required KtList<SuggestionModel> suggestions,
    required Option<String> searchQuery,
    required KtList<String> searchHistories,
    required bool isLoading,
  }) = _InstanceSearchState;
  @factoryMethod
  factory InstanceSearchState.init() => InstanceSearchState(
        instanceSearchResults: emptyList(),
        searchHistories: emptyList(),
        instanceSearchData: none(),
        searchQuery: none(),
        suggestions: emptyList(),
        isLoading: false,
      );
}
