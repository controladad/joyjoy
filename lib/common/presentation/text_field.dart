import 'dart:async';

import 'package:motivation_flutter/common/presentation/common_imports.dart';

class CustomTextField extends StatefulWidget {
  final Function(String value)? onSubmit;
  final Function(String value) onChanged;
  final Function(bool hasFocus)? onFocusedChanged;
  final double? maxWidth;
  final Widget? icon;
  final String? focusedLabel;
  final String? label;
  final String? hintText;
  final String? initialValue;
  final bool hasClearButton;
  final Color? focusedBorderColor;
  final Color? unfocusedBorderColor;
  final EdgeInsetsGeometry? padding;
  final TextEditingController? controller;
  final int? maxLength;
  final Widget? leading;
  final bool isEnable;
  final bool autofocus;
  final int maxLine;

  const CustomTextField({
    this.controller,
    this.onSubmit,
    this.hintText,
    required this.onChanged,
    this.focusedLabel,
    this.label,
    this.initialValue,
    this.onFocusedChanged,
    this.hasClearButton = false,
    this.icon,
    this.maxWidth = 333,
    this.focusedBorderColor,
    this.unfocusedBorderColor,
    this.padding,
    this.maxLength,
    this.leading,
    this.isEnable = true,
    this.autofocus = false,
    this.maxLine = 1,
  });

  @override
  State<CustomTextField> createState() => _CustomTextFieldState();
}

class _CustomTextFieldState extends State<CustomTextField> {
  late TextEditingController controller;
  late FocusNode focusNode;

  @override
  void initState() {
    super.initState();
    controller =
        widget.controller ?? TextEditingController(text: widget.initialValue);

    controller.addListener(() {
      setState(() {});
    });

    focusNode = FocusNode();
    focusNode.addListener(() {
      if (widget.onFocusedChanged != null) {
        // ignore: prefer_null_aware_method_calls
        widget.onFocusedChanged!(focusNode.hasFocus);
      }
      setState(() {});
    });
  }

  @override
  void dispose() {
    super.dispose();
    controller.dispose();
    focusNode.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Widget? icon;

    if (widget.hasClearButton && controller.text.isNotEmpty) {
      icon = IconButton(
        icon: const Icon(
          Icons.close,
          color: Colors.white,
        ),
        onPressed: () {
          controller.clear();
          widget.onChanged(controller.value.text);
        },
      );
    } else if (widget.icon != null) {
      icon = widget.icon;
    }

    return SContainer(
      margin: widget.padding,
      maxWidth: widget.maxWidth ?? double.infinity,
      child: Row(
        children: [
          Expanded(
            child: TextField(
              maxLines: widget.maxLine,
              autofocus: widget.autofocus,
              onChanged: (value) => widget.onChanged(value),
              enabled: widget.isEnable,
              onSubmitted: (value) {
                if (widget.onSubmit != null) {
                  // ignore: prefer_null_aware_method_calls
                  widget.onSubmit!(value);
                }
              },
              inputFormatters: [
                if (widget.maxLength != null)
                  LengthLimitingTextInputFormatter(widget.maxLength)
              ],
              focusNode: focusNode,
              controller: controller,
              cursorColor: AppTheme.primary_80,
              decoration: InputDecoration(
                hintText: widget.hintText,
                prefixIcon: widget.leading != null
                    ? Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          widget.leading!,
                        ],
                      )
                    : null,
                label: widget.label != null
                    ? SText(
                        focusNode.hasFocus
                            ? (widget.focusedLabel ?? widget.label!)
                            : (widget.label!),
                        style: TStyle.BodyLarge,
                        color: AppTheme.outline_dark,
                      )
                    : null,
                suffixIcon: icon,
                contentPadding: pad(20, 0, 5, 0),
                disabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                    color: AppTheme.on_surface_dark.withOpacity(.12),
                  ),
                  borderRadius: const BorderRadius.all(Radius.circular(100)),
                ),
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                    color:
                        widget.unfocusedBorderColor ?? AppTheme.on_outline_dark,
                  ),
                  borderRadius: const BorderRadius.all(Radius.circular(100)),
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                    color: widget.focusedBorderColor ?? AppTheme.primary_40,
                    width: 2,
                  ),
                  borderRadius: const BorderRadius.all(Radius.circular(100)),
                ),
                border: const OutlineInputBorder(
                  borderSide: BorderSide(width: 2),
                  borderRadius: BorderRadius.all(Radius.circular(100)),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class SearchTextField extends StatefulWidget {
  final Function(String query) onChanged;
  final String? label;
  final Function(String value)? onSubmit;
  final EdgeInsetsGeometry? padding;
  final TextEditingController? controller;
  final bool autofocus;
  final String? hintText;

  const SearchTextField({
    super.key,
    required this.onChanged,
    this.label,
    this.onSubmit,
    this.padding,
    this.controller,
    this.autofocus = false,
    this.hintText,
  });

  @override
  State<SearchTextField> createState() => _SearchTextFieldState();
}

class _SearchTextFieldState extends State<SearchTextField> {
  Timer? _debounce;

  void _onSearchChanged(String query) {
    if (_debounce?.isActive ?? false) _debounce?.cancel();
    _debounce =
        Timer(const Duration(milliseconds: 500), () => widget.onChanged(query));
  }

  @override
  Widget build(BuildContext context) {
    return CustomTextField(
      hintText: widget.hintText,
      onChanged: _onSearchChanged,
      hasClearButton: true,
      label: widget.label,
      onSubmit: widget.onSubmit,
      autofocus: widget.autofocus,
      padding: widget.padding,
      controller: widget.controller,
    );
  }
}
