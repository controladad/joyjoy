import 'package:dartz/dartz.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'user_subscription_model.freezed.dart';

@freezed
class UserSubscription with _$UserSubscription {
  const factory UserSubscription.premium({
    required String name,
    required bool hasPermissionToSeeAllQuotes,
    required bool hasPermissionChat,
    required bool shouldShowAds,
    required DateTime startDate,
    required DateTime nextBillingDate,
    required bool shouldRenew,
  }) = PremiumCase;

  const factory UserSubscription.free({
    required Option<DateTime> lastPlanStartDate,
    required Option<DateTime> lastPlanEndDate,
  }) = FreeCase;

  factory UserSubscription.empty() =>
      UserSubscription.free(lastPlanStartDate: none(), lastPlanEndDate: none());

  const UserSubscription._();

  bool get isPremium {
    if (this is PremiumCase) {
      return (this as PremiumCase).nextBillingDate.isAfter(DateTime.now());
    } else {
      return false;
    }
  }
}
