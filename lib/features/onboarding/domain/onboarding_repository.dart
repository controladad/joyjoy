import 'package:dartz/dartz.dart';
import 'package:kt_dart/kt.dart';
import 'package:motivation_flutter/core/failures.dart';

import 'package:motivation_flutter/features/onboarding/domain/models/onboarding_models.dart';

abstract class IOnboardingRepository {
  Future<Either<GeneralFailure, OnboardingModel>> get getData;
  Future<Either<GeneralFailure, OnboardingCacheDataModel>> get getCacheData;
  Future<Either<GeneralFailure, void>> get userDidTryChat;
  Future<Either<GeneralFailure, void>> get userPassedOnboarding;
  Future<Either<GeneralFailure, Option<String>>> setUserName(String name);
  Future<Either<GeneralFailure, KtList<String>>> toggleFavoriteAuthor(
    String authorId,
  );
  Future<Either<GeneralFailure, KtList<String>>> toggleFavoriteCategory(
    String categoryId,
  );
}
