// part of 'favorite_characters_bloc.dart';

// @injectable
// @freezed
// class FavoriteCharactersState with _$FavoriteCharactersState {
//   const factory FavoriteCharactersState({
//     required Option<Either<GeneralFailure, ListOfCharactersModel>> data,
//     required Option<Either<GeneralFailure, Unit>> isSucceed,
//     required Option<String> searchString,
//     required bool isLoading,
//     required bool hasReachedMax,
//     required bool hasReachedMaxOfSearchQueryResult,
//     required KtList<FavoriteCharacterModel> charactersList,
//     required KtList<FavoriteCharacterModel> searchQueryResultCharacterList,
//   }) = _FavoriteCharactersState;

//   @factoryMethod
//   factory FavoriteCharactersState.init() => FavoriteCharactersState(
//         data: none(),
//         isSucceed: none(),
//         hasReachedMax: false,
//         searchQueryResultCharacterList: emptyList(),
//         charactersList: emptyList(),
//         isLoading: false,
//         searchString: none(),
//         hasReachedMaxOfSearchQueryResult: false,
//       );
// }
