part of './profile_data_source.dart';

const String _TERMS_OF_USE = '''
By using this app, you agree to be bound by the following terms and conditions. Please read these terms carefully before using the app.

User Conduct:
You are responsible for all activity that occurs under your account, including any content that you create, submit, or share through the app.
You agree to use the app only for lawful purposes and in accordance with these Terms of Use.
You may not use the app in any way that could damage, disable, overburden, or impair any JoyJoy server, or interfere with any other party's use and enjoyment of the app.
Content:
The quotes and other content on JoyJoy are provided for informational and inspirational purposes only. They do not constitute advice and should not be relied upon for any purpose.
You may read, share, and download the quotes for your personal, non-commercial use only. You may not use them for any commercial purpose without our prior written consent.
You are responsible for ensuring that any content you share through JoyJoy does not infringe on the intellectual property rights of others.
Termination:
We reserve the right to terminate or suspend your access to JoyJoy at any time, for any reason, without notice or liability.
Upon termination, all rights and licenses granted to you under these Terms of Use will immediately terminate.
Disclaimers and Limitations of Liability:
JoyJoy is provided on an "as is" and "as available" basis. We make no representations or warranties of any kind, express or implied, as to the operation of the app or the information, content, materials, or products included on or accessible through the app.
To the fullest extent permissible by applicable law, we disclaim all warranties, express or implied, including but not limited to implied warranties of merchantability and fitness for a particular purpose.
In no event will JoyJoy be liable for any damages of any kind arising from the use of the app, including but not limited to direct, indirect, incidental, punitive, and consequential damages.
By using JoyJoy, you agree to these Terms of Use and our Privacy Policy, which is incorporated by reference. If you do not agree to these terms, please do not use JoyJoy.''';

const String _ABOUT_US =
    '''JoyJoy is an app designed to inspire and motivate you with the words of famous and successful people. Our platform allows users to read and share quotes, as well as participate in our incentive program to earn rewards for engaging with the app. Whether you need a daily dose of inspiration or a way to stay motivated, JoyJoy is here to help you find joy in the journey towards success.''';

const String _PRIVACY_POLICY = '''
Privacy Policy for JoyJoy:



1. Collection of Information:

We do not collect any personal information from our users. However, our third-party provider, Google Ad Mob, may collect non-personal information such as device type, operating system, and location. This information is used to provide relevant ads to the user.



2. Use of Information:

The information collected by Google Ad Mob is solely used for providing relevant ads to the user. We do not use any personal information for any purpose.



3. Sharing of Information:

We do not share any personal information with any third-party providers. However, Google Ad Mob may share non-personal information with their partners for advertising purposes.



4. Security:

We take the security of our users' information seriously. We have implemented industry-standard security measures to protect the data collected by Google Ad Mob.



5. Changes to Privacy Policy:

We reserve the right to make changes to this privacy policy at any time. Users will be notified of any changes made.



By using JoyJoy, you agree to this privacy policy.

''';
