import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:kt_dart/kt.dart';
import 'package:motivation_flutter/features/quotes/domain/models/quotes_models.dart';

part 'category_model.freezed.dart';

@freezed
class CategoryModel with _$CategoryModel {
  const factory CategoryModel({
    required String id,
    required String name,
    required KtList<SubCategoryModel> subCategories,
  }) = _CategoryModel;
}
