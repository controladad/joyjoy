import 'package:motivation_flutter/features/language/domain/models/language_entity.dart';
import 'package:hive/hive.dart';
import 'package:injectable/injectable.dart';

import 'package:motivation_flutter/constants/db_constants.dart';
import 'package:motivation_flutter/features/profile/domain/models/user_subscription_entity.dart';
import 'package:motivation_flutter/features/quotes/domain/models/author_entity.dart';
import 'package:motivation_flutter/features/quotes/domain/models/quote_entity.dart';
import 'package:motivation_flutter/features/user/domain/models/user_entity.dart';
import 'package:motivation_flutter/features/user/domain/models/user_watch_ads_entity.dart';
import 'package:motivation_flutter/shared/domain/shared_cache_props_entity.dart';

@module
abstract class HiveBoxesModule {
  @Named(DBConstants.USER)
  @preResolve
  Future<Box<UserEntity>> user(HiveInterface hive) {
    return hive.openBox(DBConstants.USER);
  }

  @Named(DBConstants.LANGUAGE)
  @preResolve
  Future<Box<LanguageEntity>> language(HiveInterface hive) {
    return hive.openBox(DBConstants.LANGUAGE);
  }

  @Named(DBConstants.ALL_QUOTES)
  @preResolve
  Future<LazyBox<QuoteEntity>> quotes(HiveInterface hive) async {
    return hive.openLazyBox<QuoteEntity>(DBConstants.ALL_QUOTES);
  }

  @Named(DBConstants.NEW_QUOTES)
  @preResolve
  Future<LazyBox<QuoteEntity>> newQuotes(HiveInterface hive) async {
    return hive.openLazyBox<QuoteEntity>(DBConstants.NEW_QUOTES);
  }

  @Named(DBConstants.NOTIFICATION_QUOTES)
  @preResolve
  Future<Box<QuoteEntity>> notificationQuotes(HiveInterface hive) async {
    return hive.openBox<QuoteEntity>(DBConstants.NOTIFICATION_QUOTES);
  }

  @Named(DBConstants.AUTHORS)
  @preResolve
  Future<LazyBox<AuthorEntity>> authors(HiveInterface hive) async {
    return hive.openLazyBox<AuthorEntity>(DBConstants.AUTHORS);
  }

  @Named(DBConstants.QUOTES_CACHE_PROPS)
  @preResolve
  Future<Box<SharedCachePropsEntity>> quotesCacheProps(HiveInterface hive) {
    return hive.openBox(DBConstants.QUOTES_CACHE_PROPS);
  }

  @Named(DBConstants.SEARCH_HISTORY)
  @preResolve
  Future<Box<String>> searchHistory(HiveInterface hive) {
    return hive.openBox(DBConstants.SEARCH_HISTORY);
  }

  @Named(DBConstants.SUBSCRIPTION)
  @preResolve
  Future<Box<UserSubscriptionEntity>> subscription(HiveInterface hive) {
    return hive.openBox(DBConstants.SUBSCRIPTION);
  }
@Named(DBConstants.USER_WATCH_ADS)
  @preResolve
  Future<Box<UserWatchAdsEntity>> userWatchAds(HiveInterface hive) {
    return hive.openBox(DBConstants.USER_WATCH_ADS);
  }
}
