import 'package:kt_dart/kt.dart';
import 'package:motivation_flutter/features/profile/domain/models/collection_entity.dart';
import 'package:motivation_flutter/features/profile/domain/models/reminder_entity.dart';
import 'package:motivation_flutter/features/theme/domain/models/theme_entity.dart';
import 'package:motivation_flutter/shared/domain/shared_cache_props_entity.dart';
import 'package:motivation_flutter/shared/infrastructure/dtos/shared_cache_props_dto.dart';

String createChangeLog(
  List<SharedCachePropsEntity> listOfCacheProps,
) {
  final StringBuffer changeLog = StringBuffer();

  for (int index = 1; index <= listOfCacheProps.length - 1; index++) {
    final oldModel = listOfCacheProps[index - 1];
    final newModel = listOfCacheProps[index];

    final diff = compareTwoCachePropsItem(oldModel, newModel);
    if (diff.isNotEmpty) changeLog.write(diff);
  }

  return changeLog.toString().replaceAll(RegExp(r'^\s*$\n'), '');
}

String compareTwoCachePropsItem(
  SharedCachePropsEntity oldModel,
  SharedCachePropsEntity newModel,
) {
  final StringBuffer changeLog = StringBuffer();
  final remindersChangeLog = _compareReminders(
    oldModel.reminders ?? [],
    newModel.reminders ?? [],
  );
  if (remindersChangeLog.isNotEmpty) changeLog.writeln(remindersChangeLog);

  final collectionsChangeLog = _compareCollections(
    oldModel.collections ?? [],
    newModel.collections ?? [],
  );
  if (collectionsChangeLog.isNotEmpty) changeLog.writeln(collectionsChangeLog);

  final favoriteAuthorsChangeLog = _compareIds(
    oldModel.favoriteAuthorsIds ?? [],
    newModel.favoriteAuthorsIds ?? [],
    'FavoriteAuthors',
  );
  if (favoriteAuthorsChangeLog.isNotEmpty) {
    changeLog.writeln(favoriteAuthorsChangeLog);
  }

  final favoriteCategoriesChangeLog = _compareIds(
    oldModel.favoriteCategoriesIds ?? [],
    newModel.favoriteCategoriesIds ?? [],
    'FavoriteCategories',
  );
  if (favoriteCategoriesChangeLog.isNotEmpty) {
    changeLog.writeln(favoriteCategoriesChangeLog);
  }

  final likedQuotesChangeLog = _compareIds(
    oldModel.likedQuotesIds ?? [],
    newModel.likedQuotesIds ?? [],
    'LikedQuotesIds',
  );
  if (likedQuotesChangeLog.isNotEmpty) changeLog.writeln(likedQuotesChangeLog);

  final historyQuotesChangeLog = _compareIds(
    oldModel.quotesHistoryIds ?? [],
    newModel.quotesHistoryIds ?? [],
    'HistoryQuotesIds',
  );
  if (historyQuotesChangeLog.isNotEmpty) {
    changeLog.writeln(historyQuotesChangeLog);
  }

  final themesChangeLog = _compareThemes(
    oldModel.themes ?? [],
    newModel.themes ?? [],
  );
  if (themesChangeLog.isNotEmpty) changeLog.writeln(themesChangeLog);

  return changeLog.toString();
}

String _compareIds(
  List<String> oldIds,
  List<String> newIds,
  String groupName,
) {
  final StringBuffer changeLog = StringBuffer();

  final addedIds = newIds.kt.minus(oldIds.kt).asList();
  final removedIds = oldIds.kt.minus(newIds.kt).asList();

  if (removedIds.isNotEmpty) {
    for (final id in removedIds) {
      changeLog.writeln('Remove From $groupName : $id');
    }

    if (addedIds.isNotEmpty) {
      for (final id in addedIds) {
        changeLog.writeln('Add to $groupName : $id');
      }
    }
  }

  return changeLog.toString();
}

String _compareCollections(
  List<CollectionEntity> oldCollections,
  List<CollectionEntity> newCollections,
) {
  final StringBuffer changeLog = StringBuffer();

  for (final collection in oldCollections) {
    final newOne = newCollections.kt.find((p0) => p0.id == collection.id);
    if (newOne == null) {
      final collectionJson = CollectionDto.fromEntity(collection).toJson();
      changeLog.writeln('remove_collection:$collectionJson');
      continue;
    }

    final removedQuotes =
        collection.quotesIds.kt.minus(newOne.quotesIds.kt).asList();
    if (removedQuotes.isNotEmpty) {
      for (final removedQuote in removedQuotes) {
        changeLog.writeln(
          'remove_quote_from_collection_${collection.id}:$removedQuote',
        );
      }
    }
  }

  for (final collection in newCollections) {
    final oldOne = oldCollections.kt.find((p0) => p0.id == collection.id);
    if (oldOne == null) {
      final collectionJson = CollectionDto.fromEntity(collection).toJson();
      changeLog.writeln('add_collection:$collectionJson');
      continue;
    }

    final addedQuotes =
        collection.quotesIds.kt.minus(oldOne.quotesIds.kt).asList();
    if (addedQuotes.isNotEmpty) {
      for (final addedQuote in addedQuotes) {
        changeLog
            .writeln('add_quote_to_collection_${collection.id}:$addedQuote');
      }
    }
  }

  return changeLog.toString();
}

String _compareThemes(
  List<ThemeEntity> oldThemes,
  List<ThemeEntity> newThemes,
) {
  final StringBuffer changeLog = StringBuffer();

  final List<ThemeEntity> addedThemes = newThemes.where((element) {
    final result = oldThemes.kt.find((p0) => p0.themeId == element.themeId);
    if (result == null) return true;
    return false;
  }).toList();

  final List<ThemeEntity> removedThemes = oldThemes.where((element) {
    final result = newThemes.kt.find((p0) => p0.themeId == element.themeId);
    if (result == null) return true;
    return false;
  }).toList();

  if (removedThemes.isNotEmpty) {
    for (final theme in removedThemes) {
      final removedThemeJson = ThemeDto.fromEntity(theme).toJson();
      changeLog.writeln('- Remove Theme:$removedThemeJson');
    }
  }

  if (addedThemes.isNotEmpty) {
    for (final theme in addedThemes) {
      final newThemeJson = ThemeDto.fromEntity(theme).toJson();
      changeLog.writeln('+ Add Theme:$newThemeJson');
    }
  }

  return changeLog.toString();
}

String _compareReminders(
  List<ReminderEntity> oldReminders,
  List<ReminderEntity> newReminders,
) {
  final StringBuffer changeLog = StringBuffer();

  final List<ReminderEntity> addedReminders = newReminders.where((element) {
    final result = oldReminders.kt.find((p0) => p0.id == element.id);
    if (result == null || element.isActive != result.isActive) return true;
    return false;
  }).toList();

  final List<ReminderEntity> removedReminders = oldReminders.where((element) {
    final result = newReminders.kt.find((p0) => p0.id == element.id);
    if (result == null || element.isActive != result.isActive) return true;
    return false;
  }).toList();

  if (removedReminders.isNotEmpty) {
    for (final reminder in removedReminders) {
      final removedReminderJson = ReminderDto.fromEntity(reminder).toJson();
      changeLog.writeln('- Delete Reminder: $removedReminderJson');
    }
  }

  if (addedReminders.isNotEmpty) {
    for (final reminder in addedReminders) {
      final newReminderJson = ReminderDto.fromEntity(reminder).toJson();
      changeLog.writeln('+ Add Reminder: $newReminderJson');
    }
  }
  return changeLog.toString();
}
