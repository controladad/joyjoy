import 'dart:async';

import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:kt_dart/kt.dart';
import 'package:motivation_flutter/common/common_models.dart';
import 'package:motivation_flutter/common/presentation/common_imports.dart';
import 'package:motivation_flutter/core/failures.dart';

import 'package:motivation_flutter/features/profile/domain/models/collection_model.dart';
import 'package:motivation_flutter/features/profile/domain/models/reminder_model.dart';
import 'package:motivation_flutter/features/profile/domain/models/update_model.dart';

import 'package:motivation_flutter/features/profile/infrastructure/profile_data_source.dart';
import 'package:motivation_flutter/features/profile/domain/profile_repository.dart';
import 'package:motivation_flutter/features/profile/domain/models/profile_models.dart';
import 'package:motivation_flutter/features/profile/sub_features/select_author_category/select_author_category_bloc/select_author_category_bloc.dart';
import 'package:motivation_flutter/features/profile/sub_features/select_author_category/select_author_category_args.dart';
import 'package:motivation_flutter/features/quotes/domain/models/quotes_model.dart';
import 'package:motivation_flutter/features/quotes/domain/models/saved_quote_union.dart';
import 'package:motivation_flutter/features/quotes/domain/models/sub_category_model.dart';
import 'package:motivation_flutter/features/sub_auth/domain/subscription_service.dart';
import 'package:motivation_flutter/features/theme/domain/models/theme_model.dart';
import 'package:motivation_flutter/shared/infrastructure/shared_data_source.dart';

@Injectable(as: IProfileRepository)
class ProfileRepositoryImplementation implements IProfileRepository {
  final IProfileApiDataSource profileDataSource;
  final ISharedDataSource sharedDataSource;
  final ISubscriptionService subscriptionService;

  ProfileRepositoryImplementation(
    this.profileDataSource,
    this.sharedDataSource,
    this.subscriptionService,
  );

  @override
  Stream<void> get cacheUpdated {
    return sharedDataSource.cacheUpdated;
  }

  @override
  Future<Either<GeneralFailure, KtList<ReminderModel>>> get getReminders =>
      tryCacheBlock(() => sharedDataSource.getReminders);

  @override
  Future<Either<GeneralFailure, KtList<ReminderModel>>> removeReminder(
    String reminderId,
  ) =>
      tryCacheBlock(() async {
        final result = await sharedDataSource.removeReminder(reminderId);
        return result;
      });

  @override
  Future<Either<GeneralFailure, ReminderModel>> addOrUpdateReminder(
    ReminderModel reminder,
  ) {
    return tryCacheBlock(() async {
      final newReminders =
          await sharedDataSource.addOrUpdateReminders(reminder);
      final newReminder = newReminders.find((p0) => p0.id == reminder.id);
      return newReminder!;
    });
  }

  @override
  Future<void> clearAllCacheDebug() {
    return sharedDataSource.logout();
  }

  @override
  Future<Either<GeneralFailure, KtList<CollectionModel>>> deleteCollection(
    String id,
  ) {
    return tryCacheBlock(() {
      return sharedDataSource.removeCollection(id);
    });
  }

  @override
  Future<Either<GeneralFailure, LazyLoadResponseModel<AuthorOrCategoryUnion>>>
      getAllAuthorsOrCategoriesLazyLoad({
    required AuthorOrCategoryEnum type,
    required Option<String> searchQuery,
    required Option<KtList<String>> ids,
    required int page,
    required int limit,
  }) {
    return tryCacheBlock(() async {
      if (type == AuthorOrCategoryEnum.Category) {
        final response = await profileDataSource.getCategories(
          page: page,
          searchQuery: searchQuery,
          ids: ids,
          limit: limit,
        );
        return LazyLoadResponseModel(
          results: response.results
              .map((category) => AuthorOrCategoryUnion.category(category)),
          total: response.total,
        );
      } else {
        final response = await profileDataSource.getAuthors(
          page: page,
          searchQuery: searchQuery,
          ids: ids,
          limit: limit,
        );
        return LazyLoadResponseModel(
          results: response.results
              .map((author) => AuthorOrCategoryUnion.author(author)),
          total: response.total,
        );
      }
    });
  }

  @override
  Future<Either<GeneralFailure, ProfileModel>> get getProfile async {
    return tryCacheBlock(() async {
      final subscriptionPlan = await subscriptionService.subscription;
      final cacheData = await sharedDataSource.getCacheData;

      final ProfileModel profile = ProfileModel(
        // banner: none(),
        currentTheme: cacheData.allThemes
                .find((theme) => theme.themeId == cacheData.selectedThemeId) ??
            ThemeModel.defaultTheme(),
        likedQuotes: cacheData.favoriteQuotesIds,
        collections: cacheData.collections,
        preferredTopics: cacheData.favoriteCategoriesIds,
        favoriteAuthors: cacheData.favoriteAuthorsIds,
        subscriptionPlan: subscriptionPlan,
        accountInfo: cacheData.account,
      );
      return profile;
    });
  }

  @override
  Future<Either<GeneralFailure, ProfileContentsModel>>
      get getOnlineContents async => tryCacheBlock(() async {
            final res = await profileDataSource.getContents;
            return res;
          });

  @override
  Future<Either<GeneralFailure, KtList<CollectionModel>>>
      get getCollections async {
    return tryCacheBlock(() async {
      final cacheData = await sharedDataSource.getCacheData;
      return cacheData.collections;
    });
  }

  @override
  Future<Either<GeneralFailure, KtList<QuoteModel>>> getQuotesByDestination({
    required SavedQuoteType destination,
  }) async {
    return tryCacheBlock(() => sharedDataSource.getSavedQuotes(destination));
  }

  @override
  Future<Either<GeneralFailure, KtList<QuoteModel>>> removeQuote({
    required SavedQuoteType type,
    required String quoteId,
  }) {
    return tryCacheBlock(
      () => sharedDataSource.toggleSavedQuote(type: type, quoteId: quoteId),
    );
  }

  @override
  Future<Either<GeneralFailure, Unit>> updatePreferrers(
    AuthorOrCategoryEnum type,
    KtList<String> ids,
  ) async {
    final result = await tryCacheBlock(() async {
      switch (type) {
        case AuthorOrCategoryEnum.Category:
          return sharedDataSource.updateFavorites(
            categories: some(ids),
            authors: none(),
          );
        case AuthorOrCategoryEnum.Author:
          return sharedDataSource.updateFavorites(
            authors: some(ids),
            categories: none(),
          );
      }
    });

    return result.fold((l) => left(l), (r) => right(unit));
  }

  @override
  Future<Either<GeneralFailure, KtList<CollectionModel>>> addOrUpdateCollection(
    CollectionModel collection,
  ) {
    return tryCacheBlock(() {
      return sharedDataSource.addOrUpdateCollections(collection);
    });
  }

  @override
  Future<Either<GeneralFailure, KtList<SubCategoryModel>>> getCategoriesByIds(
    KtList<String> ids,
  ) async =>
      tryCacheBlock(() async {
        final categories = await profileDataSource.getCategories(
          page: 1,
          searchQuery: none(),
          ids: some(ids),
          limit: ids.size,
        );
        return categories.results;
      });

  @override
  Future<Either<GeneralFailure, Unit>> changeUserName(String newName) async {
    final accountInfo = await sharedDataSource.getAccountInfo();
    await sharedDataSource
        .updateAccountInfo(accountInfo.copyWith(name: some(newName)));
    return right(unit);
  }

  @override
  Future<Either<GeneralFailure, UpdateModel>> get hasUpdate =>
      tryCacheBlock(() => profileDataSource.hasUpdate);
}
