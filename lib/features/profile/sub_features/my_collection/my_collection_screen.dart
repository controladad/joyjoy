import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:kt_dart/kt.dart';
import 'package:motivation_flutter/common/presentation/buttons.dart';
import 'package:motivation_flutter/common/presentation/common_imports.dart';
import 'package:motivation_flutter/features/onboarding/presentation/components/profile_empty_state_widget.dart';
import 'package:motivation_flutter/features/profile/domain/models/collection_model.dart';
import 'package:motivation_flutter/common/presentation/utils/custom_bottom_sheets.dart';
import 'package:motivation_flutter/features/profile/sub_features/my_collection/my_collection_bloc/my_collection_bloc.dart';
import 'package:motivation_flutter/features/quotes/domain/models/saved_quote_union.dart';

@RoutePage()
class MyCollectionsScreen extends StatelessWidget {
  const MyCollectionsScreen({super.key});

  @override
  Widget build(BuildContext context) {
    final state = context.watch<MyCollectionBloc>().state;
    final bloc = context.read<MyCollectionBloc>();

    return CommonScaffoldWithAppbar(
      title: context.translate.myCollections,
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: state.data.fold(
        () => null,
        (a) => SContainer(
          width: double.infinity,
          topperColor: AppTheme.surface1_dark.withOpacity(.05),
          decoration: const BoxDecoration(
            color: AppTheme.background_dark,
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(25),
              topRight: Radius.circular(25),
            ),
          ),
          child: CustomFilledButton(
            label: context.translate.addNewCollection,
            padding: pad(16, 12, 16, 32),
            onTap: () {
              showAddNewCollectionBottomSheets(context, bloc);
            },
          ),
        ),
      ),
      body: state.data
          .fold(() => const CommonLoadingScreen(), (a) => const _Body()),
    );
  }
}

void showAddNewCollectionBottomSheets(
  BuildContext context,
  MyCollectionBloc bloc, {
  bool reserveCollection = false,
}) {
  showCustomBottomSheet(
    context: context,
    onClose: () {},
    child: _AddNewCollectionBottomSheet(bloc, reserveCollection),
  );
}

class _Body extends StatelessWidget {
  const _Body();

  @override
  Widget build(BuildContext context) {
    final state = context.watch<MyCollectionBloc>().state;
    final collections = state.collections;

    if (collections.isEmpty()) {
      return ProfileEmptyState(
        title: context.translate.thereIsNoCollectionHere,
        subtitle: context.translate.youCanListYourFavoriteQuotes,
      );
    }

    return ListView(
      padding: pad(10, 0, 10, 100),
      children:
          collections.map((collection) => _ListItemWidget(collection)).asList(),
    );
  }
}

class _ListItemWidget extends StatelessWidget {
  final CollectionModel collection;
  const _ListItemWidget(this.collection);

  @override
  Widget build(BuildContext context) {
    final bloc = context.read<MyCollectionBloc>();
    return SContainer(
      margin: pad(0, 10, 0, 10),
      height: 75,
      maxWidth: 400,
      child: ClipRRect(
        borderRadius: BorderRadius.circular(15),
        child: MaterialButton(
          padding: EdgeInsets.zero,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15),
            side: const BorderSide(
              color: AppTheme.outline_variant_dark,
              width: .5,
            ),
          ),
          onPressed: () => context.pushRoute(
            ListOfQuoteRoute(
              type: SavedQuoteType.collection(collection),
            ),
          ),
          child: Slidable(
            key: ValueKey(collection.id),
            endActionPane: ActionPane(
              dismissible: DismissiblePane(
                dismissalDuration: const Duration(milliseconds: 20),
                resizeDuration: Duration.zero,
                onDismissed: () =>
                    bloc.add(MyCollectionEvent.removeCollection(collection.id)),
              ),
              motion: const ScrollMotion(),
              children: [
                SlidableAction(
                  onPressed: (_) => bloc
                      .add(MyCollectionEvent.removeCollection(collection.id)),
                  icon: Icons.delete,
                  backgroundColor: Colors.red,
                ),
              ],
            ),
            child: Padding(
              padding: pad(12, 0, 12, 0),
              child: Row(
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SText(collection.name, style: TStyle.TitleLarge),
                      SText(
                        collection.quotesIds.isEmpty()
                            ? context.translate.empty.capitalize()
                            : '${collection.quotesIds.size} ${context.translate.quotes}'
                                .toTitleCase(),
                        style: TStyle.LabelSmall,
                      ),
                    ],
                  ),
                  const Spacer(),
                  const ImageWidget(
                    image: AppIcons.rightChevron,
                    color: AppTheme.on_surface_dark,
                    height: 24,
                    width: 24,
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class _AddNewCollectionBottomSheet extends StatefulWidget {
  final MyCollectionBloc bloc;
  final bool reserveCollection;
  const _AddNewCollectionBottomSheet(this.bloc, this.reserveCollection);

  @override
  State<_AddNewCollectionBottomSheet> createState() =>
      _AddNewCollectionBottomSheetState();
}

class _AddNewCollectionBottomSheetState
    extends State<_AddNewCollectionBottomSheet> {
  String? name;

  @override
  Widget build(BuildContext context) {
    return BlocProvider.value(
      value: widget.bloc,
      child: Builder(
        builder: (context) {
          final bloc = context.read<MyCollectionBloc>();
          return Padding(
            padding: pad(16, 10, 16, 12),
            child: Column(
              children: [
                SText(
                  context.translate.newCollection,
                  style: TStyle.TitleLarge,
                ),
                SText(
                  context.translate.chooseANameForYourNewCollection,
                  style: TStyle.TitleMedium,
                  color: AppTheme.on_surface_variant_dark,
                ),
                const SBox(height: 24),
                CustomTextField(
                  onChanged: (collectionName) {
                    setState(() {
                      name = collectionName;
                    });
                  },
                  autofocus: true,
                  icon: Padding(
                    padding: const EdgeInsets.all(4),
                    child: CustomFilledButton(
                      label: context.translate.save,
                      onTap: () {
                        bloc.add(
                          MyCollectionEvent.addNewCollection(
                            name ?? '',
                            addToChangeList: widget.reserveCollection,
                          ),
                        );
                        Navigator.pop(context);
                      },
                      isEnable: name?.isNotEmpty ?? false,
                      maxWidth: 73,
                      width: 73,
                    ),
                  ),
                ),
                const SBox(height: 20),
              ],
            ),
          );
        },
      ),
    );
  }
}
