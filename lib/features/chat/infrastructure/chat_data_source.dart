import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:motivation_flutter/features/chat/domain/models/chat_model.dart';

import 'package:motivation_flutter/features/chat/infrastructure/dtos/chat_dto.dart';
import 'package:motivation_flutter/features/chat/infrastructure/chat_api_services.dart';

abstract class IChatApiDataSource {
  Future<ChatDto> get getData;
  Future<Response> sendFeedback({
    required String id,
    required bool isLike,
  });
  Future<ChatResponseParentDto> sendMessage({
    required MessageModel message,
    required List<MessageModel> prevMessageList,
  });
}

@Injectable(as: IChatApiDataSource)
class ChatApiDataSourceImplementation implements IChatApiDataSource {
  final IChatApiService apiService;

  ChatApiDataSourceImplementation(this.apiService);

  @override
  Future<ChatDto> get getData async {
    throw UnimplementedError();
  }

  @override
  Future<ChatResponseParentDto> sendMessage({
    required MessageModel message,
    required List<MessageModel> prevMessageList,
  }) async {
    final response = await apiService.sendMessage(
      message: message,
      prevMessageList: prevMessageList,
    );
    print('');
    return ChatResponseParentDto.fromJson(
      response.data as Map<String, dynamic>,
    );
  }

  @override
  Future<Response> sendFeedback({
    required String id,
    required bool isLike,
  }) {
    return apiService.sendResponseFeedback(id: id, isLike: isLike);
  }
}
