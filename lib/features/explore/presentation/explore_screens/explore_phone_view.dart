import 'package:kt_dart/kt.dart';
import 'package:motivation_flutter/common/presentation/common_imports.dart';
import 'package:motivation_flutter/common/presentation/utils/failure_handler_widget.dart';
import 'package:motivation_flutter/features/explore/application/explore_bloc/explore_bloc.dart';
import 'package:motivation_flutter/features/explore/domain/models/explore_model.dart';
import 'package:motivation_flutter/features/explore/presentation/components/category_list_view_widget.dart';
import 'package:motivation_flutter/features/explore/presentation/search_screen/instance_search_screen.dart';

class ExplorePhoneView extends StatelessWidget {
  const ExplorePhoneView();

  @override
  Widget build(BuildContext context) {
    return const CommonBackgroundContainer(
      child: SafeArea(
        child: Scaffold(
          backgroundColor: Colors.transparent,
          body: _Body(),
        ),
      ),
    );
  }
}

class _Body extends StatelessWidget {
  const _Body();

  @override
  Widget build(BuildContext context) {
    final state = context.watch<ExploreBloc>().state;
    final bloc = context.read<ExploreBloc>();
    return NestedScrollView(
      headerSliverBuilder: (context, innerBoxIsScrolled) =>
          [_HeaderWidget(isScrolled: innerBoxIsScrolled)],
      body: FailureHandlerWidget<ExploreModel>(
        data: state.categoriesData,
        onRetry: () => bloc.add(const ExploreEvent.getCategories()),
        child: (explore) => SingleChildScrollView(
          padding: pad(0, 0, 0, 30),
          child: Column(
            children: explore.categories
                .map(
                  (category) => CategoryListViewWidget(
                    title: category.name,
                    categories: category.subCategories.asList(),
                  ),
                )
                .asList(),
          ),
        ),
      ),
    );
  }
}

class _HeaderWidget extends StatelessWidget {
  final bool isScrolled;
  // ignore: avoid_positional_boolean_parameters
  const _HeaderWidget({required this.isScrolled});

  @override
  Widget build(BuildContext context) {
    return SliverAppBar(
      pinned: true,
      floating: true,
      backgroundColor: isScrolled ? AppTheme.color1 : Colors.transparent,
      surfaceTintColor: Colors.transparent,
      expandedHeight: kToolbarHeight * 2.5,
      title: SText(
        context.translate.explore.capitalize(),
        style: TStyle.HeadlineSmall,
      ),
      flexibleSpace: LayoutBuilder(
        builder: (context, constrains) {
          final bool showSearch =
              (constrains.maxHeight - kToolbarHeight) > kToolbarHeight;
          return Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              AnimatedSwitcher(
                duration: const Duration(milliseconds: 400),
                child: showSearch
                    ? IconButton(
                        onPressed: () => Navigator.push(
                          context,
                          fullDialogPageRouteBuilder(
                            const InstanceSearchScreen(),
                          ),
                        ),
                        icon: const _SearchButton(),
                      )
                    : const SizedBox(height: 10),
              ),
              const SBox(height: 10),
            ],
          );
        },
      ),
      actions: [
        AnimatedCrossFade(
          duration: const Duration(milliseconds: 200),
          crossFadeState:
              isScrolled ? CrossFadeState.showFirst : CrossFadeState.showSecond,
          secondChild: const SizedBox(height: 10),
          firstChild: IconButton(
            padding: pad(16, 0, 16, 0),
            //revenueCat
            onPressed: () {
              Navigator.push(
                context,
                fullDialogPageRouteBuilder(const InstanceSearchScreen()),
              );
            },
            icon: const ImageWidget(
              image: AppIcons.searchExplore,
              height: 24,
              width: 24,
              color: AppTheme.on_surface_dark,
            ),
          ),
        ),
      ],
    );
  }
}

class _SearchButton extends StatelessWidget {
  const _SearchButton();

  @override
  Widget build(BuildContext context) {
    return SContainer(
      height: kToolbarHeight,
      decoration: BoxDecoration(
        borderRadius: const BorderRadius.all(Radius.circular(100)),
        border: Border.all(color: AppTheme.outline_dark),
      ),
      child: Center(
        child: Row(
          children: [
            const SBox(width: 12),
            const ImageWidget(
              image: AppIcons.searchExplore,
              height: 24,
              width: 24,
              color: AppTheme.on_surface_dark,
            ),
            const SBox(width: 12),
            SText(
              context.translate.searchCategoryAuthorKeyword,
              style: TStyle.BodyLarge,
              color: AppTheme.outline_dark,
            ),
          ],
        ),
      ),
    );
  }
}
