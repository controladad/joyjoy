import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:kt_dart/kt.dart';
import 'package:motivation_flutter/common/common_models.dart';
import 'package:motivation_flutter/common/presentation/common_imports.dart';
import 'package:motivation_flutter/core/failures.dart';

import 'package:motivation_flutter/features/explore/domain/models/explore_model.dart';
import 'package:motivation_flutter/features/explore/domain/models/search_models.dart';

import 'package:motivation_flutter/features/explore/infrastructure/explore_data_source.dart';
import 'package:motivation_flutter/features/explore/domain/explore_repository.dart';

@Injectable(as: IExploreRepository)
class ExploreRepositoryImplementation implements IExploreRepository {
  final IExploreApiDataSource _dataSource;

  ExploreRepositoryImplementation(this._dataSource);

  @override
  Future<Either<GeneralFailure, ExploreModel>> get getExplore async {
    final result = await tryCacheBlock(() async {
      final dto = await _dataSource.getExplore;
      return dto.toDomain();
    });
    return result;
  }

  @override
  Future<Either<GeneralFailure, SearchModel>> instanceSearchQuery(
    String query,
  ) =>
      tryCacheBlock(() => _dataSource.instantSearch(query));

  @override
  Future<Either<GeneralFailure, LazyLoadResponseModel<SearchResultUnion>>>
      fullSearchQuery({
    required String query,
    required SearchResultUnion firstItem,
    required int page,
  }) {
    return tryCacheBlock(
      () => _dataSource.fullSearch(
        searchQuery: query,
        firstItem: firstItem,
        page: page,
      ),
    );
  }

  @override
  Future<Either<GeneralFailure, KtList<String>>> addNewSearchHistory(
    String query,
  ) async {
    try {
      final result = await _dataSource.addNewSearchHistory(query);
      return right(result);
    } catch (e) {
      return left(const GeneralFailure.unexpected());
    }
  }

  @override
  Future<Either<GeneralFailure, KtList<String>>> get getSearchHistories async {
    try {
      final result = await _dataSource.getSearchHistories;
      return right(result);
    } catch (e) {
      return left(const GeneralFailure.unexpected());
    }
  }

  @override
  Future<Either<GeneralFailure, KtList<String>>> removeSearchHistories() async {
    try {
      final result = await _dataSource.removeSearchHIstories();
      return right(result);
    } catch (e) {
      return left(const GeneralFailure.unexpected());
    }
  }
}
