part of 'paywall_cubit.dart';

@injectable
@freezed
class PaywallState with _$PaywallState {
  const factory PaywallState({
    required Option<Either<GeneralFailure, PaywallModel>> data,
    required Option<PaywallModel> paywallModel,
    required Option<PayWallSubscriptionPlanModel> selectedSubscription,
    required Option<UserSubscription> currentSubscription,
  }) = _PaywallState;
  @factoryMethod
  factory PaywallState.init() => PaywallState(
        data: none(),
        paywallModel: none(),
        selectedSubscription: none(),
        currentSubscription: none(),
      );
}
