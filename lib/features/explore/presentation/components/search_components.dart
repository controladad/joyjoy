import 'package:motivation_flutter/common/presentation/common_imports.dart';
import 'package:motivation_flutter/features/explore/domain/models/search_models.dart';
import 'package:motivation_flutter/features/quotes/domain/models/quotes_models.dart';

class SearchListItemWidget extends StatelessWidget {
  final SearchResultUnion item;

  const SearchListItemWidget(this.item);

  @override
  Widget build(BuildContext context) {
    late String title;
    late String subTitle;
    late bool isLocked;
    late Function() onTap;

    item.map(
      category: (e) {
        title = e.category.name;
        subTitle = e.category.description;
        isLocked = e.category.status == AccessabilityStatus.Premium;
        onTap = () {};
      },
      author: (e) {
        title = e.author.name;
        subTitle = e.author.shortDescription;
        isLocked = e.author.status == AccessabilityStatus.Premium;
        onTap = () {};
      },
      quote: (e) {
        title = e.quote.quote;
        subTitle = e.quote.author?.name ?? '';
        isLocked = false;
        onTap = () {};
      },
    );

    return MaterialButton(
      onPressed: () => onTap(),
      padding: pad(9, 9, 9, 9),
      highlightColor: AppTheme.outline_dark.withOpacity(.1),
      splashColor: AppTheme.outline_dark.withOpacity(.2),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
      child: SizedBox(
        width: 260,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SText(
                    title,
                    style: TStyle.BodyLarge,
                    maxLines: isLocked ? 1 : 2,
                    overflow: TextOverflow.ellipsis,
                  ),
                  if (subTitle.isNotEmpty)
                    SText(
                      subTitle,
                      style: TStyle.LabelSmall,
                      color: AppTheme.outline_dark,
                    ),
                ],
              ),
            ),
            if (isLocked)
              const ImageWidget(
                image: AppIcons.whiteLock,
                height: 24,
                width: 24,
              )
          ],
        ),
      ),
    );
  }
}
