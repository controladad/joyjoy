import 'package:motivation_flutter/common/presentation/buttons.dart';
import 'package:motivation_flutter/common/presentation/common_presentation.dart';
import 'package:motivation_flutter/common/presentation/utils/custom_bottom_sheets.dart';
import 'package:motivation_flutter/common/presentation/utils/failure_handler_widget.dart';
import 'package:motivation_flutter/features/quotes/application/quotes_bloc/quotes_bloc.dart';
import 'package:motivation_flutter/features/quotes/domain/models/quotes_models.dart';

Future<void> showReportBottomSheets({
  required BuildContext context,
  required Function() onSubmit,
  required QuoteModel quote,
}) {
  return showCustomBottomSheet(
    context: context,
    onClose: () {},
    child: _ReportBottomSheetsWidget(
      quote: quote,
      shareScreenContext: context,
      onSubmit: onSubmit,
    ),
  );
}

class _ReportBottomSheetsWidget extends StatelessWidget {
  final QuoteModel quote;
  final BuildContext shareScreenContext;
  final Function() onSubmit;
  const _ReportBottomSheetsWidget({
    required this.quote,
    required this.shareScreenContext,
    required this.onSubmit,
  });

  @override
  Widget build(BuildContext context) {
    void onReport(String reportText) {
      showReportSubmitBottomSheets(
        context: shareScreenContext,
        reportText: reportText,
        quoteId: quote.id,
        onSubmit: onSubmit,
      );
    }

    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        SText(context.translate.report, style: TStyle.TitleLarge),
        const SizedBox(height: 10),
        SText(
          context
              .translate.ifQuoteHasAProblemReportItWithAvailableOptionsDonTWait,
          style: TStyle.BodyLarge,
          maxWidth: 333,
        ),
        const SizedBox(height: 8),
        _ReportBottomSheetItem(
          label: context.translate.itSSpam,
          onTap: () {
            Navigator.pop(context);
            onReport(context.translate.itSSpam);
          },
        ),
        _ReportBottomSheetItem(
          label: context.translate.hateSpeechOrSymbols,
          onTap: () {
            Navigator.pop(context);
            onReport(context.translate.hateSpeechOrSymbols);
          },
        ),
        _ReportBottomSheetItem(
          label: context.translate.falseInformation,
          onTap: () {
            Navigator.pop(context);
            onReport(context.translate.falseInformation);
          },
        ),
        _ReportBottomSheetItem(
          label: context.translate.iJustDonTLikeIt,
          onTap: () {
            Navigator.pop(context);
            onReport(context.translate.iJustDonTLikeIt);
          },
        ),
        _ReportBottomSheetItem(
          label: context.translate.others,
          onTap: () {
            context.popRoute();
            shareScreenContext.pushRoute(
              ReportRoute(
                onSubmit: (reportText) {
                  return onReport(reportText);
                },
              ),
            );
          },
        ),
        const SBox(height: 32),
      ],
    );
  }
}

class _ReportBottomSheetItem extends StatelessWidget {
  final String label;
  final Function() onTap;
  const _ReportBottomSheetItem({required this.label, required this.onTap});

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      onPressed: onTap,
      child: SContainer(
        width: double.infinity,
        height: 56,
        padding: pad(16, 16, 16, 16),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            SText(label, style: TStyle.BodyLarge),
            const ImageWidget(
              image: AppIcons.rightChevron,
              height: 25,
              width: 25,
              color: AppTheme.primary_dark,
            ),
          ],
        ),
      ),
    );
  }
}

Future<void> showReportSubmitBottomSheets({
  required BuildContext context,
  required String quoteId,
  required String reportText,
  required Function() onSubmit,
}) {
  return showCustomBottomSheet(
    context: context,
    onClose: onSubmit,
    child: _ReportSubmitWidget(
      quoteId: quoteId,
      reportText: reportText,
    ),
  );
}

class _ReportSubmitWidget extends StatefulWidget {
  final String quoteId;
  final String reportText;
  const _ReportSubmitWidget({required this.quoteId, required this.reportText});

  @override
  State<_ReportSubmitWidget> createState() => _ReportSubmitWidgetState();
}

class _ReportSubmitWidgetState extends State<_ReportSubmitWidget> {
  @override
  void initState() {
    super.initState();
    final bloc = context.read<QuotesBloc>();
    bloc.add(
      QuotesEvent.reportQuote(
        quoteId: widget.quoteId,
        reportText: widget.reportText,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final state = context.watch<QuotesBloc>().state;
    final bloc = context.read<QuotesBloc>();

    void close() => Navigator.pop(context);

    return AnimatedContainer(
      duration: const Duration(milliseconds: 200),
      height: state.reportData
          .fold(() => 190, (a) => a.fold((l) => 500, (r) => 270)),
      child: FailureHandlerWidget(
        data: state.reportData,
        onRetry: () => bloc.add(
          QuotesEvent.reportQuote(
            quoteId: widget.quoteId,
            reportText: widget.reportText,
          ),
        ),
        child: (r) => Column(
          children: [
            const ImageWidget(image: Images.bitCheck, height: 64, width: 64),
            const SBox(height: 17),
            SText(
              context.translate.thanksForLettingUsKnow,
              style: TStyle.TitleLarge,
            ),
            const Spacer(),
            CustomOutlinedButton(
              label: context.translate.iWantToSeeLessOfThisKindOfQuote,
              scaleDown: true,
              onTap: () {
                bloc.add(QuotesEvent.deleteQuote(widget.quoteId));
                close();
              },
            ),
            const SBox(height: 16),
            CustomFilledButton(
              label: context.translate.close,
              onTap: close,
            ),
            const SBox(height: 32),
          ],
        ),
      ),
    );
  }
}
