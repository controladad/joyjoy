import 'package:freezed_annotation/freezed_annotation.dart';

import 'package:motivation_flutter/features/theme/domain/models/theme_models.dart';

part 'theme_dto.freezed.dart';
part 'theme_dto.g.dart';

@freezed
class ThemeDto with _$ThemeDto {
  factory ThemeDto({
    @JsonKey(name: 'id') required String id,
  }) = _ThemeDto;

  factory ThemeDto.fromJson(Map<String, dynamic> json) =>
      _$ThemeDtoFromJson(json);
  const ThemeDto._();

  ThemeModel toDomain() => throw UnimplementedError();
}
