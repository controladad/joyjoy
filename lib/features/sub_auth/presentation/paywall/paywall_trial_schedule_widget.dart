import 'package:kt_dart/kt.dart';
import 'package:motivation_flutter/common/presentation/common_imports.dart';
import 'package:motivation_flutter/features/sub_auth/domain/models/paywall_model.dart';

class PaywallTrialScheduleWidget extends StatelessWidget {
  final KtList<PayWallTrialScheduleItemModel> schedules;
  const PaywallTrialScheduleWidget(this.schedules);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: pad(20, 0, 20, 0),
      child: Column(
        children: [
          Stack(
            alignment: Alignment.center,
            children: [
              SText(
                context.translate.howYourFreeTrialWorks,
                style: TStyle.TitleLarge,
                maxLines: 1,
              ),
              const Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  SizedBox(width: 30),
                  ImageWidget(image: Images.circle, height: 50, width: 100),
                ],
              ),
            ],
          ),
          const SBox(height: 30),
          Stack(
            clipBehavior: Clip.none,
            children: [
              SBox(
                height: schedules.size * 90,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      width: 35,
                      // height: 360,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        gradient: AppGradients.primaryLight,
                      ),
                      child: Column(
                        children: [
                          Flexible(
                            flex: schedules.size * 3,
                            child: Container(
                              width: 35,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(20),
                                gradient: AppGradients.primaryDark,
                              ),
                              child: Padding(
                                padding: pad(0, 13, 0, 13),
                                child: Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: schedules
                                      .map(
                                        (e) => ImageWidget(
                                          image: e.icon,
                                          height: 20,
                                          width: 20,
                                        ),
                                      )
                                      .asList(),
                                ),
                              ),
                            ),
                          ),
                          const Spacer(flex: 2),
                        ],
                      ),
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: schedules
                          .map(
                            (e) => Flexible(
                              child: SContainer(
                                margin: pad(12, 6, 12, 12),
                                width: 250,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    SText(
                                      e.title.toTitleCase(),
                                      style: TStyle.TitleLarge,
                                    ),
                                    Padding(
                                      padding: pad(2, 0, 2, 0),
                                      child: SText(
                                        e.subtitle.toTitleCase(),
                                        style: TStyle.LabelLarge,
                                        maxLines: 2,
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          )
                          .asList(),
                    ),
                  ],
                ),
              ),
              const Positioned(
                top: -20,
                right: 20,
                child: ImageWidget(
                  image: Images.pointer,
                  height: 130,
                  width: 100,
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}
