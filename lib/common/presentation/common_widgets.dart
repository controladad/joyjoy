import 'package:motivation_flutter/common/presentation/common_imports.dart';

class CommonBackgroundContainer extends StatelessWidget {
  final Widget child;
  const CommonBackgroundContainer({super.key, required this.child});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: double.infinity,
      width: double.infinity,
      decoration: BoxDecoration(
        gradient: AppGradients.backGround,
      ),
      child: child,
    );
  }
}

class AvailableWidthSizeBox extends StatelessWidget {
  final Widget child;
  const AvailableWidthSizeBox({super.key, required this.child});

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraints) {
        return SizedBox(width: constraints.maxWidth, child: child);
      },
    );
  }
}

class CommonScaffoldWithAppbar extends StatelessWidget {
  final Widget body;
  final String title;
  final String? subtitle;
  final Widget? floatingActionButton;
  final FloatingActionButtonLocation? floatingActionButtonLocation;
  final bool resizeToAvoidBottomInset;
  final bool automaticallyImplyLeading;
  const CommonScaffoldWithAppbar({
    super.key,
    required this.title,
    required this.body,
    this.subtitle,
    this.floatingActionButton,
    this.floatingActionButtonLocation,
    this.resizeToAvoidBottomInset = false,
    this.automaticallyImplyLeading = true,
  });

  @override
  Widget build(BuildContext context) {
    return CommonBackgroundContainer(
      child: Scaffold(
        backgroundColor: Colors.transparent,
        floatingActionButton: floatingActionButton,
        floatingActionButtonLocation: floatingActionButtonLocation,
        resizeToAvoidBottomInset: resizeToAvoidBottomInset,
        appBar: AppBar(
          surfaceTintColor: Colors.transparent,
          backgroundColor: Colors.transparent,
          leading: const CommonBackArrowButton(),
          title: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SText(title.toTitleCase(), style: TStyle.HeadlineSmall),
              if (subtitle != null)
                SText(
                  subtitle!,
                  style: TStyle.TitleSmall,
                ),
            ],
          ),
        ),
        body: body,
      ),
    );
  }
}

class CommonBottomInsetsWidget extends StatelessWidget {
  final double fraction;
  const CommonBottomInsetsWidget({
    super.key,
    this.fraction = 1,
  });

  @override
  Widget build(BuildContext context) {
    return SBox(height: MediaQuery.of(context).viewInsets.bottom * fraction);
  }
}

class CommonLoadingScreen extends StatelessWidget {
  const CommonLoadingScreen({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container();
  }
}

class CommonBackArrowButton extends StatelessWidget {
  const CommonBackArrowButton({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return IconButton(
      onPressed: () => Navigator.pop(context),
      icon: const ImageWidget(
        quarterTurns: 3,
        image: AppIcons.backArrow,
        color: AppTheme.on_surface_variant_dark,
        height: 15,
        width: 15,
      ),
    );
  }
}

PageRouteBuilder fullDialogPageRouteBuilder(Widget screen) {
  return PageRouteBuilder(
    pageBuilder: (
      context,
      animation,
      secondaryAnimation,
    ) {
      return screen;
    },
    opaque: false,
    reverseTransitionDuration: const Duration(milliseconds: 200),
    transitionsBuilder: (context, animation, secondaryAnimation, child) =>
        SlideTransition(
      position: Tween<Offset>(begin: const Offset(0, 1), end: Offset.zero)
          .animate(animation),
      child: child,
    ),
  );
}

class OnKeyboardUpDisappearWidget extends StatelessWidget {
  final Widget child;
  const OnKeyboardUpDisappearWidget({super.key, required this.child});

  @override
  Widget build(BuildContext context) {
    return AnimatedSwitcher(
      duration: const Duration(milliseconds: 1000),
      child: keyboardIsVisible(context) ? Container() : child,
    );
  }
}
