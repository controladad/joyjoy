import 'dart:math';

import 'package:dartz/dartz.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:motivation_flutter/features/language/domain/models/language_entity.dart';
import 'package:kt_dart/collection.dart';

import 'package:motivation_flutter/common/presentation/common_imports.dart';
import 'package:hive/hive.dart';
import 'package:motivation_flutter/constants/db_constants.dart';
import 'package:motivation_flutter/features/quotes/domain/models/quotes_models.dart';
import 'package:motivation_flutter/features/sub_auth/application/sub_auth_bloc/sub_auth_bloc.dart';
import 'package:motivation_flutter/features/sub_auth/domain/models/paywall_model.dart';

Future<void> toast(BuildContext context, String text, {double padding = 255}) {
  return showSnackbar(
    context,
    text,
    paddingFromBottom: padding,
    color: const Color(0xff332F32),
    textColor: AppTheme.white,
  );
}

bool hasSubscription(BuildContext context) {
  final state = context.watch<SubAuthCubit>().state;
  return state.subscription.fold(
    () => false,
    (a) => a.isPremium,
  );
}

List<DateTime> getDateTimeList(
  DateTime startDateTime,
  DateTime endDateTime,
  int interval,
) {
  final Duration difference = endDateTime.difference(startDateTime);
  final int minutesToAdd = (difference.inMinutes / interval).floor();

  final List<DateTime> dateTimeList = [];
  for (int i = 0; i < interval; i++) {
    final DateTime currentDateTime =
        startDateTime.add(Duration(minutes: minutesToAdd * i));
    dateTimeList.add(currentDateTime);
  }

  return dateTimeList;
}

Future<Uint8List> loadAssetImageAsUint8List(String assetPath) async {
  final ByteData data = await rootBundle.load(assetPath);
  final Uint8List bytes =
      data.buffer.asUint8List(data.offsetInBytes, data.lengthInBytes);
  return bytes;
}

String getShareQuoteText(QuoteModel quote) {
  return '${quote.quote}\n\n\n${quote.author?.name ?? ''}\n\napi.JoyJoy.chat';
}

Future<bool> get hasInternetConnection async {
  return InternetConnectionChecker().hasConnection;
}

void forceBlocToStart<T extends BlocBase>(BuildContext context) =>
    context.read<T>();

bool keyboardIsVisible(BuildContext context) {
  return KeyboardVisibilityProvider.isKeyboardVisible(context);
}

Size getDeviceSize(BuildContext context) {
  return MediaQuery.of(context).size;
}

String getIconWithOptionAvailability(
  OptionAvailabilityEnum optionAvailability,
) {
  switch (optionAvailability) {
    case OptionAvailabilityEnum.Lock:
      return AppIcons.lock;
    case OptionAvailabilityEnum.Ad:
      return AppIcons.ad;
    case OptionAvailabilityEnum.Open:
      return AppIcons.checkMark;
  }
}

LinearGradient createGradient({
  required List<Color> colors,
  double? degree,
  List<double>? stops,
}) {
  return LinearGradient(
    colors: colors,
    transform: degree != null ? GradientRotation(degree * pi / 180) : null,
    stops: stops,
  );
}

AppBar changeSystemOverlayAppBar({bool isWhite = true}) {
  return AppBar(
    elevation: 0,
    toolbarHeight: 0,
    // backgroundColor: isWhite ? AppTheme.white : AppTheme.primary,
    systemOverlayStyle: SystemUiOverlayStyle(
      statusBarIconBrightness: isWhite ? Brightness.dark : Brightness.light,
      statusBarBrightness: Brightness.light,
      // statusBarColor: isWhite ? AppTheme.white : AppTheme.primary,
    ),
  );
}

//functions
bool isRtl(BuildContext context) {
  final TextDirection currentDirection = Directionality.of(context);
  final bool isRTL = currentDirection == TextDirection.rtl;
  return isRTL;
}

bool isAListSubListOfOther(List<dynamic> firstList, List<dynamic> parentList) {
  bool res = true;
  for (final item in firstList) {
    res = res && parentList.contains(item);
  }
  return res;
}

bool isAListSubListOfOneListOfOtherList(
  List<dynamic> firstList,
  List<List<dynamic>> parentList,
) {
  for (final list in parentList) {
    if (isAListSubListOfOther(firstList, list)) {
      return true;
    }
  }
  return false;
}

double degreeToRadian(double degrees) {
  return degrees * pi / 180;
}

String timeAgoSinceDate({
  required BuildContext context,
  required DateTime createdTime,
  required bool numericDates,
}) {
  final DateTime date = createdTime.toLocal();
  final date2 = DateTime.now().toLocal();
  final difference = date2.difference(date);

  if (difference.inSeconds < 5) {
    return context.translate.justNow;
  } else if (difference.inSeconds <= 60) {
    return '${difference.inSeconds} ${context.translate.secondsAgo}';
  } else if (difference.inMinutes <= 1) {
    return numericDates ? '1 ${context.translate.minuteAgo}' : 'A minute ago';
  } else if (difference.inMinutes <= 60) {
    return '${difference.inMinutes}  ${context.translate.minuteAgo}';
  } else if (difference.inHours <= 1) {
    return numericDates ? '1 ${context.translate.hourAgo}' : 'An hour ago';
  } else if (difference.inHours <= 60) {
    return '${difference.inHours} ${context.translate.hoursAgo}';
  } else if (difference.inDays <= 1) {
    return numericDates
        ? '1 ${context.translate.dayAgo}'
        : context.translate.yesterday;
  } else if (difference.inDays <= 6) {
    return '${difference.inDays} ${context.translate.dayAgo}';
  } else if ((difference.inDays / 7).ceil() <= 1) {
    return numericDates ? '1 ${context.translate.weekAgo}' : 'Last week';
  } else if ((difference.inDays / 7).ceil() <= 4) {
    return '${(difference.inDays / 7).ceil()} ${context.translate.weeksAgo}';
  } else if ((difference.inDays / 30).ceil() <= 1) {
    return numericDates ? '1 ${context.translate.mountAgo}' : 'Last month';
  } else if ((difference.inDays / 30).ceil() <= 30) {
    return '${(difference.inDays / 30).ceil()} ${context.translate.mountsAgo}';
  } else if ((difference.inDays / 365).ceil() <= 1) {
    return numericDates ? '1 ${context.translate.yearAgo}' : 'Last year';
  }
  return '${(difference.inDays / 365).floor()} ${context.translate.yearsAgo}';
}

extension KtListToNormalListX<T> on KtList<T> {
  List<T> toNormalList() {
    final List<T> list = [];
    for (final item in asList()) {
      list.add(item);
    }
    return list;
  }
}

extension ContextEx on BuildContext {
  void dismissKeyboard() => FocusScope.of(this).unfocus();
  String get getCurrentRoute => ModalRoute.of(this)!.settings.name.toString();

  void pushAndRemoveAll(PageRouteInfo route) =>
      router.pushAndPopUntil(route, predicate: (_) => false);
}

extension StringExtension on String {
  String capitalize() {
    if (isNotEmpty) {
      return '${this[0].toUpperCase()}${substring(1).toLowerCase()}';
    }
    return '';
  }

  String toTitleCase() => replaceAll(RegExp(' +'), ' ')
      .split(' ')
      .map((str) => str.capitalize())
      .join(' ');
}

bool isRightForOptionOfEither(Option<Either<dynamic, dynamic>> option) {
  return option.fold(() => false, (a) => a.isRight());
}

void popByCount({required int popNumber, required BuildContext context}) {
  int total = 0;
  Navigator.popUntil(context, (route) {
    if (total == popNumber) {
      return true;
    }
    total += 1;

    return false;
  });
}

extension KtListViewX<T> on KtList<T> {
  List<Widget> toListOfWidget(Widget Function(T item) builder) {
    return map((p0) => builder(p0)).asList();
  }

  Widget toWidgetByIndex(int index, Widget Function(T item) builder) {
    return builder(get(index));
  }
}

// extension

extension DoubleX on String? {
  double get toDouble => double.tryParse(this ?? '') ?? 0;
  double? get toDoubleNullable => double.tryParse(this ?? '');
}

extension ResultX<L, R> on Option<Either<L, R>> {
  Option<R> get getOrNone {
    return fold(() => none(), (a) => a.fold((l) => none(), (r) => some(r)));
  }

  R? get getOrNull {
    return fold(() => null, (a) => a.fold((l) => null, (r) => r));
  }
}

extension OptionX on Option {
  T getOrCrash<T>() {
    return fold(
      () {
        throw UnsupportedError('getOrCrashMethod');
      },
      (a) => a as T,
    );
  }

  bool isSame<T>(Option<T> option) {
    return fold(
      () => option.fold(() => true, (a) => false),
      (a) => option.fold(() => false, (b) => a == b),
    );
  }
}

extension ToOption<T> on T? {
  Option<T> toOption() {
    // ignore: null_check_on_nullable_type_parameter
    return this != null ? some(this!) : none();
  }
}

extension IntX on String? {
  int get toInt => int.tryParse(this ?? '') ?? 0;
}

extension KtListX on KtList {
  KtList<T> addToStart<T>(T item) {
    final List<T> list = asList().toList() as List<T>;
    list.insert(0, item);
    return KtList.from(list);
  }

  KtList<T> dropFirst<T>() {
    final List<T> list = asList().toList() as List<T>;
    list.removeAt(0);
    return KtList.from(list);
  }
}

DateTime convertTimeZoneToLocalDataTime(String? dateTime) =>
    DateTime.tryParse(dateTime!)?.toLocal() ?? DateTime.now();

String get userLanguage {
  final box = Hive.box<LanguageEntity>(DBConstants.LANGUAGE);
  if (box.values.isEmpty) {
    return 'en';
  }
  return box.values.first.locale ?? 'en';
}

enum DaysOfWeek {
  Sun,
  Mon,
  Tue,
  Wen,
  Thu,
  Fri,
  Sat,
}

int getWeekDay(DaysOfWeek daysOfWeek) {
  switch (daysOfWeek) {
    case DaysOfWeek.Sun:
      return 7;
    case DaysOfWeek.Mon:
      return 1;
    case DaysOfWeek.Tue:
      return 2;
    case DaysOfWeek.Wen:
      return 3;
    case DaysOfWeek.Thu:
      return 4;
    case DaysOfWeek.Fri:
      return 5;
    case DaysOfWeek.Sat:
      return 6;
  }
}

String getDaysPreview(List<DaysOfWeek> days) {
  if (days.length == DaysOfWeek.values.length) {
    return 'Every Day';
  } else {
    return days.map((e) => e.name).join(' ');
  }
}
