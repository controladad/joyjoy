# flutter_boilerplate

This is a boilerplate for starting new projects in CAC.

## Getting Started

- First run our beloved flutter run command.

```CMD
flutter pub run build_runner build --delete-conflicting-outputs
```

- For renaming the app and bundleId first install `rename` package. and then run these commands as follows.

```CMD
pub global activate rename

pub global run rename --bundleId com.onatcipli.networkUpp
pub global run rename --appname "Network Upp"
```

- To change Splash screen and icon of the app. in the `pubspec.yaml` file change `flutter_icons` section and `flutter_native_splash` section. and run these commands accordingly. (flutter icons had a conflict with another package so I had to comment it) you have to uncomment then do the following commands.

```Terminal
flutter pub run flutter_native_splash:create
flutter pub run flutter_launcher_icons:main
```

- To add languages to the app go to `lib/utils/locale/app_localization.dart` and in `_AppLocalizationsDelegate` class add the country codes you want to add to the app. and add the language strings to `assets/lang/{language_code}.json`.

- In `lib/presentation/utils/size_config.dart` you may have to change some values from project to project

- In `lib/presentation/utils/styling.dart` you have to change the color and font and font size based on project

## Naming Conventions

- All Facade or Repository implementations should end with `_imp`, and classes end with `Imp`.

## Extension Functions

- For getting the TextTheme use `context.textTheme`.

- For getting a string from app localization use `context.translate('key')`.

## App Naming for Dev and Production

- Change name of the app for dev and production in app/build.gradle `TODO`.
