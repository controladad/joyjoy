import 'package:dartz/dartz.dart';
import 'package:motivation_flutter/shared/domain/shared_cache_props_model.dart';
import 'package:motivation_flutter/core/failures.dart';

import 'package:motivation_flutter/features/chat/domain/models/chat_models.dart';

abstract class IChatRepository {
  Future<Either<GeneralFailure, ChatModel>> get getChatList;
  Future<Either<GeneralFailure, ChatModel>> get clearMessages;
  Future<Either<GeneralFailure, ChatModel>> get stopGenerating;
  Future<Either<GeneralFailure, double>> get chatCredit;
  Future<Either<GeneralFailure, Unit>> toggleResponseFeedback({
    required String id,
    required bool isLike,
  });
  Future<Either<GeneralFailure, ChatNeededDataModel>> get chatNeededData;
  Stream<SharedCachePropsModel?> get cacheUpdated;
  Future<Either<GeneralFailure, MessageResponseModel>> sendMessage({
    required MessageModel message,
    required List<MessageModel> prevMessageList,
  });
}
