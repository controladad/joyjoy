import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:kt_dart/kt.dart';

import 'package:motivation_flutter/features/chat/domain/models/chat_models.dart';

part 'chat_dto.freezed.dart';
part 'chat_dto.g.dart';

@freezed
class ChatDto with _$ChatDto {
  factory ChatDto({
    @JsonKey(name: 'id') required String id,
  }) = _ChatDto;

  factory ChatDto.fromJson(Map<String, dynamic> json) =>
      _$ChatDtoFromJson(json);
  const ChatDto._();

  ChatModel toDomain() => ChatModel(
        id: id,
        messages: const KtList.empty(),
      );
}

@freezed
class ChatResponseParentDto with _$ChatResponseParentDto {
  factory ChatResponseParentDto({
    @JsonKey(name: 'data') required ChatResponseChildDto data,
  }) = _ChatResponseParentDto;
  const ChatResponseParentDto._();
  factory ChatResponseParentDto.fromJson(Map<String, dynamic> json) =>
      _$ChatResponseParentDtoFromJson(json);
}

@freezed
class ChatResponseChildDto with _$ChatResponseChildDto {
  factory ChatResponseChildDto({
    @JsonKey(name: 'prompt_response') String? response,
    @JsonKey(name: 'id') required String id,
    @JsonKey(name: 'remaining_tokens') required int remainToken,
  }) = _ChatResponseChildDto;
  const ChatResponseChildDto._();
  factory ChatResponseChildDto.fromJson(Map<String, dynamic> json) =>
      _$ChatResponseChildDtoFromJson(json);
}
