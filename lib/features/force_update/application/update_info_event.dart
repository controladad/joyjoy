part of 'update_info_bloc.dart';

@freezed
class UpdateInfoEvent with _$UpdateInfoEvent {
  const factory UpdateInfoEvent.checkInfo() = _CheckForUpdate;
}
