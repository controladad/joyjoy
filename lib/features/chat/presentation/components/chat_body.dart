import 'package:flutter_chat_ui/flutter_chat_ui.dart';
import 'package:kt_dart/kt.dart';
import 'package:motivation_flutter/common/application/ad_cubit.dart';
import 'package:motivation_flutter/common/presentation/buttons.dart';
import 'package:motivation_flutter/common/presentation/common_imports.dart';
import 'package:motivation_flutter/common/presentation/utils/failure_handler_widget.dart';
import 'package:motivation_flutter/features/chat/application/chat_bloc/chat_bloc.dart';
import 'package:motivation_flutter/features/chat/presentation/components/chat_bubble.dart';
import 'package:motivation_flutter/features/chat/presentation/components/chat_text_field.dart';
import 'package:motivation_flutter/features/chat/domain/models/chat_model.dart';
import 'package:flutter_chat_types/flutter_chat_types.dart' as types;

class ChatBody extends StatefulWidget {
  const ChatBody({
    super.key,
  });

  @override
  State<ChatBody> createState() => _ChatBodyState();
}

class _ChatBodyState extends State<ChatBody> {
  late TextEditingController textEditingController;
  @override
  void initState() {
    textEditingController = TextEditingController();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final state = context.watch<ChatBloc>().state;
    final ChatBloc bloc = context.read<ChatBloc>();

    void retry() {
      bloc.add(const ChatEvent.initial());
    }

    void onTapExample(String value) {
      bloc.add(ChatEvent.sendMessage(value));
    }

    return FailureHandlerWidget<ChatNeededDataModel>(
      data: state.chatNeededData,
      child: (chatNeededData) => Chat(
        dateHeaderBuilder: (p0) => Column(
          children: [
            Padding(
              padding: pad(0, 0, 0, 20),
              child: const Center(
                child: ImageWidget(
                  image: Images.chatHeader,
                  height: 110,
                  width: 178,
                ),
              ),
            ),
            const SBox(
              height: 30,
            ),
          ],
        ),
        emptyState: _ChatHeader(
          onTapStatement: onTapExample,
          name: chatNeededData.name,
          exampleList: chatNeededData.exampleList,
        ),
        customBottomWidget: ChatBottomWidget(
          controller: textEditingController,
        ),
        theme: const DefaultChatTheme(
          backgroundColor: Colors.transparent,
        ),
        bubbleBuilder: customBubbleBuilder,
        messages: state.messages
            .reversed()
            .map(
              (message) => types.TextMessage(
                author: types.User(id: message.sender.name),
                id: message.id,
                text: message.text,
                createdAt: int.parse(
                  '${message.dateTime.year}${message.dateTime.month}${message.dateTime.day}',
                ),
              ),
            )
            .asList(),
        onSendPressed: (message) {},
        user: types.User(id: Sender.Client.name),
      ),
      onRetry: retry,
    );
  }
}

class _ChatHeader extends StatelessWidget {
  final List<String> exampleList;
  final String name;
  final Function(String value) onTapStatement;

  const _ChatHeader({
    required this.exampleList,
    required this.name,
    required this.onTapStatement,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: pad(0, 0, 0, 30),
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              children: [
                Padding(
                  padding: pad(0, 0, 0, 20),
                  child: const Center(
                    child: ImageWidget(
                      image: Images.chatHeader,
                      height: 110,
                      width: 178,
                    ),
                  ),
                ),
                const SBox(
                  height: 10,
                ),
                SText(
                  '${name.toTitleCase()} ${context.translate.joinedChat}',
                  style: TStyle.LabelSmall,
                  color: AppTheme.on_surface_variant_dark,
                )
              ],
            ),
            const SBox(
              height: 70,
            ),
            Column(
              children: [
                SText(
                  context.translate.tryAnExample.toTitleCase(),
                  style: TStyle.LabelSmall,
                  color: AppTheme.on_surface_variant_dark,
                ),
                const SBox(
                  height: 10,
                ),
                ...exampleList.map(
                  (e) => Padding(
                    padding: pad(0, 10, 0, 0),
                    child: _Button(statement: e, onTap: onTapStatement),
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}

class _Button extends StatelessWidget {
  final String statement;
  final Function(String value) onTap;

  const _Button({required this.statement, required this.onTap});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: () => onTap(statement),
      child: SContainer(
        height: 40,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(30),
          color: AppTheme.primary_dark.withOpacity(0.16),
          boxShadow: const [
            BoxShadow(
              color: Color(0x4C000000),
              blurRadius: 4,
              offset: Offset(0, 4),
            ),
            BoxShadow(
              color: Color(0x26000000),
              blurRadius: 12,
              offset: Offset(0, 8),
              spreadRadius: 6,
            ),
          ],
        ),
        child: Padding(
          padding: pad(12, 0, 12, 0),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              SText(
                statement,
                style: TStyle.LabelLarge,
                color: AppTheme.white,
              ),
              const SBox(
                width: 10,
              ),
              const _CustomBullet(),
            ],
          ),
        ),
      ),
    );
  }
}

class _CustomBullet extends StatelessWidget {
  const _CustomBullet();

  @override
  Widget build(BuildContext context) {
    return SContainer(
      height: 12,
      width: 12,
      decoration: BoxDecoration(
        gradient: AppGradients.primaryDark,
        shape: BoxShape.circle,
      ),
    );
  }
}

class RunOutCreditBottomSheet extends StatelessWidget {
  const RunOutCreditBottomSheet({super.key});

  @override
  Widget build(BuildContext context) {
    final adCubit = context.read<AdCubit>();

    return Column(
      children: [
        SText(
          context.translate.runOutCreditStatement,
          style: TStyle.TitleLarge,
          color: AppTheme.white,
        ),
        const SBox(
          height: 40,
        ),
        CustomFilledButton(
          label: context.translate.goPremium.toUpperCase(),
          onTap: () {},
          leftIcon: Padding(
            padding: pad(0, 0, 10, 0),
            child: const ImageWidget(
              image: AppIcons.disabledDiamond,
              height: 15,
              width: 15,
            ),
          ),
          rightIcon: const Spacer(
            flex: 6,
          ),
          isEnable: false,
        ),
        const SBox(
          height: 20,
        ),
        SBox(
          height: 90,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SText(
                    context.translate.getUnlimitedChat.capitalize(),
                    style: TStyle.TitleMedium,
                    color: AppTheme.white,
                  ),
                  SText(
                    ' ${context.translate.soon}!',
                    style: TStyle.TitleLarge,
                    color: AppTheme.white,
                  ),
                ],
              ),
              SText(
                context.translate.or.capitalize(),
                style: TStyle.TitleMedium,
                color: AppTheme.white,
              ),
            ],
          ),
        ),
        CustomFilledButton(
          label: context.translate.watchAd.toUpperCase(),
          onTap: () async {
            await adCubit.showRewardedAd(true);
            if (context.mounted) {
              context.popRoute();
            }
          },
          gradient: AppGradients.secondaryContainer,
          leftIcon: Padding(
            padding: pad(0, 0, 10, 0),
            child: const ImageWidget(
              image: AppIcons.burger,
              height: 15,
              width: 15,
            ),
          ),
          rightIcon: const Spacer(
            flex: 6,
          ),
        ),
        const SBox(
          height: 10,
        ),
        SText(
          context.translate.getFreeCredit.capitalize(),
          style: TStyle.TitleMedium,
          color: AppTheme.white,
        ),
        const SBox(
          height: 20,
        ),
      ],
    );
  }
}
