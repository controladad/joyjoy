part of 'select_author_category_bloc.dart';

@injectable
@freezed
class SelectAuthorCategoryState with _$SelectAuthorCategoryState {
  const factory SelectAuthorCategoryState({
    required Option<
            Either<GeneralFailure,
                LazyLoadResponseModel<AuthorOrCategoryUnion>>>
        data,
    required KtList<AuthorOrCategoryUnion> allResults,
    required Option<String> searchQuery,
    required Option<
            Either<GeneralFailure,
                LazyLoadResponseModel<AuthorOrCategoryUnion>>>
        searchData,
    required KtList<AuthorOrCategoryUnion> searchResults,
    required bool isLoading,
    required Option<int> total,
    required RefreshController controller,
    required KtList<String> selectedIds,
    required KtList<AuthorOrCategoryUnion> uncommittedSelects,
    required Option<SelectAuthorCategoryFilterEnum> filter,
    required Option<Either<GeneralFailure, Unit>> saveResult,
  }) = _SelectAuthorCategoryState;

  @factoryMethod
  factory SelectAuthorCategoryState.init() => SelectAuthorCategoryState(
        data: none(),
        saveResult: none(),
        searchData: none(),
        searchQuery: none(),
        controller: RefreshController(initialLoadStatus: LoadStatus.canLoading),
        isLoading: false,
        allResults: emptyList(),
        total: none(),
        searchResults: emptyList(),
        selectedIds: emptyList(),
        uncommittedSelects: emptyList(),
        filter: none(),
      );
}

enum SelectAuthorCategoryFilterEnum {
  Selected,
  NotChosen,
}

@freezed
class AuthorOrCategoryUnion with _$AuthorOrCategoryUnion {
  const factory AuthorOrCategoryUnion.author(AuthorModel author) = AuthorCase;
  const factory AuthorOrCategoryUnion.category(SubCategoryModel category) =
      CategoryCase;
  const AuthorOrCategoryUnion._();

  String get id =>
      map(author: (a) => a.author.id, category: (c) => c.category.id);
  String get title =>
      map(author: (a) => a.author.name, category: (c) => c.category.name);
  List<String> get tags =>
      map(author: (a) => [a.author.name[0]], category: (c) => c.category.tags);

  AccessabilityStatus get status =>
      map(author: (a) => a.author.status, category: (c) => c.category.status);
}
