import 'package:freezed_annotation/freezed_annotation.dart';

part 'language_model.freezed.dart';

@freezed
class LanguageModel with _$LanguageModel {
  const factory LanguageModel({
    @Default('US') String? code,
    @Default('en') String? locale,
    String? language,
    Map<String, String>? dictionary,
  }) = _LanguageModel;
}
