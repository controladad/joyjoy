import 'package:motivation_flutter/common/presentation/buttons.dart';
import 'package:motivation_flutter/common/presentation/common_imports.dart';
import 'package:motivation_flutter/common/presentation/utils/custom_bottom_sheets.dart';

Future<void> changeNameBottomSheet(
  BuildContext context, {
  required Function(String) onSubmit,
}) =>
    showCustomBottomSheet(
      context: context,
      onClose: () {},
      child: _ChangeUserNameBottomSheetWidget(
        onSave: onSubmit,
      ),
    );

class _ChangeUserNameBottomSheetWidget extends StatefulWidget {
  final Function(String name) onSave;
  const _ChangeUserNameBottomSheetWidget({required this.onSave});

  @override
  State<_ChangeUserNameBottomSheetWidget> createState() =>
      _ChangeUserNameBottomSheetWidgetState();
}

class _ChangeUserNameBottomSheetWidgetState
    extends State<_ChangeUserNameBottomSheetWidget> {
  String? name;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: pad(16, 10, 16, 12),
      child: Column(
        children: [
          SText(context.translate.newCollection, style: TStyle.TitleLarge),
          SText(
            context.translate.chooseANameForYourNewCollection,
            style: TStyle.TitleMedium,
            color: AppTheme.on_surface_variant_dark,
          ),
          const SBox(height: 24),
          CustomTextField(
            onChanged: (collectionName) {
              setState(() {
                name = collectionName;
              });
            },
            autofocus: true,
            icon: Padding(
              padding: const EdgeInsets.all(4),
              child: CustomFilledButton(
                label: context.translate.save,
                onTap: () {
                  Navigator.pop(context);
                  widget.onSave(name!);
                },
                isEnable: name?.isNotEmpty ?? false,
                maxWidth: 73,
                width: 73,
              ),
            ),
          ),
          const SBox(height: 20),
        ],
      ),
    );
  }
}
