import 'package:dartz/dartz.dart' hide State;
import 'package:flutter/rendering.dart';
import 'package:kt_dart/kt.dart';
import 'package:motivation_flutter/common/presentation/common_imports.dart';
import 'package:motivation_flutter/common/presentation/utils/failure_handler_widget.dart';
import 'package:motivation_flutter/features/profile/sub_features/select_author_category/select_author_category_bloc/select_author_category_bloc.dart';
import 'package:motivation_flutter/features/profile/sub_features/select_author_category/select_author_category_args.dart';
import 'package:motivation_flutter/features/quotes/domain/models/sub_category_model.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

@RoutePage()
class SelectAuthorCategoryScreen extends StatelessWidget {
  final SelectAuthorCategoryScreenType type;
  const SelectAuthorCategoryScreen({super.key, required this.type});

  @override
  Widget build(BuildContext context) {
    late String title;
    late String subTitle;

    type.map(
      preferred: (a) {
        switch (a.type) {
          case AuthorOrCategoryEnum.Category:
            title = context.translate.preferredCategories;
            subTitle = context.translate.theAppFeedWillDisplayTheseQuotes;
          case AuthorOrCategoryEnum.Author:
            title = context.translate.favoriteCharacters;
            subTitle = context.translate.seeMoreQuotesFromYourFavoriteAuthors;
        }
      },
      reminderTopic: (_) {
        title = context.translate.selectCategory;
        subTitle = '';
      },
    );

    return BlocProvider(
      create: (context) => getIt<SelectAuthorCategoryBloc>()
        ..add(SelectAuthorCategoryEvent.initial(type)),
      child: Builder(
        builder: (context) {
          final bloc = context.read<SelectAuthorCategoryBloc>();
          return CommonBackgroundContainer(
            child: Scaffold(
              backgroundColor: Colors.transparent,
              appBar: AppBar(
                surfaceTintColor: Colors.transparent,
                backgroundColor: Colors.transparent,
                actions: [
                  TextButton(
                    onPressed: () async {
                      bloc.add(
                        const SelectAuthorCategoryEvent.commitChanges(),
                      );
                      context.popRoute();
                    },
                    child: SText(
                      context.translate.save.toUpperCase(),
                      style: TStyle.LabelButton,
                    ),
                  )
                ],
                leadingWidth: double.infinity,
                leading: Row(
                  children: [
                    const SBox(width: 20),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SText(title.toTitleCase(), style: TStyle.HeadlineSmall),
                        if (subTitle.isNotEmpty)
                          SText(subTitle, style: TStyle.TitleSmall),
                      ],
                    ),
                  ],
                ),
              ),
              body: _Body(type),
            ),
          );
        },
      ),
    );
  }
}

class _SearchFAB extends StatefulWidget {
  final ScrollController scrollController;
  final Function() onTap;
  const _SearchFAB({required this.scrollController, required this.onTap});

  @override
  State<_SearchFAB> createState() => _SearchFABState();
}

class _SearchFABState extends State<_SearchFAB> {
  ScrollDirection _dir = ScrollDirection.forward;

  @override
  void initState() {
    super.initState();
    widget.scrollController.addListener(() {
      if (widget.scrollController.position.userScrollDirection != _dir &&
          mounted) {
        setState(() {
          _dir = widget.scrollController.position.userScrollDirection;
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedOpacity(
      opacity: _dir == ScrollDirection.forward ? 1 : 0,
      duration: const Duration(milliseconds: 350),
      child: Padding(
        padding: pad(0, 0, 0, 42),
        child: MaterialButton(
          onPressed: widget.onTap,
          color: AppTheme.surface3_dark.withOpacity(.11),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(16),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: [
              const ImageWidget(
                image: AppIcons.searchExplore,
                height: 24,
                width: 24,
              ),
              const SBox(width: 10),
              SText(
                context.translate.search.capitalize(),
                style: TStyle.LabelLarge,
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class _Body extends StatefulWidget {
  final SelectAuthorCategoryScreenType type;
  const _Body(this.type);

  @override
  State<_Body> createState() => _BodyState();
}

class _BodyState extends State<_Body> {
  final _scrollController = ScrollController();

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final state = context.watch<SelectAuthorCategoryBloc>().state;
    final bloc = context.read<SelectAuthorCategoryBloc>();
    Map<String, KtList<AuthorOrCategoryUnion>> items = {};

    final listOfItems = state.filter.fold(
      () => state.allResults,
      (a) => a == SelectAuthorCategoryFilterEnum.Selected
          ? state.allResults.filter(
              (p0) {
                return state.uncommittedSelects
                    .map((p0) => p0.id)
                    .plus(state.selectedIds)
                    .contains(p0.id);
              },
            )
          : state.allResults
              .filterNot((p0) => state.selectedIds.contains(p0.id)),
    );
    widget.type.map(
      preferred: (p) {
        if (p.type == AuthorOrCategoryEnum.Category) {
          for (final item in listOfItems.asList()) {
            for (final tag in item.tags) {
              items[tag] = items[tag]?.plusElement(item) ?? [item].kt;
            }
          }
        } else {
          for (final item in listOfItems.asList()) {
            items[item.tags.first] =
                items[item.tags.first]?.plusElement(item) ?? [item].kt;
          }
          items = Map.fromEntries(
            items.entries.toList()..sort((a, b) => a.key.compareTo(b.key)),
          );
        }
      },
      reminderTopic: (r) {
        items[''] = listOfItems;
      },
    );

    // final isPremium = hasSubscription(context);

    return Scaffold(
      backgroundColor: Colors.transparent,
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: state.data.fold(
        () => null,
        (a) => a.fold(
          (l) => null,
          (r) => _SearchFAB(
            scrollController: _scrollController,
            onTap: () => context.pushRoute(
              AuthorCategorySearchRoute(
                bloc: bloc,
                type: widget.type,
              ),
            ),
          ),
        ),
      ),
      body: Column(
        children: [
          Padding(
            padding: pad(24, 10, 24, 10),
            child: const Row(
              children: [
                _FilterItem(SelectAuthorCategoryFilterEnum.Selected),
                _FilterItem(SelectAuthorCategoryFilterEnum.NotChosen),
              ],
            ),
          ),
          Expanded(
            child: FailureHandlerWidget(
              data: state.data,
              onRetry: () =>
                  bloc.add(const SelectAuthorCategoryEvent.getFirstPage()),
              child: (r) => _RefreshWidget(
                child: ListView(
                  controller: _scrollController,
                  children: items.entries
                      .map(
                        (e) => Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: pad(22, 12, 12, 4),
                              child: SText(
                                e.key.toUpperCase(),
                                style: TStyle.LabelButton,
                                color: AppTheme.outline_dark,
                              ),
                            ),
                            Column(
                              children: e.value.map(
                                (item) {
                                  return SelectAuthorCategoryItemWidget(
                                    item: item,
                                    isSearch: false,
                                    isPremiumUser: true,
                                    isSelected: state.uncommittedSelects
                                        .map((p0) => p0.id)
                                        .contains(item.id),
                                    onTap: () => bloc.add(
                                      SelectAuthorCategoryEvent.toggle(item),
                                    ),
                                  );
                                },
                              ).asList(),
                            ),
                          ],
                        ),
                      )
                      .toList(),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class _FilterItem extends StatelessWidget {
  final SelectAuthorCategoryFilterEnum filter;
  const _FilterItem(this.filter);

  @override
  Widget build(BuildContext context) {
    final state = context.watch<SelectAuthorCategoryBloc>().state;
    final bloc = context.read<SelectAuthorCategoryBloc>();

    final bool isSelected = state.filter.fold(() => false, (a) => a == filter);
    final label = filter == SelectAuthorCategoryFilterEnum.Selected
        ? context.translate.selected
        : context.translate.notSelected;

    return GestureDetector(
      onTap: () {
        if (isSelected == false) {
          bloc.add(SelectAuthorCategoryEvent.changeFilter(some(filter)));
        } else {
          bloc.add(SelectAuthorCategoryEvent.changeFilter(none()));
        }
      },
      child: Container(
        padding: pad(12, 6, 12, 6),
        margin: pad(0, 0, 10, 0),
        decoration: BoxDecoration(
          border: Border.all(
            color: !isSelected
                ? AppTheme.outline_dark
                : AppTheme.secondary_container_dark,
          ),
          borderRadius: BorderRadius.circular(8),
          gradient: isSelected ? AppGradients.secondaryContainer : null,
        ),
        child: SText(
          label.toTitleCase(),
          style: TStyle.LabelLarge,
        ),
      ),
    );
  }
}

class SelectAuthorCategoryItemWidget extends StatelessWidget {
  final AuthorOrCategoryUnion item;
  final bool isSearch;
  final bool isSelected;
  final bool isPremiumUser;
  final Function() onTap;
  const SelectAuthorCategoryItemWidget({
    required this.isSelected,
    required this.onTap,
    required this.item,
    required this.isPremiumUser,
    required this.isSearch,
  });

  @override
  Widget build(BuildContext context) {
    late String title;
    late String? subTitle;
    late AccessabilityStatus status;

    item.map(
      author: (a) {
        title = a.author.name;
        subTitle = a.author.shortDescription;
        status = a.author.status;
      },
      category: (c) {
        title = c.category.name;
        status = c.category.status;
        subTitle = c.category.description;
      },
    );

    return Padding(
      padding: pad(0, 4, 0, 4),
      child: MaterialButton(
        padding: pad(20, 8, 8, 8),
        onPressed: () {
          HapticFeedback.selectionClick();
          onTap();
        },
        child: SContainer(
          width: double.infinity,
          child: Row(
            children: [
              // if (icon != null)
              //   Padding(
              //     padding: pad(0, 0, 8, 0),
              //     child: ImageWidget(image: icon, height: 32, width: 32),
              //   ),
              const SBox(width: 12),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SText(title.capitalize(), style: TStyle.BodyLarge),
                  if (subTitle != null && isSearch)
                    SText(
                      subTitle!,
                      maxWidth: 250,
                      style: TStyle.LabelSmall,
                      color: AppTheme.outline_dark,
                    ),
                ],
              ),
              const Spacer(),
              ImageWidget(
                image: status == AccessabilityStatus.Premium && !isPremiumUser
                    ? AppIcons.whiteLock
                    : isSelected
                        ? AppIcons.pinkCheck
                        : AppIcons.unselectedPinkCheck,
                height: 42,
                width: 42,
              ),
              const SBox(width: 12),
            ],
          ),
        ),
      ),
    );
  }
}

class _RefreshWidget extends StatelessWidget {
  final Widget child;
  const _RefreshWidget({required this.child});

  @override
  Widget build(BuildContext context) {
    final bloc = context.read<SelectAuthorCategoryBloc>();
    final state = context.watch<SelectAuthorCategoryBloc>().state;
    return SmartRefresher(
      controller: state.controller,
      onLoading: () {
        bloc.add(const SelectAuthorCategoryEvent.lazyLoad());
      },
      onRefresh: () {
        bloc.add(const SelectAuthorCategoryEvent.refresh());
      },
      enablePullUp: state.data.fold(() => false, (a) => true),
      enablePullDown: state.data.fold(() => false, (a) => true),
      footer: CustomFooter(
        builder: (BuildContext context, _) {
          return Container();
        },
      ),
      child: child,
    );
  }
}
