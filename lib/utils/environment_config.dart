class EnvironmentConfig {
  const EnvironmentConfig._();

  static const bool runTest = bool.fromEnvironment('TEST');
}
