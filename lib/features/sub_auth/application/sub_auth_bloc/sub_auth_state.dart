part of 'sub_auth_bloc.dart';

enum LoginMethod {
  Google,
  Apple,
}

@injectable
@freezed
class SubAuthState with _$SubAuthState {
  const factory SubAuthState({
    required Option<Either<GeneralFailure, bool>> successOrFailure,
    required Option<UserSubscription> subscription,
    required Option<bool> isAuthenticated,
    required Option<bool> hasCompleteData,
    required bool isLoading,
    required Option<LoginMethod> loginLoading,
  }) = _SubAuthState;
  @factoryMethod
  factory SubAuthState.init() => SubAuthState(
        isLoading: false,
        isAuthenticated: none(),
        successOrFailure: none(),
        subscription: some(UserSubscription.empty()),
        hasCompleteData: none(),
        loginLoading: none(),
      );
}
