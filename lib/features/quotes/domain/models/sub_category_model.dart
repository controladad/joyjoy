import 'package:freezed_annotation/freezed_annotation.dart';
part 'sub_category_model.freezed.dart';

@freezed
class SubCategoryModel with _$SubCategoryModel {
  const factory SubCategoryModel({
    required String id,
    required String name,
    required String icon,
    required String description,
    required AccessabilityStatus status,
    required List<String> tags,
  }) = _SubCategoryModel;
}

enum AccessabilityStatus {
  Free,
  Premium,
}
