//
//  Generated file. Do not edit.
//

// clang-format off

#import "GeneratedPluginRegistrant.h"

#if __has_include(<ios_widget/IosWidgetPlugin.h>)
#import <ios_widget/IosWidgetPlugin.h>
#else
@import ios_widget;
#endif

@implementation GeneratedPluginRegistrant

+ (void)registerWithRegistry:(NSObject<FlutterPluginRegistry>*)registry {
  [IosWidgetPlugin registerWithRegistrar:[registry registrarForPlugin:@"IosWidgetPlugin"]];
}

@end
