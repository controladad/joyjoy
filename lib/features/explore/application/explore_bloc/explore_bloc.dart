import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:kt_dart/kt.dart';
import 'package:motivation_flutter/core/failures.dart';

import 'package:motivation_flutter/features/explore/domain/explore_repository.dart';
import 'package:motivation_flutter/features/explore/domain/models/explore_model.dart';
import 'package:motivation_flutter/features/quotes/domain/models/category_model.dart';

part 'explore_bloc.freezed.dart';
part 'explore_event.dart';
part 'explore_state.dart';

@injectable
class ExploreBloc extends Bloc<ExploreEvent, ExploreState> {
  final IExploreRepository repository;
  ExploreBloc(this.repository, ExploreState init) : super(init) {
    on<ExploreEvent>(_bloc);
  }

  Future<void> _bloc(ExploreEvent event, Emitter emit) {
    return event.map(
      getCategories: (e) async {
        final categoriesData = await repository.getExplore;
        final KtList<CategoryModel> categories =
            categoriesData.fold((l) => emptyList(), (r) => r.categories);

        emit(
          state.copyWith(
            categoriesData: some(categoriesData),
            categories: categories,
          ),
        );
      },
    );
  }
}
