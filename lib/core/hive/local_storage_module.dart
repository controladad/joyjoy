import 'package:motivation_flutter/features/language/domain/models/language_entity.dart';
import 'package:hive/hive.dart';
import 'package:injectable/injectable.dart';

import 'package:motivation_flutter/constants/db_constants.dart';
import 'package:motivation_flutter/features/profile/domain/models/account_info_entity.dart';
import 'package:motivation_flutter/features/profile/domain/models/collection_entity.dart';
import 'package:motivation_flutter/features/profile/domain/models/reminder_entity.dart';
import 'package:motivation_flutter/features/profile/domain/models/user_subscription_entity.dart';
import 'package:motivation_flutter/features/quotes/domain/models/author_entity.dart';
import 'package:motivation_flutter/features/quotes/domain/models/category_entity.dart';
import 'package:motivation_flutter/features/quotes/domain/models/quote_entity.dart';
import 'package:motivation_flutter/features/quotes/domain/models/sub_category_entity.dart';
import 'package:motivation_flutter/features/theme/domain/models/dynamic_background_entity.dart';
import 'package:motivation_flutter/features/theme/domain/models/theme_entity.dart';
import 'package:motivation_flutter/features/user/domain/models/user_entity.dart';
import 'package:motivation_flutter/features/user/domain/models/user_watch_ads_entity.dart';
import 'package:motivation_flutter/shared/domain/shared_cache_props_entity.dart';

@module
abstract class LocalStorageModule {
  @lazySingleton
  HiveInterface initHive(@Named(DBConstants.MAIN_DB_PATH) String path) {
    Hive.init(path);
    _registerHiveAdapters();
    return Hive;
  }

  void _registerHiveAdapters() {
    Hive.registerAdapter(UserEntityAdapter());
    Hive.registerAdapter(LanguageEntityAdapter());
    Hive.registerAdapter(QuoteEntityAdapter());
    Hive.registerAdapter(SubCategoryEntityAdapter());
    Hive.registerAdapter(AuthorEntityAdapter());
    Hive.registerAdapter(SharedCachePropsEntityAdapter());
    Hive.registerAdapter(CategoryEntityAdapter());
    Hive.registerAdapter(UserSubscriptionEntityAdapter());
    Hive.registerAdapter(PremiumEntityAdapter());
    Hive.registerAdapter(FreeEntityAdapter());
    Hive.registerAdapter(CollectionEntityAdapter());
    Hive.registerAdapter(ThemeEntityAdapter());
    Hive.registerAdapter(DynamicBackgroundEntityAdapter());
    Hive.registerAdapter(ReminderEntityAdapter());
    Hive.registerAdapter(AccountInfoEntityAdapter());
    Hive.registerAdapter(UserWatchAdsEntityAdapter());
    Hive.registerAdapter(TextShadowEntityAdapter());
  }
}
