class Images {
  Images._();

  static const String successfulRegistration =
      'assets/images/img_successful_registration.svg';
  static const String loginBanner = 'assets/images/login_banner.svg';
  static const String google = 'assets/icons/google.svg';
  static const String done = 'assets/icons/done.svg';
  static const String chatHeader = 'assets/icons/chat_header.png';
  static const String apple = 'assets/icons/apple.svg';
  static const String imagePlaceholderLarge =
      'assets/images/image_placeholder_large.jpg';
  static const String imgSignIn = 'assets/icons/img_sign_in.svg';
  static const String arrow = 'assets/icons/arrow.svg';
  static const String generalFailure = 'assets/images/img_general_failure.svg';
  static const String close = 'assets/icons/close.svg';
  static const String splash = 'assets/images/splash.png';
  static const String reviewBoardLeft =
      'assets/images/review_board_left_part.png';
  static const String reviewBoardRight =
      'assets/images/review_board_right_part.png';
  static const String onboarding = 'assets/images/onboarding.png';
  static const String howDoYouFeel = 'assets/images/how_do_you_feel.png';
  static const String appLogo = 'assets/images/joyjoy_logo.png';
  static const String forceUpdate = 'assets/images/force_update.png';
  static const String forceUpdateDialog =
      'assets/images/force_update_dialog.png';
  static const String questionMark = 'assets/images/question_mark.png';
  static const String underline1 = 'assets/images/underline1.png';
  static const String underline2 = 'assets/images/underline2.png';
  static const String circle = 'assets/images/circle.png';
  static const String headphone = 'assets/images/headphone.png';
  static const String paywallImage = 'assets/images/pay_wall_top.png';
  static const String bitCheck = 'assets/images/big_check.png';
  static const String pointer = 'assets/images/pointer.png';
  static const String review = 'assets/images/review.png';
  static const String mark = 'assets/images/mark.png';
  static const String gallery = 'assets/images/gallery.png';
  static const String key = 'assets/images/key.png';
  static const String love = 'assets/images/love.png';
  static const String insta = 'assets/images/insta.png';
  static const String monitor = 'assets/images/monitor.png';
  static const String clock = 'assets/images/clock.png';
  static const String categoryRing = 'assets/images/category_ring.png';
  static const String earth = 'assets/icons/earth.png';
  static const String profileEmptyState =
      'assets/images/profile_empty_state_image.png';
  static const String profileBannerDiamond =
      'assets/images/profile_banner_diamond.png';
  static const String profileBannerFrame =
      'assets/images/profile_banner_frame.png';
  static const String connectionErrorImage =
      'assets/images/connection_error_image.png';
  static const String authenticationErrorImage =
      'assets/images/authentication_error_image.png';
  static const String underMaintenanceErrorImage =
      'assets/images/under_maintenance_error_image.png';
  static const String chatAppBarLeading =
      'assets/images/chat_appbar_leading.png';
  static const String login = 'assets/images/login.png';
}
