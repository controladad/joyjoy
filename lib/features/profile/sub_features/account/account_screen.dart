import 'package:motivation_flutter/common/presentation/buttons.dart';
import 'package:motivation_flutter/common/presentation/common_imports.dart';
import 'package:motivation_flutter/common/presentation/utils/failure_handler_widget.dart';
import 'package:motivation_flutter/features/profile/application/profile_bloc/profile_cubit.dart';
import 'package:motivation_flutter/features/profile/domain/models/account_info_model.dart';
import 'package:motivation_flutter/features/profile/domain/models/profile_models.dart';
import 'package:motivation_flutter/features/profile/sub_features/account/change_name_bottom_sheet.dart';
import 'package:motivation_flutter/features/sub_auth/application/sub_auth_bloc/sub_auth_bloc.dart';

@RoutePage()
class AccountScreen extends StatelessWidget {
  const AccountScreen({super.key});

  @override
  Widget build(BuildContext context) {
    final state = context.watch<ProfileCubit>().state;
    final bloc = context.read<ProfileCubit>();
    return CommonScaffoldWithAppbar(
      title: context.translate.account.capitalize(),
      body: FailureHandlerWidget<ProfileModel>(
        data: state.offlineData,
        onRetry: () => bloc.init(),
        child: (r) => _Body(r.accountInfo),
      ),
    );
  }
}

class _Body extends StatelessWidget {
  final AccountInfoModel account;
  const _Body(this.account);

  @override
  Widget build(BuildContext context) {
    final authBloc = context.read<SubAuthCubit>();
    final authState = context.watch<SubAuthCubit>().state;

    final bloc = context.read<ProfileCubit>();
    return account.map(
      guest: (_) => Container(),
      loggedIn: (info) => Column(
        children: [
          Expanded(
            child: ListView(
              padding: pad(16, 0, 16, 0),
              children: [
                _ItemWidget(
                  label: '${context.translate.accountStatus}:',
                  value: SText(
                    info.accountStatus.capitalize(),
                    style: TStyle.TitleMedium,
                  ),
                ),
                _ItemWidget(
                  label:
                      '${context.translate.name}: ${info.name.fold(() => '', (a) => a)}',
                  value: GestureDetector(
                    onTap: () async => changeNameBottomSheet(
                      context,
                      onSubmit: (name) => bloc.changeUserName(name),
                    ),
                    child: SText(
                      context.translate.edit.toUpperCase(),
                      style: TStyle.LabelButton,
                      color: AppTheme.primary_dark,
                    ),
                  ),
                ),
                _ItemWidget(
                  label: '${context.translate.signupDate}:',
                  value: SText(
                    info.signupDate.ddMmmmYyyy(context),
                    style: TStyle.TitleMedium,
                  ),
                ),
                _ItemWidget(
                  label: '${context.translate.premiumPlan}:',
                  value: SText(
                    info.premiumPlan,
                    style: TStyle.TitleMedium,
                  ),
                ),
                const SBox(height: 20),
                CustomOutlinedButton(
                  label: context.translate.logout,
                  isLoading: authState.isLoading,
                  onTap: () => authBloc.logout(),
                  leftIcon: const ImageWidget(
                    image: AppIcons.logout,
                    height: 18,
                    width: 18,
                  ),
                )
              ],
            ),
          ),
          Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              TextButton(
                onPressed: () => authBloc.deleteAccount(),
                child: SText(
                  context.translate.deleteAccount.toUpperCase(),
                  style: TStyle.LabelButton,
                  color: AppTheme.error,
                ),
              ),
              SText(
                context.translate.permanentlyDeleteYourAccount,
                style: TStyle.LabelSmall,
                color: AppTheme.white,
                textAlign: TextAlign.center,
                maxWidth: 310,
              ),
              const SBox(height: 32),
            ],
          ),
        ],
      ),
    );
  }
}

class _ItemWidget extends StatelessWidget {
  final String label;
  final Widget value;
  const _ItemWidget({required this.label, required this.value});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: pad(12, 16, 12, 16),
      decoration: const BoxDecoration(
        border: Border(
          bottom: BorderSide(width: 0.5, color: AppTheme.outline_variant_dark),
        ),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          SText(label.toTitleCase(), style: TStyle.TitleMedium),
          value,
        ],
      ),
    );
  }
}
