import 'package:auto_route/auto_route.dart';

import 'package:motivation_flutter/utils/router/app_router.gr.dart';

@AutoRouterConfig()
class AppRouter extends $AppRouter {
  @override
  RouteType get defaultRouteType => const RouteType.material();

  @override
  List<AutoRoute> get routes => [
        AutoRoute(page: SplashRoute.page, initial: true),
        AutoRoute(page: OnboardingRoute.page),
        AutoRoute(page: LoginRoute.page),
        AutoRoute(page: ChatRoute.page),
        AutoRoute(page: PaywallRoute.page),
        AutoRoute(
          page: NavBuilderRoute.page,
          children: [
            AutoRoute(
              page: FeedsRoute.page,
            ),
            AutoRoute(
              page: ExploreRoute.page,
            ),
            AutoRoute(
              page: ThemeRoute.page,
            ),
            AutoRoute(
              page: ProfileRoute.page,
            ),
            RedirectRoute(path: '*', redirectTo: '/'),
          ],
        ),
        AutoRoute(page: InstanceSearchRoute.page),
        AutoRoute(page: FullSearchRoute.page),
        AutoRoute(page: ListOfQuoteRoute.page),
        AutoRoute(page: MyCollectionsRoute.page),
        AutoRoute(page: RemindersRoute.page),
        AutoRoute(page: SelectAuthorCategoryRoute.page),
        AutoRoute(page: AuthorCategorySearchRoute.page),
        AutoRoute(page: ManageSubscriptionRoute.page),
        AutoRoute(page: AccountRoute.page),
        AutoRoute(page: CommonProfileAboutRoute.page),
        AutoRoute(page: QuotesRoute.page),
        AutoRoute(page: ReportRoute.page),
        AutoRoute(page: ForceUpdateRoute.page),
        RedirectRoute(path: '*', redirectTo: '/'),
      ];
}
