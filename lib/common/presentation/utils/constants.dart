class Constants {
  const Constants._();
  static const int maxLimitForEnterMobileNumber = 20;
  static const double screenToBottomSheetFraction = .82;
  static const int maxQuantityForProductPageAddToCart = 3000;
  static const int mobileVerificationTimerSeconds = 10;
  static const int characterLimitForReview = 700;
  static const int paginationLimit = 20;
  static const String uri_prefix = 'https://ttqpos.page.link';
  static const int maxFieldCharLength = 50;
  static const String androidAppId = 'com.controladad.motivation';
  static const String appStoreId = 'id6446221316';
  static const String storeLink =
      'https://apps.apple.com/us/app/joyjoy-motivation-ai-friend/id6446221316';

  static const int chunkSize = 100;
  static const int creditPerMessage = 1;
  static const int creditPerAd = 10;
  static const int freeSeenQuoteNumber = 10;
  static const int minimumCountOfNewQuotesInView = 10;
}
