import 'package:motivation_flutter/common/presentation/common_imports.dart';
import 'package:motivation_flutter/features/theme/domain/models/dynamic_background_union.dart';

class DynamicBackgroundWidget extends StatelessWidget {
  final Widget? child;
  final double height;
  final double width;
  final DynamicBackgroundUnion background;
  final double radius;
  const DynamicBackgroundWidget({
    super.key,
    this.child,
    required this.height,
    required this.width,
    required this.background,
    this.radius = 0,
  });

  @override
  Widget build(BuildContext context) {
    return background.map(
      color: (bg) => SContainer(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(radius),
          color: bg.color,
        ),
        height: height,
        width: width,
        child: child,
      ),
      gradient: (bg) => AnimatedContainer(
        duration: const Duration(milliseconds: 300),
        height: height,
        width: width,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(radius),
          gradient: createGradient(colors: bg.colors),
        ),
        child: child,
      ),
      image: (bg) => SBox(
        height: height,
        width: width,
        child: Stack(
          children: [
            Image.memory(
              bg.image,
              height: height,
              width: width,
            ),
            if (child != null) child!,
          ],
        ),
      ),
    );
  }
}
