import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:motivation_flutter/common/presentation/common_presentation.dart';
import 'package:motivation_flutter/core/failures.dart';

Future<Either<GeneralFailure, T>> tryCacheBlock<T>(
  
  Future<T> Function() result,
) async {
  try {
    return right(await result());
  } catch (exception) {
    late GeneralFailure failure;
    if (exception is DioException) {
      final error = exception.error;
      if (error is ApiException) {
        failure = error.failure;
      } else {
        failure = const GeneralFailure.unexpected();
      }
    } else {
      failure = GeneralFailure.localException(exception);
      rethrow;
    }

    _showFailureSnackbar(failure);
    return left(failure);
  }
}

void _showFailureSnackbar(GeneralFailure failure) {
  final SnackBar snackBar = SnackBar(
    backgroundColor: AppTheme.inverse_on_surface_dark,
    content: SText(
      failure.map(
        noNetwork: (e) => 'Connection Error',
        unexpected: (e) => e.message ?? 'Unexpected',
        authentication: (e) => 'Authentication Error',
        underMaintenance: (e) => 'UnderMaintenance',
        forceUpdate: (e) => 'ForceUpdate',
        localException: (e) => e.exception.toString(),
      ),
      style: TStyle.BodyLarge,
      color: AppTheme.white,
    ),
  );

  snackbarKey.currentState?.removeCurrentSnackBar();
  snackbarKey.currentState?.showSnackBar(snackBar);
}
