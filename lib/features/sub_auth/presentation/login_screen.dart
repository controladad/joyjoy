import 'dart:io';

import 'package:motivation_flutter/common/presentation/common_presentation.dart';
import 'package:motivation_flutter/common/presentation/loaders.dart';
import 'package:motivation_flutter/features/sub_auth/application/sub_auth_bloc/sub_auth_bloc.dart';

@RoutePage()
class LoginScreen extends StatelessWidget {
  final PageRouteInfo? nextRoute;
  const LoginScreen({super.key, this.nextRoute});

  @override
  Widget build(BuildContext context) {
    return BlocListener<SubAuthCubit, SubAuthState>(
      listenWhen: (previous, current) =>
          previous.successOrFailure != current.successOrFailure,
      listener: (context, state) {
        state.successOrFailure.fold(
          () => null,
          (a) => a.fold(
            (l) => showFailureSnackBar(context, l),
            (r) => r == true ? Navigator.pop(context) : false,
          ),
        );
      },
      child: Builder(
        builder: (context) {
          final bloc = context.read<SubAuthCubit>();
          final state = context.watch<SubAuthCubit>().state;
          return Stack(
            children: [
              CommonBackgroundContainer(
                child: SafeArea(
                  child: Scaffold(
                    appBar: AppBar(
                      backgroundColor: Colors.transparent,
                      automaticallyImplyLeading: false,
                      actions: [
                        IconButton(
                          onPressed: () => Navigator.pop(context),
                          icon: const ImageWidget(
                            image: AppIcons.close,
                            height: 32,
                            width: 32,
                            color: AppTheme.on_background_dark,
                          ),
                        )
                      ],
                    ),
                    backgroundColor: Colors.transparent,
                    body: Column(
                      children: [
                        Expanded(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              ImageWidget(
                                image: Images.login,
                                height: null,
                                width: getDeviceSize(context).width,
                                maxWidth: 400,
                              ),
                              const SBox(height: 16),
                              SText(
                                context.translate.unlockExclusiveAccess,
                                style: TStyle.TitleLarge,
                                maxWidth: 346,
                                textAlign: TextAlign.center,
                              ),
                            ],
                          ),
                        ),
                        Column(
                          children: [
                            if (Platform.isIOS)
                              _Button(
                                onTap: () => bloc.signInWithApple(),
                                color: Colors.white,
                                child: Row(
                                  children: [
                                    const ImageWidget(
                                      image: AppIcons.apple,
                                      height: 24,
                                      width: 24,
                                    ),
                                    const SBox(width: 16),
                                    SText(
                                      context.translate.continueWithApple,
                                      style: TStyle.TitleLarge,
                                      color: Colors.black,
                                      fontFamily: 'sf_pro',
                                    ),
                                  ],
                                ),
                              ),
                            _Button(
                              onTap: () => bloc.signInWithGoogle(),
                              color: const Color(0xff4285F4),
                              child: Row(
                                children: [
                                  const ImageWidget(
                                    image: AppIcons.google,
                                    height: 24,
                                    width: 24,
                                  ),
                                  const SBox(width: 16),
                                  SText(
                                    context.translate.continueWithGoogle,
                                    style: TStyle.TitleLarge,
                                    color: Colors.white,
                                    fontFamily: 'roboto',
                                  ),
                                ],
                              ),
                            ),
                            const SBox(height: 16),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              if (state.isLoading)
                Container(
                  width: double.infinity,
                  height: double.infinity,
                  color: AppTheme.color1.withOpacity(.4),
                  child: const Center(child: CommonLoader()),
                )
            ],
          );
        },
      ),
    );
  }
}

class _Button extends StatelessWidget {
  final Function() onTap;
  final Widget child;
  final Color color;
  const _Button({
    required this.onTap,
    required this.child,
    required this.color,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: pad(16, 8, 16, 8),
      child: MaterialButton(
        color: color,
        height: 52,
        onPressed: onTap,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(100),
        ),
        child: child,
      ),
    );
  }
}
