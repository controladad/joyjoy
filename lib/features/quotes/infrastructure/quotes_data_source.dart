import 'package:injectable/injectable.dart';

import 'package:motivation_flutter/features/quotes/infrastructure/quotes_api_services.dart';

abstract class IQuotesDataSource {
  Future<void> reportQuote({
    required String quoteId,
    required String reportText,
  });
}

@Injectable(as: IQuotesDataSource)
class QuotesDataSourceImplementation implements IQuotesDataSource {
  final IQuotesApiServices apiService;

  QuotesDataSourceImplementation(this.apiService);

  @override
  Future<void> reportQuote({
    required String quoteId,
    required String reportText,
  }) async {
    await apiService.reportQuote(
      quoteId: quoteId,
      reportText: reportText,
    );
  }
}
