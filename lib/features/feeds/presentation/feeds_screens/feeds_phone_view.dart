import 'package:in_app_review/in_app_review.dart';
import 'package:kt_dart/kt.dart';
import 'package:motivation_flutter/common/application/ad_cubit.dart';
import 'package:motivation_flutter/common/presentation/common_imports.dart';
import 'package:motivation_flutter/common/presentation/utils/failure_handler_widget.dart';
import 'package:motivation_flutter/features/profile/application/profile_bloc/profile_cubit.dart';
import 'package:motivation_flutter/features/profile/presentation/profile_bottom_sheets/login_bottom_sheets.dart';
import 'package:motivation_flutter/features/profile/presentation/profile_bottom_sheets/profile_bottom_sheets.dart';
import 'package:motivation_flutter/features/profile/sub_features/select_author_category/select_author_category_args.dart';
import 'package:motivation_flutter/features/quotes/application/quotes_bloc/quotes_bloc.dart';
import 'package:motivation_flutter/features/quotes/application/watch_ads_data/watch_ads_data_bloc.dart';
import 'package:motivation_flutter/features/quotes/domain/models/quotes_model.dart';
import 'package:motivation_flutter/features/quotes/presentation/quotes_widget.dart';
import 'package:motivation_flutter/features/quotes/domain/models/quotes_screen_type.dart';

class FeedsPhoneView extends StatelessWidget {
  const FeedsPhoneView({super.key});

  @override
  Widget build(BuildContext context) {
    final quotesBloc = context.read<QuotesBloc>();
    final state = context.watch<QuotesBloc>().state;
    final userName = context.watch<ProfileCubit>().state.offlineData.fold(
          () => '',
          (a) => a.fold(
            (l) => '',
            (r) => r.accountInfo.name.fold(() => '', (a) => a),
          ),
        );

    final WatchAdsDataBloc watchAdsDataBloc = context.read<WatchAdsDataBloc>();
    Future<void> whatShouldWeDoAfterSeenQuote(
      WhatShouldDoAfterSeenSomeQuoteType stuff,
    ) async {
      switch (stuff) {
        case WhatShouldDoAfterSeenSomeQuoteType.inAppReview:
          final InAppReview inAppReview = InAppReview.instance;

          if (await inAppReview.isAvailable() && await hasInternetConnection) {
            watchAdsDataBloc.add(const WatchAdsDataEvent.inAppReviewSeen());
            inAppReview.requestReview();
          }
        case WhatShouldDoAfterSeenSomeQuoteType.ads:
          final cubit = context.read<AdCubit>();
          cubit.showRewardedAd(false);
        case WhatShouldDoAfterSeenSomeQuoteType.nothing:
          break;
      }
    }

    void showFeedsFilterBottomSheet() {
      HapticFeedback.lightImpact();
      context.pushRoute(
        SelectAuthorCategoryRoute(
          type: const SelectAuthorCategoryScreenType.preferred(
            AuthorOrCategoryEnum.Category,
          ),
        ),
      );
    }

    final ProfileState profileState = context.watch<ProfileCubit>().state;
    final isAuth = profileState.offlineData.fold(
      () => false,
      (a) => a.fold(
        (l) => false,
        (r) => r.accountInfo.map(loggedIn: (_) => true, guest: (_) => false),
      ),
    );
    void goToChatScreen() {
      if (isAuth) {
        context.pushRoute(const ChatRoute());
      } else {
        showLoginBottomSheet(context, (res) {
          if (res) {
            context.pushRoute(const ChatRoute());
          }
        });
      }
    }

    return BlocListener<WatchAdsDataBloc, WatchAdsDataState>(
      listener: (context, state) {
        state.whatShouldDoAfterSeenQuote
            .fold(() => null, (a) => whatShouldWeDoAfterSeenQuote(a));
      },
      child: CommonBackgroundContainer(
        child: Scaffold(
          floatingActionButton: FloatingActionButton(
            onPressed: goToChatScreen,
            child: Container(
              height: double.infinity,
              width: double.infinity,
              decoration: BoxDecoration(
                gradient: AppGradients.surface3Dark,
                borderRadius: BorderRadius.circular(16),
              ),
              foregroundDecoration: BoxDecoration(
                borderRadius: BorderRadius.circular(16),
              ),
              child: const Center(
                child: Center(
                  child: ImageWidget(
                    image: AppIcons.chat,
                    height: 24,
                    width: 24,
                  ),
                ),
              ),
            ),
          ),
          backgroundColor: Colors.transparent,
          appBar: AppBar(
            backgroundColor: Colors.transparent,
            leadingWidth: double.infinity,
            leading: Padding(
              padding: pad(24, 0, 10, 0),
              child: SBox(
                height: 64,
                child: Row(
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SText(
                          '${context.translate.hey.capitalize()} $userName,',
                          style: TStyle.TitleMedium,
                        ),
                        SText(
                          _getWelcomeText(context).toTitleCase(),
                          style: TStyle.HeadlineSmall,
                        ),
                      ],
                    ),
                    const Spacer(),
                    IconButton(
                      onPressed: () => showFeedsFilterBottomSheet(),
                      icon: const ImageWidget(
                        image: AppIcons.filter,
                        height: 48,
                        width: 48,
                      ),
                    ),
                    IconButton(
                      onPressed: () => showProfileBottomSheets(context),
                      icon: const ImageWidget(
                        image: AppIcons.user,
                        height: 32,
                        width: 32,
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
          body: SafeArea(
            child: FailureHandlerWidget<KtList<QuoteModel>>(
              data: state.data,
              onRetry: () => quotesBloc
                  .add(const QuotesEvent.fetchData(QuotesScreenType.feeds())),
              child: (r) {
                if (r.isEmpty()) {
                  return const Center(
                    child: SText(
                      'No Quotes Here',
                      style: TStyle.BodyLarge,
                    ),
                  );
                }
                return _Quotes(quotesBloc: quotesBloc);
              },
            ),
          ),
        ),
      ),
    );
  }
}

class _Quotes extends StatelessWidget {
  const _Quotes({
    required this.quotesBloc,
  });

  final QuotesBloc quotesBloc;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        const Spacer(),
        QuotesWidget(
          type: const QuotesScreenType.feeds(),
          quotesBloc: quotesBloc,
        ),
        const Spacer(),
      ],
    );
  }
}

String _getWelcomeText(BuildContext context) {
  final hour = DateTime.now().hour;
  if (hour >= 5 && hour < 12) {
    return context.translate.goodMorning;
  } else if (hour >= 12 && hour < 17) {
    return context.translate.goodAfternoon;
  } else {
    return context.translate.goodEvening;
  }
}
