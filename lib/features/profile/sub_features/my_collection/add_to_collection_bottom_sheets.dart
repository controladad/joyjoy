import 'package:kt_dart/kt.dart';
import 'package:motivation_flutter/common/presentation/buttons.dart';
import 'package:motivation_flutter/common/presentation/common_imports.dart';
import 'package:motivation_flutter/common/presentation/utils/custom_bottom_sheets.dart';
import 'package:motivation_flutter/features/profile/sub_features/my_collection/my_collection_bloc/my_collection_bloc.dart';
import 'package:motivation_flutter/features/profile/sub_features/my_collection/my_collection_screen.dart';
import 'package:motivation_flutter/features/quotes/domain/models/quotes_model.dart';

Future<void> showAddToCollectionBottomSheet({
  required BuildContext context,
  required QuoteModel quote,
}) async {
  return showCustomBottomSheet(
    context: context,
    onClose: () {},
    child: _Widget(quote),
  );
}

class _Widget extends StatefulWidget {
  final QuoteModel quote;
  const _Widget(this.quote);

  @override
  State<_Widget> createState() => _WidgetState();
}

class _WidgetState extends State<_Widget> {
  late MyCollectionBloc bloc;
  @override
  void didChangeDependencies() {
    bloc = context.read<MyCollectionBloc>();
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    bloc.add(const MyCollectionEvent.cancelChanges());

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final state = context.watch<MyCollectionBloc>().state;
    final bloc = context.read<MyCollectionBloc>();

    void addNewCollection() {
      showAddNewCollectionBottomSheets(
        context,
        bloc,
        reserveCollection: true,
      );
    }

    if (state.collections.isEmpty()) {
      return Row(
        children: [
          Column(
            children: [
              Padding(
                padding: pad(24, 8, 24, 8),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SText(
                      context.translate.addToCollection.capitalize(),
                      style: TStyle.HeadlineSmall,
                    ),
                    const SBox(height: 10),
                    SText(
                      context.translate.youHaveNotCreatedACollectionYet,
                      maxWidth: 300,
                      style: TStyle.BodyLarge,
                    ),
                    const SBox(height: 20),
                  ],
                ),
              ),
              Container(
                width: SizeConfig.screenWidth,
                decoration: BoxDecoration(
                  color: AppTheme.surface1_dark.withOpacity(.05),
                  borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(25),
                    topRight: Radius.circular(25),
                  ),
                ),
                child: CustomFilledButton(
                  label: context.translate.addNewCollection,
                  padding: pad(16, 12, 16, 32),
                  onTap: () => addNewCollection(),
                ),
              ),
            ],
          ),
        ],
      );
    } else {
      return Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(12),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SText(
                      context.translate.addToCollection.capitalize(),
                      style: TStyle.HeadlineSmall,
                    ),
                    TextButton(
                      onPressed: () => addNewCollection(),
                      child: SText(
                        context.translate.newWord.toUpperCase(),
                        style: TStyle.LabelButton,
                        color: AppTheme.primary_dark,
                      ),
                    ),
                  ],
                ),
                const SBox(height: 20),
                ...state.collections.map(
                  (collection) {
                    final bool isSelected =
                        collection.quotesIds.contains(widget.quote.id) !=
                            state.changeList.contains(collection.id);
                    print(isSelected);
                    return GestureDetector(
                      onTap: () {
                        bloc.add(
                          MyCollectionEvent.toggleQuoteInChangeList(
                            quote: widget.quote,
                            collectionId: collection.id,
                          ),
                        );
                      },
                      child: Container(
                        height: 75,
                        padding: pad(12, 16, 12, 16),
                        margin: pad(0, 0, 0, 8),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(15),
                          color: isSelected
                              ? AppTheme.inverse_primary_dart.withOpacity(.16)
                              : null,
                          border: Border.all(
                            width: .5,
                            color: AppTheme.outline_variant_dark,
                          ),
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SText(
                                  collection.name.capitalize(),
                                  style: TStyle.TitleLarge,
                                ),
                                SText(
                                  '${collection.quotesIds.size} ${context.translate.quotes.capitalize()}',
                                  style: TStyle.LabelSmall,
                                ),
                              ],
                            ),
                            ImageWidget(
                              image: isSelected
                                  ? AppIcons.pinkCheck
                                  : AppIcons.unselectedPinkCheck,
                              height: 42,
                              width: 42,
                            ),
                          ],
                        ),
                      ),
                    );
                  },
                ).asList()
              ],
            ),
          ),
          Container(
            width: SizeConfig.screenWidth,
            decoration: BoxDecoration(
              color: AppTheme.surface1_dark.withOpacity(.05),
              borderRadius: const BorderRadius.only(
                topLeft: Radius.circular(25),
                topRight: Radius.circular(25),
              ),
            ),
            child: CustomFilledButton(
              isEnable: state.changeList.isNotEmpty(),
              label: context.translate.save,
              padding: pad(16, 12, 16, 32),
              onTap: () {
                bloc.add(MyCollectionEvent.saveChanges(widget.quote.id));

                toast(
                  context,
                  context.translate.theQuoteWasAddedToDesiredCollection,
                  padding: 260,
                );
                return context.popRoute();
              },
            ),
          ),
        ],
      );
    }
  }
}
