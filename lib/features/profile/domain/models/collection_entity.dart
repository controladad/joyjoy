import 'package:hive/hive.dart';
import 'package:kt_dart/kt.dart';
import 'package:motivation_flutter/features/profile/domain/models/collection_model.dart';

part 'collection_entity.g.dart';

@HiveType(typeId: 8)
class CollectionEntity extends HiveObject {
  @HiveField(0)
  String id;
  @HiveField(1)
  String name;
  @HiveField(2)
  List<String> quotesIds;

  CollectionEntity({
    required this.id,
    required this.name,
    required this.quotesIds,
  });

  CollectionEntity copyWith({
    String? id,
    String? name,
    List<String>? quotesIds,
  }) {
    return CollectionEntity(
      id: id ?? this.id,
      name: name ?? this.name,
      quotesIds: quotesIds ?? this.quotesIds,
    );
  }

  factory CollectionEntity.fromDomain(CollectionModel model) {
    return CollectionEntity(
      id: model.id,
      name: model.name,
      quotesIds: model.quotesIds.asList(),
    );
  }

  CollectionModel toDomain() {
    return CollectionModel(
      id: id,
      name: name,
      quotesIds: quotesIds.kt,
    );
  }
}
