import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';

abstract class ISubAuthApiService {
  Future<Response> getData();
  Future<Response> logout({required String uuid, required String fcm});
  Future<Response> deleteAccount();
  Future<Response> getPaywall();
  Future<Response> loginWithGoogle({
    required String accessToken,
    required String fcm,
    required String uuid,
    required String device,
  });
  Future<Response> loginWithApple({
    required String accessToken,
    required String fcm,
    required String uuid,
  });
}

@Injectable(as: ISubAuthApiService)
class SubAuthApiServiceImplementation implements ISubAuthApiService {
  final Dio api;

  SubAuthApiServiceImplementation(this.api);

  @override
  Future<Response> getData() {
    return api.get(
      '/rest/V1/',
      queryParameters: {},
    );
  }

  @override
  Future<Response> loginWithGoogle({
    required String accessToken,
    required String fcm,
    required String uuid,
    required String device,
  }) async {
    final res = await api.post(
      'api/auth/google',
      data: FormData.fromMap(
        {
          'access_token': accessToken,
          'fcm': fcm,
          'uuid': uuid,
          //TODO  should connect to local storage
          'app_user_data': '{}',

          'device': device.toLowerCase(),
        },
      ),
    );
    return res;
  }

  @override
  Future<Response> loginWithApple({
    required String accessToken,
    required String fcm,
    required String uuid,
  }) {
    return api.post(
      '/api/auth/apple',
      data: FormData.fromMap({
        'access_token': accessToken,
        'fcm': fcm,
        'uuid': uuid,
        'device': 'ios',
        'app_user_data': '{}',
      }),
    );
  }

  @override
  Future<Response> logout({
    required String uuid,
    required String fcm,
  }) async {
    final data = {'fcm': fcm, 'uuid': uuid};
    print('data -> $data');
    final res = await api.post(
      'api/V1/user/logout',
      data: data,
    );
    return res;
  }

  @override
  Future<Response> deleteAccount() {
    return api.delete('api/V1/user/delete');
  }

  @override
  Future<Response> getPaywall() {
    return api.get('api/V1/subscription/paywall');
  }
}
