import 'dart:ui';

import 'package:dartz/dartz.dart';
import 'package:hive/hive.dart';
import 'package:kt_dart/collection.dart';
import 'package:motivation_flutter/features/theme/domain/models/dynamic_background_entity.dart';
import 'package:motivation_flutter/features/theme/domain/models/theme_model.dart';

part 'theme_entity.g.dart';

@HiveType(typeId: 12)
class ThemeEntity extends HiveObject {
  @HiveField(0)
  String themeId;
  @HiveField(1)
  String name;
  @HiveField(2)
  String tag;
  @HiveField(3)
  List<DynamicBackgroundEntity> backgrounds;
  @HiveField(4)
  String fontFamily;
  @HiveField(5)
  int? textColor;
  @HiveField(6)
  TextShadowEntity? textShadow;

  ThemeEntity({
    required this.themeId,
    required this.name,
    required this.tag,
    required this.backgrounds,
    required this.fontFamily,
    this.textColor,
    this.textShadow,
  });

  ThemeEntity copyWith({
    String? themeId,
    String? name,
    String? tag,
    List<DynamicBackgroundEntity>? backgrounds,
    String? fontFamily,
    int? textColor,
    TextShadowEntity? textShadow,
  }) {
    return ThemeEntity(
      themeId: themeId ?? this.themeId,
      name: name ?? this.name,
      tag: tag ?? this.tag,
      backgrounds: backgrounds ?? this.backgrounds,
      fontFamily: fontFamily ?? this.fontFamily,
      textColor: textColor ?? this.textColor,
      textShadow: textShadow ?? this.textShadow,
    );
  }

  factory ThemeEntity.fromDomain(ThemeModel model) {
    return ThemeEntity(
      themeId: model.themeId,
      name: model.name,
      tag: model.tag,
      fontFamily: model.fontFamily,
      textColor: model.textColor.value,
      backgrounds: model.backGrounds
          .map((p0) => DynamicBackgroundEntity.fromDomain(p0))
          .asList(),
      textShadow: model.textShadow.fold(
        () => null,
        (a) => TextShadowEntity.fromDomain(a),
      ),
    );
  }

  ThemeModel toDomain() {
    return ThemeModel(
      themeId: themeId,
      name: name,
      tag: tag,
      backGrounds: backgrounds.map((e) => e.toDomain()).toList().kt,
      fontFamily: fontFamily,
      textColor: Color(textColor ?? 0xff000000),
      textShadow: textShadow == null ? none() : some(textShadow!.toDomain()),
    );
  }
}

@HiveType(typeId: 17)
class TextShadowEntity extends HiveObject {
  @HiveField(0)
  int color;
  @HiveField(1)
  double blurRadius;
  @HiveField(2)
  double xOffset;
  @HiveField(3)
  double yOffset;

  TextShadowEntity({
    required this.color,
    required this.blurRadius,
    required this.xOffset,
    required this.yOffset,
  });

  TextShadowEntity copyWith({
    int? color,
    double? blurRadius,
    double? xOffset,
    double? yOffset,
  }) {
    return TextShadowEntity(
      color: color ?? this.color,
      blurRadius: blurRadius ?? this.blurRadius,
      xOffset: xOffset ?? this.xOffset,
      yOffset: yOffset ?? this.yOffset,
    );
  }

  factory TextShadowEntity.fromDomain(Shadow model) {
    return TextShadowEntity(
      blurRadius: model.blurRadius,
      color: model.color.value,
      xOffset: model.offset.dx,
      yOffset: model.offset.dy,
    );
  }

  Shadow toDomain() {
    return Shadow(
      blurRadius: blurRadius,
      color: Color(color),
      offset: Offset(xOffset, yOffset),
    );
  }
}
