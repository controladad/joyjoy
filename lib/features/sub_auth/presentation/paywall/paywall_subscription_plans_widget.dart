import 'package:kt_dart/kt.dart';
import 'package:motivation_flutter/common/presentation/common_imports.dart';
import 'package:motivation_flutter/features/sub_auth/domain/models/paywall_model.dart';

class PaywallSubscriptionPlansWidget extends StatelessWidget {
  final KtList<PayWallSubscriptionPlanModel> plans;
  final PayWallSubscriptionPlanModel selectedPlan;
  final Function(PayWallSubscriptionPlanModel plan) onPlanSelection;
  const PaywallSubscriptionPlansWidget({
    required this.selectedPlan,
    required this.plans,
    required this.onPlanSelection,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: pad(15, 0, 15, 0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: plans.map(
          (e) {
            return Flexible(
              child: GestureDetector(
                onTap: () => onPlanSelection(e),
                child: _ItemWidget(
                  isSelected: e.id == selectedPlan.id,
                  plan: e,
                ),
              ),
            );
          },
        ).asList(),
      ),
    );
  }
}

class _ItemWidget extends StatelessWidget {
  final PayWallSubscriptionPlanModel plan;
  final bool isSelected;
  const _ItemWidget({required this.plan, required this.isSelected});

  @override
  Widget build(BuildContext context) {
    final bool hasDiscount = plan.discount != null && plan.discount != 0;
    final bool hasTrial = plan.trialModel != null;
    return SContainer(
      margin: pad(6, 0, 6, 0),
      height: 174,
      decoration: BoxDecoration(
        color: isSelected ? AppTheme.secondary_60 : AppTheme.primary_60,
        borderRadius: BorderRadius.circular(20),
      ),
      child: Center(
        child: Column(
          children: [
            const Spacer(flex: 5),
            SText(
              plan.title.toUpperCase(),
              style: TStyle.BodySmall,
            ),
            const Spacer(),
            SContainer(
              height: 140,
              margin: pad(4, 4, 4, 4),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                gradient: AppGradients.primaryLight,
              ),
              child: Center(
                child: Stack(
                  children: [
                    Column(
                      children: [
                        Flexible(
                          child: Column(
                            children: [
                              const Spacer(),
                              SText(
                                plan.duration.toString(),
                                style: TStyle.HeadlineMedium,
                              ),
                              const SBox(height: 1),
                              SText(
                                plan.durationType.capitalize(),
                                style: TStyle.LabelMedium,
                              ),
                              const Spacer(flex: 5),
                            ],
                          ),
                        ),
                        Flexible(
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius: const BorderRadius.only(
                                bottomLeft: Radius.circular(20),
                                bottomRight: Radius.circular(20),
                              ),
                              gradient: AppGradients.onPrimaryDark,
                            ),
                            child: Center(
                              child: Column(
                                children: [
                                  const Spacer(flex: 5),
                                  SText(
                                    plan.subTitle ?? '',
                                    style: TStyle.LabelMedium,
                                  ),
                                  const Spacer(),
                                  SText(
                                    '\$${plan.price.toStringAsFixed(2)}',
                                    style: TStyle.LabelMedium,
                                  ),
                                  const Spacer(flex: 2),
                                ],
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                    Center(
                      child: SContainer(
                        margin: pad(8, 0, 8, 0),
                        padding: pad(8, 4, 8, 4),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: hasDiscount || hasTrial
                              ? AppTheme.secondary_container_dark
                              : AppTheme.on_error_container_dark
                                  .withOpacity(.16),
                          gradient: AppGradients.secondaryContainer,
                        ),
                        child: SText(
                          hasTrial
                              ? '${plan.trialModel!.duration}-${context.translate.day} ${context.translate.trial}'
                                  .toTitleCase()
                              : hasDiscount
                                  ? '${context.translate.save.capitalize()} ${plan.discount?.toStringAsFixed(0)}%'
                                  : context.translate.noDiscount.toTitleCase(),
                          style: TStyle.LabelMedium,
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
