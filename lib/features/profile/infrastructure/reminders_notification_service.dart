import 'dart:async';

import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:kt_dart/kt.dart';
import 'package:motivation_flutter/common/presentation/common_presentation.dart';
import 'package:motivation_flutter/core/failures.dart';
import 'package:motivation_flutter/core/local_notification_services.dart';
import 'package:motivation_flutter/features/profile/domain/models/reminder_model.dart';
import 'package:motivation_flutter/features/profile/infrastructure/profile_data_source.dart';
import 'package:motivation_flutter/features/quotes/domain/models/quotes_models.dart';
import 'package:motivation_flutter/shared/infrastructure/shared_data_source.dart';
// import 'package:workmanager/workmanager.dart';

@injectable
class ReminderNotificationService {
  final IProfileApiDataSource profileDataSource;
  final ISharedDataSource sharedDataSource;
  final LocalNotificationService notificationService;

  KtList<ReminderModel>? reminders;

  ReminderNotificationService(
    this.profileDataSource,
    this.sharedDataSource,
    this.notificationService,
  );

  Future<void> initialize() async =>
      sharedDataSource.cacheUpdated.listen((event) => checkChanges());

  Future<void> checkChanges() async {
    final newReminders = await sharedDataSource.getReminders;
    if (reminders == null) {
      reminders = newReminders;
      return;
    } else {
      if (reminders != newReminders) {
        updateNotificationsWithDebounce();
      }
    }
  }

  Timer? _reminderDebounceTimer;
  Future<void> updateNotificationsWithDebounce() async {
    if (_reminderDebounceTimer?.isActive ?? false) {
      _reminderDebounceTimer?.cancel();
    }

    _reminderDebounceTimer =
        Timer(const Duration(seconds: 50), _pushNotificationsForNextWeek);
  }

  Future<Either<GeneralFailure, int>> checkPendingNotifications() async {
    return tryCacheBlock(() async {
      final currentPending = await notificationService.pendingCount;

      if (currentPending == 0) {
        return _pushNotificationsForNextWeek();
      } else {
        return currentPending;
      }
    });
  }

  Future<void> reserveNextPushFromBackground({
    required int total,
    required int successfullyAdded,
  }) async {
    // late Duration duration;

    if (total == 0 || successfullyAdded == 0) return;

    if (total == successfullyAdded) {
      // duration = const Duration(days: 7);
    } else {
      final days = (successfullyAdded * 7) / 500;

      if (days > 1) {
        // duration = Duration(days: days.ceil());
      } else {
        // final hours = days * 24;
        // duration = Duration(hours: hours.ceil());
      }
    }

    try {
      // return Workmanager().registerPeriodicTask(
      //   'check_update_pending_reminder',
      //   'check_update_pending_reminder',
      //   frequency: duration,
      //   initialDelay: duration,
      //   constraints: Constraints(
      //     networkType: NetworkType.connected,
      //     requiresBatteryNotLow: false,
      //     requiresCharging: false,
      //     requiresDeviceIdle: false,
      //     requiresStorageNotLow: false,
      //   ),
      // );
    } catch (e) {
      print('background reminder reservation Failed');
    }
  }

  Future<int> _pushNotificationsForNextWeek() async {
    await notificationService.deleteAllPendingNotifications();

    final reminders = await sharedDataSource.getReminders;
    final quotesByReminders = await _getNotificationQuotes();

    if (quotesByReminders.isEmpty) return 0;

    KtList<CustomNotification> notifications = emptyList();

    int notificationId = 0;
    for (final entry in quotesByReminders.entries) {
      final quotes = entry.value;
      final reminder = reminders.find((p0) => p0.id == entry.key)!;

      int quoteIndex = 0;

      for (final weekDay in reminder.daysOfWeek.asList()) {
        //calculate next date from now
        final now = DateTime.now();
        final days = (getWeekDay(weekDay) - now.weekday).abs();

        final dateTimes = getDateTimeList(
          reminder.startTime,
          reminder.endTime,
          reminder.countPerDuration,
        );

        for (final date in dateTimes) {
          final quote = quotes[quoteIndex];
          final notificationDate = now
              .add(Duration(days: days))
              .copyWith(hour: date.hour, minute: date.minute);
          if (notificationDate.isBefore(now)) continue;

          notifications = notifications.plusElement(
            CustomNotification(
              id: notificationId,
              scheduleDate: notificationDate,
              body: quote.quote,
              title: quote.author?.name ?? 'Author',
            ),
          );
          notificationId++;
          quoteIndex++;
        }
      }
    }
    notifications =
        notifications.sortedByDescending((p0) => p0.scheduleDate).reversed();

    final successfullyAddedNotifications =
        await notificationService.addGroupOfNotifications(notifications);

    await reserveNextPushFromBackground(
      total: notifications.size,
      successfullyAdded: successfullyAddedNotifications,
    );

    return successfullyAddedNotifications;
  }

  Future<Map<String, KtList<QuoteModel>>> _getNotificationQuotes() async {
    final reminders = await sharedDataSource.getReminders;
    final Map<String, KtList<QuoteModel>> quotesByReminders = {};

    for (final reminder in reminders.asList()) {
      if (reminder.isActive == false) continue;

      final quotes = await sharedDataSource.getNotificationQuotes(
        categoryIds: reminder.categories.map((p0) => p0.id),
        count: reminder.countPerDuration * reminder.daysOfWeek.size,
      );
      quotesByReminders.addAll({reminder.id: quotes});
    }
    return quotesByReminders;
  }
}
