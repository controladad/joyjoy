import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:motivation_flutter/features/quotes/domain/models/quotes_models.dart';

part 'author_dto.g.dart';
part 'author_dto.freezed.dart';

@freezed
class AuthorDto with _$AuthorDto {
  factory AuthorDto({
    @JsonKey(name: 'id') required int id,
    @JsonKey(name: 'name') required String name,
    // @JsonKey(name: 'gender') required String gender,
    // @JsonKey(name: 'birth') required DateTime birthYear,
    // @JsonKey(name: 'death') required DateTime deathYear,
    // @JsonKey(name: 'country') required String country,
    // @JsonKey(name: 'short_description') required String shortDescription,
    // @JsonKey(name: 'job') required String job,
    // @JsonKey(name: 'type') required String status,
  }) = _AuthorDto;
  const AuthorDto._();
  factory AuthorDto.fromJson(Map<String, dynamic> json) =>
      _$AuthorDtoFromJson(json);

  AuthorModel toDomain() {
    return AuthorModel(
      id: id.toString(),
      name: name,
      // shortDescription: shortDescription,
      // birthYear: birthYear.year,
      // deathYear: deathYear.year,
      // job: job,
      // country: country,
      // gender: gender,
      // status: status == 'free'
      // ? AccessabilityStatus.Free
      // : AccessabilityStatus.Premium,
      shortDescription: '',
      birthYear: 1000,
      deathYear: 1000,
      job: '',
      country: '',
      gender: '',
      status: AccessabilityStatus.Free,
    );
  }

  AuthorEntity toEntity() {
    return AuthorEntity(
      id: id.toString(),
      name: name,
      // shortDescription: shortDescription,
      // birthYear: birthYear.year,
      // deathYear: deathYear.year,
      // job: job,
      // country: country,
      // gender: gender,
      // accessabilityStatus: status == 'premium' ? 1 : 0,
      shortDescription: '',
      birthYear: 1000,
      deathYear: 1000,
      job: '',
      country: '',
      gender: '',
      accessabilityStatus: 0,
    );
  }
}
