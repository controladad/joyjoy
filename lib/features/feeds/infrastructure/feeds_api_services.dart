import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';

abstract class IFeedsApiService{
  Future<Response> getData();
}

@Injectable(as: IFeedsApiService)
class FeedsApiServiceImplementation implements IFeedsApiService{
  final Dio api;

  FeedsApiServiceImplementation(this.api);

  @override
  Future<Response> getData() {
    return api.get(
      '/rest/V1/',
      queryParameters: {},
    );
  }
}
