import 'package:dartz/dartz.dart';
import 'package:flutter_share/flutter_share.dart';
import 'package:flutter_swiper_null_safety/flutter_swiper_null_safety.dart';
import 'package:in_app_review/in_app_review.dart';
import 'package:kt_dart/collection.dart';
import 'package:motivation_flutter/common/presentation/common_imports.dart';
import 'package:motivation_flutter/common/presentation/utils/custom_bottom_sheets.dart';
import 'package:motivation_flutter/features/profile/application/profile_bloc/profile_cubit.dart';
import 'package:motivation_flutter/features/profile/domain/models/account_info_model.dart';
import 'package:motivation_flutter/features/profile/domain/models/profile_models.dart';
import 'package:motivation_flutter/features/profile/presentation/profile_bottom_sheets/about_widget.dart';
import 'package:motivation_flutter/features/profile/presentation/profile_bottom_sheets/account_info_bottom_sheet.dart';
import 'package:motivation_flutter/features/profile/presentation/profile_bottom_sheets/login_bottom_sheets.dart';
import 'package:motivation_flutter/features/profile/presentation/profile_bottom_sheets/profile_list_item_widget.dart';
import 'package:motivation_flutter/features/profile/presentation/profile_screens/profile_phone_view.dart';
import 'package:motivation_flutter/features/profile/sub_features/my_collection/my_collection_screen.dart';
import 'package:motivation_flutter/features/quotes/domain/models/saved_quote_union.dart';
import 'package:motivation_flutter/features/theme/presentation/theme_screens/theme_screen.dart';
import 'package:url_launcher/url_launcher.dart';

void showProfileBottomSheets(BuildContext context) {
  showCustomBottomSheet(
    context: context,
    onClose: () {},
    child: const _ProfileBottomSheetWidget(),
  );
}

void showScreenInBottomSheets(BuildContext context, Widget screen) {
  showCustomBottomSheet(
    context: context,
    onClose: () {},
    child: SizedBox(
      height: SizeConfig.screenHeight * Constants.screenToBottomSheetFraction,
      child: screen,
    ),
  );
}

class _ProfileBottomSheetWidget extends StatelessWidget {
  const _ProfileBottomSheetWidget();

  @override
  Widget build(BuildContext context) {
    final profileState = context.watch<ProfileCubit>().state;
    final profileData = profileState.offlineData
        .fold(() => null, (a) => a.fold((l) => null, (r) => r));

    final groups = [
      ProfileGroupModel(
        title: context.translate.profile,
        items: [
          ProfileItemModel(
            label: context.translate.account,
            icon: AppIcons.profileManageAccount,
            onTap: () {
              profileData?.accountInfo.map(
                loggedIn: (account) => showAccountInfoBottomSheet(context),
                guest: (_) {
                  context
                      .popRoute()
                      .then((value) => showLoginBottomSheet(context, (_) {}));
                },
              );
            },
            subTitle: profileData?.accountInfo.map(
              loggedIn: (user) => user.email,
              guest: (_) => null,
            ),
          ),
          ProfileItemModel(
            label: context.translate.likes,
            icon: AppIcons.profileManageLikes,
            onTap: () => context.pushRoute(
              ListOfQuoteRoute(type: const SavedQuoteType.liked()),
            ),
            subTitle: switch (profileData?.likedQuotes.size) {
              0 => null,
              1 => '1 ${context.translate.quote}',
              final s => '$s ${context.translate.quotes}',
            },
          ),
          ProfileItemModel(
            label: context.translate.myCollections,
            icon: AppIcons.profileMyCollection,
            onTap: () => showScreenInBottomSheets(
              context,
              const MyCollectionsScreen(),
            ),
            subTitle: switch (profileData?.collections.size) {
              0 => null,
              1 => '1 ${context.translate.collection}',
              final s => '$s ${context.translate.collections}',
            },
          ),
          ProfileItemModel(
            label: context.translate.theme,
            icon: AppIcons.themeIcon,
            onTap: () => showScreenInBottomSheets(context, const ThemeScreen()),
            subTitle: switch (profileData?.currentTheme.name) {
              final String themeName => themeName,
              var _ => null,
            },
          ),
        ],
      ),
      ProfileGroupModel(
        title: context.translate.appName,
        items: [
          ProfileItemModel(
            label: context.translate.rateUs,
            icon: AppIcons.profileRateUs,
            subTitle: null,
            onTap: () async {
              final InAppReview inAppReview = InAppReview.instance;

              if (await inAppReview.isAvailable() &&
                  await hasInternetConnection) {
                inAppReview.requestReview();
              }
            },
          ),
          ProfileItemModel(
            label: context.translate.referFriends,
            icon: AppIcons.profileReferFriends,
            subTitle: null,
            onTap: () {
              return FlutterShare.share(
                title: 'JoyJoy Application',
                text: Constants.storeLink,
              );
            },
          ),
          ProfileItemModel(
            label: context.translate.about,
            icon: AppIcons.profileAbout,
            subTitle: null,
            onTap: () => showScreenInBottomSheets(
              context,
              const AboutWidget(),
            ),
          ),
        ],
      ),
    ];

    return SizedBox(
      height: SizeConfig.screenHeight * .9,
      child: SingleChildScrollView(
        child: Column(
          children: [
            if (profileState.banners.isNotEmpty())
              _Banner(profileState.banners),
            Padding(
              padding: pad(12, 9, 12, 9),
              child: Column(
                children: groups
                    .map(
                      (e) => Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: pad(14, 9, 14, 9),
                            child: SText(
                              e.title.toUpperCase(),
                              style: TStyle.LabelButton,
                            ),
                          ),
                          ...e.items.map((e) => ProfileListItemWidget(item: e))
                        ],
                      ),
                    )
                    .toList(),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class _Banner extends StatelessWidget {
  const _Banner(this.banners);

  final KtList<ProfileBannerModel> banners;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: pad(0, 0, 0, 10),
      height: 110,
      child: Swiper(
        physics: banners.size < 2 ? const NeverScrollableScrollPhysics() : null,
        plugins: [
          _swiperPlugin(),
        ],
        itemCount: banners.size,
        itemBuilder: (context, index) => banners.toWidgetByIndex(
          index,
          (item) => Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: _BannerItemWidget(item),
          ),
        ),
      ),
    );
  }
}

class _BannerItemWidget extends StatelessWidget {
  final ProfileBannerModel item;
  const _BannerItemWidget(this.item);

  @override
  Widget build(BuildContext context) {
    final state = context.watch<ProfileCubit>().state;
    final Option<AccountInfoModel> auth = state.offlineData.getOrNone
        .fold(() => none(), (a) => some(a.accountInfo));

    void showLogin() {
      context.popRoute().then((value) => showLoginBottomSheet(context, (_) {}));
    }

    return AspectRatio(
      aspectRatio: 358 / 110,
      child: GestureDetector(
        onTap: () {
          item.link.map(
            noAction: (value) => null,
            // ignore: void_checks
            link: (value) async {
              final url = Uri.parse(value.link);
              if (await canLaunchUrl(url)) {
                return launchUrl(url, mode: LaunchMode.externalApplication);
              }
            },
            inApp: (app) {
              return switch (app.section) {
                InAppSection.Chat => auth.fold(
                    () {},
                    (a) => a.map(
                      loggedIn: (_) => context.pushRoute(const ChatRoute()),
                      guest: (_) => showLogin(),
                    ),
                  ),
                InAppSection.Login => showLogin(),
              };
            },
          );
        },
        child: ImageWidget(
          image: item.image,
          height: null,
          width: null,
          color: null,
          fit: BoxFit.cover,
          borderRadius: BorderRadius.circular(20),
        ),
      ),
    );
  }
}

SwiperCustomPagination _swiperPlugin() {
  return SwiperCustomPagination(
    builder: (context, config) {
      if (config.itemCount < 2) return Container();

      return Stack(
        children: [
          Positioned(
            bottom: 10,
            right: 25,
            child: Row(
              children: List.generate(
                config.itemCount,
                (index) => Container(
                  height: 6,
                  width: 6,
                  margin: pad(2, 2, 2, 2),
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: config.activeIndex == index
                        ? AppTheme.secondary_dark
                        : AppTheme.white,
                  ),
                ),
              ),
            ),
          ),
        ],
      );
    },
  );
}
