import 'package:dartz/dartz.dart';
import 'package:kt_dart/kt.dart';
import 'package:motivation_flutter/core/failures.dart';

import 'package:motivation_flutter/features/theme/domain/models/theme_models.dart';

abstract class IThemeRepository {
  Future<Either<GeneralFailure, KtList<ThemeModel>>> get getThemes;
  Future<Either<GeneralFailure, ThemeModel>> get getSelectedTheme;
  Future<Either<GeneralFailure, ThemeModel>> selectTheme(String themeId);
}
