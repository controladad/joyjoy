import 'dart:async';
import 'package:dartz/dartz.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:kt_dart/kt.dart';
import 'package:motivation_flutter/common/presentation/common_imports.dart';
import 'package:motivation_flutter/core/failures.dart';
import 'package:motivation_flutter/features/profile/domain/models/reminder_model.dart';

import 'package:motivation_flutter/features/profile/domain/profile_repository.dart';

part 'reminders_bloc.freezed.dart';
part 'reminders_event.dart';
part 'reminders_state.dart';

@injectable
class RemindersBloc extends Bloc<RemindersEvent, RemindersState> {
  final IProfileRepository repository;
  late StreamSubscription<void>? cacheUpdatesSubscription;
  RemindersBloc(this.repository, RemindersState init) : super(init) {
    on<RemindersEvent>(_bloc);
    cacheUpdatesSubscription = repository.cacheUpdated.listen(
      (event) => add(
        const RemindersEvent.getReminders(),
      ),
    );
  }

  @override
  Future<void> close() {
    cacheUpdatesSubscription?.cancel();
    return super.close();
  }

  Future<void> _bloc(RemindersEvent event, Emitter emit) {
    return event.map(
      getReminders: (e) async {
        emit(
          state.copyWith(
            data: none(),
            reminders: emptyList(),
          ),
        );
        final result = await repository.getReminders;

        emit(state.copyWith(data: some(result)));
        result.fold(
          (l) => null,
          (r) => emit(
            state.copyWith(reminders: r),
          ),
        );
      },
      addNewReminder: (e) async {
        emit(
          state.copyWith(
            loadingReminders: state.loadingReminders.plusElement(e.reminder),
          ),
        );
        await repository.addOrUpdateReminder(e.reminder);

        emit(
          state.copyWith(
            loadingReminders: state.loadingReminders
                .filterNot((reminder) => reminder.id == e.reminder.id),
          ),
        );
      },
      editReminder: (e) async {
        emit(
          state.copyWith(
            loadingReminders: state.loadingReminders.plusElement(e.reminder),
          ),
        );
        await repository.addOrUpdateReminder(e.reminder);
        emit(
          state.copyWith(
            loadingReminders: state.loadingReminders
                .filterNot((reminder) => reminder.id == e.reminder.id),
          ),
        );
      },
      toggleReminder: (e) async {
        emit(
          state.copyWith(
            loadingReminders: state.loadingReminders.plusElement(e.reminder),
          ),
        );
        await repository.addOrUpdateReminder(
          e.reminder.copyWith(isActive: !e.reminder.isActive),
        );
        emit(
          state.copyWith(
            loadingReminders: state.loadingReminders
                .filterNot((reminder) => reminder.id == e.reminder.id),
          ),
        );
      },
      removeReminder: (e) async {
        final result = await repository.removeReminder(e.id);
        emit(state.copyWith(data: some(result)));

        result.fold(
          (l) {},
          (r) => emit(state.copyWith(reminders: r)),
        );
      },
    );
  }
}
