// import 'package:firebase_core/firebase_core.dart';
// import 'package:motivation_flutter/common/presentation/common_presentation.dart';
// import 'package:motivation_flutter/core/home_widget_service.dart';
// import 'package:motivation_flutter/core/local_notification_services.dart';
// import 'package:motivation_flutter/features/profile/infrastructure/reminders_notification_service.dart';
// import 'package:workmanager/workmanager.dart';

// @pragma('vm:entry-point')
// Future<void> backgroundTasks() async {
//   WidgetsFlutterBinding.ensureInitialized();
//   await Firebase.initializeApp();
//   await configureInjection();

//   Workmanager().executeTask((task, inputData) async {
//     switch (task) {
//       case 'check_update_pending_reminder':
//         return checkPendingRemindersNotifications();
//       case 'update_widget':
//         return homeWidgetTask();
//     }
//     return true;
//   });
// }

// Future<bool> checkPendingRemindersNotifications() async {
//   await getIt<LocalNotificationService>().initialize();
//   final reminderService = getIt<ReminderNotificationService>();
//   final result = await reminderService.checkPendingNotifications();
//   return result.fold((l) => false, (r) => true);
// }

// Future<bool> homeWidgetTask() async {
//   final widgetService = getIt<HomeWidgetService>();
//   final result = await widgetService.nextQuote();
//   return result.fold((l) => false, (r) => true);
// }
