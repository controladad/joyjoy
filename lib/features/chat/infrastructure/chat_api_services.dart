import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:motivation_flutter/features/chat/domain/models/chat_model.dart';

abstract class IChatApiService {
  Future<Response> getData();
  Future<Response> sendResponseFeedback({
    required String id,
    required bool isLike,
  });
  Future<Response> sendMessage({
    required MessageModel message,
    required List<MessageModel> prevMessageList,
  });
}

@Injectable(as: IChatApiService)
class ChatApiServiceImplementation implements IChatApiService {
  final Dio api;

  ChatApiServiceImplementation(this.api);

  @override
  Future<Response> getData() {
    return api.get(
      '/rest/V1/',
      queryParameters: {},
    );
  }

  @override
  Future<Response> sendMessage({
    required MessageModel message,
    required List<MessageModel> prevMessageList,
  }) {
    prevMessageList.removeLast();
    if (prevMessageList.length > 8) {
      prevMessageList.removeRange(0, prevMessageList.length - 4);
    }
    final Map<String, dynamic> data = {
      'messages': [
        ...prevMessageList.map(
          (message) => {
            'role': message.sender == Sender.Client ? 'user' : 'assistant',
            'content': message.text,
          },
        ),
      ]
    };
    return api.post('api/V1/chat', data: data);
  }

  @override
  Future<Response> sendResponseFeedback({
    required String id,
    required bool isLike,
  }) {
    return api.post(
      'api/V1/chat/feedback',
      data: {'feedback': isLike ? '1' : '2', 'id': id},
    );
  }
}
