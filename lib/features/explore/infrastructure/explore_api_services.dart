import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:motivation_flutter/common/presentation/utils/constants.dart';
import 'package:motivation_flutter/features/explore/domain/models/search_models.dart';

abstract class IExploreApiService {
  Future<Response> getExplore();
  Future<Response> instanceSearch(String searchQuery);
  Future<Response> fullSearch({
    required String searchQuery,
    required SearchResultUnion firstItem,
    required int page,
  });
}

@Injectable(as: IExploreApiService)
class ExploreApiServiceImplementation implements IExploreApiService {
  final Dio api;

  ExploreApiServiceImplementation(this.api);

  @override
  Future<Response> getExplore() async {
    final result = await api.get('api/V1/quote/category-list');
    return result;
  }

  @override
  Future<Response> fullSearch({
    required String searchQuery,
    required SearchResultUnion firstItem,
    required int page,
  }) async {
    final String endpoint = firstItem.map(
      category: (e) => 'api/V1/quote/category-list',
      author: (e) => 'api/V1/quote/author-list',
      quote: (e) => 'api/V1/quote/list',
    );

    return api.get(
      endpoint,
      queryParameters: {
        'search': searchQuery,
        'page': page,
        'limit': Constants.paginationLimit,
      },
    );
  }

  @override
  Future<Response> instanceSearch(String searchQuery) {
    return api.get(
      'api/V1/quote/instant-search',
      queryParameters: {'search': searchQuery},
    );
  }
}
