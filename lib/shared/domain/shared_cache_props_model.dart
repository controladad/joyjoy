import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:kt_dart/kt.dart';
import 'package:motivation_flutter/features/profile/domain/models/account_info_model.dart';
import 'package:motivation_flutter/features/profile/domain/models/collection_model.dart';
import 'package:motivation_flutter/features/profile/domain/models/reminder_model.dart';
import 'package:motivation_flutter/features/theme/domain/models/dynamic_background_union.dart';
import 'package:motivation_flutter/features/theme/domain/models/theme_model.dart';
import 'package:uuid/uuid.dart';

part 'shared_cache_props_model.freezed.dart';

@freezed
class SharedCachePropsModel with _$SharedCachePropsModel {
  const factory SharedCachePropsModel({
    required String id,
    required KtList<ReminderModel> reminders,
    required KtList<String> favoriteCategoriesIds,
    required KtList<String> favoriteAuthorsIds,
    required KtList<String> favoriteQuotesIds,
    required KtList<String> quotesHistoryIds,
    required KtList<CollectionModel> collections,
    required KtList<ThemeModel> allThemes,
    required String selectedThemeId,
    required DateTime versionDate,
    required bool didTryChat,
    required bool shouldShowOnboarding,
    required AccountInfoModel account,
    bool? isFirst,
  }) = _SharedCachePropsModel;

  factory SharedCachePropsModel.empty() => SharedCachePropsModel(
        id: const Uuid().v4(),
        allThemes: initialThemes(),
        selectedThemeId: ThemeModel.defaultTheme().themeId,
        favoriteCategoriesIds: emptyList(),
        favoriteAuthorsIds: emptyList(),
        favoriteQuotesIds: emptyList(),
        quotesHistoryIds: emptyList(),
        reminders: emptyList(),
        collections: emptyList(),
        versionDate: DateTime.now(),
        didTryChat: false,
        shouldShowOnboarding: true,
        isFirst: true,
        account: AccountInfoModel.guest(name: none()),
      );
  const SharedCachePropsModel._();
}

KtList<ThemeModel> initialThemes() {
  final themes = [
    ThemeModel.defaultTheme(),
    ThemeModel(
      themeId: 'kyoo_pal',
      name: 'Kyoo Pal',
      tag: 'linear',
      backGrounds: [
        const DynamicBackgroundUnion.gradient(
          [Color(0xff40E0D0), Color(0xffF7797D)],
        ),
        const DynamicBackgroundUnion.gradient(
          [
            Color(0xff59C173),
            Color(0xffA17FE0),
            Color(0xff5D26C1),
          ],
        ),
        const DynamicBackgroundUnion.gradient(
          [
            Color(0xff6A90BD),
            Color(0xff67AFAE),
            Color(0xffFDB99B),
          ],
        ),
        const DynamicBackgroundUnion.gradient(
          [Color(0xffDD3E54), Color(0xff6BE585)],
        ),
        const DynamicBackgroundUnion.gradient(
          [
            Color(0xff0DB1D6),
            Color(0xffA862CB),
            Color(0xffE34650),
          ],
        ),
        const DynamicBackgroundUnion.gradient(
          [
            Color(0xff9866DA),
            Color(0xffBC7DDE),
            Color(0xffFDB99B),
          ],
        ),
        const DynamicBackgroundUnion.gradient(
          [Color(0xff659999), Color(0xffF4791F)],
        ),
      ].kt,
      fontFamily: 'bree_serif',
      textColor: Colors.white,
      textShadow: some(
        const Shadow(
          color: Color.fromARGB(88, 61, 61, 61),
          blurRadius: 4,
          offset: Offset(0, 2),
        ),
      ),
    ),
    ThemeModel(
      themeId: 'rainbow',
      name: 'Rainbow',
      tag: 'linear',
      backGrounds: [
        const DynamicBackgroundUnion.gradient(
          [Color(0xffB92B27), Color(0xff1565C0)],
        ),
        const DynamicBackgroundUnion.gradient(
          [
            Color(0xff34BDC1),
            Color(0xffE98704),
            Color(0xffCF0380),
          ],
        ),
        const DynamicBackgroundUnion.gradient(
          [Color(0xff0B8793), Color(0xff45A247)],
        ),
        const DynamicBackgroundUnion.gradient(
          [
            Color(0xffAA4B6B),
            Color(0xff6B6B83),
            Color(0xff3B8D99),
          ],
        ),
        const DynamicBackgroundUnion.gradient(
          [Color(0xff2F7336), Color(0xffAA3A38)],
        ),
        const DynamicBackgroundUnion.gradient(
          [Color(0xff240B36), Color(0xffC31432)],
        ),
        const DynamicBackgroundUnion.gradient(
          [Color(0xff8E2DE2), Color(0xff4100C3)],
        ),
      ].kt,
      fontFamily: 'bree_serif',
      textColor: Colors.white,
      textShadow: some(
        const Shadow(
          color: Color.fromARGB(88, 61, 61, 61),
          blurRadius: 4,
          offset: Offset(0, 2),
        ),
      ),
    ),
    ThemeModel(
      themeId: 'margo',
      name: 'Margo',
      tag: 'linear',
      backGrounds: [
        const DynamicBackgroundUnion.gradient(
          [
            Color(0xffE5A8D8),
            Color(0xffEDE5C1),
            Color(0xffEEE6C3),
            Color(0xff9BC1F1),
          ],
        ),
        const DynamicBackgroundUnion.gradient(
          [Color(0xffFFA7F5), Color(0xff89FFFD)],
        ),
        const DynamicBackgroundUnion.gradient(
          [Color(0xffC9FFBF), Color(0xffFFAFBD)],
        ),
        const DynamicBackgroundUnion.gradient(
          [Color(0xffA1FFCE), Color(0xffFAFFD1)],
        ),
        const DynamicBackgroundUnion.gradient(
          [
            Color(0xffCAEFD7),
            Color(0xffF5BFD7),
            Color(0xffABC9E9),
          ],
        ),
        const DynamicBackgroundUnion.gradient(
          [
            Color(0xffC6FFDD),
            Color(0xffFBD786),
            Color(0xffF7797D),
          ],
        ),
        const DynamicBackgroundUnion.gradient(
          [
            Color(0xffCAF2EF),
            Color(0xffC9EFDC),
            Color(0xffF2BBF1),
            Color(0xffEAEAEA),
          ],
        ),
      ].kt,
      fontFamily: 'bree_serif',
      textColor: Colors.black,
      textShadow: none(),
    ),
    ThemeModel(
      themeId: 'sherbert',
      name: 'Sherbert',
      tag: 'linear',
      backGrounds: [
        const DynamicBackgroundUnion.gradient(
          [Color(0xffA8FF78), Color(0xff78FFD6)],
        ),
        const DynamicBackgroundUnion.gradient(
          [Color(0xffD6DBF6), Color(0xff9FFFEE)],
        ),
        const DynamicBackgroundUnion.gradient(
          [Color(0xffEDE574), Color(0xffE1F5C4)],
        ),
        const DynamicBackgroundUnion.gradient(
          [Color(0xffC9D6FF), Color(0xffE2E2E2)],
        ),
        const DynamicBackgroundUnion.gradient(
          [Color(0xffFFB4BE), Color(0xffECFFA1)],
        ),
        const DynamicBackgroundUnion.gradient(
          [Color(0xffF6CFBE), Color(0xffB9DCF2)],
        ),
        const DynamicBackgroundUnion.gradient(
          [Color(0xffF5E4FF), Color(0xffF2BEFF)],
        ),
      ].kt,
      fontFamily: 'bree_serif',
      textColor: Colors.black,
      textShadow: none(),
    ),
    ThemeModel(
      themeId: 'asteroid',
      name: 'Asteroid',
      tag: 'linear',
      backGrounds: [
        const DynamicBackgroundUnion.gradient(
          [Color(0xff42275A), Color(0xff734B6D)],
        ),
        const DynamicBackgroundUnion.gradient(
          [Color(0xff360033), Color(0xff0B8793)],
        ),
        const DynamicBackgroundUnion.gradient(
          [Color(0xff5A3F37), Color(0xff2C7744)],
        ),
        const DynamicBackgroundUnion.gradient(
          [
            Color(0xff1A2766),
            Color(0xffAE1B1E),
            Color(0xffFC9F32),
          ],
        ),
        const DynamicBackgroundUnion.gradient(
          [Color(0xff295270), Color(0xff524175)],
        ),
        const DynamicBackgroundUnion.gradient(
          [
            Color(0xff8E2DE2),
            Color(0xffB92B27),
            Color(0xff4A00E0),
            Color(0xff1565C0),
          ],
        ),
        const DynamicBackgroundUnion.gradient(
          [Color(0xff1E6C8E), Color(0xff2E7775)],
        ),
      ].kt,
      fontFamily: 'bree_serif',
      textColor: Colors.white,
      textShadow: some(
        const Shadow(
          color: Color.fromARGB(88, 61, 61, 61),
          blurRadius: 4,
          offset: Offset(0, 2),
        ),
      ),
    ),
    ThemeModel(
      themeId: 'delicate',
      name: 'Delicate',
      tag: 'linear',
      backGrounds: [
        const DynamicBackgroundUnion.gradient(
          [Color(0xffE0E2F8), Color(0xffFFE7E7)],
        ),
        const DynamicBackgroundUnion.gradient(
          [Color(0xffDAE2F8), Color(0xffEFBEBE)],
        ),
        const DynamicBackgroundUnion.gradient(
          [Color(0xffECE9E6), Color(0xffFFFFFF)],
        ),
        const DynamicBackgroundUnion.gradient(
          [Color(0xffD9A7C7), Color(0xffFFFCDC)],
        ),
        const DynamicBackgroundUnion.gradient(
          [Color(0xffFC9F32), Color(0xffFFDDE1)],
        ),
        const DynamicBackgroundUnion.gradient(
          [Color(0xffFFEFBA), Color(0xffFFFFFF)],
        ),
        const DynamicBackgroundUnion.gradient(
          [
            Color(0xffD9D5C3),
            Color(0xffF2F2F2),
            Color(0xffDBDBDB),
            Color(0xffEAEAEA),
          ],
        ),
      ].kt,
      fontFamily: 'bree_serif',
      textColor: Colors.black,
      textShadow: none(),
    ),
    ThemeModel(
      themeId: 'light',
      name: 'Light',
      tag: 'linear',
      backGrounds: [
        const DynamicBackgroundUnion.gradient(
          [Color(0xffC3CFE2), Color(0xffF5F7FA)],
        ),
        const DynamicBackgroundUnion.gradient(
          [Color(0xffCFD9DF), Color(0xffE2EBF0)],
        ),
        const DynamicBackgroundUnion.gradient(
          [Color(0xffCFD9DF), Color(0xffE2EBF0)],
        ),
        const DynamicBackgroundUnion.gradient(
          [Color(0xffC3E0E2), Color(0xffFDFCFB)],
        ),
        const DynamicBackgroundUnion.gradient(
          [Color(0xffC0C3C6), Color(0xffFBFBFB)],
        ),
        const DynamicBackgroundUnion.gradient(
          [Color(0xffBFDDCE), Color(0xffE4EFE9)],
        ),
        const DynamicBackgroundUnion.gradient(
          [Color(0xffFDFBFB), Color(0xffEBEDEE)],
        ),
      ].kt,
      fontFamily: 'bree_serif',
      textColor: Colors.black,
      textShadow: none(),
    ),
  ].kt;

  return themes;
}
