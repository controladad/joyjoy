part of 'ad_cubit.dart';

enum AdStatus {
  Rewarded,
  Failed,
}

@injectable
@freezed
class AdState with _$AdState {
  const factory AdState({
    required Option<AdStatus> status,
  }) = _AdState;

  @factoryMethod
  factory AdState.init() => AdState(status: none());
}
