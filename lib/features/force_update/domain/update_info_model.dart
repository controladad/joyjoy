import 'package:freezed_annotation/freezed_annotation.dart';

part 'update_info_model.freezed.dart';

@freezed
class UpdateInfoModel with _$UpdateInfoModel {
  factory UpdateInfoModel({
    required bool isForceUpdate,
    required bool existNewUpdate,
    String? descriptionsOfVersions,
    String? newVersion,
  }) = _UpdateInfoModel;
}
