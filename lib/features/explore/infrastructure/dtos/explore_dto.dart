import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:kt_dart/kt.dart';

import 'package:motivation_flutter/features/explore/domain/models/explore_models.dart';
import 'package:motivation_flutter/features/quotes/domain/models/category_model.dart';
import 'package:motivation_flutter/features/quotes/domain/models/sub_category_model.dart';

part 'explore_dto.freezed.dart';
part 'explore_dto.g.dart';

@freezed
class ExploreDto with _$ExploreDto {
  factory ExploreDto({
    @JsonKey(name: 'data')
    required List<Map<String, dynamic>> subCategoriesData,
  }) = _ExploreDto;

  factory ExploreDto.fromJson(Map<String, dynamic> json) =>
      _$ExploreDtoFromJson(json);
  const ExploreDto._();

  ExploreModel toDomain() {
    final Map<dynamic, KtList<SubCategoryModel>> categoriesMap = {};

    for (final category in subCategoriesData) {
      late AccessabilityStatus status;

      switch (category['type']) {
        case 'free':
          status = AccessabilityStatus.Free;
        case 'premium':
          status = AccessabilityStatus.Premium;
      }

      final SubCategoryModel subCategory = SubCategoryModel(
        id: (category['id'] as int).toString(),
        name: category['name'] as String,
        icon: category['image'] as String,
        tags: (category['tags'] as List<dynamic>).join(',').split(','),
        description: '',
        status: status,
      );
      final tags = category['tags'] as List<dynamic>;

      for (final tag in tags) {
        categoriesMap[tag] =
            categoriesMap[tag]?.plusElement(subCategory) ?? [subCategory].kt;
      }
    }

    KtList<CategoryModel> categories = emptyList();
    for (final key in categoriesMap.keys) {
      categories = categories.plusElement(
        CategoryModel(
          id: key.toString(),
          name: key.toString(),
          subCategories: categoriesMap[key] ?? emptyList(),
        ),
      );
    }

    return ExploreModel(categories: categories);
  }
}
