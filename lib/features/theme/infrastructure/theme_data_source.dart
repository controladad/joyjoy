import 'package:injectable/injectable.dart';

import 'package:motivation_flutter/features/theme/infrastructure/dtos/theme_dto.dart';
import 'package:motivation_flutter/features/theme/infrastructure/theme_api_services.dart';

abstract class IThemeApiDataSource {
  Future<ThemeDto> get getData;
}

@Injectable(as: IThemeApiDataSource)
class ThemeApiDataSourceImplementation implements IThemeApiDataSource {
  final IThemeApiService apiService;

  ThemeApiDataSourceImplementation(this.apiService);

  @override
  Future<ThemeDto> get getData async {
    throw UnimplementedError('theme data source unimplemented');
  }
}
