import 'package:freezed_annotation/freezed_annotation.dart';

part 'failures.freezed.dart';

@freezed
class GeneralFailure with _$GeneralFailure {
  const factory GeneralFailure.noNetwork() = _NoNetwork;
  const factory GeneralFailure.unexpected({String? message}) = _Unexpected;
  const factory GeneralFailure.authentication() = _Authentication;
  const factory GeneralFailure.underMaintenance() = _UnderMaintenance;
  const factory GeneralFailure.forceUpdate(String storeLink) = _ForceUpdate;
  const factory GeneralFailure.localException(Object exception) =
      _LocalException;
}

class ApiException implements Exception {
  final GeneralFailure failure;

  ApiException(this.failure);
}
