import 'package:flutter_html/flutter_html.dart';
import 'package:kt_dart/kt.dart';
import 'package:motivation_flutter/common/presentation/common_imports.dart';
import 'package:motivation_flutter/features/sub_auth/domain/models/paywall_model.dart';
import 'package:url_launcher/url_launcher.dart';

class PaywallFrequentlyAskedQuestions extends StatelessWidget {
  final KtList<QuestionAndAnswerModel> questions;
  const PaywallFrequentlyAskedQuestions(this.questions);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: pad(20, 0, 20, 0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: pad(13, 0, 13, 0),
            child: SText(
              context.translate.frequentlyAskedQuestions.toTitleCase(),
              style: TStyle.TitleLarge,
            ),
          ),
          ...questions
              .mapIndexed(
                (index, item) => Column(
                  children: [
                    _QuestionItemWidget(
                      question: item,
                      isLast: index == questions.size - 1,
                    ),
                  ],
                ),
              )
              .asList(),
        ],
      ),
    );
  }
}

class _QuestionItemWidget extends StatefulWidget {
  final bool isLast;
  final QuestionAndAnswerModel question;
  const _QuestionItemWidget({
    required this.isLast,
    required this.question,
  });

  @override
  State<_QuestionItemWidget> createState() => _QuestionItemWidgetState();
}

class _QuestionItemWidgetState extends State<_QuestionItemWidget> {
  bool isExpanded = false;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: pad(13, 20, 13, 20),
      decoration: BoxDecoration(
        border: Border(
          bottom: !widget.isLast
              ? const BorderSide(
                  color: AppTheme.outline_variant_dark,
                  width: .5,
                )
              : BorderSide.none,
        ),
      ),
      child: Center(
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Flexible(
                  child: SText(
                    widget.question.question.capitalize(),
                    style: TStyle.TitleMedium,
                  ),
                ),
                IconButton(
                  icon: AnimatedRotation(
                    turns: isExpanded ? .5 : 0,
                    duration: const Duration(milliseconds: 200),
                    child: const ImageWidget(
                      image: AppIcons.downChevron,
                      height: 8,
                      width: 11.5,
                    ),
                  ),
                  onPressed: () {
                    setState(() {
                      isExpanded = !isExpanded;
                    });
                  },
                )
              ],
            ),
            AnimatedSwitcher(
              duration: const Duration(milliseconds: 200),
              child: isExpanded
                  ? Html(
                      data: widget.question.answer,
                      onLinkTap: (url, attributes, element) =>
                          launchUrl(Uri.parse(url ?? 'https://google.com')),
                    )
                  : null,
            )
          ],
        ),
      ),
    );
  }
}
