import 'package:dartz/dartz.dart';
import 'package:motivation_flutter/core/failures.dart';
import 'package:motivation_flutter/features/profile/domain/models/account_info_model.dart';
import 'package:motivation_flutter/features/profile/domain/models/user_subscription_model.dart';
import 'package:motivation_flutter/features/sub_auth/domain/models/paywall_model.dart';

import 'package:motivation_flutter/features/sub_auth/domain/models/sub_auth_models.dart';

abstract class ISubAuthRepository {
  Future<void> initialize();
  Future<Either<GeneralFailure, SubAuthModel>> get getData;
  Future<Either<GeneralFailure, PaywallModel>> get getPaywall;
  Future<Either<GeneralFailure, bool>> signInWithGoogle();
  Future<Either<GeneralFailure, bool>> signInWithApple();
  Future<Either<GeneralFailure, bool>> userHasToken();
  Future<Either<GeneralFailure, bool>> signOut();
  Future<Either<GeneralFailure, bool>> deleteAccount();

  Future<Either<GeneralFailure, UserSubscription>> getSubscription();
  Stream<UserSubscription> get subscriptionUpdated;
  Future<Either<GeneralFailure, UserSubscription>> restoreSubscription();
  Future<Either<GeneralFailure, UserSubscription>> changeSubscription(
    PayWallSubscriptionPlanModel plan,
  );
  Future<Either<GeneralFailure, AccountInfoModel>> accountInfo();

  Future<Either<GeneralFailure, Unit>> userRewarded(double rewardValue);

  Future<bool> isAuthenticated();
  Stream<void> get userUpdated;

  Future<bool> isDataCompleted();
  Stream<void> get cacheUpdated;
}
