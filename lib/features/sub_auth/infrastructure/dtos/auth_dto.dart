// ignore_for_file: unused_element
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:motivation_flutter/common/presentation/common_presentation.dart';

import 'package:motivation_flutter/features/profile/domain/models/account_info_model.dart';

part 'auth_dto.g.dart';
part 'auth_dto.freezed.dart';

@freezed
class AuthResponseDto with _$AuthResponseDto {
  factory AuthResponseDto({
    @JsonKey(name: 'token') String? token,
    @JsonKey(name: 'data') _Data? data,
  }) = _GoogleAuthResponseDto;
  const AuthResponseDto._();
  factory AuthResponseDto.fromJson(Map<String, dynamic> json) =>
      _$AuthResponseDtoFromJson(json);

  AccountInfoModel toAccountInfo({String? name}) {
    return AccountInfoModel.loggedIn(
      userId: data?.id.toString() ?? '',
      accountStatus: (data?.status ?? false) ? 'Active' : 'inactive',
      // ignore: unnecessary_parenthesis
      name: (name ?? (data?.name)).toOption(),
      signupDate: DateTime.tryParse(data?.createdAt ?? '') ?? DateTime.now(),
      email: data?.email ?? '',
      premiumPlan: (data?.premium ?? false) ? 'Premium' : 'Free',
      credits: data?.chatToken?.toDouble() ?? 0,
    );
  }
}

@freezed
class _Data with _$_Data {
  factory _Data({
    @JsonKey(name: 'id') required int id,
    @JsonKey(name: 'name') String? name,
    @JsonKey(name: 'gender') String? gender,
    @JsonKey(name: 'email') String? email,
    @JsonKey(name: 'status') bool? status,
    @JsonKey(name: 'premium') bool? premium,
    @JsonKey(name: 'created_at') String? createdAt,
    @JsonKey(name: 'app_user_data') String? userCacheData,
    @JsonKey(name: 'chat_token') int? chatToken,
  }) = __Data;
  const _Data._();
  factory _Data.fromJson(Map<String, dynamic> json) => _$_DataFromJson(json);
}
