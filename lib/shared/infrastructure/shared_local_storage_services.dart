import 'dart:math';

import 'package:dartz/dartz.dart';
import 'package:hive/hive.dart';
import 'package:injectable/injectable.dart';
import 'package:kt_dart/kt.dart';
import 'package:motivation_flutter/common/presentation/common_imports.dart';
import 'package:motivation_flutter/constants/db_constants.dart';

import 'package:motivation_flutter/features/quotes/domain/models/quotes_models.dart';
import 'package:motivation_flutter/features/quotes/domain/models/saved_quote_union.dart';
import 'package:motivation_flutter/shared/domain/shared_cache_props_entity.dart';
import 'package:motivation_flutter/shared/domain/shared_cache_props_model.dart';

abstract class ISharedLocalStorageService {
  Stream<SharedCachePropsModel?> get cacheUpdated;
  List<SharedCachePropsEntity> get getCacheChanges;
  Future<void> clearAllCache();
  Future<SharedCachePropsEntity> get getCacheProps;
  Future<SharedCachePropsModel> updateCacheProps(
    SharedCachePropsModel cacheProps, {
    bool clearPrevious,
  });

  Future<void> deleteQuote(String quoteId);
  Future<void> deleteNewQuotes();
  Future<KtList<QuoteModel>> getQuotes({
    required bool newQuote,
    int count,
  });
  Future<KtList<QuoteModel>> quoteEntitiesToDomain(
    KtList<QuoteEntity> entities,
  );

  Future<KtList<QuoteModel>> getNotificationQuotesByCategoryIds({
    required KtList<String> categoryIds,
  });

  Future<void> deleteQuoteFromNotificationQuotes(String quoteId);

  Future<Tuple2<KtList<QuoteModel>, KtList<String>>> getSavedQuotes(
    SavedQuoteType type,
  );

  Future<Tuple2<KtList<QuoteModel>, KtList<String>>> getQuotesByIds(
    KtList<String> ids,
  );

  Future<int> addNewBundleToNotifications({
    required KtList<AuthorEntity> authors,
    required KtList<QuoteEntity> quotes,
  });

  Future<int> addNewBundle({
    required KtList<AuthorEntity> authors,
    required KtList<QuoteEntity> quotes,
  });

  Future<int> get notificationsQuotesCount;

  Future<int> get newQuotesCount;
  Future<int> get allQuotesCount;
}

@Injectable(as: ISharedLocalStorageService)
class SharedLocalStorageServicesImplementation
    implements ISharedLocalStorageService {
  final HiveInterface hive;
  final Box<SharedCachePropsEntity> cachePropsBox;
  final Box<QuoteEntity> notificationQuotesBox;
  final LazyBox<AuthorEntity> authorsBox;
  final LazyBox<QuoteEntity> allQuotesBox;
  final LazyBox<QuoteEntity> newQuotesBox;

  SharedLocalStorageServicesImplementation(
    this.hive,
    @Named(DBConstants.QUOTES_CACHE_PROPS) this.cachePropsBox,
    @Named(DBConstants.AUTHORS) this.authorsBox,
    @Named(DBConstants.ALL_QUOTES) this.allQuotesBox,
    @Named(DBConstants.NEW_QUOTES) this.newQuotesBox,
    @Named(DBConstants.NOTIFICATION_QUOTES) this.notificationQuotesBox,
  );

  @override
  Stream<SharedCachePropsModel?> get cacheUpdated {
    return cachePropsBox.watch().map((event) {
      final model = (event.value as SharedCachePropsEntity?)?.toDomain;
      return model;
    });
  }

  @override
  List<SharedCachePropsEntity> get getCacheChanges {
    return cachePropsBox.values.toList();
  }

  @override
  Future<void> clearAllCache() async {
    await cachePropsBox.deleteAll(cachePropsBox.keys);
    await allQuotesBox.deleteAll(allQuotesBox.keys);
    await newQuotesBox.deleteAll(newQuotesBox.keys);
    await notificationQuotesBox.deleteAll(notificationQuotesBox.keys);
  }

  @override
  Future<void> deleteQuote(String quoteId) async {
    await allQuotesBox.delete(quoteId);
    await newQuotesBox.delete(quoteId);
    await notificationQuotesBox.delete(quoteId);
  }

  @override
  Future<KtList<QuoteModel>> quoteEntitiesToDomain(
    KtList<QuoteEntity> entities,
  ) async {
    KtList<QuoteModel> quotes = emptyList();

    for (final entity in entities.asList()) {
      final author = await authorsBox.get(entity.authorId);
      // if (author == null) continue;

      final quote = QuoteModel(
        id: entity.id,
        quote: entity.quote,
        author: author?.toDomain(),
        categoryIds: entity.categoryIds.kt,
      );

      quotes = quotes.plusElement(quote);
    }
    return quotes;
  }

  @override
  Future<void> deleteQuoteFromNotificationQuotes(
    String quoteId,
  ) async {
    if (notificationQuotesBox.containsKey(quoteId)) {
      await notificationQuotesBox.delete(quoteId);
    }
  }

  @override
  Future<int> addNewBundleToNotifications({
    required KtList<AuthorEntity> authors,
    required KtList<QuoteEntity> quotes,
  }) async {
    int newAddedQuotesCount = 0;
    final KtList<Future> authorsFutures = authors.map((author) async {
      if (authorsBox.containsKey(author.id) == false) {
        return authorsBox.put(author.id, author);
      }
    });
    await Future.wait(authorsFutures.asList());

    final List<Future> quotesFutures = quotes.map((quote) async {
      if (notificationQuotesBox.containsKey(quote.id) == false) {
        newAddedQuotesCount += 1;
        return [
          notificationQuotesBox.put(quote.id, quote),
          // allQuotesBox.put(quote.id, quote),
        ];
      }
    }).asList();
    await Future.wait(quotesFutures);

    return newAddedQuotesCount;
  }

  @override
  Future<int> addNewBundle({
    required KtList<AuthorEntity> authors,
    required KtList<QuoteEntity> quotes,
  }) async {
    int newAddedQuotesCount = 0;
    final KtList<Future> authorsFutures = authors.map((author) async {
      if (authorsBox.containsKey(author.id) == false) {
        return authorsBox.put(author.id, author);
      }
    });
    await Future.wait(authorsFutures.asList());

    final List<Future> quotesFutures = quotes.map((quote) async {
      if (allQuotesBox.containsKey(quote.id) == false) {
        return allQuotesBox.put(quote.id, quote);
        // await newQuotesBox.put(quote.id, quote)
      }
    }).asList();

    final List<Future> newQuotesFeatures = quotes.map((quote) async {
      newAddedQuotesCount += 1;
      return newQuotesBox.put(quote.id, quote);
    }).asList();

    await Future.wait(quotesFutures);
    await Future.wait(newQuotesFeatures);

    return newAddedQuotesCount;
  }

  @override
  Future<Tuple2<KtList<QuoteModel>, KtList<String>>> getQuotesByIds(
    KtList<String> ids,
  ) async {
    KtList<String> notFoundQuotesIds = emptyList();
    KtList<QuoteEntity> foundQuotes = emptyList();

    for (final id in ids.asList()) {
      final quote = await allQuotesBox.get(id);
      if (quote == null) {
        notFoundQuotesIds = notFoundQuotesIds.plusElement(id);
      } else {
        foundQuotes = foundQuotes.plusElement(quote);
      }
    }

    final quotes = await quoteEntitiesToDomain(foundQuotes);

    return Tuple2(quotes, notFoundQuotesIds);
  }

  @override
  Future<SharedCachePropsEntity> get getCacheProps async {
    if (cachePropsBox.isEmpty) {
      final propsModel = SharedCachePropsModel.empty();
      await cachePropsBox.add(SharedCachePropsEntity.fromDomain(propsModel));
    }

    return cachePropsBox.values.last;
  }

  @override
  Future<Tuple2<KtList<QuoteModel>, KtList<String>>> getSavedQuotes(
    SavedQuoteType type,
  ) async {
    final lastCacheProps = await getCacheProps;
    final List<String> ids = type.map(
      history: (e) => lastCacheProps.quotesHistoryIds ?? [],
      liked: (e) => lastCacheProps.likedQuotesIds ?? [],
      collection: (e) {
        if (lastCacheProps.collections == null) return [];
        return lastCacheProps.collections!
            .firstWhere((element) => element.id == e.collection.id)
            .quotesIds;
      },
    );

    final result = await getQuotesByIds(ids.kt);

    return result;
    // allQuotesBox.deleteAll(allQuotesBox.keys);
    // newQuotesBox.deleteAll(newQuotesBox.keys);
    // authorsBox.deleteAll(authorsBox.keys);
    // throw UnimplementedError();
  }

  @override
  Future<KtList<QuoteModel>> getQuotes({
    required bool newQuote,
    int count = Constants.minimumCountOfNewQuotesInView,
  }) async {
    KtList<QuoteEntity> quotes = emptyList();
    late int iteration;

    if (newQuote) {
      iteration = newQuotesBox.length < count ? newQuotesBox.length : count;

      for (int i = 1; i <= iteration; i++) {
        final quote = await newQuotesBox.getAt(newQuotesBox.length - i);
        if (quote != null) {
          quotes = quotes.plusElement(quote);
          newQuotesBox.delete(quote.id);
        }
      }
    } else {
      iteration = count;

      for (int i = 1; i <= iteration; i++) {
        final index = Random().nextInt(allQuotesBox.length);
        final quote = await allQuotesBox.getAt(index);
        if (quote != null) quotes = quotes.plusElement(quote);
      }
    }

    return quoteEntitiesToDomain(quotes);
  }

  @override
  Future<KtList<QuoteModel>> getNotificationQuotesByCategoryIds({
    required KtList<String> categoryIds,
  }) async {
    final filteredQuotes = notificationQuotesBox.values.kt.filter((p0) {
      return p0.categoryIds.any(
        (element) => categoryIds.contains(element),
      );
    });

    return quoteEntitiesToDomain(filteredQuotes);
  }

  @override
  Future<SharedCachePropsModel> updateCacheProps(
    SharedCachePropsModel cacheProps, {
    bool clearPrevious = false,
  }) async {
    final entity =
        SharedCachePropsEntity.fromDomain(cacheProps.copyWith(isFirst: false));
    await cachePropsBox.add(entity);

    final newCacheProps = cachePropsBox.values.last;

    return newCacheProps.toDomain;
  }

  @override
  Future<int> get notificationsQuotesCount async {
    final size = notificationQuotesBox.length;
    return size;
  }

  @override
  Future<int> get allQuotesCount async {
    final size = allQuotesBox.length;
    return size;
  }

  @override
  Future<int> get newQuotesCount async {
    final size = newQuotesBox.length;
    return size;
  }

  @override
  Future<void> deleteNewQuotes() async {
    await newQuotesBox.deleteAll(newQuotesBox.keys);
  }
}
