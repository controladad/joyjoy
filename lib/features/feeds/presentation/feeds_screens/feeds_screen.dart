import 'package:auto_route/auto_route.dart';
import 'package:flutter/widgets.dart';

import 'package:motivation_flutter/utils/view/layout_builder.dart';
import 'package:motivation_flutter/features/feeds/presentation/feeds_screens/feeds_phone_view.dart';

@RoutePage()
class FeedsScreen extends StatelessWidget {
  const FeedsScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return ResponsiveLayoutBuilder(
      phone: (context, child) {
        return const FeedsPhoneView();
      },
    );
  }
}
