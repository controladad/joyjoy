import 'package:flutter/material.dart';

class ExpandedSection extends StatefulWidget {
  final Widget? child;
  final bool expand;
  final Curve? curve;
  final Axis? direction;

  final Duration? duration;
  final Function(Animation animation)? listener;

  const ExpandedSection({
    required this.expand,
    this.child,
    this.duration,
    this.curve,
    this.listener,
    this.direction,
  });

  @override
  _ExpandedSectionState createState() => _ExpandedSectionState();
}

class _ExpandedSectionState extends State<ExpandedSection>
    with SingleTickerProviderStateMixin {
  late AnimationController expandController;
  late Animation<double> animation;

  @override
  void initState() {
    super.initState();
    prepareAnimations();
    _runExpandCheck();
  }

  @override
  void didUpdateWidget(ExpandedSection oldWidget) {
    super.didUpdateWidget(oldWidget);
    _runExpandCheck();
  }

  @override
  void dispose() {
    expandController.dispose();
    super.dispose();
  }

  void prepareAnimations() {
    expandController = AnimationController(
      vsync: this,
      duration: widget.duration ?? const Duration(milliseconds: 350),
    );
    animation = CurvedAnimation(
      parent: expandController,
      curve: widget.curve ?? Curves.fastOutSlowIn,
    );
    animation.addListener(() {
      if (widget.listener != null) {
        // ignore: prefer_null_aware_method_calls
        widget.listener!(animation);
      }
    });
  }

  void _runExpandCheck() {
    if (widget.expand) {
      expandController.forward();
    } else {
      expandController.reverse();
    }
  }

  @override
  Widget build(BuildContext context) {
    return SizeTransition(
      axisAlignment: 1.0,
      axis: widget.direction ?? Axis.vertical,
      sizeFactor: animation,
      child: widget.child,
    );
  }
}
