import 'dart:async';

import 'package:dartz/dartz.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:injectable/injectable.dart';
import 'package:motivation_flutter/constants/env.dart';
import 'package:motivation_flutter/features/sub_auth/domain/sub_auth_repository.dart';

part 'ad_cubit.freezed.dart';
part 'ad_state.dart';

@injectable
class AdCubit extends Cubit<AdState> {
  RewardedAd? chatRewardedAd;
  RewardedAd? globalRewardedAd;
  late StreamSubscription<void>? userSubscription;

  late ISubAuthRepository subAuthRepository;
  int _numRewardedLoadAttempts = 0;

  AdCubit(super.initialState, this.subAuthRepository) {
    _init();
    userSubscription = subAuthRepository.userUpdated.listen((event) {
      chatRewardedAd = null;
      globalRewardedAd = null;

      _init();
    });
  }

  Future<void> _init() async {
    _createRewardedAd(loadForChat: true);
    _createRewardedAd(loadForChat: false);
  }

  @override
  Future<void> close() async {
    chatRewardedAd?.dispose();
    globalRewardedAd?.dispose();
    userSubscription?.cancel();

    super.close();
  }

  Future<void> _createRewardedAd({required bool loadForChat}) async {
    final accountInfo = await subAuthRepository.accountInfo();
    final userId = accountInfo.fold(
      (l) => '',
      (r) => r.map(loggedIn: (loggedId) => loggedId.userId, guest: (_) => ''),
    );

    RewardedAd.load(
      adUnitId: loadForChat
          ? EnvConstants.admob_chat_unit_id
          : EnvConstants.admob_global_unit_id,
      request: const AdRequest(),
      rewardedAdLoadCallback: RewardedAdLoadCallback(
        onAdLoaded: (RewardedAd ad) {
          print('$ad loaded.');
          if (loadForChat) {
            chatRewardedAd = ad;
            chatRewardedAd?.setServerSideOptions(
              ServerSideVerificationOptions(userId: userId),
            );
          } else {
            globalRewardedAd = ad;
          }
          _numRewardedLoadAttempts = 0;
        },
        onAdFailedToLoad: (LoadAdError error) {
          print('RewardedAd failed to load: $error');
          if (loadForChat) {
            chatRewardedAd = null;
          } else {
            globalRewardedAd = null;
          }
          _numRewardedLoadAttempts += 1;

          if (_numRewardedLoadAttempts < 10) {
            _createRewardedAd(loadForChat: loadForChat);
          }
        },
      ),
    );
  }

  Future<bool> showRewardedAd(bool showForChat) async {
    final Completer<bool> completer = Completer();
    if (showForChat) {}
    if (chatRewardedAd == null) {
      print('Warning: attempt to show rewarded before loaded.');
      return false;
    }

    final fullScreenCallBack = FullScreenContentCallback(
      onAdShowedFullScreenContent: (RewardedAd ad) =>
          print('ad onAdShowedFullScreenContent.'),
      onAdDismissedFullScreenContent: (RewardedAd ad) {
        print('$ad onAdDismissedFullScreenContent.');
        ad.dispose();
        _createRewardedAd(loadForChat: showForChat);
        completer.complete(false);
      },
      onAdFailedToShowFullScreenContent: (RewardedAd ad, AdError error) {
        print('$ad onAdFailedToShowFullScreenContent: $error');
        ad.dispose();
        _createRewardedAd(loadForChat: showForChat);
        completer.complete(false);
      },
    );
    if (showForChat) {
      chatRewardedAd!.fullScreenContentCallback = fullScreenCallBack;
    } else {
      globalRewardedAd!.fullScreenContentCallback = fullScreenCallBack;
    }

    (showForChat ? chatRewardedAd : globalRewardedAd)?.show(
      onUserEarnedReward: (AdWithoutView ad, RewardItem reward) async {
        if (showForChat) {
          await subAuthRepository.userRewarded(reward.amount.toDouble());
        }
        completer.complete(true);
      },
    );

    if (showForChat) {
      chatRewardedAd = null;
    } else {
      globalRewardedAd = null;
    }

    return completer.future;
  }
}
