// ignore_for_file: avoid_bool_literals_in_conditional_expressions

import 'package:kt_dart/kt.dart';
import 'package:motivation_flutter/common/presentation/buttons.dart';
import 'package:motivation_flutter/common/presentation/common_presentation.dart';
import 'package:motivation_flutter/common/presentation/utils/failure_handler_widget.dart';
import 'package:motivation_flutter/features/onboarding/application/onboarding_bloc/onboarding_bloc.dart';
import 'package:motivation_flutter/features/onboarding/domain/models/onboarding_models.dart';
import 'package:motivation_flutter/features/onboarding/presentation/components/circular_categories_listview.dart';
import 'package:motivation_flutter/features/quotes/domain/models/sub_category_model.dart';

class OnboardingFavoriteCategoriesScreen extends StatelessWidget {
  final Function() onNext;
  const OnboardingFavoriteCategoriesScreen({super.key, required this.onNext});

  @override
  Widget build(BuildContext context) {
    return Builder(
      builder: (context) {
        final state = context.watch<OnboardingBloc>().state;
        final bloc = context.read<OnboardingBloc>();
        return Scaffold(
          backgroundColor: Colors.transparent,
          body: SafeArea(
            child: FailureHandlerWidget<OnboardingModel>(
              data: state.onboardingData,
              child: (r) => _Body(data: r, onNext: onNext),
              onRetry: () => bloc.add(const OnboardingEvent.tryAgain()),
            ),
          ),
        );
      },
    );
  }
}

class _Body extends StatelessWidget {
  final OnboardingModel data;
  final Function() onNext;
  const _Body({required this.data, required this.onNext});

  @override
  Widget build(BuildContext context) {
    final state = context.watch<OnboardingBloc>().state;
    final bloc = context.read<OnboardingBloc>();

    final KtList<SubCategoryModel> categories = data.categories;
    final KtList<String> selectedCategoriesIds =
        state.selectedFavoriteCategoriesIds;

    final canSubmit = selectedCategoriesIds.size >= 3;

    return Padding(
      padding: pad(0, 20, 0, 30),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          SBox(
            width: 320,
            child: Column(
              children: [
                SText(
                  context.translate.whatsYourPreferences,
                  style: TStyle.TitleLarge,
                ),
                const SBox(height: 5),
                SText(
                  context.translate
                      .tapTheCirclesWithInterestYouLikeToGetRecommendations,
                  style: TStyle.TitleSmall,
                  textAlign: TextAlign.center,
                ),
              ],
            ),
          ),
          Flexible(
            flex: 10,
            child: SingleChildScrollView(
              child: CircularCategoriesListView(
                selectedCategoriesIds: selectedCategoriesIds,
                categories: categories,
                onCategoryTap: (category) =>
                    bloc.add(OnboardingEvent.categoryClick(category.id)),
              ),
            ),
          ),
          Flexible(
            child: CustomFilledButton(
              onTap: onNext,
              isEnable: canSubmit,
              label: context.translate.submit,
              subtitle: context.translate.selectAMinimumOfThreeOptions,
            ),
          ),
        ],
      ),
    );
  }
}
