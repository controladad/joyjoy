part of 'reminders_bloc.dart';

@injectable
@freezed
class RemindersState with _$RemindersState {
  const factory RemindersState({
    required Option<Either<GeneralFailure, KtList<ReminderModel>>> data,
    required KtList<ReminderModel> reminders,
    required KtList<ReminderModel> loadingReminders,
  }) = _RemindersState;

  @factoryMethod
  factory RemindersState.init() => RemindersState(
        data: none(),
        reminders: emptyList(),
        loadingReminders: emptyList(),
      );
}
