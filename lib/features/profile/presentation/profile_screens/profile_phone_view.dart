import 'package:flutter_share/flutter_share.dart';
import 'package:in_app_review/in_app_review.dart';
import 'package:kt_dart/kt.dart';
import 'package:motivation_flutter/common/presentation/common_imports.dart';
import 'package:motivation_flutter/common/presentation/utils/failure_handler_widget.dart';
import 'package:motivation_flutter/features/profile/application/profile_bloc/profile_cubit.dart';
import 'package:motivation_flutter/features/profile/domain/models/profile_models.dart';
import 'package:motivation_flutter/features/profile/sub_features/select_author_category/select_author_category_args.dart';
import 'package:motivation_flutter/features/quotes/domain/models/saved_quote_union.dart';
import 'package:motivation_flutter/features/sub_auth/application/sub_auth_bloc/sub_auth_bloc.dart';
import 'package:url_launcher/url_launcher.dart';

class ProfilePhoneView extends StatelessWidget {
  const ProfilePhoneView({super.key});

  @override
  Widget build(BuildContext context) {
    final state = context.watch<ProfileCubit>().state;
    final bloc = context.read<ProfileCubit>();

    return CommonBackgroundContainer(
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: SafeArea(
          child: FailureHandlerWidget<ProfileModel>(
            data: state.offlineData,
            onRetry: () => bloc.init(),
            child: (r) => _Body(r),
          ),
        ),
      ),
    );
  }
}

class _Body extends StatelessWidget {
  final ProfileModel profile;
  const _Body(this.profile);

  @override
  Widget build(BuildContext context) {
    final subAuthState = context.watch<SubAuthCubit>().state;

    Future<void> launchEmailApp() async {
      try {
        await launchUrl(Uri.parse('mailto:support@chatfly.com'));
      } catch (e) {
        print(e);
      }
    }

    final List<ProfileGroupModel> groups = [
      ProfileGroupModel(
        title: context.translate.profile,
        items: [
          ProfileItemModel(
            label: context.translate.likes,
            icon: AppIcons.profileManageLikes,
            onTap: () => context.pushRoute(
              ListOfQuoteRoute(type: const SavedQuoteType.liked()),
            ),
            subTitle: profile.likedQuotes.isNotEmpty()
                ? '${profile.likedQuotes.size} ${context.translate.likes}'
                : null,
          ),
          ProfileItemModel(
            label: context.translate.myCollections,
            icon: AppIcons.profileMyCollection,
            onTap: () => context.pushRoute(const MyCollectionsRoute()),
            subTitle: profile.collections.isNotEmpty()
                ? '${profile.collections.size} ${context.translate.collections}'
                : null,
          ),
          ProfileItemModel(
            label: context.translate.preferredCategories,
            icon: AppIcons.profilePreferredTopics,
            onTap: () => context.pushRoute(
              SelectAuthorCategoryRoute(
                type: const SelectAuthorCategoryScreenType.preferred(
                  AuthorOrCategoryEnum.Category,
                ),
              ),
            ),
            subTitle: profile.preferredTopics.isNotEmpty()
                ? '${profile.preferredTopics.size} ${context.translate.categories}'
                : null,
          ),
          ProfileItemModel(
            label: context.translate.favoriteCharacters,
            icon: AppIcons.profileFavoriteAuthors,
            onTap: () => context.pushRoute(
              SelectAuthorCategoryRoute(
                type: const SelectAuthorCategoryScreenType.preferred(
                  AuthorOrCategoryEnum.Author,
                ),
              ),
            ),
            subTitle: profile.favoriteAuthors.isNotEmpty()
                ? '${profile.favoriteAuthors.size} ${context.translate.authors}'
                : null,
          ),
        ],
      ),
      ProfileGroupModel(
        title: context.translate.settings,
        items: [
          ProfileItemModel(
            label: context.translate.reminders,
            icon: AppIcons.profileReminders,
            subTitle: null,
            onTap: () => context.pushRoute(const RemindersRoute()),
          ),
          ProfileItemModel(
            label: context.translate.manageSubscription,
            icon: AppIcons.profileManageSubscription,
            subTitle: subAuthState.subscription.fold(
              () => null,
              (plan) => plan.map(
                premium: (premium) => premium.name.capitalize(),
                free: (free) => context.translate.free,
              ),
            ),
            // subTitle: profile.,
            onTap: () => context.pushRoute(const ManageSubscriptionRoute()),
          ),
          ProfileItemModel(
            label: context.translate.account,
            icon: AppIcons.profileManageAccount,
            subTitle: profile.accountInfo.map(
              loggedIn: (e) => e.email,
              guest: (_) => context.translate.signUpLogin,
            ),
            onTap: () => profile.accountInfo.map(
              loggedIn: (_) => context.pushRoute(const AccountRoute()),
              guest: (_) => context.pushRoute(LoginRoute()),
            ),
          ),
        ],
      ),
      ProfileGroupModel(
        title: context.translate.supports,
        items: [
          ProfileItemModel(
            label: context.translate.restorePurchase,
            icon: AppIcons.profileRestorePurchase,
            subTitle: null,
            onTap: () {},
          ),
          ProfileItemModel(
            label: context.translate.fAQ,
            icon: AppIcons.profileFAQ,
            subTitle: null,
            onTap: () {},
          ),
          ProfileItemModel(
            label: context.translate.customerService,
            icon: AppIcons.profileCustomerServices,
            subTitle: null,
            onTap: launchEmailApp,
          ),
        ],
      ),
      ProfileGroupModel(
        title: context.translate.appName,
        items: [
          ProfileItemModel(
            label: context.translate.rateUs,
            icon: AppIcons.profileRateUs,
            subTitle: null,
            onTap: () async {
              final InAppReview inAppReview = InAppReview.instance;

              if (await inAppReview.isAvailable()) {
                inAppReview.requestReview();
              } else {
                print("could'nt show in app review");
              }
            },
          ),
          ProfileItemModel(
            label: context.translate.referFriends,
            icon: AppIcons.profileReferFriends,
            subTitle: null,
            onTap: () => FlutterShare.share(
              title: 'Share Quote',
              text: 'Some Thing to Share',
              chooserTitle: 'Share Quote via',
            ),
          ),
        ],
      ),
    ];

    return ListView(
      children: [
        // profile.banner.fold(() => Container(), (a) => _BannerWidget(a)),
        ...groups.map(
          (group) => Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: pad(22, 17, 22, 5),
                child: SText(
                  group.title.toUpperCase(),
                  style: TStyle.LabelButton,
                ),
              ),
              Padding(
                padding: pad(12, 0, 12, 0),
                child: Column(
                  children: group.items
                      .map(
                        (item) => _ListItemWidget(item: item),
                      )
                      .toList(),
                ),
              ),
            ],
          ),
        ),
        const SBox(height: 20),
      ].toList(),
    );
  }
}


class _ListItemWidget extends StatelessWidget {
  final ProfileItemModel item;
  const _ListItemWidget({required this.item});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: pad(0, 5, 0, 5),
      child: MaterialButton(
        height: 60.w,
        onPressed: () => item.onTap(),
        padding: EdgeInsets.zero,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15),
        ),
        child: SContainer(
          height: 80,
          maxWidth: 366,
          padding: pad(18, 17, 18, 17),
          decoration: BoxDecoration(
            border: Border.all(
              width: .15,
              color: AppTheme.outline_variant_dark,
            ),
            borderRadius: BorderRadius.circular(15),
          ),
          child: Row(
            children: [
              ImageWidget(
                image: item.icon,
                height: 32,
                width: 32,
              ),
              const SBox(width: 16),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: item.subTitle == null
                    ? MainAxisAlignment.center
                    : MainAxisAlignment.spaceBetween,
                children: [
                  SText(
                    item.label.toTitleCase(),
                    style: TStyle.TitleMedium,
                  ),
                  if (item.subTitle != null)
                    SText(
                      item.subTitle!.capitalize(),
                      style: TStyle.LabelSmall,
                    ),
                ],
              ),
              const Spacer(),
              const ImageWidget(
                image: AppIcons.rightChevron,
                color: AppTheme.white,
                height: 32,
                width: 32,
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class ProfileGroupModel {
  final String title;
  final List<ProfileItemModel> items;

  ProfileGroupModel({required this.title, required this.items});
}

class ProfileItemModel {
  final String label;
  final String icon;
  final String? subTitle;
  final Function() onTap;

  ProfileItemModel({
    required this.label,
    required this.icon,
    required this.subTitle,
    required this.onTap,
  });
}
