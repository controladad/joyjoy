import 'package:injectable/injectable.dart';
import 'package:motivation_flutter/features/sub_auth/infrastructure/dtos/auth_dto.dart';
import 'package:motivation_flutter/features/sub_auth/infrastructure/dtos/paywall_dto.dart';

import 'package:motivation_flutter/features/sub_auth/infrastructure/dtos/sub_auth_dto.dart';
import 'package:motivation_flutter/features/sub_auth/infrastructure/sub_auth_api_services.dart';

abstract class ISubAuthApiDataSource {
  Future<SubAuthDto> get getData;
  Future<PayWallDto> get getPaywall;
  Future<AuthResponseDto> loginWithGoogle({
    required String accessToken,
    required String fcm,
    required String uuid,
    required String device,
  });
  Future<AuthResponseDto> loginWithApple({
    required String accessToken,
    required String fcm,
    required String uuid,
  });

  Future<bool> logout({
    required String uuid,
    required String fcm,
  });

  Future<bool> deleteAccount();
}

@Injectable(as: ISubAuthApiDataSource)
class SubAuthApiDataSourceImplementation implements ISubAuthApiDataSource {
  final ISubAuthApiService _apiService;

  SubAuthApiDataSourceImplementation(this._apiService);

  @override
  Future<bool> logout({
    required String uuid,
    required String fcm,
  }) async {
    final result = await _apiService.logout(uuid: uuid, fcm: fcm);
    final message = (result.data as Map<String, dynamic>)['message'];

    if (message == 'successful') {
      return true;
    } else {
      return false;
    }
  }

  @override
  Future<bool> deleteAccount() async {
    final result = await _apiService.deleteAccount();
    final message = (result.data as Map<String, dynamic>)['message'];

    if (message == 'successful') {
      return true;
    } else {
      return false;
    }
  }

  @override
  Future<AuthResponseDto> loginWithGoogle({
    required String accessToken,
    required String fcm,
    required String uuid,
    required String device,
  }) async {
    final result = await _apiService.loginWithGoogle(
      accessToken: accessToken,
      fcm: fcm,
      uuid: uuid,
      device: device,
    );
    return AuthResponseDto.fromJson(result.data as Map<String, dynamic>);
  }

  @override
  Future<AuthResponseDto> loginWithApple({
    required String accessToken,
    required String fcm,
    required String uuid,
  }) async {
    final result = await _apiService.loginWithApple(
      accessToken: accessToken,
      fcm: fcm,
      uuid: uuid,
    );

    return AuthResponseDto.fromJson(result.data as Map<String, dynamic>);
  }

  @override
  Future<SubAuthDto> get getData async {
    throw UnimplementedError('SubAuth get data');
  }

  @override
  // TODO: implement getPaywall
  Future<PayWallDto> get getPaywall async {
    final result = await _apiService.getPaywall();
    return PayWallDto.fromJson(result.data as Map<String, dynamic>);
  }
}
