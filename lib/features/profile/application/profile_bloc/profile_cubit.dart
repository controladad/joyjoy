import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:kt_dart/kt.dart';
import 'package:motivation_flutter/common/presentation/utils/common_functions.dart';
import 'package:motivation_flutter/core/failures.dart';
import 'package:motivation_flutter/features/profile/domain/models/update_model.dart';

import 'package:motivation_flutter/features/profile/domain/profile_repository.dart';
import 'package:motivation_flutter/features/profile/domain/models/profile_models.dart';

part 'profile_cubit.freezed.dart';
part 'profile_state.dart';

@injectable
class ProfileCubit extends Cubit<ProfileState> {
  final IProfileRepository profileRepository;
  late StreamSubscription<void>? cacheUpdatesSubscription;
  ProfileCubit(
    this.profileRepository,
    ProfileState init,
  ) : super(init) {
    cacheUpdatesSubscription =
        profileRepository.cacheUpdated.listen((event) => this.init());
  }

  @override
  Future<void> close() {
    cacheUpdatesSubscription?.cancel();
    return super.close();
  }

  Future<void> init() async {
    await _emitOfflineData();
    await _emitOnlineData();
  }

  Future<void> _emitOnlineData() async {
    final onlineData = await profileRepository.getOnlineContents;

    final loggedIn = state.offlineData.getOrNone
        .fold(() => false, (a) => a.accountInfo.isLoggedIn);

    final KtList<ProfileBannerModel> banners =
        onlineData.fold((l) => emptyList(), (r) {
      if (!loggedIn) {
        return r.banner.plusElement(ProfileBannerModel.login());
      } else {
        return r.banner;
      }
    });

    emit(
      state.copyWith(
        onlineData: some(onlineData),
        banners: banners.sortedBy((p0) => p0.sortOrder),
      ),
    );
  }

  Future<void> _emitOfflineData() async {
    final offlineData = await profileRepository.getProfile;
    emit(state.copyWith(offlineData: some(offlineData)));
  }

  Future<void> changeUserName(String name) async {
    await profileRepository.changeUserName(name);
  }

  Future<Option<UpdateModel>> get checkUpdate async {
    final result = await profileRepository.hasUpdate;
    return result.fold(
      (l) {
        return l.maybeMap(
          orElse: () => none(),
          forceUpdate: (_) => some(
            UpdateModel(
              updateAvailable: true,
              forceUpdate: true,
              googlePlayLink: none(),
              appStoreLink: none(),
              version: '',
              changeLog: none(),
            ),
          ),
        );
      },
      (r) => some(r),
    );
  }
}
