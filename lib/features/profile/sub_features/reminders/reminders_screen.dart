import 'package:kt_dart/kt.dart';
import 'package:motivation_flutter/common/presentation/buttons.dart';
import 'package:motivation_flutter/common/presentation/common_imports.dart';
import 'package:motivation_flutter/common/presentation/utils/custom_bottom_sheets.dart';
import 'package:motivation_flutter/common/presentation/utils/failure_handler_widget.dart';
import 'package:motivation_flutter/features/onboarding/presentation/components/profile_empty_state_widget.dart';
import 'package:motivation_flutter/features/profile/domain/models/reminder_model.dart';
import 'package:motivation_flutter/features/profile/sub_features/reminders/add_reminder_bottom_sheet.dart';
import 'package:motivation_flutter/features/profile/sub_features/reminders/reminders_bloc/reminders_bloc.dart';

@RoutePage()
class RemindersScreen extends StatelessWidget {
  const RemindersScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) =>
          getIt<RemindersBloc>()..add(const RemindersEvent.getReminders()),
      child: Builder(
        builder: (context) {
          final bloc = context.read<RemindersBloc>();
          final state = context.watch<RemindersBloc>().state;
          return CommonScaffoldWithAppbar(
            title: context.translate.reminders,
            subtitle:
                context.translate.adjustYourNotificationAccordingToYourTaste,
            floatingActionButton: CustomFilledButton(
              label: context.translate.addNew,
              onTap: () => showCustomBottomSheet(
                context: context,
                onClose: () {},
                child: AddOrEditReminderBottomSheets(
                  bloc: bloc,
                  onSubmit: (reminder) =>
                      bloc.add(RemindersEvent.addNewReminder(reminder)),
                ),
              ),
            ),
            body: FailureHandlerWidget<KtList<ReminderModel>>(
              data: state.data,
              onRetry: () => bloc.add(const RemindersEvent.getReminders()),
              child: (r) {
                if (r.isEmpty()) {
                  return ProfileEmptyState(
                    title: context.translate.thereIsNoCollectionHere,
                    subtitle: '',
                  );
                }
                return _Body(r);
              },
            ),
          );
        },
      ),
    );
  }
}

class _Body extends StatelessWidget {
  final KtList<ReminderModel> reminders;
  const _Body(this.reminders);

  @override
  Widget build(BuildContext context) {
    return ListView(
      padding: pad(16, 16, 16, 80),
      children: reminders.map((e) {
        return _ReminderItemWidget(e);
      }).asList(),
    );
  }
}

class _ReminderItemWidget extends StatelessWidget {
  final ReminderModel reminder;
  const _ReminderItemWidget(this.reminder);

  @override
  Widget build(BuildContext context) {
    final String label =
        reminder.categories.map((p0) => p0.name).joinToString(separator: ',');

    final bloc = context.read<RemindersBloc>();

    return GestureDetector(
      onTap: () => showCustomBottomSheet(
        context: context,
        onClose: () {},
        child: AddOrEditReminderBottomSheets(
          bloc: bloc,
          reminder: reminder,
          onSubmit: (reminder) =>
              bloc.add(RemindersEvent.addNewReminder(reminder)),
        ),
      ),
      child: SContainer(
        maxWidth: 400,
        height: 92,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
          border: Border.all(
            width: .5,
            color: AppTheme.outline_variant_dark,
          ),
        ),
        padding: pad(16, 0, 16, 0),
        margin: pad(0, 8, 0, 8),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              children: [
                const ImageWidget(
                  image: AppIcons.sampleReminderIcon,
                  height: 38,
                  width: 38,
                ),
                const SBox(width: 12),
                SBox(
                  width: 150,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SText(
                        label.toTitleCase(),
                        style: TStyle.TitleLarge,
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                      ),
                      const SBox(height: 8),
                      SText(
                        '${reminder.countPerDuration}x ${getDaysPreview(reminder.daysOfWeek.asList())}',
                        style: TStyle.LabelLarge,
                        color: AppTheme.on_surface_variant_dark,
                        maxLines: 1,
                      ),
                    ],
                  ),
                ),
              ],
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                SText(
                  '${formatDateTimeToHour(reminder.startTime)} - ${formatDateTimeToHour(reminder.endTime)}',
                  style: TStyle.LabelLarge,
                  color: AppTheme.on_surface_variant_dark,
                ),
                Switch(
                  value: reminder.isActive,
                  onChanged: (value) =>
                      bloc.add(RemindersEvent.toggleReminder(reminder)),
                  activeColor: AppTheme.on_primary_dark,
                  activeTrackColor: AppTheme.primary_dark,
                  inactiveTrackColor: AppTheme.surface_variant_dark,
                  // activeThumbImage: state.loadingReminders.map((p0) => p0.id).contains(reminder.id) ? null : ,
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
