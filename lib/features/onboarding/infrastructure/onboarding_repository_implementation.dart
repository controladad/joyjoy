import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:kt_dart/kt.dart';
import 'package:motivation_flutter/common/infrastructure/try_cache_block.dart';
import 'package:motivation_flutter/core/failures.dart';

import 'package:motivation_flutter/features/onboarding/infrastructure/onboarding_data_source.dart';
import 'package:motivation_flutter/features/onboarding/domain/onboarding_repository.dart';
import 'package:motivation_flutter/features/onboarding/domain/models/onboarding_models.dart';

import 'package:motivation_flutter/shared/infrastructure/shared_data_source.dart';

@Injectable(as: IOnboardingRepository)
class OnboardingRepositoryImplementation implements IOnboardingRepository {
  final IOnboardingApiDataSource dataSource;
  final ISharedDataSource sharedDataSource;

  OnboardingRepositoryImplementation(
    this.dataSource,
    this.sharedDataSource,
  );

  @override
  Future<Either<GeneralFailure, OnboardingModel>> get getData async {
    return tryCacheBlock(() async {
      return dataSource.getData;
    });
  }

  @override
  Future<Either<GeneralFailure, Option<String>>> setUserName(
    String name,
  ) async {
    return tryCacheBlock(() async {
      final accountInfo = await sharedDataSource.getAccountInfo();
      final result = await sharedDataSource.updateAccountInfo(
        accountInfo.copyWith(name: name.isNotEmpty ? some(name) : none()),
      );
      return result.name;
    });
  }

  @override
  Future<Either<GeneralFailure, KtList<String>>> toggleFavoriteAuthor(
    String authorId,
  ) {
    final result = tryCacheBlock(
      () async => sharedDataSource.toggleFavoriteAuthor(authorId),
    );
    return result;
  }

  @override
  Future<Either<GeneralFailure, KtList<String>>> toggleFavoriteCategory(
    String categoryId,
  ) {
    final result = tryCacheBlock(
      () async => sharedDataSource.toggleFavoriteCategory(categoryId),
    );
    return result;
  }

  @override
  Future<Either<GeneralFailure, OnboardingCacheDataModel>>
      get getCacheData async {
    final cacheData =
        await tryCacheBlock(() async => sharedDataSource.getCacheData);
    return cacheData.fold((l) => left(l), (r) {
      return right(
        OnboardingCacheDataModel(
          selectedFavoriteCategories: r.favoriteCategoriesIds,
          selectedFavoriteAuthors: r.favoriteAuthorsIds,
          userName: r.account.name,
          didTryChat: r.didTryChat,
          shouldShowOnboarding: r.shouldShowOnboarding,
          loggedIn: r.account.map(loggedIn: (e) => true, guest: (e) => false),
        ),
      );
    });
  }

  @override
  Future<Either<GeneralFailure, void>> get userDidTryChat async =>
      tryCacheBlock(() => sharedDataSource.userDidTryChat);

  @override
  Future<Either<GeneralFailure, void>> get userPassedOnboarding async =>
      tryCacheBlock(() => sharedDataSource.userPassedOnboarding);
}
