import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:motivation_flutter/common/presentation/common_imports.dart';
import 'package:motivation_flutter/core/failures.dart';

import 'package:motivation_flutter/core/network/network_info.dart';
import 'package:motivation_flutter/features/chat/domain/chat_status.dart';

import 'package:motivation_flutter/features/chat/infrastructure/dtos/chat_dto.dart';
import 'package:motivation_flutter/features/chat/infrastructure/chat_data_source.dart';
import 'package:motivation_flutter/features/chat/domain/chat_repository.dart';
import 'package:motivation_flutter/features/chat/domain/models/chat_models.dart';
import 'package:motivation_flutter/shared/domain/shared_cache_props_model.dart';
import 'package:motivation_flutter/shared/infrastructure/shared_data_source.dart';

@Injectable(as: IChatRepository)
class ChatRepositoryImplementation implements IChatRepository {
  final INetworkInfo _networkInfo;
  final IChatApiDataSource _chatDataSource;
  final ISharedDataSource _sharedDataSource;

  ChatRepositoryImplementation(
    this._networkInfo,
    this._chatDataSource,
    this._sharedDataSource,
  );

  @override
  Future<Either<GeneralFailure, ChatModel>> get getChatList async {
    if (await _networkInfo.isConnected == false) {
      return left(const GeneralFailure.noNetwork());
    }
    try {
      final ChatDto dto = await _chatDataSource.getData;
      final ChatModel result = dto.toDomain();

      return right(result);
    } catch (e) {
      return left(const GeneralFailure.unexpected());
    }
  }

  @override
  Future<Either<GeneralFailure, ChatModel>> get clearMessages =>
      throw UnimplementedError();

  @override
  Future<Either<GeneralFailure, MessageResponseModel>> sendMessage({
    required MessageModel message,
    required List<MessageModel> prevMessageList,
  }) async {
    return tryCacheBlock(() async {
      final ChatResponseParentDto responseParentDto = await _chatDataSource
          .sendMessage(message: message, prevMessageList: prevMessageList);

      final String response = responseParentDto.data.response ?? '';
      final String id = responseParentDto.data.id;
      final int remainingCredit = responseParentDto.data.remainToken;
      await _sharedDataSource.updateChatCredit(remainingCredit.toDouble());

      return MessageResponseModel(
        message: MessageModel(
          id: id,
          dateTime: DateTime.now(),
          sender: Sender.Server,
          text: response,
        ),
        chatStatus: const ChatStatus.onboardingTrialFinished(),
      );
    });
  }

  @override
  // TODO: implement stopGenerating
  Future<Either<GeneralFailure, ChatModel>> get stopGenerating =>
      throw UnimplementedError();

  @override
  Future<Either<GeneralFailure, ChatNeededDataModel>> get chatNeededData {
    return tryCacheBlock(
      () async {
          final name =(await _sharedDataSource.getAccountInfo()).name;
        return ChatNeededDataModel(
        name: name.fold(() => '', (a) => a),
        exampleList: [
          'How will I reward myself?',
          'Is there any book you recommend?',
          'What stops or reduces my motivation?',
          'How can I overcome those obstacles?',
          'Who can support me?'
        ],
      );
      },
    );
  }

  @override
  Future<Either<GeneralFailure, double>> get chatCredit {
    return tryCacheBlock(() async {
      final cacheProps = await _sharedDataSource.getCacheData;
      return cacheProps.account.map(
        loggedIn: (value) => value.credits,
        guest: (value) => 0,
      );
    });
  }

  @override
  Stream<SharedCachePropsModel?> get cacheUpdated {
    return _sharedDataSource.cacheUpdated;
  }

  @override
  Future<Either<GeneralFailure, Unit>> toggleResponseFeedback({
    required String id,
    required bool isLike,
  }) {
    return tryCacheBlock(
      () async {
        await _chatDataSource.sendFeedback(id: id, isLike: isLike);
        return unit;
      },
    );
  }
}
