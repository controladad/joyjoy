import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:kt_dart/kt.dart';
import 'package:motivation_flutter/features/theme/domain/models/dynamic_background_union.dart';

part 'theme_model.freezed.dart';

@freezed
class ThemeModel with _$ThemeModel {
  const factory ThemeModel({
    required String themeId,
    required String name,
    required String tag,
    required KtList<DynamicBackgroundUnion> backGrounds,
    required String fontFamily,
    required Color textColor,
    required Option<Shadow> textShadow,
  }) = _ThemeModel;

  const ThemeModel._();
  factory ThemeModel.defaultTheme() => ThemeModel(
        themeId: 'default',
        name: 'Default',
        tag: 'linear',
        backGrounds: [
          const DynamicBackgroundUnion.gradient(
            [Color(0xffE0CDFF), Color(0xffFFC8C5)],
          ),
          const DynamicBackgroundUnion.gradient(
            [Color(0xffE0FFCD), Color(0xffFFC8C5)],
          ),
          const DynamicBackgroundUnion.gradient(
            [Color(0xffCDFFF6), Color(0xffFFC8C5)],
          ),
          const DynamicBackgroundUnion.gradient(
            [Color(0xffFFCDCD), Color(0xffC5FFCE)],
          ),
          const DynamicBackgroundUnion.gradient(
            [Color(0xffCDE1FF), Color(0xffFFC8C5)],
          ),
          const DynamicBackgroundUnion.gradient(
            [Color(0xffFFCDFD), Color(0xffFFC8C5)],
          ),
          const DynamicBackgroundUnion.gradient(
            [Color(0xffFFDBCD), Color(0xffFFC8C5)],
          ),
        ].kt,
        fontFamily: 'Bosan',
        textColor: Colors.black,
        textShadow: none(),
      );
}
