part of 'theme_bloc.dart';

@freezed
class ThemeEvent with _$ThemeEvent {
  const factory ThemeEvent.getThemes() = _FetchData;
  const factory ThemeEvent.selectTheme(String themeId) = _SelectTheme;
}
