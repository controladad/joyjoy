part of 'watch_ads_data_bloc.dart';

@injectable
@freezed
class WatchAdsDataState with _$WatchAdsDataState {
  const factory WatchAdsDataState({
    required Option<WhatShouldDoAfterSeenSomeQuoteType>
        whatShouldDoAfterSeenQuote,
  }) = _WatchAdsDataState;
  @factoryMethod
  factory WatchAdsDataState.init() =>
      WatchAdsDataState(whatShouldDoAfterSeenQuote: none());
}

enum WhatShouldDoAfterSeenSomeQuoteType { inAppReview, ads,nothing }
