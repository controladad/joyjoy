import 'package:intl/intl.dart';
import 'package:kt_dart/kt.dart';
import 'package:motivation_flutter/common/presentation/buttons.dart';
import 'package:motivation_flutter/common/presentation/common_presentation.dart';
import 'package:motivation_flutter/features/profile/domain/models/reminder_model.dart';
import 'package:motivation_flutter/features/profile/sub_features/reminders/reminders_bloc/reminders_bloc.dart';
import 'package:motivation_flutter/features/profile/sub_features/select_author_category/select_author_category_args.dart';

class AddOrEditReminderBottomSheets extends StatefulWidget {
  final RemindersBloc bloc;
  final ReminderModel? reminder;
  final Function(ReminderModel reminder) onSubmit;
  const AddOrEditReminderBottomSheets({
    required this.onSubmit,
    required this.bloc,
    this.reminder,
  });

  @override
  State<AddOrEditReminderBottomSheets> createState() =>
      _AddOrEditReminderBottomSheetsState();
}

class _AddOrEditReminderBottomSheetsState
    extends State<AddOrEditReminderBottomSheets> {
  ReminderModel reminder = ReminderModel.empty();

  @override
  void initState() {
    super.initState();
    if (widget.reminder != null) {
      reminder = widget.reminder!.copyWith(isActive: true);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SText(
          context.translate.newReminder,
          style: TStyle.TitleLarge,
        ),
        const SBox(height: 10),
        Padding(
          padding: pad(16, 0, 16, 0),
          child: Column(
            children: [
              _BottomSheetsListItemWidget(
                onTap: () => context.pushRoute(
                  SelectAuthorCategoryRoute(
                    type: SelectAuthorCategoryScreenType.reminderTopic(
                      onSubmit: (categories) {
                        setState(() {
                          reminder = reminder.copyWith(categories: categories);
                        });
                      },
                      selectedCategoriesIds:
                          reminder.categories.map((p0) => p0.id),
                    ),
                  ),
                ),
                label: context.translate.category,
                action: Row(
                  children: [
                    SContainer(
                      maxWidth: 170,
                      child: SText(
                        reminder.categories
                            .map((p0) => p0.name.capitalize())
                            .joinToString(separator: ', '),
                        style: TStyle.TitleMedium,
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                    const ImageWidget(
                      image: AppIcons.rightChevron,
                      height: 24,
                      width: 24,
                      color: AppTheme.white,
                    )
                  ],
                ),
              ),
              _BottomSheetsListItemWidget(
                label: context.translate.startAt,
                action: Row(
                  children: [
                    _CounterButton(
                      onChanged: () {
                        setState(() {
                          reminder = reminder.copyWith(
                            startTime: reminder.startTime
                                .subtract(const Duration(hours: 1)),
                          );
                        });
                      },
                      isEnable: true,
                      type: _CounterType.Minus,
                    ),
                    Padding(
                      padding: pad(10, 0, 10, 0),
                      child: SText(
                        formatDateTimeToHour(reminder.startTime),
                        style: TStyle.BodyLarge,
                      ),
                    ),
                    _CounterButton(
                      onChanged: () {
                        setState(() {
                          reminder = reminder.copyWith(
                            startTime: reminder.startTime
                                .add(const Duration(hours: 1)),
                          );
                        });
                      },
                      isEnable: true,
                      type: _CounterType.Plus,
                    ),
                  ],
                ),
              ),
              _BottomSheetsListItemWidget(
                label: context.translate.endAt,
                action: Row(
                  children: [
                    _CounterButton(
                      isEnable: true,
                      type: _CounterType.Minus,
                      onChanged: () {
                        setState(() {
                          reminder = reminder.copyWith(
                            endTime: reminder.endTime
                                .subtract(const Duration(hours: 1)),
                          );
                        });
                      },
                    ),
                    Padding(
                      padding: pad(10, 0, 10, 0),
                      child: SText(
                        formatDateTimeToHour(reminder.endTime),
                        style: TStyle.BodyLarge,
                      ),
                    ),
                    _CounterButton(
                      isEnable: true,
                      type: _CounterType.Plus,
                      onChanged: () {
                        setState(() {
                          reminder = reminder.copyWith(
                            endTime:
                                reminder.endTime.add(const Duration(hours: 1)),
                          );
                        });
                      },
                    ),
                  ],
                ),
              ),
              _BottomSheetsListItemWidget(
                label: context.translate.howMany,
                action: Row(
                  children: [
                    _CounterButton(
                      isEnable: reminder.countPerDuration > 1,
                      type: _CounterType.Minus,
                      onChanged: () {
                        setState(() {
                          reminder = reminder.copyWith(
                            countPerDuration: reminder.countPerDuration - 1,
                          );
                        });
                      },
                    ),
                    SBox(
                      width: 75,
                      child: Center(
                        child: SText(
                          '${reminder.countPerDuration}x',
                          style: TStyle.BodyLarge,
                        ),
                      ),
                    ),
                    _CounterButton(
                      isEnable: reminder.countPerDuration < 100,
                      type: _CounterType.Plus,
                      onChanged: () {
                        setState(() {
                          reminder = reminder.copyWith(
                            countPerDuration: reminder.countPerDuration + 1,
                          );
                        });
                      },
                    ),
                  ],
                ),
              ),
              _BottomSheetsListItemWidget(
                label: context.translate.repeat,
                action: _DaysOfWeekWidget(
                  selectedDays: reminder.daysOfWeek,
                  onDaySelect: (day) {
                    setState(() {
                      reminder = reminder.copyWith(
                        daysOfWeek: reminder.daysOfWeek.contains(day)
                            ? reminder.daysOfWeek.minusElement(day)
                            : reminder.daysOfWeek.plusElement(day),
                      );
                    });
                  },
                ),
              ),
            ],
          ),
        ),
        const SBox(height: 20),
        BlocProvider.value(
          value: widget.bloc,
          child: Builder(
            builder: (context) {
              final bloc = context.read<RemindersBloc>();
              return CustomFilledButton(
                label: context.translate.save,
                onTap: () {
                  if (widget.reminder != null) {
                    bloc.add(RemindersEvent.editReminder(reminder));
                  } else {
                    bloc.add(RemindersEvent.addNewReminder(reminder));
                  }
                  Navigator.pop(context);
                },
                isEnable: reminder.categories.isNotEmpty() &&
                    reminder.daysOfWeek.isNotEmpty() &&
                    reminder.startTime.hour <= reminder.endTime.hour,
              );
            },
          ),
        ),
        const SBox(height: 32),
      ],
    );
  }
}

class _DaysOfWeekWidget extends StatelessWidget {
  final KtList<DaysOfWeek> selectedDays;
  final Function(DaysOfWeek day) onDaySelect;
  const _DaysOfWeekWidget({
    required this.selectedDays,
    required this.onDaySelect,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: DaysOfWeek.values.map(
        (e) {
          final bool isSelected = selectedDays.contains(e);
          return GestureDetector(
            onTap: () => onDaySelect(e),
            child: Container(
              margin: pad(2, 0, 2, 0),
              height: 32,
              width: 32,
              decoration: BoxDecoration(
                gradient: isSelected ? AppGradients.primaryDark : null,
                shape: BoxShape.circle,
                border: Border.all(
                  color: isSelected
                      ? AppTheme.primary_dark
                      : AppTheme.outline_dark,
                ),
              ),
              child: Center(
                child: SText(
                  e.name[0],
                  style: TStyle.BodyLarge,
                  color: isSelected
                      ? AppTheme.inverse_on_surface_dark
                      : AppTheme.primary_dark,
                ),
              ),
            ),
          );
        },
      ).toList(),
    );
  }
}

enum _CounterType { Plus, Minus }

class _CounterButton extends StatelessWidget {
  final Function() onChanged;
  final bool isEnable;
  final _CounterType type;
  const _CounterButton({
    required this.onChanged,
    required this.isEnable,
    required this.type,
  });

  @override
  Widget build(BuildContext context) {
    late String icon;
    switch (type) {
      case _CounterType.Plus:
        icon = AppIcons.plus;
      case _CounterType.Minus:
        icon = AppIcons.minus;
    }
    return GestureDetector(
      onTap: isEnable ? onChanged : null,
      child: SContainer(
        height: 32,
        width: 32,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          border: Border.all(
            color: isEnable ? AppTheme.primary_dark : AppTheme.outline_dark,
          ),
        ),
        child: ImageWidget(
          height: 13,
          width: 13,
          image: icon,
          color: isEnable ? AppTheme.primary_dark : AppTheme.outline_dark,
        ),
      ),
    );
  }
}

class _BottomSheetsListItemWidget extends StatelessWidget {
  final String label;
  final Widget action;
  final Function()? onTap;
  const _BottomSheetsListItemWidget({
    required this.label,
    required this.action,
    this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: SContainer(
        maxWidth: 400,
        decoration: const BoxDecoration(
          border: Border(
            bottom: BorderSide(color: AppTheme.outline_variant_dark, width: .5),
          ),
        ),
        padding: pad(16, 16, 0, 16),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Flexible(
              child: SText(
                label.toTitleCase(),
                style: TStyle.TitleMedium,
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
              ),
            ),
            action,
          ],
        ),
      ),
    );
  }
}

String formatDateTimeToHour(DateTime date) => DateFormat('h:mm a').format(date);
