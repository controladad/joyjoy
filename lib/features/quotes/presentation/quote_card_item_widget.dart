import 'dart:ui';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter_tts/flutter_tts.dart';
import 'package:motivation_flutter/common/presentation/common_presentation.dart';
import 'package:motivation_flutter/features/quotes/application/quotes_bloc/quotes_bloc.dart';
import 'package:motivation_flutter/features/quotes/domain/models/quotes_model.dart';
import 'package:motivation_flutter/features/quotes/presentation/dynamic_background.dart';
import 'package:motivation_flutter/features/quotes/presentation/share_screen.dart';
import 'package:motivation_flutter/features/theme/domain/models/dynamic_background_union.dart';

class QuoteItemWidget extends StatefulWidget {
  final QuoteModel quote;
  final bool frontCard;
  final TransformClass transform;
  final TransformClass endTransform;
  final AnimationController controller;
  final double height;
  final double width;
  final bool isShare;
  final int themeIndex;
  final Future<void> Function() onReport;
  final void Function(int newIdx) onColorChanged;
  const QuoteItemWidget({
    super.key,
    required this.quote,
    required this.transform,
    required this.endTransform,
    required this.controller,
    required this.frontCard,
    required this.onColorChanged,
    this.height = 400,
    this.width = 415,
    this.isShare = false,
    required this.themeIndex,
    required this.onReport,
  });

  @override
  State<QuoteItemWidget> createState() => _QuoteItemWidgetState();
}

enum TtsState { playing, stopped, paused, continued }

class _QuoteItemWidgetState extends State<QuoteItemWidget>
    with TickerProviderStateMixin {
  late Animation<TransformClass> animation;
  late int themeIndex;

  @override
  void initState() {
    super.initState();
    themeIndex = widget.themeIndex;
  }

  void changeTheme() {
    setState(() {
      themeIndex += 1;
    });
    widget.onColorChanged(themeIndex);
  }

  @override
  Widget build(BuildContext context) {
    animation = TransformClassTween(
      begin: widget.transform,
      end: widget.endTransform,
    ).animate(CurvedAnimation(parent: widget.controller, curve: Curves.easeIn));

    final state = context.watch<QuotesBloc>().state;
    final bloc = context.read<QuotesBloc>();

    DynamicBackgroundUnion getTheme() {
      final backgrounds = state.theme.backGrounds;
      return backgrounds[themeIndex % backgrounds.size];
    }

    final isLiked = state.likedQuotesIds.contains(widget.quote.id);

    return GestureDetector(
      onTap: changeTheme,
      child: AnimatedBuilder(
        animation: animation,
        builder: (context, child) => Transform.translate(
          offset: animation.value.translate,
          child: Stack(
            alignment: Alignment.centerLeft,
            children: [
              Opacity(
                opacity: animation.value.opacity,
                child: Transform.rotate(
                  angle: degreeToRadian(animation.value.rotate),
                  child: Center(
                    child: DynamicBackgroundWidget(
                      height: widget.height,
                      width: widget.width,
                      radius: 4,
                      background: getTheme(),
                      child: Container(),
                    ),
                  ),
                ),
              ),
              if (widget.frontCard)
                Row(
                  children: [
                    const SBox(width: 30),
                    Column(
                      children: [
                        if (!widget.isShare) const SBox(height: 40),
                        SContainer(
                          width: 300,
                          height: 350,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              if (widget.isShare) const Spacer(),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    height: 200,
                                    margin: pad(0, 0, 20, 0),
                                    child: Center(
                                      child: GestureDetector(
                                        onTap: () => bloc.add(
                                          const QuotesEvent.changeFont(),
                                        ),
                                        child: AutoSizeText(
                                          widget.quote.quote,
                                          maxLines: 8,
                                          presetFontSizes: const [
                                            100,
                                            64,
                                            50,
                                            32,
                                            28,
                                            26,
                                            24,
                                            22,
                                            16,
                                          ],
                                          style: TextStyle(
                                            color: state.theme.textColor,
                                            fontFamily: state.theme.fontFamily,
                                            shadows: state.theme.textShadow
                                                .fold(() => null, (a) => [a]),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                  if (widget.quote.author != null)
                                    TextButton(
                                      onPressed: () {
                                        // final subAuthCubit =
                                        //     context.read<SubAuthCubit>();
                                        // showAuthorInfoBottomSheets(
                                        //   author: widget.quote.author,
                                        //   context: context,
                                        //   SubAuthCubit: subAuthCubit,
                                        // );
                                      },
                                      child: SText(
                                        '- ${widget.quote.author!.name}',
                                        style: TStyle.TitleMedium,
                                        color: state.theme.textColor,
                                      ),
                                    ),
                                ],
                              ),
                              if (!widget.isShare)
                                Row(
                                  children: [
                                    const SBox(width: 50),
                                    IconButton(
                                      padding: EdgeInsets.zero,
                                      onPressed: () {
                                        HapticFeedback.lightImpact();
                                        Navigator.push(
                                          context,
                                          PageRouteBuilder(
                                            opaque: false,
                                            pageBuilder: (
                                              context,
                                              animation,
                                              secondaryAnimation,
                                            ) {
                                              return QuoteShareScreen(
                                                quote: widget.quote,
                                                themeIndex: themeIndex,
                                                onColorChanged: changeTheme,
                                                onReport: widget.onReport,
                                              );
                                            },
                                            transitionDuration: Duration.zero,
                                            reverseTransitionDuration:
                                                Duration.zero,
                                          ),
                                        );
                                      },
                                      icon: ImageWidget(
                                        image: AppIcons.share,
                                        height: 48,
                                        width: 48,
                                        color: state.theme.textColor,
                                      ),
                                    ),
                                    _TtsWidget(
                                      quote: widget.quote.quote,
                                      iconColor: state.theme.textColor,
                                    ),
                                    IconButton(
                                      padding: EdgeInsets.zero,
                                      onPressed: () => bloc.add(
                                        QuotesEvent.likeQuote(widget.quote),
                                      ),
                                      icon: ImageWidget(
                                        image: isLiked
                                            ? AppIcons.filledHeart
                                            : AppIcons.heart,
                                        height: 48,
                                        width: 48,
                                        color: state.theme.textColor,
                                      ),
                                    )
                                  ],
                                ),
                              if (!widget.isShare) const SBox(height: 10),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
            ],
          ),
        ),
      ),
    );
  }
}

class TransformClass {
  final double rotate;
  final Offset translate;
  final double opacity;

  TransformClass({
    required this.rotate,
    required this.translate,
    required this.opacity,
  });

  TransformClass copyWith({
    double? rotate,
    Offset? translate,
    double? opacity,
  }) {
    return TransformClass(
      rotate: rotate ?? this.rotate,
      translate: translate ?? this.translate,
      opacity: opacity ?? this.opacity,
    );
  }
}

class TransformClassTween extends Tween<TransformClass> {
  TransformClassTween({
    required TransformClass begin,
    required TransformClass end,
  }) : super(begin: begin, end: end);

  @override
  TransformClass lerp(double t) => TransformClass(
        rotate: lerpDouble(begin!.rotate, end!.rotate, t)!,
        translate: Offset.lerp(begin!.translate, end!.translate, t)!,
        opacity: lerpDouble(begin!.opacity, end!.opacity, t)!,
      );
}

class _TtsWidget extends StatefulWidget {
  final String quote;
  final Color iconColor;
  const _TtsWidget({required this.quote, required this.iconColor});

  @override
  State<_TtsWidget> createState() => __TtsWidgetState();
}

class __TtsWidgetState extends State<_TtsWidget> {
  late FlutterTts flutterTts;
  TtsState ttsState = TtsState.stopped;

  @override
  void initState() {
    super.initState();
    initTts();
  }

  @override
  void dispose() {
    super.dispose();
    flutterTts.stop();
  }

  Future<void> initTts() async {
    flutterTts = FlutterTts();

    await flutterTts.awaitSpeakCompletion(true);

    await flutterTts.setVolume(.2);
    await flutterTts.setSpeechRate(.4);
    await flutterTts.setPitch(1);

    flutterTts.setStartHandler(() {
      setState(() {
        ttsState = TtsState.playing;
      });
    });

    flutterTts.setCompletionHandler(() {
      setState(() {
        ttsState = TtsState.stopped;
      });
    });

    flutterTts.setCancelHandler(() {
      setState(() {
        ttsState = TtsState.stopped;
      });
    });

    flutterTts.setContinueHandler(() {
      setState(() {
        ttsState = TtsState.continued;
      });
    });
  }

  Future _stop() async {
    final result = await flutterTts.stop();
    if (result == 1) setState(() => ttsState = TtsState.stopped);
  }

  Future _speak() async {
    await flutterTts.speak(widget.quote);
  }

  @override
  Widget build(BuildContext context) {
    return IconButton(
      padding: EdgeInsets.zero,
      onPressed: () {
        HapticFeedback.lightImpact();
        ttsState == TtsState.stopped ? _speak() : _stop();
      },
      icon: ImageWidget(
        image: ttsState == TtsState.stopped ? AppIcons.play : AppIcons.pause,
        height: 48,
        width: 48,
        color: widget.iconColor,
      ),
    );
  }
}
