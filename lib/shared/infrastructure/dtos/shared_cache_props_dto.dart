import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:motivation_flutter/features/profile/domain/models/account_info_entity.dart';
import 'package:motivation_flutter/features/profile/domain/models/collection_entity.dart';
import 'package:motivation_flutter/features/profile/domain/models/reminder_entity.dart';
import 'package:motivation_flutter/features/theme/domain/models/dynamic_background_entity.dart';
import 'package:motivation_flutter/features/theme/domain/models/theme_entity.dart';
import 'package:motivation_flutter/shared/domain/shared_cache_props_entity.dart';
import 'package:motivation_flutter/shared/domain/shared_cache_props_model.dart';
import 'package:motivation_flutter/shared/infrastructure/dtos/sub_category_dto.dart';

part 'shared_cache_props_dto.g.dart';
part 'shared_cache_props_dto.freezed.dart';

@freezed
class SharedCachePropsDto with _$SharedCachePropsDto {
  @JsonSerializable(fieldRename: FieldRename.snake)
  factory SharedCachePropsDto({
    List<ReminderDto>? reminders,
    List<String>? favoriteCategoriesIds,
    List<String>? favoriteAuthorsIds,
    List<String>? likedQuotesIds,
    List<String>? historyQuotesIds,
    List<CollectionDto>? collections,
    required AccountDto account,
    String? versionDate,
    bool? isFirst,
    String? selectedThemeId,
    bool? shouldShowOnboarding,
    bool? didTryChat,
    List<ThemeDto>? themes,
  }) = _SharedCachePropsDto;
  const SharedCachePropsDto._();
  factory SharedCachePropsDto.fromJson(Map<String, dynamic> json) =>
      _$SharedCachePropsDtoFromJson(json);

  factory SharedCachePropsDto.fromEntity(SharedCachePropsEntity entity) =>
      SharedCachePropsDto(
        account: AccountDto.fromEntity(
          entity.account ?? AccountInfoEntity(isGuest: true),
        ),
        collections: entity.collections
            ?.map((e) => CollectionDto.fromEntity(e))
            .toList(),
        didTryChat: entity.didTryChat,
        favoriteAuthorsIds: entity.favoriteAuthorsIds,
        favoriteCategoriesIds: entity.favoriteCategoriesIds,
        historyQuotesIds: entity.quotesHistoryIds,
        isFirst: entity.isFirst,
        likedQuotesIds: entity.likedQuotesIds,
        reminders:
            entity.reminders?.map((e) => ReminderDto.fromEntity(e)).toList(),
        selectedThemeId: entity.selectedThemeId,
        shouldShowOnboarding: entity.shouldShowOnboarding,
        themes: entity.themes?.map((e) => ThemeDto.fromEntity(e)).toList(),
        versionDate: entity.versionDate.toString(),
      );

  SharedCachePropsEntity toEntity() {
    return SharedCachePropsEntity(
      reminders: reminders?.map((e) => e.toEntity()).toList() ?? [],
      favoriteAuthorsIds: favoriteAuthorsIds ?? [],
      favoriteCategoriesIds: favoriteCategoriesIds ?? [],
      versionDate: DateTime.tryParse(versionDate ?? '') ?? DateTime.now(),
      likedQuotesIds: likedQuotesIds ?? [],
      collections: collections?.map((e) => e.toEntity()).toList() ?? [],
      quotesHistoryIds: historyQuotesIds ?? [],
      didTryChat: didTryChat ?? false,
      shouldShowOnboarding: shouldShowOnboarding ?? false,
      themes: themes?.map((e) => e.toEntity()).toList() ?? [],
      selectedThemeId: 'default',
    );
  }

  SharedCachePropsModel toDomain() {
    return toEntity().toDomain;
  }
}

@freezed
class ReminderDto with _$ReminderDto {
  factory ReminderDto({
    @JsonKey(name: 'reminder_id') required String reminderId,
    @JsonKey(name: 'categories') required List<SubCategoryDto> categories,
    @JsonKey(name: 'start_time') required String startTime,
    @JsonKey(name: 'end_time') required String endTime,
    @JsonKey(name: 'count_per_duration') required int countPerDuration,
    @JsonKey(name: 'alarm_sound_id') required String alarmSoundId,
    @JsonKey(name: 'days_of_week') required List<int> daysOfWeek,
    @JsonKey(name: 'is_active') required bool isActive,
  }) = _ReminderDto;
  const ReminderDto._();
  factory ReminderDto.fromJson(Map<String, dynamic> json) =>
      _$ReminderDtoFromJson(json);

  factory ReminderDto.fromEntity(ReminderEntity entity) => ReminderDto(
        reminderId: entity.id,
        categories:
            entity.categories.map((e) => SubCategoryDto.fromEntity(e)).toList(),
        startTime: entity.startTime.toString(),
        endTime: entity.endTime.toString(),
        countPerDuration: entity.countPerDuration,
        alarmSoundId: entity.alarmSoundId,
        daysOfWeek: entity.daysOfWeek,
        isActive: entity.isActive,
      );

  ReminderEntity toEntity() {
    return ReminderEntity(
      id: reminderId,
      categories: categories.map((e) => e.toEntity()).toList(),
      startTime: DateTime.parse(startTime),
      endTime: DateTime.parse(endTime),
      countPerDuration: countPerDuration,
      alarmSoundId: alarmSoundId,
      daysOfWeek: daysOfWeek,
      isActive: isActive,
    );
  }
}

@freezed
class CollectionDto with _$CollectionDto {
  @JsonSerializable(fieldRename: FieldRename.snake)
  factory CollectionDto({
    required String collectionId,
    required String name,
    required List<String> quotesIds,
  }) = _CollectionDto;
  const CollectionDto._();
  factory CollectionDto.fromJson(Map<String, dynamic> json) =>
      _$CollectionDtoFromJson(json);

  factory CollectionDto.fromEntity(CollectionEntity entity) => CollectionDto(
        collectionId: entity.id,
        name: entity.name,
        quotesIds: entity.quotesIds,
      );

  CollectionEntity toEntity() {
    return CollectionEntity(id: collectionId, name: name, quotesIds: quotesIds);
  }
}

@freezed
class ThemeDto with _$ThemeDto {
  @JsonSerializable(fieldRename: FieldRename.snake)
  factory ThemeDto({
    required String themeId,
    required String name,
    required String tag,
    required List<DynamicBackgroundDto> backgrounds,
    @JsonKey(name: 'font_family') required String fontFamily,
  }) = _ThemeDto;
  const ThemeDto._();
  factory ThemeDto.fromJson(Map<String, dynamic> json) =>
      _$ThemeDtoFromJson(json);

  factory ThemeDto.fromEntity(ThemeEntity entity) => ThemeDto(
        themeId: entity.themeId,
        name: entity.name,
        tag: entity.tag,
        backgrounds: entity.backgrounds
            .map((e) => DynamicBackgroundDto.fromEntity(e))
            .toList(),
        fontFamily: entity.fontFamily,
      );

  ThemeEntity toEntity() {
    return ThemeEntity(
      themeId: themeId,
      name: name,
      tag: tag,
      backgrounds: backgrounds.map((e) => e.toEntity()).toList(),
      fontFamily: fontFamily,
    );
  }
}

@freezed
class DynamicBackgroundDto with _$DynamicBackgroundDto {
  @JsonSerializable(fieldRename: FieldRename.snake)
  factory DynamicBackgroundDto({
    int? color,
    List<int>? gradient,
    List<int>? image,
  }) = _DynamicBackgroundDto;
  const DynamicBackgroundDto._();
  factory DynamicBackgroundDto.fromJson(Map<String, dynamic> json) =>
      _$DynamicBackgroundDtoFromJson(json);
  factory DynamicBackgroundDto.fromEntity(DynamicBackgroundEntity entity) =>
      DynamicBackgroundDto(
        color: entity.color,
        gradient: entity.gradient,
        image: entity.image,
      );

  DynamicBackgroundEntity toEntity() {
    return DynamicBackgroundEntity(
      color: color,
      gradient: gradient,
      image: image,
    );
  }
}

@freezed
class AccountDto with _$AccountDto {
  @JsonSerializable(fieldRename: FieldRename.snake)
  factory AccountDto({
    required bool isGuest,
    String? userName,
    String? accountStatus,
    String? signupDate,
    String? premiumPlan,
    String? email,
  }) = _AccountDto;
  const AccountDto._();
  factory AccountDto.fromJson(Map<String, dynamic> json) =>
      _$AccountDtoFromJson(json);

  factory AccountDto.fromEntity(AccountInfoEntity entity) {
    if (entity.isGuest) {
      return AccountDto(isGuest: true, userName: entity.name);
    } else {
      return AccountDto(
        isGuest: false,
        accountStatus: entity.accountStatus,
        email: entity.email,
        premiumPlan: entity.premiumPlan,
        signupDate: entity.signupDate.toString(),
        userName: entity.name,
      );
    }
  }

  AccountInfoEntity toEntity() {
    if (isGuest) {
      return AccountInfoEntity(isGuest: true, name: userName);
    } else {
      return AccountInfoEntity(
        isGuest: false,
        name: userName,
        accountStatus: accountStatus,
        email: email,
        signupDate: DateTime.tryParse(signupDate ?? ''),
        premiumPlan: premiumPlan,
      );
    }
  }
}
