import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:motivation_flutter/common/presentation/common_presentation.dart';

import 'package:motivation_flutter/features/language/domain/models/language_model.dart';
import 'package:motivation_flutter/features/quotes/application/watch_ads_data/watch_ads_data_bloc.dart';
import 'package:motivation_flutter/features/user/domain/models/user_facade.dart';
import 'package:motivation_flutter/features/user/domain/models/user_watch_ads_entity.dart';
import 'package:motivation_flutter/features/user/infrastructure/user_local_data.dart';

@Injectable(as: IUserFacade)
class UserFacadeImp implements IUserFacade {
  final IUserData _userData;

  UserFacadeImp(this._userData);

  @override
  Option<LanguageModel> get currentLanguage =>
      optionOf(_userData.language.toNullable()?.toDomain());

  @override
  Future<void> storeUserLocale({
    required LanguageModel languageModel,
  }) async {
    await _userData.storeLanguage(languageModel: languageModel);
  }

  @override
  DateTime? get getRemainTimeForPhone {
    final DateTime? remainTime = _userData.getRemailTimeForVerification;
    return remainTime;
  }

  @override
  Future<WhatShouldDoAfterSeenSomeQuoteType> persistWatchAdsData({
    bool? seenInAppReview,
    bool? anotherQuoteSeen,
  }) async {
    await _userData.persistWatchAdsData(
      anotherQuoteSeen: anotherQuoteSeen,
      showInAppReview: seenInAppReview,
    );
    final UserWatchAdsEntity? userWatchAdsEntity =
        _userData.getUserWatchAdsData;

    return whatShouldDoAfterSeenSomeQuoteType(userWatchAdsEntity);
  }

  @override
  Future<WhatShouldDoAfterSeenSomeQuoteType> get getWatchAdsData async {
    final UserWatchAdsEntity? userWatchAdsEntity =
        _userData.getUserWatchAdsData;
    return whatShouldDoAfterSeenSomeQuoteType(userWatchAdsEntity);
  }

  WhatShouldDoAfterSeenSomeQuoteType whatShouldDoAfterSeenSomeQuoteType(
    UserWatchAdsEntity? userWatchAdsEntity,
  ) {
    if (userWatchAdsEntity != null) {
      final seenQuotesNumber = userWatchAdsEntity.seenQuoteNumber!;
      if (seenQuotesNumber == 5 &&
          userWatchAdsEntity.showInAppReviewBefore == null) {
        return WhatShouldDoAfterSeenSomeQuoteType.inAppReview;
      } else if (seenQuotesNumber > 10 &&
          seenQuotesNumber % Constants.freeSeenQuoteNumber == 0) {
        return WhatShouldDoAfterSeenSomeQuoteType.ads;
      }
    }
    return WhatShouldDoAfterSeenSomeQuoteType.nothing;
  }
}
