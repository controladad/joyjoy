import 'package:intl/intl.dart';
import 'package:motivation_flutter/common/presentation/buttons.dart';
import 'package:motivation_flutter/common/presentation/common_imports.dart';
import 'package:motivation_flutter/common/presentation/loaders.dart';
import 'package:motivation_flutter/features/profile/domain/models/user_subscription_model.dart';
import 'package:motivation_flutter/features/sub_auth/application/sub_auth_bloc/sub_auth_bloc.dart';

@RoutePage()
class ManageSubscriptionScreen extends StatelessWidget {
  const ManageSubscriptionScreen();

  @override
  Widget build(BuildContext context) {
    final state = context.watch<SubAuthCubit>().state;
    final subscription = state.subscription;

    return Builder(
      builder: (context) {
        return CommonScaffoldWithAppbar(
          title: context.translate.manageSubscription,
          floatingActionButton: CustomFilledButton(
            label: context.translate.goPremium,
            onTap: () => context.pushRoute(
              PaywallRoute(onClose: (ctx) {}),
            ),
          ),
          body: subscription.fold(
            () => const Center(
              child: CommonLoader(),
            ),
            (a) => _Body(subscription: a),
          ),
        );
      },
    );
  }
}

class _Body extends StatelessWidget {
  final UserSubscription subscription;
  const _Body({required this.subscription});

  @override
  Widget build(BuildContext context) {
    final dateFormat = DateFormat('d MMM y');
    return subscription.map(
      premium: (p) => ListView(
        padding: pad(16, 0, 16, 0),
        children: [
          _ItemWidget(
            label: context.translate.plan,
            value: context.translate.premium.toUpperCase(),
          ),
          _ItemWidget(
            label: context.translate.type,
            value: p.name,
          ),
          _ItemWidget(
            label: context.translate.startDate,
            value: dateFormat.format(p.startDate),
          ),
          _ItemWidget(
            label: context.translate.nextBillingDate,
            value: dateFormat.format(p.nextBillingDate),
          ),
          TextButton(
            onPressed: () {},
            child: SText(
              context.translate.cancelRenew,
              style: TStyle.LabelButton,
              color: AppTheme.secondary_container_dark,
            ),
          ),
        ],
      ),
      free: (f) => ListView(
        padding: pad(16, 0, 16, 0),
        children: [
          _ItemWidget(
            label: context.translate.plan,
            value: context.translate.free.toUpperCase(),
          ),
          f.lastPlanStartDate.fold(
            () => Container(),
            (a) => _ItemWidget(
              label: context.translate.lastPremiumPlanStartDate,
              value: dateFormat.format(a),
            ),
          ),
          f.lastPlanEndDate.fold(
            () => Container(),
            (a) => _ItemWidget(
              label: context.translate.endDate,
              value: dateFormat.format(a),
            ),
          ),
        ],
      ),
    );
  }
}

class _ItemWidget extends StatelessWidget {
  final String label;
  final String value;
  const _ItemWidget({required this.label, required this.value});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: pad(12, 16, 12, 16),
      decoration: const BoxDecoration(
        border: Border(
          bottom: BorderSide(width: 0.5, color: AppTheme.outline_variant_dark),
        ),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          SText('$label:'.toTitleCase(), style: TStyle.TitleMedium),
          SText(value, style: TStyle.BodyLarge),
        ],
      ),
    );
  }
}
