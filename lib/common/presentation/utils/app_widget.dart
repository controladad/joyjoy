import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:motivation_flutter/common/application/ad_cubit.dart';

import 'package:motivation_flutter/common/presentation/common_imports.dart';
import 'package:motivation_flutter/features/profile/application/profile_bloc/profile_cubit.dart';
import 'package:motivation_flutter/features/profile/sub_features/my_collection/my_collection_bloc/my_collection_bloc.dart';
import 'package:motivation_flutter/features/quotes/application/quotes_bloc/quotes_bloc.dart';
import 'package:motivation_flutter/features/quotes/application/watch_ads_data/watch_ads_data_bloc.dart';
import 'package:motivation_flutter/features/quotes/domain/models/quotes_screen_type.dart';
import 'package:motivation_flutter/features/sub_auth/application/sub_auth_bloc/sub_auth_bloc.dart';
import 'package:motivation_flutter/utils/router/app_router.dart';
import 'package:package_info_plus/package_info_plus.dart';

extension LocalizationsX on BuildContext {
  AppLocalizations get translate {
    return AppLocalizations.of(this)!;
  }
}

final GlobalKey<ScaffoldMessengerState> snackbarKey =
    GlobalKey<ScaffoldMessengerState>();

PackageInfo? globalPackageInfo;

class AppWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: () {
        HapticFeedback.lightImpact();
        primaryFocus?.unfocus();
      },
      child: KeyboardVisibilityProvider(
        child: LayoutBuilder(
          builder: (
            BuildContext context,
            BoxConstraints constraints,
          ) {
            return OrientationBuilder(
              builder: (BuildContext context, Orientation orientation) {
                SizeConfig().init(constraints, orientation);

                return MultiBlocProvider(
                  providers: [
                    BlocProvider(
                      create: (context) => getIt<WatchAdsDataBloc>(),
                    ),
                    BlocProvider(
                      create: (context) => getIt<SubAuthCubit>()..init(),
                    ),
                    // BlocProvider(
                    //   create: (context) {
                    //     final bloc = getIt<ExploreBloc>();
                    //     bloc.add(const ExploreEvent.getCategories());
                    //     return bloc;
                    //   },
                    // ),
                    BlocProvider(
                      create: (context) => getIt<ProfileCubit>()..init(),
                    ),
                    BlocProvider(
                      create: (context) => getIt<AdCubit>(),
                    ),
                    BlocProvider(
                      create: (context) => getIt<QuotesBloc>()
                        ..add(
                          const QuotesEvent.fetchData(QuotesScreenType.feeds()),
                        ),
                    ),
                    BlocProvider(
                      create: (context) => getIt<MyCollectionBloc>()
                        ..add(const MyCollectionEvent.getCollections()),
                    )
                  ],
                  child: Builder(
                    builder: (context) {
                      return const _MaterialApp();
                    },
                  ),
                );
              },
            );
          },
        ),
      ),
    );
  }
}

class _MaterialApp extends StatefulWidget {
  const _MaterialApp();

  @override
  State<_MaterialApp> createState() => __MaterialAppState();
}

class __MaterialAppState extends State<_MaterialApp> {
  final _rootRouter = getIt<AppRouter>();

  @override
  void initState() {
    forceBlocToStart<SubAuthCubit>(context);
    forceBlocToStart<AdCubit>(context);

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // final languageCubitState = context.watch<LanguageCubit>().state;
    // final supportedLocales = languageCubitState.supportedLanguages.asList().map(
    //       (language) => Locale(
    //         language.locale!,
    //         language.code,
    //       ),
    //     );

    const localeDelegates = [
      AppLocalizations.delegate,
      GlobalMaterialLocalizations.delegate,
      GlobalWidgetsLocalizations.delegate,
      GlobalCupertinoLocalizations.delegate
    ];
    return ScreenUtilInit(
      designSize: SizeConfig.designSize,
      minTextAdapt: true,
      splitScreenMode: true,
      builder: (context, child) => MaterialApp.router(
        scaffoldMessengerKey: snackbarKey,
        debugShowCheckedModeBanner: false,
        // localeResolutionCallback: (deviceLocale, _) => _localeResCallback(
        //   deviceLocale,
        //   _,
        //   languageCubitState,
        // ),
        // locale: Locale(languageCubitState.currentLanguage.locale!),
        // supportedLocales: supportedLocales,
        localizationsDelegates: localeDelegates,
        onGenerateTitle: (BuildContext context) => context.translate.appName,
        theme: AppTheme.getThemeData(
          // languageCubitState.currentLanguage.locale ?? 'en',
          'en',
        ),
        routerDelegate: AutoRouterDelegate(
          _rootRouter,
          navigatorObservers: () => [
            AutoRouteObserver(),
          ],
        ),
        routeInformationParser: _rootRouter.defaultRouteParser(),
      ),
    );
  }
}
