part of 'quotes_bloc.dart';

@injectable
@freezed
class QuotesState with _$QuotesState {
  const factory QuotesState({
    required Option<String> userName,
    required Option<Either<GeneralFailure, KtList<QuoteModel>>> data,
    required Option<Either<GeneralFailure, Unit>> reportData,
    required Option<Either<GeneralFailure, SharedCachePropsModel>> cacheData,
    required ThemeModel theme,
    required KtList<QuoteModel> allQuotes,
    required KtList<QuoteModel> remainingQuotes,
    required KtList<QuoteModel> previousQuotes,
    required KtList<QuoteModel> visibleQuotes,
    required KtList<String> likedQuotesIds,
    required bool hasPrevious,
    required bool isLoading,
  }) = _QuotesState;
  @factoryMethod
  factory QuotesState.init() => QuotesState(
        userName: none(),
        data: none(),
        cacheData: none(),
        theme: ThemeModel.defaultTheme(),
        allQuotes: emptyList(),
        remainingQuotes: emptyList(),
        previousQuotes: emptyList(),
        visibleQuotes: emptyList(),
        likedQuotesIds: emptyList(),
        hasPrevious: false,
        isLoading: true,
        reportData: none(),
      );
}
