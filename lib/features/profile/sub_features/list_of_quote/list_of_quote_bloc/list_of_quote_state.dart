part of 'list_of_quote_bloc.dart';

@injectable
@freezed
class ListOfQuoteState with _$ListOfQuoteState {
  const factory ListOfQuoteState({
    required Option<Either<GeneralFailure, KtList<QuoteModel>>>
        listOfQuoteResultsData,
    required KtList<QuoteModel> results,
    required bool isLoading,
    required Option<int> total,
    required Option<String> searchQuery,
    required RefreshController controller,
  }) = _ListOfQuoteState;

  @factoryMethod
  factory ListOfQuoteState.init() => ListOfQuoteState(
        listOfQuoteResultsData: none(),
        searchQuery: none(),
        controller: RefreshController(initialLoadStatus: LoadStatus.canLoading),
        isLoading: false,
        results: emptyList(),
        total: none(),
      );
}
