import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:kt_dart/kt.dart';
import 'package:motivation_flutter/common/presentation/common_presentation.dart';
import 'package:motivation_flutter/core/failures.dart';
import 'package:motivation_flutter/features/profile/domain/models/user_subscription_model.dart';
import 'package:motivation_flutter/features/sub_auth/domain/models/paywall_model.dart';
import 'package:motivation_flutter/features/sub_auth/domain/sub_auth_repository.dart';

part 'paywall_cubit.freezed.dart';
part 'paywall_state.dart';

@injectable
class PaywallCubit extends Cubit<PaywallState> {
  final ISubAuthRepository repository;
  PaywallCubit(super.init, this.repository);

  Future<void> initialize() async {
    emit(state.copyWith(data: none()));
    final result = await repository.getPaywall;
    final subscription = await repository.getSubscription();
    subscription.fold(
      (l) => null,
      (r) => emit(state.copyWith(currentSubscription: some(r))),
    );
    emit(state.copyWith(data: some(result)));
    result.fold((l) => null, (r) {
      emit(
        state.copyWith(
          paywallModel: some(r),
          selectedSubscription: some(r.plans.first()),
        ),
      );
    });
  }

  Future<void> selectPlan(
    PayWallSubscriptionPlanModel plan,
  ) async {
    emit(state.copyWith(selectedSubscription: some(plan)));
  }

  Future<void> changePlan(
    PayWallSubscriptionPlanModel plan,
  ) async {
    final lastSub = state.currentSubscription;
    emit(state.copyWith(currentSubscription: none()));
    final result = await repository.changeSubscription(plan);
    result.fold(
      (l) => emit(state.copyWith(currentSubscription: lastSub)),
      (r) => emit(state.copyWith(currentSubscription: some(r))),
    );
  }
}
