import 'package:freezed_annotation/freezed_annotation.dart';

part 'chat_status.freezed.dart';

@freezed
class ChatStatus with _$ChatStatus {
  const factory ChatStatus.limitReached() = _LimitReached;
  const factory ChatStatus.subscriptionEnd() = _SubscriptionEnd;
  const factory ChatStatus.onboardingTrialFinished() = _OnboardingTrialFinished;
  const factory ChatStatus.limitedChat({required int remainingCharacters}) =
      LimitedChat;
  const factory ChatStatus.unlimitedChat() = _UnlimitedChat;
}
