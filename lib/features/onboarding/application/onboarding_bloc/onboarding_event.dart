part of 'onboarding_bloc.dart';

@freezed
class OnboardingEvent with _$OnboardingEvent {
  const factory OnboardingEvent.getData() = _GetData;
  const factory OnboardingEvent.tryAgain() = _TryAgain;
  const factory OnboardingEvent.changeUsersName(String value) =
      _ChangeUsersName;
  const factory OnboardingEvent.selectFeelingItem(FeelingClass feeling) =
      _SelectFeelingItem;
  const factory OnboardingEvent.writeFeeling(String value) = _WriteFeeling;
  const factory OnboardingEvent.submitName() = _SubmitName;
  const factory OnboardingEvent.passedFeeling() = _SubmitFeeling;
  const factory OnboardingEvent.passedPaywall() = _PassedPaywall;
  const factory OnboardingEvent.categoryClick(
    String categoryId,
  ) = _CategoryClick;
  const factory OnboardingEvent.characterClick(
    String characterId,
  ) = _CharacterClick;
}
