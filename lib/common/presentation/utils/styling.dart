import 'dart:math';

import 'package:motivation_flutter/common/presentation/common_presentation.dart';

extension StyleX on BuildContext {
  TextTheme get textTheme {
    return Theme.of(this).textTheme;
  }
}

class AppTheme {
  AppTheme._();
  static const primary_light = Color(0xff4519A2);
  static const primary_dark = Color(0xffC8AEFF);
  static const on_primary_dark = Color(0xff28007D);
  static const primary_10 = Color(0xff36003E);
  static const primary_20 = Color(0xff580064);
  static const primary_30 = Color(0xff72227D);
  static const primary_40 = Color(0xff8E3D97);
  static const primary_50 = Color(0xffAA56B2);
  static const primary_60 = Color(0xffC770CE);
  static const primary_70 = Color(0xffE48AEB);
  static const primary_80 = Color(0xffFBAAFF);
  static const primary_90 = Color(0xffFFD6FC);
  static const primary_95 = Color(0xffFFEBFB);
  static const primary_99 = Color(0xffFFFBFF);

  static const secondary_light = Color(0xffFE5F55);
  static const secondary_dark = Color(0xffFFB597);
  static const secondary_container_dark = Color(0xffD34F2B);
  static const secondary_10 = Color(0xff360F00);
  static const secondary_20 = Color(0xff591D00);
  static const secondary_30 = Color(0xff7D2D00);
  static const secondary_40 = Color(0xffA43D00);
  static const secondary_50 = Color(0xffCD4E00);
  static const secondary_60 = Color(0xffF26419);
  static const secondary_70 = Color(0xffFF8C59);
  static const secondary_80 = Color(0xffFFB597);
  static const secondary_90 = Color(0xffFFDBCD);
  static const secondary_95 = Color(0xffFFEDE7);

  static const tertiary = Color(0xff008D86);
  static const tertiary_10 = Color(0xff00201F);
  static const tertiary_20 = Color(0xff003736);
  static const tertiary_30 = Color(0xff00504F);
  static const tertiary_40 = Color(0xff006A69);
  static const tertiary_50 = Color(0xff008584);
  static const tertiary_60 = Color(0xff00A1A0);
  static const tertiary_70 = Color(0xff1FBEBC);
  static const tertiary_80 = Color(0xff4DDAD8);
  static const tertiary_90 = Color(0xff6FF7F4);
  static const tertiary_95 = Color(0xffAEFFFD);

  static const error = Color(0xffFFB4AB);
  static const error_40 = Color(0xffBA1A1A);
  static const on_error_dark = Color(0xff690005);
  static const on_error_container_dark = Color(0xff93000A);

  static const white = Color(0xffFFFFFF);
  static const black = Color(0xff000000);
  static const blue = Color(0xff4285F4);

  static const neutral = Color(0xffBA1A1A);
  static const neutral_10 = Color(0xff1E1A1D);
  static const neutral_70 = Color(0xffB1A9AD);
  static const neutral_variant = Color(0xffBA1A1A);

  static const background_light = Color(0xffFFFBFF);
  static const background_dark = Color(0xff0E0722);

  static const on_background_light = Color(0xff1E1A1D);
  static const on_background_dark = Color(0xffE9E0E4);

  static const surface_light = Color(0xffFCFAFF);
  static const surface_dark = Color(0xff110828);
  static const surface1_dark = Color(0xffFBAAFF);
  static const surface3_dark = Color(0xffFBAAFF);

  static const surface_5_dark = Color(0xffFBAAFF);
  static const on_surface_light = Color(0xff1E1A1D);
  static const on_surface_dark = Color(0xffE9E0E4);
  static const on_outline_dark = Color(0xff998D96);
  static const outline_dark = Color(0xff998D96);
  static const outline_variant_dark = Color(0xff49454F);

  static const surface_variant_light = Color(0xffEDDFE8);
  static const surface_variant_dark = Color(0xff4D444C);

  static const on_surface_variant_light = Color(0xff4D444C);
  static const on_surface_variant_dark = Color(0xffD0C3CC);

  static const inverse_on_surface_dark = Color(0xff1E1A1D);
  static const inverse_primary_dart = Color(0xff6750A4);

  static const color1 = Color(0xff210F18);

  static ThemeData getThemeData(String languageCode) {
    return ThemeData(
      useMaterial3: true,
      scaffoldBackgroundColor: AppTheme.color1,
      brightness: Brightness.dark,
      textTheme: lightTextTheme,
    );
  }

  static final TextTheme lightTextTheme = TextTheme(
    displayLarge: _displayLarge,
    displayMedium: _displayMedium,
    displaySmall: _displaySmall,
    headlineLarge: _headlineLarge,
    headlineMedium: _headlineMedium,
    headlineSmall: _headlineSmall,
    titleLarge: _titleLarge,
    titleMedium: _titleMedium,
    titleSmall: _titleSmall,
    labelLarge: _labelLarge,
    labelMedium: _labelMedium,
    labelSmall: _labelSmall,
    bodyLarge: _bodyLarge,
    bodyMedium: _bodyMedium,
    bodySmall: _bodySmall,
  );

  static const TextStyle englishFont = TextStyle(
    color: AppTheme.on_surface_dark,
    fontFamily: 'Bosan',
  );

  static final TextStyle _displayLarge = englishFont.copyWith(
    fontSize: 57 * SizeConfig.textMultiplier,
    fontWeight: FontWeight.w100,
  );
  static final TextStyle _displayMedium = englishFont.copyWith(
    fontSize: 45 * SizeConfig.textMultiplier,
    fontWeight: FontWeight.w100,
  );
  static final TextStyle _displaySmall = englishFont.copyWith(
    fontSize: 36 * SizeConfig.textMultiplier,
    fontWeight: FontWeight.w100,
  );
  static final TextStyle _headlineLarge = englishFont.copyWith(
    fontSize: 32 * SizeConfig.textMultiplier,
    fontWeight: FontWeight.w100,
  );
  static final TextStyle _headlineMedium = englishFont.copyWith(
    fontSize: 28 * SizeConfig.textMultiplier,
    fontWeight: FontWeight.w100,
  );
  static final TextStyle _headlineSmall = englishFont.copyWith(
    fontSize: 24 * SizeConfig.textMultiplier,
    fontWeight: FontWeight.w100,
  );
  static final TextStyle _titleLarge = englishFont.copyWith(
    fontSize: 22 * SizeConfig.textMultiplier,
    fontWeight: FontWeight.w100,
  );
  static final TextStyle _titleMedium = englishFont.copyWith(
    fontSize: 16 * SizeConfig.textMultiplier,
    fontWeight: FontWeight.w100,
  );
  static final TextStyle _titleSmall = englishFont.copyWith(
    fontSize: 14 * SizeConfig.textMultiplier,
    fontWeight: FontWeight.w100,
  );
  static final TextStyle _labelLarge = englishFont.copyWith(
    fontSize: 14 * SizeConfig.textMultiplier,
    fontWeight: FontWeight.w100,
  );
  static final TextStyle _labelMedium = englishFont.copyWith(
    fontSize: 16 * SizeConfig.textMultiplier,
    fontWeight: FontWeight.w400,
  );
  static final TextStyle _labelSmall = englishFont.copyWith(
    fontSize: 11 * SizeConfig.textMultiplier,
    fontWeight: FontWeight.w100,
  );
  static final TextStyle _bodyLarge = englishFont.copyWith(
    fontSize: 16 * SizeConfig.textMultiplier,
    fontWeight: FontWeight.w100,
  );
  static final TextStyle _bodyMedium = englishFont.copyWith(
    fontSize: 14 * SizeConfig.textMultiplier,
    fontWeight: FontWeight.w100,
  );
  static final TextStyle _bodySmall = englishFont.copyWith(
    fontSize: 12 * SizeConfig.textMultiplier,
    fontWeight: FontWeight.w100,
  );
}

TextStyle? getTextStyle({
  required TStyle style,
  required BuildContext context,
}) {
  switch (style) {
    case TStyle.DisplayLarge:
      return context.textTheme.displayLarge;
    case TStyle.DisplayMedium:
      return context.textTheme.displayMedium;
    case TStyle.DisplaySmall:
      return context.textTheme.displaySmall;
    case TStyle.HeadlineLarge:
      return context.textTheme.headlineLarge;
    case TStyle.HeadlineMedium:
      return context.textTheme.headlineMedium;
    case TStyle.HeadlineSmall:
      return context.textTheme.headlineSmall;
    case TStyle.TitleLarge:
      return context.textTheme.titleLarge;
    case TStyle.TitleMedium:
      return context.textTheme.titleMedium;
    case TStyle.TitleSmall:
      return context.textTheme.titleSmall;
    case TStyle.LabelLarge:
      return context.textTheme.labelLarge;
    case TStyle.LabelMedium:
      return context.textTheme.bodySmall;
    case TStyle.LabelButton:
      return context.textTheme.labelMedium;
    case TStyle.LabelSmall:
      return context.textTheme.labelSmall;
    case TStyle.BodyLarge:
      return context.textTheme.bodyLarge;
    case TStyle.BodyMedium:
      return context.textTheme.bodyMedium;
    case TStyle.BodySmall:
      return context.textTheme.bodySmall;
  }
}

enum TStyle {
  DisplayLarge,
  DisplayMedium,
  DisplaySmall,
  HeadlineLarge,
  HeadlineMedium,
  HeadlineSmall,
  TitleLarge,
  TitleMedium,
  TitleSmall,
  LabelLarge,
  LabelMedium,
  LabelSmall,
  LabelButton,
  BodyLarge,
  BodyMedium,
  BodySmall,
}

abstract class AppGradients {
  const AppGradients._();
  static final backGround = LinearGradient(
    transform: const GradientRotation(pi / 4),
    colors: [
      const Color(0xff210F18),
      AppTheme.surface_dark.withOpacity(.9),
    ],
  );

  static final primaryLight = createGradient(
    colors: [const Color(0xff8E3D97), const Color(0xff4519A2)],
  );
  static final primaryDark = createGradient(
    colors: [const Color(0xffFBAAFF), const Color(0xffC8AEFF)],
  );
  static final onPrimaryDark = createGradient(
    colors: [const Color(0xff580064), const Color(0xff28007D).withOpacity(.9)],
  );
  static final secondaryContainer = createGradient(
    colors: [const Color(0xffBE6028), const Color(0xffEC3B30)],
  );

  static final chatBubble = createGradient(
    colors: [const Color(0xff8E3D97), const Color(0xff4519A2)],
    degree: 25,
    stops: [0, .5],
  );

  static final secondary = createGradient(
    colors: [const Color(0xffDA580B), const Color(0xffFE5F55)],
    degree: 90,
  );
  static final surface5Dark = createGradient(
    colors: [const Color(0xffDA580B), const Color(0xffFE5F55)],
    degree: 90,
  );
  static final surface3Dark = createGradient(
    colors: [const Color(0xff210F18), const Color(0xff110828).withOpacity(.9)],
    degree: 90,
  );
}

abstract class AppBoxShadow {
  const AppBoxShadow._();
}
