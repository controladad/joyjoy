import 'package:flutter/widgets.dart';

import 'package:motivation_flutter/utils/view/layout_builder.dart';
import 'package:motivation_flutter/features/sub_auth/presentation/sub_auth/sub_auth_phone_view.dart';

class SubAuthScreen extends StatelessWidget {
  const SubAuthScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return ResponsiveLayoutBuilder(
      phone: (context, child) {
        return const SubAuthPhoneView();
      },
    );
  }
}
