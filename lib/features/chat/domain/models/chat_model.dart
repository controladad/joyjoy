import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:kt_dart/kt.dart';
import 'package:motivation_flutter/features/chat/domain/chat_status.dart';
import 'package:uuid/uuid.dart';

part 'chat_model.freezed.dart';

@freezed
class ChatModel with _$ChatModel {
  const factory ChatModel({
    required String id,
    required KtList<MessageModel> messages,
  }) = _ChatModel;
}

@freezed
class MessageResponseModel with _$MessageResponseModel {
  const factory MessageResponseModel({
    required MessageModel message,
    required ChatStatus chatStatus,
  }) = _MessageResponseModel;
}

@freezed
class MessageModel with _$MessageModel {
  const factory MessageModel({
    required String id,
    required String text,
    required DateTime dateTime,
    required Sender sender,
    bool? isLiked,
  }) = _MessageModel;

  const MessageModel._();
  factory MessageModel.clientMessage(String message) => MessageModel(
        id: const Uuid().v4(),
        text: message,
        dateTime: DateTime.now(),
        sender: Sender.Client,
      );

  factory MessageModel.loading(String id) => MessageModel(
        id: id,
        text: '@loading1234',
        dateTime: DateTime.now(),
        sender: Sender.Server,
      );
  factory MessageModel.errorHandlerMessage(String message) => MessageModel(
        id: const Uuid().v4(),
        text: message,
        dateTime: DateTime.now(),
        sender: Sender.Server,
      );
}

enum Sender {
  Client,
  Server,
}

@freezed
class ChatNeededDataModel with _$ChatNeededDataModel {
  factory ChatNeededDataModel({
    required String name,
    required List<String> exampleList,
  }) = _ChatNeededDataModel;
}
