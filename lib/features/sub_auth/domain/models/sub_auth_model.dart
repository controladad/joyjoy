import 'package:freezed_annotation/freezed_annotation.dart';

part 'sub_auth_model.freezed.dart';

@freezed
class SubAuthModel with _$SubAuthModel {
  const factory SubAuthModel({
    required bool isAuth,
    required bool hasSubscription,
    required bool hasTokenToChat,
    required QuoteStatus chatStatus,
    required QuoteStatus quoteStatus,
  }) = _SubAuthModel;
}

enum QuoteStatus {
  Ad,
  Open,
  Auth,
  Close,
}
