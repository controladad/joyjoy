import 'dart:async';
import 'dart:developer';

import 'package:firebase_core/firebase_core.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:motivation_flutter/common/presentation/common_presentation.dart';
import 'package:motivation_flutter/core/firebase/push_notification_services.dart';
import 'package:motivation_flutter/core/local_notification_services.dart';
import 'package:motivation_flutter/features/sub_auth/domain/subscription_service.dart';
import 'package:motivation_flutter/utils/environment_config.dart';
import 'package:package_info_plus/package_info_plus.dart';

class AppBlocObserver extends BlocObserver {
  @override
  void onChange(BlocBase bloc, Change change) {
    super.onChange(bloc, change);
    log('onChange( \n\r ${bloc.state}, \n\r current: ${change.currentState}, \n\r next: ${change.nextState} \n\r) \n\r');
  }

  @override
  void onError(BlocBase bloc, Object error, StackTrace stackTrace) {
    super.onError(bloc, error, stackTrace);
    log('onError( \n\r ${bloc.runtimeType}, \n\r $error, \n\r $stackTrace \n\r) \n\r');
  }

  @override
  void onEvent(Bloc bloc, Object? event) {
    super.onEvent(bloc, event);
    log('onEvent( \n\r $bloc, $event \n\r) \n\r');
  }
}

Future<void> bootstrap(FutureOr<Widget> Function() builder) async {
  if (EnvironmentConfig.runTest) {
    //TODO: Run test
  }

  await runZonedGuarded(
    () async {
      await initialize();

      await SystemChrome.setPreferredOrientations(
        [DeviceOrientation.portraitUp],
      );

      SystemChrome.setSystemUIOverlayStyle(
        SystemUiOverlayStyle.dark.copyWith(
          statusBarColor: AppTheme.color1,
          statusBarIconBrightness: Brightness.light,
          statusBarBrightness: Brightness.dark,
        ),
      );

      FlutterError.onError = (details) {
        log(details.exceptionAsString(), stackTrace: details.stack);
      };

      runApp(await builder());
    },
    (error, stackTrace) => log(error.toString(), stackTrace: stackTrace),
  );
}

Future<void> initialize() async {
  WidgetsFlutterBinding.ensureInitialized();

  MobileAds.instance.initialize();

  await Firebase.initializeApp();
  await configureInjection();
  await getIt<PushNotificationService>().initialize();
  await getIt<LocalNotificationService>().initialize();

  // await Workmanager().initialize(backgroundTasks);

  // await getIt<HomeWidgetService>().initialize();
  // await getIt<ReminderNotificationService>().initialize();

  final PackageInfo packageInfo = await PackageInfo.fromPlatform();
  globalPackageInfo = packageInfo;

  await SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]);

  await getIt<ISubscriptionService>().initialize();
}
