import 'dart:async';
import 'package:motivation_flutter/bootstrap.dart';
import 'package:motivation_flutter/common/presentation/common_imports.dart';

Future<void> main() async {
  bootstrap(
    () => AppWidget(),
  );
}
