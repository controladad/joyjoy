import 'package:freezed_annotation/freezed_annotation.dart';

import 'package:motivation_flutter/features/feeds/domain/models/feeds_models.dart';

part 'feeds_dto.freezed.dart';
part 'feeds_dto.g.dart';

@freezed
class FeedsDto with _$FeedsDto {
  factory FeedsDto({
    @JsonKey(name: 'id') required String id,
  }) = _FeedsDto;

  factory FeedsDto.fromJson(Map<String, dynamic> json) =>
      _$FeedsDtoFromJson(json);
  const FeedsDto._();

  FeedsModel toDomain() => FeedsModel(id: id);
}
