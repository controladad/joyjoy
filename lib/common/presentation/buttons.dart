import 'package:motivation_flutter/common/presentation/common_imports.dart';
import 'package:motivation_flutter/common/presentation/loaders.dart';

class CustomFilledButton extends StatelessWidget {
  final double maxWidth;
  final double height;
  final double? width;
  final String label;
  final String? subtitle;
  final Widget? rightIcon;
  final Widget? leftIcon;
  final bool isEnable;
  final bool isLoading;
  final Function() onTap;
  final Gradient? gradient;
  final EdgeInsetsGeometry? padding;
  const CustomFilledButton({
    super.key,
    required this.label,
    this.gradient,
    required this.onTap,
    this.maxWidth = 330,
    this.isEnable = true,
    this.isLoading = false,
    this.height = 45,
    this.padding,
    this.subtitle,
    this.rightIcon,
    this.leftIcon,
    this.width,
  });

  @override
  Widget build(BuildContext context) {
    const borderRadius = BorderRadius.all(Radius.circular(100));
    final targetGradient = gradient ?? AppGradients.primaryLight;

    return SContainer(
      margin: padding,
      maxWidth: maxWidth,
      height: height,
      width: width,
      decoration: BoxDecoration(
        borderRadius: borderRadius,
        gradient: isEnable ? targetGradient : null,
        color: isEnable == false
            ? AppTheme.on_surface_dark.withOpacity(.12)
            : null,
      ),
      child: Card(
        elevation: 0,
        margin: EdgeInsets.zero,
        shape: const RoundedRectangleBorder(borderRadius: borderRadius),
        color: Colors.transparent,
        child: InkWell(
          borderRadius: borderRadius,
          onTap: () {
            if (isEnable && isLoading == false) {
              HapticFeedback.lightImpact();
              onTap();
            } else {
              HapticFeedback.mediumImpact();
            }
          },
          highlightColor: AppTheme.white.withOpacity(.05),
          splashColor: AppTheme.white.withOpacity(.05),
          child: Center(
            child: isLoading
                ? const Center(child: CommonLoader(size: 15))
                : Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const Spacer(),
                      if (leftIcon != null) leftIcon!,
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          SText(
                            label.toUpperCase(),
                            style: TStyle.LabelButton,
                            color: isEnable
                                ? AppTheme.white
                                : AppTheme.on_surface_dark.withOpacity(.4),
                          ),
                          if (subtitle != null)
                            SText(
                              subtitle!.capitalize(),
                              style: TStyle.LabelSmall,
                              color: isEnable
                                  ? AppTheme.white
                                  : AppTheme.on_surface_dark.withOpacity(.4),
                            ),
                        ],
                      ),
                      if (rightIcon != null) rightIcon!,
                      const Spacer(),
                    ],
                  ),
          ),
        ),
      ),
    );
  }
}

class CustomOutlinedButton extends StatelessWidget {
  final double maxWidth;
  final double height;
  final String label;
  final Widget? rightIcon;
  final Widget? leftIcon;
  final bool isEnable;
  final bool isLoading;
  final bool scaleDown;
  final Function() onTap;
  final EdgeInsetsGeometry? padding;
  const CustomOutlinedButton({
    super.key,
    required this.label,
    required this.onTap,
    this.maxWidth = 330,
    this.isEnable = true,
    this.isLoading = false,
    this.height = 45,
    this.padding,
    this.rightIcon,
    this.leftIcon,
    this.scaleDown = false,
  });

  @override
  Widget build(BuildContext context) {
    const borderRadius = BorderRadius.all(Radius.circular(100));

    return Center(
      child: SContainer(
        margin: padding,
        maxWidth: maxWidth,
        height: height,
        decoration: BoxDecoration(
          borderRadius: borderRadius,
          color: isEnable
              ? Colors.transparent
              : AppTheme.on_surface_dark.withOpacity(.12),
          border: Border.all(color: AppTheme.outline_dark),
        ),
        child: Card(
          elevation: 0,
          margin: EdgeInsets.zero,
          shape: const RoundedRectangleBorder(borderRadius: borderRadius),
          color: Colors.transparent,
          child: InkWell(
            borderRadius: borderRadius,
            onTap: () {
              if (isEnable && isLoading == false) {
                HapticFeedback.lightImpact();
                onTap();
              } else {
                HapticFeedback.mediumImpact();
              }
            },
            highlightColor: AppTheme.white.withOpacity(.05),
            splashColor: AppTheme.white.withOpacity(.05),
            child: Center(
              child: isLoading
                  ? const Center(child: CommonLoader(size: 15))
                  : Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        if (leftIcon != null)
                          Padding(
                            padding: pad(5, 0, 5, 0),
                            child: leftIcon,
                          )
                        else
                          Container(),
                        Flexible(
                          child: Padding(
                            padding: pad(
                              leftIcon != null ? 10 : 5,
                              0,
                              rightIcon != null ? 10 : 5,
                              0,
                            ),
                            child: SText(
                              label.toUpperCase(),
                              scaleDown: scaleDown,
                              style: TStyle.LabelButton,
                              color: isEnable
                                  ? AppTheme.primary_80
                                  : AppTheme.on_surface_dark.withOpacity(.4),
                            ),
                          ),
                        ),
                        if (rightIcon != null)
                          Padding(
                            padding: pad(5, 0, 5, 0),
                            child: rightIcon,
                          )
                        else
                          Container(),
                      ],
                    ),
            ),
          ),
        ),
      ),
    );
  }
}
