// import 'dart:io';

// import 'package:dartz/dartz.dart';
// // import 'package:home_widget/home_widget.dart';
// import 'package:injectable/injectable.dart';
// import 'package:motivation_flutter/common/infrastructure/common_infrastructure.dart';
// import 'package:motivation_flutter/common/presentation/common_presentation.dart';
// import 'package:motivation_flutter/core/failures.dart';

// import 'package:motivation_flutter/features/quotes/domain/models/quotes_model.dart';
// import 'package:motivation_flutter/shared/infrastructure/shared_data_source.dart';
// import 'package:workmanager/workmanager.dart';

// @singleton
// class HomeWidgetService {
//   final ISharedDataSource dataSource;

//   HomeWidgetService(this.dataSource);

//   Future<void> initialize() async {
//     await HomeWidget.setAppGroupId('joy_joy');
//     dataSource.cacheUpdated.listen((event) async {
//       final hasQuote = await dataSource.hasQuotesInCache;
//       if (hasQuote) {
//         startBackgroundTask();
//       } else {
//         stopBackgroundTask();
//       }
//     });
//   }

//   Future<void> startBackgroundTask() {
//     return Workmanager().registerPeriodicTask(
//       'update_widget',
//       'update_widget',
//       frequency: const Duration(minutes: 15),
//     );
//   }

//   Future<void> stopBackgroundTask() {
//     return Workmanager().cancelByTag('update_widget');
//   }

//   Future<Either<GeneralFailure, Unit>> nextQuote() async {
//     return tryCacheBlock(() async {
//       final nextQuote = await dataSource.getRandomQuote();
//       await changeQuote(nextQuote);
//       return unit;
//     });
//   }

//   Future<void> changeBackgroundColor(String color) async {
//     if (Platform.isAndroid) {
//       await HomeWidget.saveWidgetData<String>(
//         'color',
//         color,
//       );
//       await _updateWidget();
//     }
//   }

//   Future<void> changeQuote(QuoteModel quote) async {
//     if (Platform.isAndroid) {
//       await HomeWidget.saveWidgetData<String>(
//         'quoteId',
//         quote.author.id,
//       );
//       await HomeWidget.saveWidgetData<String>(
//         'title',
//         quote.author.name,
//       );
//       await HomeWidget.saveWidgetData<String>(
//         'message',
//         quote.quote,
//       );
//       await _updateWidget();
//     } else {

//     }
//   }

//   Future<void> _updateWidget() {
//     return HomeWidget.updateWidget(
//       name: 'HomeWidgetExampleProvider',
//       iOSName: 'HomeWidgetExample',
//     );
//   }

//   Future<String> getCurrentQuote() async {
//     final result = await HomeWidget.getWidgetData('id');
//     return result as String;
//   }
// }
