import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:motivation_flutter/common/presentation/buttons.dart';
import 'package:motivation_flutter/common/presentation/common_presentation.dart';
import 'package:motivation_flutter/common/presentation/utils/failure_handler_widget.dart';
import 'package:motivation_flutter/features/onboarding/application/onboarding_bloc/onboarding_bloc.dart';

class OnboardingUsersNameScreen extends StatelessWidget {
  final Function() onNext;
  const OnboardingUsersNameScreen({super.key, required this.onNext});

  @override
  Widget build(BuildContext context) {
    return Builder(
      builder: (context) {
        final state = context.watch<OnboardingBloc>().state;
        return Scaffold(
          backgroundColor: Colors.transparent,
          resizeToAvoidBottomInset: true,
          body: FailureHandlerWidget(
            data: state.onboardingCacheData,
            onRetry: () {},
            child: (r) => _Body(onNext: onNext),
          ),
        );
      },
    );
  }
}

class _Body extends StatefulWidget {
  final Function() onNext;
  const _Body({required this.onNext});

  @override
  State<_Body> createState() => _BodyState();
}

class _BodyState extends State<_Body> {
  bool isLogin = false;

  @override
  Widget build(BuildContext context) {
    final bloc = context.read<OnboardingBloc>();
    final state = context.watch<OnboardingBloc>().state;

    return SafeArea(
      child: Column(
        children: [
          Expanded(
            child: Column(
              children: [
                const Spacer(),
                Flexible(
                  flex: 6,
                  fit: FlexFit.tight,
                  child: Padding(
                    padding: pad(20, 0, 20, 0),
                    child: const ImageWidget(
                      image: Images.onboarding,
                      height: null,
                      width: null,
                    ),
                  ),
                ),
                Flexible(
                  flex: 2,
                  child: Column(
                    children: [
                      SText(
                        context.translate.onboardingTitle,
                        style: TStyle.TitleLarge,
                      ),
                      const Spacer(),
                      OnKeyboardUpDisappearWidget(
                        child: SizedBox(
                          width: 333,
                          child: SText(
                            context.translate.onboardingSubtitle,
                            style: TStyle.BodyMedium,
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ),
                      const Spacer(flex: 3),
                    ],
                  ),
                ),
              ],
            ),
          ),
          // if (isLogin)
          //   const _LoginSection()
          // else
          Column(
            children: [
              CustomTextField(
                padding: pad(20, 0, 20, 0),
                onChanged: (value) =>
                    bloc.add(OnboardingEvent.changeUsersName(value)),
                hasClearButton: true,
                initialValue: state.userName.toNullable(),
                label: context.translate.whatShouldICallYou,
                focusedLabel: context.translate.whatShouldICallYou,
              ),
              const SizedBox(height: 16),
              KeyboardDismissOnTap(
                dismissOnCapturedTaps: state.userName.isSome(),
                child: CustomFilledButton(
                  padding: pad(20, 0, 20, 0),
                  label: context.translate.getStarted,
                  isEnable: state.userName.isSome(),
                  onTap: () {
                    bloc.add(const OnboardingEvent.submitName());
                    widget.onNext();
                  },
                ),
              ),
            ],
          ),
          // if (keyboardIsVisible(context)) const SizedBox(height: 20),
          const SizedBox(height: 20),

          // if (isAuthenticated(context))
          //   Container(height: 50)
          // else
          //   OnKeyboardUpDisappearWidget(
          //     child: Padding(
          //       padding: pad(18, 0, 18, 0),
          //       child: Row(
          //         mainAxisAlignment: MainAxisAlignment.center,
          //         mainAxisSize: MainAxisSize.min,
          //         children: [
          //           SText(
          //             isLogin
          //                 ? context.translate.dontHaveAnAccount.capitalize()
          //                 : context.translate.alreadyHaveAnAccount.capitalize(),
          //             style: TStyle.LabelLarge,
          //           ),
          //           const SBox(width: 5),
          //           TextButton(
          //             onPressed: () {
          //               setState(() => isLogin = !isLogin);
          //             },
          //             child: SText(
          //               isLogin
          //                   ? context.translate.signUp
          //                   : context.translate.login.toUpperCase(),
          //               style: TStyle.LabelButton,
          //               color: AppTheme.primary_dark,
          //             ),
          //           ),
          //         ],
          //       ),
          //     ),
          //   )
        ],
      ),
    );
  }
}

// class _LoginSection extends StatelessWidget {
//   const _LoginSection();

//   @override
//   Widget build(BuildContext context) {
//     return BlocListener<SubAuthCubit, SubAuthState>(
//       listener: (context, state) {
//         state.successOrFailure.fold(
//           () => null,
//           (a) => a.fold(
//             (l) => showFailureSnackBar(context, l),
//             (r) => r == true
//                 ? context.pushAndRemoveAll(const SplashRoute())
//                 : false,
//           ),
//         );
//       },
//       child: Builder(
//         builder: (context) {
//           final bloc = context.read<SubAuthCubit>();
//           return Column(
//             children: [
//               if (Platform.isIOS)
//                 _Button(
//                   onTap: () => bloc.add(
//                     const SubAuthEvent.signInWithApple(),
//                   ),
//                   color: Colors.white,
//                   child: Row(
//                     children: [
//                       const ImageWidget(
//                         image: AppIcons.apple,
//                         height: 24,
//                         width: 24,
//                       ),
//                       const SBox(width: 16),
//                       SText(
//                         context.translate.continueWithApple,
//                         style: TStyle.TitleLarge,
//                         color: Colors.black,
//                         fontFamily: 'sf_pro',
//                       ),
//                     ],
//                   ),
//                 ),
//               _Button(
//                 onTap: () => bloc.add(const SubAuthEvent.signInWithGoogle()),
//                 color: const Color(0xff4285F4),
//                 child: Row(
//                   children: [
//                     const ImageWidget(
//                       image: AppIcons.google,
//                       height: 24,
//                       width: 24,
//                     ),
//                     const SBox(width: 16),
//                     SText(
//                       context.translate.continueWithGoogle,
//                       style: TStyle.TitleLarge,
//                       color: Colors.white,
//                       fontFamily: 'roboto',
//                     ),
//                   ],
//                 ),
//               ),
//               const SBox(height: 16),
//             ],
//           );
//         },
//       ),
//     );
//   }
// }

// class _Button extends StatelessWidget {
//   final Function() onTap;
//   final Widget child;
//   final Color color;
//   const _Button({
//     required this.onTap,
//     required this.child,
//     required this.color,
//   });

//   @override
//   Widget build(BuildContext context) {
//     return Padding(
//       padding: pad(16, 8, 16, 8),
//       child: MaterialButton(
//         color: color,
//         height: 52,
//         onPressed: onTap,
//         shape: RoundedRectangleBorder(
//           borderRadius: BorderRadius.circular(100),
//         ),
//         child: child,
//       ),
//     );
//   }
// }
