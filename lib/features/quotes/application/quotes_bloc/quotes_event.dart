part of 'quotes_bloc.dart';

@freezed
class QuotesEvent with _$QuotesEvent {
  const factory QuotesEvent.fetchData(QuotesScreenType type) = _FetchData;
  const factory QuotesEvent.updateCacheData({
    required ThemeModel theme,
    required KtList<String> likedQuotes,
  }) = _UpdateCacheData;
  const factory QuotesEvent.nextQuote() = _NextQuote;
  const factory QuotesEvent.previousQuote() = _PreviousQuote;
  const factory QuotesEvent.changeFont() = _ChangeFont;
  const factory QuotesEvent.seeQuote(QuoteModel quote) = _SeeQuote;
  const factory QuotesEvent.likeQuote(QuoteModel quote) = _LikeQuote;
  const factory QuotesEvent.deleteQuote(String quoteId) = _DeleteQuote;
  const factory QuotesEvent.reportQuote({
    required String quoteId,
    required String reportText,
  }) = _Report;
}
