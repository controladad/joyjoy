import 'package:motivation_flutter/common/presentation/common_presentation.dart';
import 'package:motivation_flutter/features/quotes/domain/models/quotes_models.dart';
import 'package:motivation_flutter/features/quotes/domain/models/quotes_screen_type.dart';

class CategoryListViewWidget extends StatelessWidget {
  final List<SubCategoryModel> categories;
  final String title;
  const CategoryListViewWidget({required this.categories, required this.title});

  @override
  Widget build(BuildContext context) {
    final bool isPremium = hasSubscription(context);

    late int rowsCount;

    if (categories.length < 4) {
      rowsCount = 1;
    } else {
      rowsCount = 2;
    }
    return Column(
      children: [
        _ExploreTitle(title: title, padding: pad(24, 24, 12, 16)),
        SizedBox(
          height: rowsCount * 100,
          child: GridView.count(
            padding: pad(16, 10, 16, 0),
            mainAxisSpacing: 10,
            crossAxisSpacing: 12,
            crossAxisCount: rowsCount,
            scrollDirection: Axis.horizontal,
            childAspectRatio: 0.47,
            children: categories
                .map(
                  (category) => _CategoryItemWidget(
                    category: category,
                    isPremium: isPremium,
                  ),
                )
                .toList(),
          ),
        )
      ],
    );
  }
}

class _CategoryItemWidget extends StatelessWidget {
  final SubCategoryModel category;
  final bool isPremium;
  const _CategoryItemWidget({required this.category, required this.isPremium});

  @override
  Widget build(BuildContext context) {
    void onCategoryTap() {
      context.pushRoute(
        QuotesRoute(
          type: QuotesScreenType.category(category.id, category.name),
        ),
      );
    }

    return GestureDetector(
      onTap: onCategoryTap,
      child: Stack(
        alignment: Alignment.topRight,
        clipBehavior: Clip.none,
        children: [
          Container(
            height: double.infinity,
            width: double.infinity,
            padding: pad(12, 12, 12, 12),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(30),
              gradient: AppGradients.surface5Dark,
              color: AppTheme.surface_5_dark.withOpacity(.14),
            ),
            child: Row(
              children: [
                ImageWidget(
                  image: category.icon,
                  height: 54,
                  width: 54,
                ),
                const SBox(width: 12),
                SText(
                  category.name.capitalize(),
                  style: TStyle.TitleMedium,
                )
              ],
            ),
          ),
          if (category.status != AccessabilityStatus.Free && isPremium == false)
            Positioned(
              top: -5,
              right: -5,
              child: Container(
                height: 24,
                width: 24,
                padding: pad(5, 5, 5, 5),
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  gradient: AppGradients.secondaryContainer,
                ),
                child: const Center(
                  child: ImageWidget(
                    height: null,
                    width: null,
                    image: AppIcons.smallDiamond,
                  ),
                ),
              ),
            ),
        ],
      ),
    );
  }
}

class _ExploreTitle extends StatelessWidget {
  final String title;
  final EdgeInsetsGeometry padding;
  const _ExploreTitle({required this.title, required this.padding});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: padding,
      child: Row(
        children: [
          SText(
            title.toTitleCase(),
            style: TStyle.HeadlineSmall,
          ),
        ],
      ),
    );
  }
}
