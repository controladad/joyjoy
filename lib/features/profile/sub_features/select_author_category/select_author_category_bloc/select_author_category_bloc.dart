import 'package:dartz/dartz.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:kt_dart/kt.dart';
import 'package:motivation_flutter/common/common_models.dart';
import 'package:motivation_flutter/common/presentation/common_imports.dart';
import 'package:motivation_flutter/core/failures.dart';

import 'package:motivation_flutter/features/profile/domain/profile_repository.dart';
import 'package:motivation_flutter/features/profile/sub_features/select_author_category/select_author_category_args.dart';
import 'package:motivation_flutter/features/quotes/domain/models/author_overview_model.dart';
import 'package:motivation_flutter/features/quotes/domain/models/sub_category_model.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

part 'select_author_category_bloc.freezed.dart';
part 'select_author_category_event.dart';
part 'select_author_category_state.dart';

@injectable
class SelectAuthorCategoryBloc
    extends Bloc<SelectAuthorCategoryEvent, SelectAuthorCategoryState> {
  final IProfileRepository repository;
  SelectAuthorCategoryBloc(this.repository, SelectAuthorCategoryState init)
      : super(init) {
    on<SelectAuthorCategoryEvent>(_bloc);
  }

  late SelectAuthorCategoryScreenType type;

  int page = 0;
  int searchPage = 0;
  Future<void> _bloc(SelectAuthorCategoryEvent event, Emitter emit) {
    return event.map(
      initial: (e) async {
        type = e.type;

        await type.map(
          preferred: (preferred) async {
            final profileResult = await repository.getProfile;
            profileResult.fold((l) => null, (r) {
              emit(
                state.copyWith(
                  selectedIds: preferred.type == AuthorOrCategoryEnum.Category
                      ? r.preferredTopics
                      : r.favoriteAuthors,
                ),
              );
            });

            if (state.selectedIds.isEmpty()) return;

            final result = await repository.getAllAuthorsOrCategoriesLazyLoad(
              type: preferred.type,
              ids: some(state.selectedIds),
              searchQuery: none(),
              page: page,
              limit: state.selectedIds.size,
            );

            result.fold(
              (l) => null,
              (r) => emit(state.copyWith(uncommittedSelects: r.results)),
            );
          },
          reminderTopic: (reminderTopic) async {
            if (reminderTopic.selectedCategoriesIds.isEmpty()) return;

            emit(
              state.copyWith(
                selectedIds: reminderTopic.selectedCategoriesIds,
              ),
            );
            final result = await repository.getAllAuthorsOrCategoriesLazyLoad(
              type: AuthorOrCategoryEnum.Category,
              ids: some(state.selectedIds),
              searchQuery: none(),
              page: page,
              limit: state.selectedIds.size,
            );

            result.fold(
              (l) => null,
              (r) => emit(state.copyWith(uncommittedSelects: r.results)),
            );
          },
        );

        add(const SelectAuthorCategoryEvent.getFirstPage());
      },
      getFirstPage: (e) async {
        page = 1;
        emit(
          state.copyWith(
            data: none(),
            allResults: emptyList(),
          ),
        );

        final result = await repository.getAllAuthorsOrCategoriesLazyLoad(
          type: type.map(
            preferred: (a) => a.type,
            reminderTopic: (_) => AuthorOrCategoryEnum.Category,
          ),
          page: page,
          searchQuery: none(),
          limit: state.filter.fold(
            () => Constants.paginationLimit,
            (a) => a == SelectAuthorCategoryFilterEnum.Selected
                ? state.selectedIds.size
                : Constants.paginationLimit,
          ),
          ids: state.filter.fold(
            () => none(),
            (a) => a == SelectAuthorCategoryFilterEnum.Selected
                ? some(state.uncommittedSelects.map((p0) => p0.id))
                : none(),
          ),
        );

        emit(state.copyWith(data: some(result)));

        result.fold(
          (l) => null,
          (r) => emit(state.copyWith(allResults: r.results)),
        );
      },
      search: (e) async {
        page = 1;
        emit(
          state.copyWith(
            searchData: none(),
            searchResults: emptyList(),
            searchQuery: e.searchQuery,
          ),
        );

        if (e.searchQuery.isNone()) return;

        final result = await repository.getAllAuthorsOrCategoriesLazyLoad(
          type: type.map(
            preferred: (a) => a.type,
            reminderTopic: (_) => AuthorOrCategoryEnum.Category,
          ),
          searchQuery: e.searchQuery,
          limit: Constants.paginationLimit,
          page: searchPage,
          ids: none(),
        );

        emit(state.copyWith(searchData: some(result)));
        result.fold(
          (l) => null,
          (r) => emit(state.copyWith(searchResults: r.results)),
        );
      },
      lazyLoad: (e) async {
        late int total;
        late int received;
        late int nextPage;

        if (state.searchQuery.isNone()) {
          total = state.data.fold(
            () => 0,
            (a) => a.fold((l) => 0, (r) => r.total),
          );
          received = state.allResults.size;
          if (total > received) nextPage = (page += 1);
        } else {
          total = state.searchData.getOrCrash<LazyLoadResponseModel>().total;
          received = state.searchResults.size;
          if (total > received) nextPage = (searchPage += 1);
        }

        if (total <= received) {
          state.controller.loadNoData();
          return;
        }

        final result = await repository.getAllAuthorsOrCategoriesLazyLoad(
          type: type.map(
            preferred: (a) => a.type,
            reminderTopic: (_) => AuthorOrCategoryEnum.Category,
          ),
          searchQuery: state.searchQuery,
          limit: Constants.paginationLimit,
          ids: state.filter.fold(
            () => none(),
            (a) => a == SelectAuthorCategoryFilterEnum.Selected
                ? some(state.selectedIds)
                : none(),
          ),
          page: nextPage,
        );

        if (state.searchQuery.isNone()) {
          emit(state.copyWith(data: some(result)));
          result.fold((l) => state.controller.loadFailed(), (r) {
            emit(state.copyWith(allResults: state.allResults.plus(r.results)));
            state.controller.loadComplete();
          });
        } else {
          emit(state.copyWith(searchData: some(result)));
          result.fold((l) => state.controller.loadFailed(), (r) {
            emit(
              state.copyWith(
                searchResults: state.searchResults.plus(r.results),
              ),
            );
            state.controller.loadComplete();
          });
        }
      },
      refresh: (e) async {
        state.controller.resetNoData();
        if (state.searchQuery.isNone()) {
          add(const SelectAuthorCategoryEvent.getFirstPage());
        } else {
          add(SelectAuthorCategoryEvent.search(state.searchQuery));
        }
      },
      toggle: (e) async {
        state.controller.resetNoData();
        emit(
          state.copyWith(
            uncommittedSelects: state.uncommittedSelects.contains(e.item)
                ? state.uncommittedSelects.minusElement(e.item)
                : state.uncommittedSelects.plusElement(e.item),
          ),
        );
      },
      changeFilter: (e) async {
        emit(state.copyWith(filter: e.filter));
        add(const SelectAuthorCategoryEvent.getFirstPage());
      },
      commitChanges: (e) async {
        type.map(
          preferred: (preferred) async {
            await repository.updatePreferrers(
              preferred.type,
              state.uncommittedSelects.map((p0) => p0.id),
            );
          },
          reminderTopic: (reminder) async {
            reminder.onSubmit(
              state.uncommittedSelects
                  .map((p0) => (p0 as CategoryCase).category),
            );
          },
        );
      },
    );
  }
}
