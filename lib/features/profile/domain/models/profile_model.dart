import 'package:dartz/dartz.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:kt_dart/kt.dart';
import 'package:motivation_flutter/common/presentation/utils/images.dart';
import 'package:motivation_flutter/features/profile/domain/models/account_info_model.dart';
import 'package:motivation_flutter/features/profile/domain/models/collection_model.dart';
import 'package:motivation_flutter/features/profile/domain/models/user_subscription_model.dart';
import 'package:motivation_flutter/features/theme/domain/models/theme_model.dart';

part 'profile_model.freezed.dart';

@freezed
class ProfileModel with _$ProfileModel {
  const factory ProfileModel({
    required KtList<String> likedQuotes,
    required KtList<CollectionModel> collections,
    required KtList<String> preferredTopics,
    required KtList<String> favoriteAuthors,
    required ThemeModel currentTheme,
    required UserSubscription subscriptionPlan,
    required AccountInfoModel accountInfo,
  }) = _ProfileModel;

  const ProfileModel._();

  factory ProfileModel.empty() => ProfileModel(
        currentTheme: ThemeModel.defaultTheme(),
        likedQuotes: emptyList(),
        collections: emptyList(),
        preferredTopics: emptyList(),
        favoriteAuthors: emptyList(),
        subscriptionPlan: UserSubscription.empty(),
        accountInfo: AccountInfoModel.guest(name: none()),
      );
}

//     DateTime? dateFrom,
//     DateTime? dateTo,
//     int? sortOrder,

@freezed
class ProfileBannerModel with _$ProfileBannerModel {
  const factory ProfileBannerModel({
    required String id,
    required String image,
    required BannerLinkUnion link,
    required int sortOrder,
    required Option<DateTime> dateFrom,
    required Option<DateTime> dateTo,
  }) = _ProfileBannerModel;

  const ProfileBannerModel._();

  factory ProfileBannerModel.login() => ProfileBannerModel(
        id: 'login',
        sortOrder: 0,
        dateFrom: none(),
        dateTo: none(),
        link: const BannerLinkUnion.inApp(InAppSection.Login),
        image: Images.loginBanner,
      );
}

@freezed
class BannerLinkUnion with _$BannerLinkUnion {
  const factory BannerLinkUnion.noAction() = _NoAction;
  const factory BannerLinkUnion.link(String link) = _Link;
  const factory BannerLinkUnion.inApp(InAppSection section) = _InApp;
}

enum InAppSection {
  Chat,
  Login,
}

@freezed
class ProfileContentsModel with _$ProfileContentsModel {
  const factory ProfileContentsModel({
    required KtList<ProfileBannerModel> banner,
    required String aboutUs,
    required String termsOfUse,
    required String privacyPolicy,
    required String supportEmail,
  }) = _ProfileContentsModel;
}
