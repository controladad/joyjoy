import 'package:motivation_flutter/common/presentation/common_presentation.dart';
import 'package:motivation_flutter/features/profile/application/profile_bloc/profile_cubit.dart';
import 'package:motivation_flutter/features/profile/presentation/profile_bottom_sheets/profile_bottom_sheets.dart';
import 'package:motivation_flutter/features/profile/presentation/profile_bottom_sheets/profile_list_item_widget.dart';
import 'package:motivation_flutter/features/profile/presentation/profile_screens/profile_phone_view.dart';
import 'package:motivation_flutter/features/profile/sub_features/others/common_profile_about_screen.dart';
import 'package:url_launcher/url_launcher.dart';

class AboutWidget extends StatelessWidget {
  const AboutWidget({super.key});

  @override
  Widget build(BuildContext context) {
    final state = context.watch<ProfileCubit>().state;
    final contentModel = state.onlineData.fold(
      () => null,
      (a) => a.fold((l) => null, (r) => r),
    );

    Future<void> launchEmailApp() async {
      final email = state.onlineData.fold(
        () => null,
        (a) => a.fold((l) => null, (r) => r.supportEmail),
      );
      try {
        await launchUrl(Uri.parse('mailto:$email'));
      } catch (e) {
        print(e);
      }
    }

    final items = [
      ProfileItemModel(
        label: context.translate.customerService,
        icon: AppIcons.profileCustomerServices,
        subTitle: null,
        onTap: launchEmailApp,
      ),
      ProfileItemModel(
        label: context.translate.termsOfUse,
        icon: AppIcons.termsOfUse,
        subTitle: null,
        onTap: () => showScreenInBottomSheets(
          context,
          CommonProfileAboutScreen(
            title: context.translate.termsOfUse.toTitleCase(),
            content: contentModel?.termsOfUse ?? '',
          ),
        ),
      ),
      ProfileItemModel(
        label: context.translate.privacyPolicy,
        icon: AppIcons.privacy,
        subTitle: null,
        onTap: () => showScreenInBottomSheets(
          context,
          CommonProfileAboutScreen(
            title: context.translate.privacyPolicy,
            content: contentModel?.privacyPolicy ?? '',
          ),
        ),
      ),
    ];

    return CommonScaffoldWithAppbar(
      title: context.translate.about.capitalize(),
      body: SingleChildScrollView(
        padding: pad(16, 0, 16, 0),
        child: Column(
          children: [
            const ImageWidget(image: Images.appLogo, height: 160, width: 160),
            SText(contentModel?.aboutUs ?? '', style: TStyle.BodyMedium),
            const SBox(height: 24),
            ...items.map((e) => ProfileListItemWidget(item: e))
          ],
        ),
      ),
    );
  }
}
