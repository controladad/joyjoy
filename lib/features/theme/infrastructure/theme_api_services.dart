import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';

abstract class IThemeApiService{
  Future<Response> getData();
}

@Injectable(as: IThemeApiService)
class ThemeApiServiceImplementation implements IThemeApiService{
  final Dio api;

  ThemeApiServiceImplementation(this.api);

  @override
  Future<Response> getData() {
    return api.get(
      '/rest/V1/',
      queryParameters: {},
    );
  }
}
