import 'dart:async';

import 'package:dartz/dartz.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:kt_dart/kt.dart';
import 'package:motivation_flutter/common/presentation/common_imports.dart';
import 'package:motivation_flutter/core/failures.dart';
import 'package:motivation_flutter/features/quotes/domain/models/quotes_screen_type.dart';

import 'package:motivation_flutter/features/quotes/domain/quotes_repository.dart';
import 'package:motivation_flutter/features/quotes/domain/models/quotes_models.dart';
import 'package:motivation_flutter/features/theme/domain/models/theme_model.dart';
import 'package:motivation_flutter/shared/domain/shared_cache_props_model.dart';

part 'quotes_bloc.freezed.dart';
part 'quotes_event.dart';
part 'quotes_state.dart';

@injectable
class QuotesBloc extends Bloc<QuotesEvent, QuotesState> {
  final IQuotesRepository repository;

  late StreamSubscription<void>? cacheUpdatesSubscription;
  SharedCachePropsModel? prevCache;

  QuotesBloc(this.repository, QuotesState init) : super(init) {
    on<QuotesEvent>(_bloc);

    cacheUpdatesSubscription = repository.cacheUpdated.listen(
      (newCache) async {
        if (newCache != null) {
          prevCache ??= newCache;

          add(
            QuotesEvent.updateCacheData(
              theme: newCache.allThemes
                      .find((p0) => p0.themeId == newCache.selectedThemeId) ??
                  ThemeModel.defaultTheme(),
              likedQuotes: newCache.favoriteQuotesIds,
            ),
          );

          final favoriteCategoriesChanges =
              newCache.favoriteCategoriesIds.filterNot(
                    (cId) => prevCache!.favoriteCategoriesIds.contains(cId),
                  ) +
                  prevCache!.favoriteCategoriesIds.filterNot(
                    (cId) => newCache.favoriteCategoriesIds.contains(cId),
                  );

          if (favoriteCategoriesChanges.isNotEmpty()) {
            await repository.deleteAllQuotes();
            add(QuotesEvent.fetchData(type));
          }

          prevCache = newCache;
        }
      },
    );
  }

  @override
  Future<void> close() {
    cacheUpdatesSubscription?.cancel();
    return super.close();
  }

  late QuotesScreenType type;

  Future<void> _bloc(QuotesEvent event, Emitter emit) {
    Future<void> checkQuotesCount() async {
      print(state.remainingQuotes.size);
      if (state.remainingQuotes.size <
          Constants.minimumCountOfNewQuotesInView) {
        emit(state.copyWith(isLoading: true));
        final result = await repository.getQuotes(type);
        emit(state.copyWith(data: some(result), isLoading: false));

        result.fold(
          (l) => null,
          (r) {
            emit(
              state.copyWith(
                allQuotes: state.allQuotes.plus(r),
                remainingQuotes: state.remainingQuotes.plus(r),
              ),
            );
          },
        );
      }
    }

    return event.map(
      fetchData: (e) async {
        type = e.type;
        emit(
          state.copyWith(
            data: none(),
            cacheData: none(),
            isLoading: true,
          ),
        );
        final userName = await repository.getUserName;
        emit(
          state.copyWith(
            userName: some(userName.fold((l) => '', (r) => r)),
          ),
        );

        final result = await repository.getQuotes(type);
        final cacheResult = await repository.getCacheData();
        emit(
          state.copyWith(
            data: some(result),
            cacheData: some(cacheResult),
            isLoading: false,
          ),
        );
        cacheResult.fold((l) => null, (r) {
          prevCache ??= r;
          emit(
            state.copyWith(
              likedQuotesIds: r.favoriteQuotesIds,
              theme: r.allThemes
                      .find((theme) => r.selectedThemeId == theme.themeId) ??
                  ThemeModel.defaultTheme(),
            ),
          );
        });

        result.fold(
          (l) => null,
          (r) {
            // if (r.isEmpty()) {
            //   add(QuotesEvent.fetchData(type));
            //   return;
            // }
            emit(
              state.copyWith(
                allQuotes: r,
                visibleQuotes: r.subList(0, r.size > 7 ? 7 : r.size),
              ),
            );
          },
        );
        emit(
          state.copyWith(
            remainingQuotes: state.allQuotes.minus(state.visibleQuotes),
          ),
        );
      },
      reportQuote: (e) async {
        emit(state.copyWith(reportData: none()));
        final result = await repository.reportQuote(
          quoteId: e.quoteId,
          reportText: e.reportText,
        );
        emit(
          state.copyWith(reportData: some(result)),
        );
      },
      deleteQuote: (e) async {
        await repository.deleteQuote(e.quoteId);
      },
      updateCacheData: (e) async {
        emit(state.copyWith(likedQuotesIds: e.likedQuotes, theme: e.theme));
      },
      nextQuote: (e) async {
        final nextQuote = state.remainingQuotes.isNotEmpty()
            ? state.remainingQuotes.first()
            : null;
        final lastVisibleQuote = state.visibleQuotes.last();
        emit(
          state.copyWith(
            previousQuotes: state.previousQuotes.plusElement(lastVisibleQuote),
            visibleQuotes: state.visibleQuotes.dropLast(1),
            hasPrevious: state.previousQuotes.isNotEmpty(),
          ),
        );

        if (nextQuote != null) {
          emit(
            state.copyWith(
              visibleQuotes: state.visibleQuotes.addToStart(nextQuote),
              remainingQuotes: state.remainingQuotes.dropFirst(),
            ),
          );
        }
        await checkQuotesCount();
      },
      previousQuote: (e) async {
        if (state.hasPrevious == false) return;
        final previousQuote = state.previousQuotes.last();
        if (state.visibleQuotes.size > 6) {
          emit(
            state.copyWith(
              remainingQuotes:
                  state.remainingQuotes.addToStart(state.visibleQuotes.first()),
              visibleQuotes: state.visibleQuotes.dropFirst(),
            ),
          );
        }

        emit(
          state.copyWith(
            visibleQuotes: state.visibleQuotes.plusElement(previousQuote),
            previousQuotes: state.previousQuotes.dropLast(1),
          ),
        );
        emit(state.copyWith(hasPrevious: state.previousQuotes.isNotEmpty()));
      },
      changeFont: (e) async {
        await repository.changeFontRandomly();
      },
      seeQuote: (e) async {},
      likeQuote: (e) async {
        repository.toggleQuoteLike(e.quote);
      },
    );
  }
}
