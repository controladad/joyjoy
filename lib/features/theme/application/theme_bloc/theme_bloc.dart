import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:kt_dart/kt.dart';
import 'package:motivation_flutter/core/failures.dart';

import 'package:motivation_flutter/features/theme/domain/theme_repository.dart';
import 'package:motivation_flutter/features/theme/domain/models/theme_models.dart';

part 'theme_bloc.freezed.dart';
part 'theme_event.dart';
part 'theme_state.dart';

@injectable
class ThemeBloc extends Bloc<ThemeEvent, ThemeState> {
  final IThemeRepository repository;
  ThemeBloc(this.repository, ThemeState init) : super(init) {
    on<ThemeEvent>(_bloc);
  }

  Future<void> _bloc(ThemeEvent event, Emitter emit) {
    return event.map(
      getThemes: (e) async {
        final result = await repository.getThemes;
        emit(state.copyWith(themeData: some(result)));
        final selectedResult = await repository.getSelectedTheme;
        emit(state.copyWith(selectedThemeData: some(selectedResult)));
        selectedResult.fold((l) => null, (r) {
          emit(state.copyWith(selectedTheme: some(r)));
        });
      },
      selectTheme: (e) async {
        final result = await repository.selectTheme(e.themeId);
        emit(state.copyWith(selectedThemeData: some(result)));

        result.fold((l) => null, (r) {
          emit(state.copyWith(selectedTheme: some(r)));
        });
      },
    );
  }
}
