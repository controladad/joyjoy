import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:motivation_flutter/common/presentation/common_presentation.dart';
import 'package:motivation_flutter/features/sub_auth/application/sub_auth_bloc/sub_auth_bloc.dart';

Future<void> showDeleteAccountDialog(BuildContext context) {
  return showDialog(
    context: context,
    builder: (context) {
      return const Dialog(child: _DeleteAccountDialog());
    },
  );
}

class _DeleteAccountDialog extends HookWidget {
  const _DeleteAccountDialog();

  @override
  Widget build(BuildContext context) {
    final isDeleting = useState(false);

    return WillPopScope(
      onWillPop: () async {
        if (isDeleting.value) {
          return false;
        } else {
          return true;
        }
      },
      child: Container(
        padding: pad(24, 24, 24, 24),
        height: 330,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(28),
          gradient: AppGradients.backGround,
        ),
        child: Column(
          children: [
            context.translate.deletingAccount.toTitleCase().widget(
                  style: TStyle.HeadlineSmall,
                ),
            const SBox(height: 16),
            context.translate.deleteAccountMessage
                .widget(style: TStyle.BodyMedium),
            const SBox(height: 24),
            const Spacer(),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                if (isDeleting.value)
                  const Flexible(child: _Loader())
                else
                  TextButton(
                    onPressed: () async {
                      isDeleting.value = true;
                      final accountDeleted =
                          await context.read<SubAuthCubit>().deleteAccount();
                      if (context.mounted && accountDeleted) {
                        context.pushAndRemoveAll(const SplashRoute());
                      }
                      isDeleting.value = false;
                    },
                    child: context.translate.deleteAccount.toUpperCase().widget(
                          style: TStyle.LabelButton,
                          color: AppTheme.secondary_container_dark,
                        ),
                  ),
                TextButton(
                  onPressed: () => context.popRoute(),
                  child: context.translate.cancel.toUpperCase().widget(
                        style: TStyle.LabelButton,
                        color: AppTheme.primary_dark,
                      ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}

class _Loader extends StatelessWidget {
  const _Loader();

  @override
  Widget build(BuildContext context) {
    return const Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SizedBox.square(
          dimension: 18,
          child: Center(
            child: CircularProgressIndicator(
              strokeWidth: 3,
              color: AppTheme.primary_dark,
            ),
          ),
        ),
      ],
    );
  }
}
