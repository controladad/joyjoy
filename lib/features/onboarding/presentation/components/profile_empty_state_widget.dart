import 'package:motivation_flutter/common/presentation/common_imports.dart';

class ProfileEmptyState extends StatelessWidget {
  final String title;
  final String subtitle;
  const ProfileEmptyState({
    super.key,
    required this.title,
    required this.subtitle,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: pad(42, 42, 42, 0),
      child: Column(
        children: [
          const ImageWidget(
            image: Images.profileEmptyState,
            width: null,
            height: null,
            maxWidth: 400,
          ),
          const SBox(height: 24),
          SText(
            title,
            style: TStyle.TitleLarge,
            maxLines: 2,
            textAlign: TextAlign.center,
          ),
          SText(
            subtitle,
            style: TStyle.BodyLarge,
            maxLines: 3,
            textAlign: TextAlign.center,
          ),
        ],
      ),
    );
  }
}
