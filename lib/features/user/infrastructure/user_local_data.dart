import 'package:dartz/dartz.dart';
import 'package:motivation_flutter/features/language/domain/models/language_entity.dart';
import 'package:hive/hive.dart';
import 'package:injectable/injectable.dart';

import 'package:motivation_flutter/features/language/domain/models/language_model.dart';
import 'package:motivation_flutter/constants/db_constants.dart';
import 'package:motivation_flutter/features/user/domain/models/user_entity.dart';
import 'package:motivation_flutter/features/user/domain/models/user_watch_ads_entity.dart';

abstract class IUserData {
  Future<void> clearToken();
  Future<void> persistWatchAdsData({
    bool? showInAppReview,
    bool? anotherQuoteSeen,
  });
  UserWatchAdsEntity? get getUserWatchAdsData;

  Future<void> storeToken({
    required String token,
  });
  DateTime? get getRemailTimeForVerification;
  Future<void> get persistTimerForVerification;
  Future<bool> get hasToken;
  Future<void> get logout;

  Option<UserEntity> get user;

  Option<LanguageEntity> get language;

  Future<void> storeLanguage({
    required LanguageModel languageModel,
  });

  Stream<void> get userUpdated;
}

@Injectable(as: IUserData)
class UserData implements IUserData {
  final Box<UserEntity> _userBox;
  final Box<LanguageEntity> _languageBox;
  final Box<UserWatchAdsEntity> _userWatchAdsBox;

  UserData(
    @Named(DBConstants.USER) this._userBox,
    @Named(DBConstants.USER_WATCH_ADS) this._userWatchAdsBox,
    @Named(DBConstants.LANGUAGE) this._languageBox,
  );

  @override
  Option<UserEntity> get user => optionOf(_userBox.get(0));

  @override
  Option<LanguageEntity> get language => optionOf(_languageBox.get(0));
  @override
  Future<void> get persistTimerForVerification async {
    late UserEntity userToStore;
    final DateTime now = DateTime.now();
    await user.fold(
      () async {
        userToStore = UserEntity(timerForVerification: now);
        await _userBox.put(0, userToStore);
      },
      (user) async {
        userToStore = user.copyWith(timerForVerification: now);
        await _userBox.put(0, userToStore);
      },
    );
  }

  @override
  Future<void> storeToken({
    required String token,
  }) async {
    UserEntity userToStore;

    await user.fold(
      () async {
        userToStore = UserEntity(token: token);
        await _userBox.put(0, userToStore);
      },
      (user) async {
        userToStore = user.copyWith(token: token);
        await _userBox.put(0, userToStore);
      },
    );
  }

  @override
  DateTime? get getRemailTimeForVerification {
    return user.fold(() => null, (a) => a.timerForVerification);
  }

  @override
  Future<void> storeLanguage({
    required LanguageModel languageModel,
  }) async {
    final languageToStore = LanguageEntity.fromDomain(
      languageModel,
    );

    await _languageBox.put(0, languageToStore);
  }

  @override
  Future<bool> get hasToken async {
    return user.fold(() => false, (a) => a.token != null);
  }

  @override
  Future<void> get logout async {
    await _userBox.deleteAll(_userBox.keys);
  }

  @override
  Future<void> clearToken() async {
    UserEntity userToStore;

    await user.fold(
      () async {},
      (user) async {
        userToStore = user..token = null;
        await _userBox.put(0, userToStore);
      },
    );
  }

  @override
  Stream<void> get userUpdated => _userBox.watch().map((event) {});

  @override
  Future<void> persistWatchAdsData({
    bool? showInAppReview,
    bool? anotherQuoteSeen,
  }) async {
    UserWatchAdsEntity userWatchAdsEntity =
        _userWatchAdsBox.get(0) ?? UserWatchAdsEntity();
    int seenQuoteNumber = userWatchAdsEntity.seenQuoteNumber ?? 0;
    if (anotherQuoteSeen ?? false) {
      seenQuoteNumber = seenQuoteNumber + 1;
    }
    userWatchAdsEntity = userWatchAdsEntity
      ..seenQuoteNumber = seenQuoteNumber
      ..showInAppReviewBefore =
          showInAppReview ?? userWatchAdsEntity.showInAppReviewBefore;

    await _userWatchAdsBox.put(0, userWatchAdsEntity);
  }

  @override
  UserWatchAdsEntity? get getUserWatchAdsData {
    return _userWatchAdsBox.get(0);
  }
}
