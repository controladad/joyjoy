import 'package:flutter_animate/flutter_animate.dart';
import 'package:kt_dart/kt.dart';
import 'package:motivation_flutter/common/presentation/common_imports.dart';
import 'package:motivation_flutter/features/quotes/domain/models/sub_category_model.dart';

class CircularCategoriesListView extends StatefulWidget {
  const CircularCategoriesListView({
    super.key,
    required this.selectedCategoriesIds,
    required this.categories,
    required this.onCategoryTap,
  });

  final KtList<String> selectedCategoriesIds;
  final KtList<SubCategoryModel> categories;
  final Function(SubCategoryModel category) onCategoryTap;

  @override
  State<CircularCategoriesListView> createState() =>
      _CircularCategoriesListViewState();
}

class _CircularCategoriesListViewState
    extends State<CircularCategoriesListView> {
  late KtList<SubCategoryModel> copyListOfCategories;
  late KtList<KtList<SubCategoryModel>> rows = emptyList();

  @override
  void initState() {
    super.initState();
    copyListOfCategories = widget.categories;
    int counter = 0;
    while (copyListOfCategories.isNotEmpty()) {
      final rowSize = counter.isEven ? 2 : 3;
      final actualRowSize = rowSize < copyListOfCategories.size
          ? rowSize
          : copyListOfCategories.size;
      final items = copyListOfCategories.takeLast(actualRowSize);
      rows = rows.plusElement(items);
      copyListOfCategories = copyListOfCategories.dropLast(actualRowSize);
      counter++;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: rows
          .mapIndexed(
            (rowIndex, row) => Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: row.mapIndexed(
                (columnIndex, category) {
                  final index = rows.flatten().indexOf(category);
                  return Flexible(
                    child: _ItemWidget(
                      isSelected:
                          widget.selectedCategoriesIds.contains(category.id),
                      index: index,
                      model: category,
                      totalLength: widget.categories.size,
                      onSelect: widget.onCategoryTap,
                    ),
                  );
                },
              ).asList(),
            ),
          )
          .asList(),
    );
  }
}

class _ItemWidget extends StatefulWidget {
  final int index;
  final int totalLength;
  final SubCategoryModel? model;
  final bool isSelected;
  final Function(SubCategoryModel category) onSelect;
  const _ItemWidget({
    required this.index,
    required this.totalLength,
    required this.model,
    required this.isSelected,
    required this.onSelect,
  });

  @override
  State<_ItemWidget> createState() => _ItemWidgetState();
}

class _ItemWidgetState extends State<_ItemWidget>
    with SingleTickerProviderStateMixin {
  late AnimationController _animationController;
  late Animation<double> _animation;

  @override
  void initState() {
    super.initState();
    _animationController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 100),
    );
    _animation =
        Tween<double>(begin: 1.0, end: 1.3).animate(_animationController);
    if (widget.isSelected) {
      _animationController.value = 1.0;
    }
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double getSize() {
      switch (widget.index % 10) {
        case 0:
          return 110;
        case 1:
          return 120;
        case 2:
        case 3:
          return 110;
        case 4:
        case 5:
        case 6:
          return 120;
        case 7:
        case 8:
          return 110;

        case 9:
          return 100;
        default:
          return 110;
      }
    }

    Color? getColor() {
      if (widget.model != null) {
        if (widget.isSelected) {
          return AppTheme.secondary_container_dark;
        } else {
          return AppTheme.primary_dark.withOpacity(.16);
        }
      } else {
        return null;
      }
    }

    return GestureDetector(
      onTap: () async {
        if (widget.model != null) {
          widget.onSelect(widget.model!);
        }

        if (_animationController.status == AnimationStatus.completed) {
          await _animationController.reverse();
        } else {
          await _animationController.forward();
        }
        setState(() {});
      },
      child: SContainer(
        margin: pad(7.5, 0, 7.5, 0),
        alignment: Alignment.center,
        maxHeight: getSize(),
        maxWidth: getSize(),
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: getColor(),
          gradient: widget.isSelected ? AppGradients.secondaryContainer : null,
        ),
        child: widget.model != null
            ? AnimatedBuilder(
                animation: _animation,
                builder: (context, child) => Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Flexible(
                      child: const ImageWidget(
                        // image: widget.model!.icon,
                        image: Images.headphone,
                        height: 40,
                        width: 40,
                      )
                          .animate(target: widget.isSelected ? 1 : 0)
                          .scaleXY(end: 1.1, begin: .9),
                    ),
                    const SBox(height: 5),
                    SBox(
                      width: getSize(),
                      padding: pad(8, 0, 8, 0),
                      child: SText(
                        widget.model!.name.toUpperCase(),
                        style: TStyle.LabelButton,
                        textAlign: TextAlign.center,
                        overflow: TextOverflow.clip,
                        maxLines: 1,
                      ),
                    )
                        .animate(target: widget.isSelected ? 0 : 1)
                        .scaleXY(end: 1.1, begin: .9),
                    const SBox(height: 5),
                  ],
                ),
              )
            : null,
      ),
    );
  }
}
