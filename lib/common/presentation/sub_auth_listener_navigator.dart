import 'package:motivation_flutter/common/presentation/common_imports.dart';
import 'package:motivation_flutter/features/sub_auth/application/sub_auth_bloc/sub_auth_bloc.dart';

class SubAuthListenerNavigator extends StatelessWidget {
  final Widget child;
  const SubAuthListenerNavigator({super.key, required this.child});

  @override
  Widget build(BuildContext context) {
    late SubAuthState previousState;
    return BlocListener<SubAuthCubit, SubAuthState>(
      listenWhen: (previous, current) {
        final doesSubsChanged = previous.subscription.toNullable()?.isPremium !=
            current.subscription.toNullable()?.isPremium;

        final shouldListen = doesSubsChanged ||
            previous.hasCompleteData.isSame(current.hasCompleteData) == false ||
            previous.isAuthenticated.isSame(current.isAuthenticated) == false;

        previousState = previous;

        return shouldListen;
      },
      listener: (context, state) {
        final isAuth = state.isAuthenticated.toNullable();
        final prevIsAuth = previousState.isAuthenticated.toNullable();

        final hasSub = state.subscription.toNullable()?.isPremium;

        if (prevIsAuth == true && isAuth == false) {
          context.pushAndRemoveAll(const SplashRoute());
          return;
        }

        if (hasSub == true && isAuth == false) {
          context.pushRoute(LoginRoute());
          return;
        }
      },
      child: child,
    );
  }
}
