import 'package:dartz/dartz.dart';
import 'package:hive/hive.dart';
import 'package:kt_dart/kt.dart';
import 'package:motivation_flutter/features/profile/domain/models/account_info_entity.dart';
import 'package:motivation_flutter/features/profile/domain/models/account_info_model.dart';
import 'package:motivation_flutter/features/profile/domain/models/collection_entity.dart';
import 'package:motivation_flutter/features/profile/domain/models/reminder_entity.dart';
import 'package:motivation_flutter/features/profile/domain/models/user_subscription_entity.dart';
import 'package:motivation_flutter/features/theme/domain/models/theme_entity.dart';
import 'package:motivation_flutter/shared/domain/shared_cache_props_model.dart';
part 'shared_cache_props_entity.g.dart';

@HiveType(typeId: 5)
class SharedCachePropsEntity extends HiveObject {
  @HiveField(0)
  List<ReminderEntity>? reminders;
  @HiveField(1)
  List<String>? favoriteCategoriesIds;
  @HiveField(2)
  List<String>? favoriteAuthorsIds;
  @HiveField(3)
  List<String>? likedQuotesIds;
  @HiveField(4)
  List<String>? quotesHistoryIds;
  @HiveField(5)
  List<CollectionEntity>? collections;
  @HiveField(6)
  AccountInfoEntity? account;
  @HiveField(7)
  DateTime? versionDate;
  @HiveField(8)
  List<ThemeEntity>? themes;
  @HiveField(9)
  bool? isFirst;
  @HiveField(10)
  String? selectedThemeId;
  @HiveField(11)
  bool? didTryChat;
  @HiveField(12)
  bool? shouldShowOnboarding;

  SharedCachePropsEntity({
    this.reminders,
    this.favoriteAuthorsIds,
    this.favoriteCategoriesIds,
    this.versionDate,
    this.likedQuotesIds,
    this.collections,
    this.quotesHistoryIds,
    this.didTryChat,
    this.shouldShowOnboarding,
    this.themes,
    this.selectedThemeId,
    this.account,
    this.isFirst,
  });

  SharedCachePropsEntity copyWith({
    List<ReminderEntity>? reminders,
    List<String>? favoriteCategoriesIds,
    List<String>? favoriteAuthorsIds,
    List<String>? likedQuotesIds,
    List<String>? quotesHistoryIds,
    List<CollectionEntity>? collections,
    DateTime? versionDate,
    UserSubscriptionEntity? subscription,
    bool? isFirst,
    String? userName,
    bool? didTryChat,
    bool? shouldShowOnboarding,
    List<ThemeEntity>? themes,
    String? selectedThemeId,
    AccountInfoEntity? account,
  }) {
    return SharedCachePropsEntity(
      themes: themes ?? this.themes,
      selectedThemeId: selectedThemeId ?? this.selectedThemeId,
      reminders: reminders ?? this.reminders,
      favoriteCategoriesIds:
          favoriteCategoriesIds ?? this.favoriteCategoriesIds,
      favoriteAuthorsIds: favoriteAuthorsIds ?? this.favoriteAuthorsIds,
      versionDate: versionDate ?? this.versionDate,
      likedQuotesIds: likedQuotesIds ?? this.likedQuotesIds,
      collections: collections ?? this.collections,
      quotesHistoryIds: quotesHistoryIds ?? this.quotesHistoryIds,
      isFirst: isFirst ?? this.isFirst,
      didTryChat: didTryChat ?? this.didTryChat,
      shouldShowOnboarding: shouldShowOnboarding ?? this.shouldShowOnboarding,
      // account: account ?? this.account,
    );
  }

  factory SharedCachePropsEntity.fromDomain(SharedCachePropsModel model) {
    return SharedCachePropsEntity(
      reminders:
          model.reminders.map((p0) => ReminderEntity.fromDomain(p0)).asList(),
      favoriteAuthorsIds: model.favoriteAuthorsIds.asList(),
      favoriteCategoriesIds: model.favoriteCategoriesIds.asList(),
      versionDate: model.versionDate,
      likedQuotesIds: model.favoriteQuotesIds.asList(),
      collections: model.collections
          .map((p0) => CollectionEntity.fromDomain(p0))
          .asList(),
      quotesHistoryIds: model.quotesHistoryIds.asList(),
      isFirst: model.isFirst,
      didTryChat: model.didTryChat,
      shouldShowOnboarding: model.shouldShowOnboarding,
      themes: model.allThemes.map((p0) => ThemeEntity.fromDomain(p0)).asList(),
      selectedThemeId: model.selectedThemeId,
      account: AccountInfoEntity.fromDomain(model.account),
    );
  }

  SharedCachePropsModel get toDomain {
    return SharedCachePropsModel(
      id: '',
      reminders: reminders?.kt.map((p0) => p0.toDomain()) ?? emptyList(),
      favoriteCategoriesIds: favoriteCategoriesIds?.kt ?? emptyList(),
      favoriteAuthorsIds: favoriteAuthorsIds?.kt ?? emptyList(),
      favoriteQuotesIds: likedQuotesIds?.kt ?? emptyList(),
      quotesHistoryIds: quotesHistoryIds?.kt ?? emptyList(),
      collections: collections?.kt.map((p0) => p0.toDomain()) ?? emptyList(),
      versionDate: versionDate ?? DateTime.now(),
      isFirst: isFirst,
      didTryChat: didTryChat ?? false,
      shouldShowOnboarding: shouldShowOnboarding ?? false,
      allThemes: themes?.map((e) => e.toDomain()).toList().kt ?? emptyList(),
      selectedThemeId: selectedThemeId ?? 'default',
      account: account?.toDomain() ?? AccountInfoModel.guest(name: none()),
    );
  }
}
