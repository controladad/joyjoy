import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:kt_dart/kt.dart';
import 'package:motivation_flutter/core/failures.dart';
import 'package:motivation_flutter/features/onboarding/domain/models/onboarding_model.dart';
import 'package:motivation_flutter/features/sub_auth/domain/models/review_model.dart';

import 'package:motivation_flutter/features/onboarding/domain/onboarding_repository.dart';
import 'package:motivation_flutter/features/onboarding/presentation/how_do_you_feel_screen.dart';

part 'onboarding_bloc.freezed.dart';
part 'onboarding_event.dart';
part 'onboarding_state.dart';

@injectable
class OnboardingBloc extends Bloc<OnboardingEvent, OnboardingState> {
  final IOnboardingRepository repository;
  OnboardingBloc(this.repository, OnboardingState init) : super(init) {
    on<OnboardingEvent>(_bloc);
  }

  Future<void> _bloc(OnboardingEvent event, Emitter emit) {
    return event.map(
      getData: (e) async {
        final cacheData = await repository.getCacheData;
        emit(state.copyWith(onboardingCacheData: some(cacheData)));
        cacheData.fold((l) => null, (r) {
          emit(
            state.copyWith(
              userName: r.userName,
              selectedCharacterIds: r.selectedFavoriteAuthors,
              selectedFavoriteCategoriesIds: r.selectedFavoriteCategories,
            ),
          );
        });

        emit(state.copyWith(onboardingData: none()));
        final data = await repository.getData;
        emit(state.copyWith(onboardingData: some(data)));
      },
      tryAgain: (e) async {
        emit(state.copyWith(onboardingData: none()));
        add(const OnboardingEvent.getData());
      },
      selectFeelingItem: (e) async {
        emit(
          state.copyWith(
            selectedFeeling: some(e.feeling),
            wroteFeeling: none(),
          ),
        );
      },
      writeFeeling: (e) async {
        emit(
          state.copyWith(
            selectedFeeling: none(),
            wroteFeeling: some(e.value),
          ),
        );
      },
      submitName: (e) async {
        await state.userName.fold(() => null, (a) async {
          final result = await repository.setUserName(a);
          result.fold((l) => null, (r) {
            emit(state.copyWith(userName: r));
          });
        });
      },
      passedFeeling: (e) async {
        await repository.userDidTryChat;
      },
      passedPaywall: (e) async {
        await repository.userPassedOnboarding;
      },
      changeUsersName: (e) async {
        emit(
          state.copyWith(
            userName: e.value.isEmpty ? none() : some(e.value),
          ),
        );
      },
      characterClick: (e) async {
        final result = await repository.toggleFavoriteAuthor(e.characterId);
        result.fold(
          (l) => null,
          (r) => emit(state.copyWith(selectedCharacterIds: r)),
        );
      },
      categoryClick: (e) async {
        final result = await repository.toggleFavoriteCategory(e.categoryId);
        result.fold(
          (l) => null,
          (r) => emit(state.copyWith(selectedFavoriteCategoriesIds: r)),
        );
      },
    );
  }
}
