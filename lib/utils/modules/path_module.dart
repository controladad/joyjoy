import 'package:injectable/injectable.dart';
import 'package:path_provider/path_provider.dart';

import 'package:motivation_flutter/constants/db_constants.dart';

@module
abstract class PathModule {
  @Named(DBConstants.MAIN_DB_PATH)
  @preResolve
  Future<String> get dbPath async {
    final appDirectory = await getApplicationDocumentsDirectory();
    return appDirectory.path;
  }
}
