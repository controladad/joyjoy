part of 'language_cubit.dart';

@injectable
@freezed
class LanguageState with _$LanguageState {
  const factory LanguageState({
    required LanguageModel currentLanguage,
    required KtList<LanguageModel> supportedLanguages,
    required bool isSelected,
  }) = _LanguageState;

  @factoryMethod
  factory LanguageState.init() {
    return LanguageState(
      currentLanguage: const LanguageModel(),
      isSelected: false,
      supportedLanguages: AppLocalizations.supportedLocales.kt.map(
        (supported) => LanguageModel(
          locale: supported.languageCode,
        ),
      ),
    );
  }
}
