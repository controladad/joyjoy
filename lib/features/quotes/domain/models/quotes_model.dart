import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:kt_dart/kt.dart';
import 'package:motivation_flutter/features/quotes/domain/models/author_overview_model.dart';

part 'quotes_model.freezed.dart';

@freezed
class QuoteModel with _$QuoteModel {
  const factory QuoteModel({
    required String id,
    required String quote,
    AuthorModel? author,
    required KtList<String> categoryIds,
  }) = _QuoteModel;
}
