import 'package:dartz/dartz.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:kt_dart/kt.dart';
import 'package:motivation_flutter/common/presentation/common_imports.dart';
import 'package:motivation_flutter/core/failures.dart';

import 'package:motivation_flutter/features/explore/domain/explore_repository.dart';
import 'package:motivation_flutter/features/explore/domain/models/search_models.dart';

part 'instance_search_bloc.freezed.dart';
part 'instance_search_event.dart';
part 'instance_search_state.dart';

@injectable
class InstanceSearchBloc
    extends Bloc<InstanceSearchEvent, InstanceSearchState> {
  final IExploreRepository repository;
  InstanceSearchBloc(this.repository, InstanceSearchState init) : super(init) {
    on<InstanceSearchEvent>(_bloc);
  }

  int page = 1;

  Future<void> _bloc(InstanceSearchEvent event, Emitter emit) {
    return event.map(
      onSuggestionTap: (e) async {},
      queryChanged: (e) async {
        emit(state.copyWith(searchQuery: some(e.query)));
        if (e.query.isEmpty) {
          emit(
            state.copyWith(
              instanceSearchData: none(),
              instanceSearchResults: emptyList(),
            ),
          );
        } else {
          emit(state.copyWith(isLoading: true, instanceSearchData: none()));
          final result = await repository.instanceSearchQuery(e.query);
          result.fold((l) => null, (r) {
            emit(
              state.copyWith(
                instanceSearchData: some(result),
                instanceSearchResults: r.results,
                suggestions: r.suggestions,
              ),
            );
          });
          emit(state.copyWith(isLoading: false));
        }
      },
      submitQuery: (e) async {
        final newSearchHistories =
            await repository.addNewSearchHistory(e.query);
        newSearchHistories.fold((l) => null, (r) {
          emit(state.copyWith(searchHistories: r));
        });
      },
      init: (e) async {
        final newSearchHistories = await repository.getSearchHistories;
        newSearchHistories.fold((l) => null, (r) {
          emit(state.copyWith(searchHistories: r));
        });
      },
      clearHistory: (e) async {
        final newSearchHistories = await repository.removeSearchHistories();
        newSearchHistories.fold((l) => null, (r) {
          emit(state.copyWith(searchHistories: r));
        });
      },
    );
  }
}
