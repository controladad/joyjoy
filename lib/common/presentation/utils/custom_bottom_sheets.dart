import 'package:motivation_flutter/common/presentation/common_imports.dart';

Future<void> showCustomBottomSheet({
  required BuildContext context,
  required Function() onClose,
  required Widget child,
}) async {
  await showModalBottomSheet(
    builder: (context) => _BottomSheetWidget(
      child: child,
    ),
    context: context,
    isScrollControlled: true,
  );
  onClose();
}

class _BottomSheetWidget extends StatelessWidget {
  final Widget child;

  const _BottomSheetWidget({
    required this.child,
  });

  @override
  Widget build(BuildContext context) {
    return SContainer(
      padding: pad(0, 0, 0, MediaQuery.of(context).viewInsets.bottom),
      decoration: BoxDecoration(
        borderRadius: const BorderRadius.only(
          topRight: Radius.circular(25),
          topLeft: Radius.circular(25),
        ),
        gradient: AppGradients.backGround,
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          const _Header(),
          child,
        ],
      ),
    );
  }
}

class _Header extends StatelessWidget {
  const _Header();

  @override
  Widget build(BuildContext context) {
    return const SizedBox(
      height: 36,
      child: Center(
        child: ImageWidget(image: AppIcons.dragHandler, height: 5, width: 32),
      ),
    );
  }
}
