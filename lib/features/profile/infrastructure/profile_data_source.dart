import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:kt_dart/kt.dart';
import 'package:motivation_flutter/common/common_models.dart';
import 'package:motivation_flutter/features/profile/domain/models/profile_model.dart';
import 'package:motivation_flutter/features/profile/domain/models/update_model.dart';
import 'package:motivation_flutter/features/profile/infrastructure/dtos/profile_dto.dart';

import 'package:motivation_flutter/features/profile/infrastructure/profile_api_services.dart';
import 'package:motivation_flutter/features/quotes/domain/models/author_overview_model.dart';
import 'package:motivation_flutter/features/quotes/domain/models/sub_category_model.dart';
import 'package:motivation_flutter/shared/infrastructure/dtos/author_dto.dart';
import 'package:motivation_flutter/shared/infrastructure/dtos/sub_category_dto.dart';

part './profile_content.dart';

abstract class IProfileApiDataSource {
  Future<ProfileContentsModel> get getContents;
  Future<UpdateModel> get hasUpdate;
  Future<LazyLoadResponseModel<SubCategoryModel>> getCategories({
    required int page,
    required Option<String> searchQuery,
    required Option<KtList<String>> ids,
    required int limit,
  });
  Future<LazyLoadResponseModel<AuthorModel>> getAuthors({
    required int page,
    required Option<String> searchQuery,
    required Option<KtList<String>> ids,
    required int limit,
  });
}

@Injectable(as: IProfileApiDataSource)
class ProfileApiDataSourceImplementation implements IProfileApiDataSource {
  final IProfileApiService apiService;

  ProfileApiDataSourceImplementation(this.apiService);

  @override
  Future<LazyLoadResponseModel<AuthorModel>> getAuthors({
    required int page,
    required Option<String> searchQuery,
    required Option<KtList<String>> ids,
    required int limit,
  }) async {
    final response = await apiService.getAuthors(
      page: page,
      searchQuery: searchQuery,
      ids: ids,
      limit: limit,
    );

    final responseData = response.data as Map<String, dynamic>;
    final data = responseData['data'] as List<dynamic>;
    final meta = responseData['meta'] as Map<String, dynamic>;

    final KtList<AuthorDto> results = data
        .map((e) => AuthorDto.fromJson(e as Map<String, dynamic>))
        .toList()
        .kt;
    final total = meta['total'] as int;

    return LazyLoadResponseModel(
      results: results.map((ad) => ad.toDomain()),
      total: total,
    );
  }

  @override
  Future<LazyLoadResponseModel<SubCategoryModel>> getCategories({
    required int page,
    required Option<String> searchQuery,
    required Option<KtList<String>> ids,
    required int limit,
  }) async {
    final response = await apiService.getCategories(
      page: page,
      searchQuery: searchQuery,
      ids: ids,
      limit: limit,
    );

    final responseData = response.data as Map<String, dynamic>;
    final data = responseData['data'] as List<dynamic>;

    final KtList<SubCategoryDto> results = data
        .map((e) => SubCategoryDto.fromJson(e as Map<String, dynamic>))
        .toList()
        .kt;

    return LazyLoadResponseModel(
      results: results.map((cd) => cd.toDomain()),
      total: results.size,
    );
  }

  @override
  Future<ProfileContentsModel> get getContents async {
    final customerService =
        (await apiService.getCustomerServiceInfo).data as Map<String, dynamic>;
    final homeData = HomeDto.fromJson(
      (await apiService.getBanners).data as Map<String, dynamic>,
    );

    final KtList<ProfileBannerModel> banners =
        // ignore: unnecessary_parenthesis
        (homeData.banners?.map((e) => e.toDomain).toList().kt) ?? emptyList();

    return ProfileContentsModel(
      banner: banners,
      aboutUs: _ABOUT_US,
      termsOfUse: _TERMS_OF_USE,
      privacyPolicy: _PRIVACY_POLICY,
      supportEmail: (customerService['data']
          as Map<String, dynamic>)['support_email'] as String,
    );
  }

  @override
  Future<UpdateModel> get hasUpdate async {
    final response = await apiService.appUpdateInfo;
    final result = await response.data as Map<String, dynamic>;
    final data = result['data'] as Map<String, dynamic>;
    return UpdateDto.fromJson(data).toDomain();
  }
}
