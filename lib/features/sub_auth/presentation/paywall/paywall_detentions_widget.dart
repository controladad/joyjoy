import 'package:kt_dart/kt.dart';
import 'package:motivation_flutter/common/presentation/common_imports.dart';
import 'package:motivation_flutter/features/sub_auth/domain/models/paywall_model.dart';

class PaywallDefinitionsWidget extends StatelessWidget {
  final KtList<PayWallOptionDefinitionModel> definitions;

  const PaywallDefinitionsWidget(this.definitions);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: pad(25, 0, 25, 0),
      child: Column(
        children: definitions
            .map(
              (e) => Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  ImageWidget(image: e.icon, height: 30, width: 30),
                  const SBox(width: 15),
                  SBox(
                    width: 273,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SText(e.title, style: TStyle.TitleLarge),
                        const SBox(height: 5),
                        SText(e.subtitle, style: TStyle.TitleSmall),
                        const SBox(height: 25),
                      ],
                    ),
                  )
                ],
              ),
            )
            .asList(),
      ),
    );
  }
}
