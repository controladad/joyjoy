import 'package:motivation_flutter/common/presentation/common_presentation.dart';
import 'package:motivation_flutter/features/chat/presentation/components/chat_app_bar.dart';
import 'package:motivation_flutter/features/chat/presentation/components/chat_body.dart';

final GlobalKey chatScreenKey = GlobalKey();

class ChatPhoneView extends StatelessWidget {
  const ChatPhoneView({super.key});

  @override
  Widget build(BuildContext context) {
    return CommonBackgroundContainer(
      child: Scaffold(
        key: chatScreenKey,
        backgroundColor: Colors.transparent,
        appBar: chatAppBar(context),
        body: const ChatBody(),
      ),
    );
  }
}
