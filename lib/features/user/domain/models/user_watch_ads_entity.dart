import 'package:hive/hive.dart';

part 'user_watch_ads_entity.g.dart';

@HiveType(typeId: 16)
class UserWatchAdsEntity {
  @HiveField(0)
  bool? showInAppReviewBefore;
  @HiveField(1)
  int? seenQuoteNumber;
}
