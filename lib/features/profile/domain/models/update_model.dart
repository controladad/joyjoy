import 'package:dartz/dartz.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:kt_dart/kt.dart';

part 'update_model.freezed.dart';

@freezed
class UpdateModel with _$UpdateModel {
  const factory UpdateModel({
    required bool updateAvailable,
    required bool forceUpdate,
    required Option<String> googlePlayLink,
    required Option<String> appStoreLink,
    required String version,
    required Option<KtList<UpdateHistoryModel>> changeLog,
  }) = _UpdateModel;
}

@freezed
class UpdateHistoryModel with _$UpdateHistoryModel {
  const factory UpdateHistoryModel({
    required String version,
    required String description,
  }) = _UpdateHistoryModel;
}
