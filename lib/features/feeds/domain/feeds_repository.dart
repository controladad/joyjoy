import 'package:dartz/dartz.dart';
import 'package:motivation_flutter/core/failures.dart';

import 'package:motivation_flutter/features/feeds/domain/models/feeds_models.dart';

abstract class IFeedsRepository {
  Future<Either<GeneralFailure, FeedsModel>> get getData;
}
