part of 'select_author_category_bloc.dart';

@freezed
class SelectAuthorCategoryEvent with _$SelectAuthorCategoryEvent {
  const factory SelectAuthorCategoryEvent.initial(
    SelectAuthorCategoryScreenType type,
  ) = _Initial;
  const factory SelectAuthorCategoryEvent.getFirstPage() = _GetFirstPage;
  const factory SelectAuthorCategoryEvent.refresh() = _Refresh;
  const factory SelectAuthorCategoryEvent.lazyLoad() = _LazyLoad;
  const factory SelectAuthorCategoryEvent.toggle(AuthorOrCategoryUnion item) =
      _Toggle;
  const factory SelectAuthorCategoryEvent.search(Option<String> searchQuery) =
      _Search;
  const factory SelectAuthorCategoryEvent.commitChanges() = _CommitChanges;
  const factory SelectAuthorCategoryEvent.changeFilter(
    Option<SelectAuthorCategoryFilterEnum> filter,
  ) = _Filter;
}
