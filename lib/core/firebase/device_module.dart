import 'dart:io';

import 'package:injectable/injectable.dart';

@module
abstract class DeviceModule {
  @Named('DEVICE')
  @injectable
  String get device {
    if (Platform.isIOS) {
      return 'IOS';
    } else {
      return 'ANDROID';
    }
  }
}
