import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:kt_dart/kt.dart';
import 'package:uuid/uuid.dart';

part 'collection_model.freezed.dart';

@freezed
class CollectionModel with _$CollectionModel {
  const factory CollectionModel({
    required String id,
    required String name,
    required KtList<String> quotesIds,
  }) = _CollectionModel;

  const CollectionModel._();

  factory CollectionModel.empty(String collectionName) => CollectionModel(
        id: const Uuid().v4(),
        name: collectionName,
        quotesIds: emptyList(),
      );
}
