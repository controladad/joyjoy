// import 'package:badges/badges.dart';

// import 'package:motivation_flutter/common/presentation/common_imports.dart';

// Widget cacPosBottomNavigation(BuildContext context, TabsRouter tabsRouter) {
//   return BottomAppBar(
//     shape: const CircularNotchedRectangle(),
//     elevation: 2,
//     clipBehavior: Clip.antiAlias,
//     notchMargin: 10,
//     child: Row(
//       mainAxisAlignment: MainAxisAlignment.spaceAround,
//       children: <Widget>[
//         _BottomNavigationBarItem(
//           tabsRouter: tabsRouter,
//           selectedIcon: AppIcons.selectedProfileIcon,
//           unSelectedIcon: AppIcons.unSelectedProfileIcon,
//           index: 0,
//           showBadge: false,
//         ),
//         _BottomNavigationBarItem(
//           tabsRouter: tabsRouter,
//           selectedIcon: AppIcons.selectedProfileIcon,
//           unSelectedIcon: AppIcons.unSelectedProfileIcon,
//           index: 1,
//           showBadge: false,
//         ),
//         _BottomNavigationBarItem(
//           tabsRouter: tabsRouter,
//           selectedIcon: AppIcons.selectedProfileIcon,
//           unSelectedIcon: AppIcons.unSelectedProfileIcon,
//           index: 2,
//           showBadge: false,
//         ),
//         _BottomNavigationBarItem(
//           tabsRouter: tabsRouter,
//           selectedIcon: AppIcons.selectedProfileIcon,
//           unSelectedIcon: AppIcons.unSelectedProfileIcon,
//           index: 3,
//           showBadge: false,
//         ),
//       ],
//     ),
//   );
// }

// class _BottomNavigationBarItem extends StatelessWidget {
//   final TabsRouter tabsRouter;
//   final int index;
//   final String unSelectedIcon;
//   final String selectedIcon;
//   final bool showBadge;
//   final String? badgeContent;

//   const _BottomNavigationBarItem({
//     Key? key,
//     required this.tabsRouter,
//     required this.index,
//     required this.unSelectedIcon,
//     required this.selectedIcon,
//     required this.showBadge,
//     this.badgeContent,
//   }) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     final int selectedIndex = tabsRouter.activeIndex;
//     final bool isSelected = selectedIndex == index;
//     return SizedBox(
//       width: 42.w,
//       height: 55.h,
//       child: GestureDetector(
//         behavior: HitTestBehavior.translucent,
//         onTap: () {
//           tabsRouter.setActiveIndex(index);
//         },
//         child: Badge(
//           showBadge: showBadge,
//           badgeColor: AppTheme.,
//           badgeContent: Text(
//             badgeContent ?? '',
//             style: context.textTheme.caption?.copyWith(
//               color: AppTheme.white,
//             ),
//           ),
//           position: BadgePosition(start: 20.w, top: 10.h),
//           child: SizedBox(
//             height: 70.h,
//             child: Column(
//               children: [
//                 Container(
//                   height: 4.h,
//                   decoration: BoxDecoration(
//                     color: isSelected ? AppTheme.primary : AppTheme.white,
//                     borderRadius: BorderRadiusDirectional.only(
//                       bottomEnd: Radius.circular(30.w),
//                       bottomStart: Radius.circular(30.w),
//                     ),
//                   ),
//                 ),
//                 SizedBox(height: 5.h),
//                 SizedBox(
//                   height: 40.h,
//                   child: SvgPicture.asset(
//                     isSelected ? selectedIcon : unSelectedIcon,
//                   ),
//                 ),
//               ],
//             ),
//           ),
//         ),
//       ),
//     );
//   }
// }
