// ignore_for_file: unused_field

import 'package:motivation_flutter/core/failures.dart';
import 'package:dartz/dartz.dart';
import 'package:motivation_flutter/core/network/network_info.dart';
import 'package:motivation_flutter/features/force_update/domain/update_info_model.dart';
import 'package:motivation_flutter/features/force_update/domain/update_info_repository.dart';
import 'package:injectable/injectable.dart';

@Injectable(as: UpdateInfoRepository)
class UpdateInfoRepositoryImp implements UpdateInfoRepository {
  final INetworkInfo _iNetworkInfo;

  UpdateInfoRepositoryImp(this._iNetworkInfo);

  @override
  Future<Either<GeneralFailure, UpdateInfoModel>> getUpdateInfo(
    String version,
  ) {
    // TODO: implement getUpdateInfo
    throw UnimplementedError();
  }
}
