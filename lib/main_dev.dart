import 'package:motivation_flutter/bootstrap.dart';
import 'package:motivation_flutter/common/presentation/common_presentation.dart';

Future<void> main() async {
  bootstrap(
    () => AppWidget(),
    // () => MultiBlocProvider(
    // providers: [BlocProvider(create: (_) => getIt<LanguageCubit>())],
    // child: AppWidget(),
    // ),
  );
}
