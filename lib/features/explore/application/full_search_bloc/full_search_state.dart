part of 'full_search_bloc.dart';

@injectable
@freezed
class FullSearchState with _$FullSearchState {
  const factory FullSearchState({
    required Option<
            Either<GeneralFailure, LazyLoadResponseModel<SearchResultUnion>>>
        fullSearchResultsData,
    required KtList<SearchResultUnion> results,
    required bool isLoading,
    required Option<int> total,
    required RefreshController controller,
  }) = _FullSearchState;

  @factoryMethod
  factory FullSearchState.init() => FullSearchState(
        fullSearchResultsData: none(),
        controller: RefreshController(initialLoadStatus: LoadStatus.canLoading),
        isLoading: false,
        results: emptyList(),
        total: none(),
      );
}
