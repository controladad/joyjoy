import 'package:freezed_annotation/freezed_annotation.dart';

part 'feeds_model.freezed.dart';

@freezed
class FeedsModel with _$FeedsModel {
  const factory FeedsModel({
    required String id,
  }) = _FeedsModel;
}
