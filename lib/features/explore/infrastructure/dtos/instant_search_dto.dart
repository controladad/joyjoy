import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:kt_dart/kt.dart';
import 'package:motivation_flutter/features/explore/domain/models/search_models.dart';
import 'package:motivation_flutter/features/quotes/domain/models/quotes_models.dart';
import 'package:motivation_flutter/shared/infrastructure/dtos/author_dto.dart';
import 'package:motivation_flutter/shared/infrastructure/dtos/quote_dto.dart';
import 'package:motivation_flutter/shared/infrastructure/dtos/sub_category_dto.dart';

part 'instant_search_dto.freezed.dart';
part 'instant_search_dto.g.dart';

@freezed
class InstantSearchDto with _$InstantSearchDto {
  factory InstantSearchDto({
    @JsonKey(name: 'quote') required List<QuoteDto> quotes,
    @JsonKey(name: 'category') required List<SubCategoryDto> categories,
    @JsonKey(name: 'author') required List<AuthorDto> authors,
  }) = _InstantSearchDto;
  const InstantSearchDto._();
  factory InstantSearchDto.fromJson(Map<String, dynamic> json) =>
      _$InstantSearchDtoFromJson(json);

  SearchModel toDomain() {
    KtList<SearchResultUnion> results = emptyList();
    for (final quote in quotes) {
      results = results.plusElement(
        SearchResultUnion.quote(
          quote.toEntity().toDomain(
                author: AuthorModel(
                  id: quote.authorId.toString(),
                  name: '',
                  shortDescription: '',
                  birthYear: 0,
                  deathYear: 0,
                  job: '',
                  country: '',
                  gender: '',
                  status: AccessabilityStatus.Premium,
                ),
              ),
        ),
      );
    }

    for (final author in authors) {
      results =
          results.plusElement(SearchResultUnion.author(author.toDomain()));
    }
    for (final category in categories) {
      results =
          results.plusElement(SearchResultUnion.category(category.toDomain()));
    }
    return SearchModel(results: results, suggestions: emptyList());
  }
}
