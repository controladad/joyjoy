import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:motivation_flutter/features/quotes/domain/models/quotes_model.dart';
import 'package:motivation_flutter/shared/domain/shared_cache_props_entity.dart';
import 'package:motivation_flutter/shared/domain/shared_cache_props_model.dart';
import 'package:motivation_flutter/shared/infrastructure/dtos/quotes_bundle_dto.dart';
import 'package:motivation_flutter/shared/infrastructure/dtos/shared_cache_props_dto.dart';

abstract class ISharedApiService {
  Future<SharedCachePropsDto> getUserData({
    List<SharedCachePropsEntity>? listOfCacheProps,
  });
  Future<SharedCachePropsDto> updateCacheProps(
    SharedCachePropsModel cacheProps,
  );
  Future<QuotesBundleDto> getNewBundle({
    required List<String> categoryIds,
    required List<String> authorIds,
    required int count,
  });
  Future<QuotesBundleDto> getQuotesByIds({
    required List<String> quotesIds,
  });
  Future<void> reportQuote(QuoteModel quote);
}

@Injectable(as: ISharedApiService)
class QuotesApiServiceImplementation implements ISharedApiService {
  final Dio api;

  QuotesApiServiceImplementation(this.api);

  @override
  Future<SharedCachePropsDto> getUserData({
    List<SharedCachePropsEntity>? listOfCacheProps,
  }) async {
    // String? changeLog;
    if (listOfCacheProps != null) {
      // changeLog = createChangeLog(listOfCacheProps);
    }
    //fake !
    return SharedCachePropsDto.fromEntity(
      listOfCacheProps?.last ??
          SharedCachePropsEntity.fromDomain(SharedCachePropsModel.empty()),
    );
  }

  @override
  Future<QuotesBundleDto> getNewBundle({
    required List<String> categoryIds,
    required List<String> authorIds,
    required int count,
  }) async {
    final response = await api.get(
      'api/V1/quote/random-list',
      queryParameters: {
        'category_ids': categoryIds.join(','),
        'author_ids': authorIds.join(','),
        'limit': 200,
      },
    );

    final data = response.data as Map<String, dynamic>;
    return QuotesBundleDto.fromJson(data);
  }

  @override
  Future<QuotesBundleDto> getQuotesByIds({
    required List<String> quotesIds,
  }) async {
    final response = await api.get(
      'api/V1/quote/list',
      queryParameters: {
        'limit': quotesIds.length,
        'page': 1,
        'ids': quotesIds.join(','),
      },
    );
    final data =
        (response.data as Map<String, dynamic>)['data'] as Map<String, dynamic>;
    return QuotesBundleDto.fromJson(data);
  }

  @override
  Future<SharedCachePropsDto> updateCacheProps(
    SharedCachePropsModel cacheProps,
  ) async {
    await Future.delayed(const Duration(seconds: 1));
    throw UnimplementedError();
  }

  @override
  Future<void> reportQuote(QuoteModel quote) {
    // TODO: implement reportQuote
    throw UnimplementedError();
  }
}
