import 'package:kt_dart/kt.dart';
import 'package:motivation_flutter/common/presentation/common_imports.dart';
import 'package:motivation_flutter/features/explore/application/instance_search_bloc/instance_search_bloc.dart';
import 'package:motivation_flutter/features/explore/domain/models/search_models.dart';
import 'package:motivation_flutter/features/explore/presentation/components/search_components.dart';

@RoutePage()
class InstanceSearchScreen extends StatelessWidget {
  final InstanceSearchBloc? bloc;
  const InstanceSearchScreen({super.key, this.bloc});

  @override
  Widget build(BuildContext context) {
    final bool isInstanceSearch = bloc == null;
    final widget = CommonBackgroundContainer(
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          title: SText(
            context.translate.search.capitalize(),
            style: TStyle.HeadlineSmall,
          ),
          leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: ImageWidget(
              image:
                  isInstanceSearch ? AppIcons.downChevron : AppIcons.backArrow,
              color: AppTheme.on_surface_variant_dark,
              height: 15,
              width: 15,
            ),
          ),
        ),
        body: SafeArea(child: _Body(showTextField: isInstanceSearch)),
        backgroundColor: Colors.transparent,
      ),
    );

    if (bloc != null) {
      return BlocProvider.value(
        value: bloc!,
        child: widget,
      );
    } else {
      return BlocProvider(
        create: (context) =>
            getIt<InstanceSearchBloc>()..add(const InstanceSearchEvent.init()),
        child: widget,
      );
    }
  }
}

class _Body extends StatefulWidget {
  final bool showTextField;
  const _Body({required this.showTextField});

  @override
  State<_Body> createState() => _BodyState();
}

class _BodyState extends State<_Body> {
  late TextEditingController controller;

  @override
  void initState() {
    super.initState();
    controller = TextEditingController();
  }

  void onSuggestionTap(SuggestionModel suggestion) {
    setState(() {
      controller.value = controller.value.copyWith(
        text: suggestion.name,
        composing: TextRange.empty,
        selection: TextSelection.collapsed(
          offset: suggestion.name.length,
        ),
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    final state = context.watch<InstanceSearchBloc>().state;
    final bloc = context.read<InstanceSearchBloc>();

    return Column(
      children: [
        Flexible(
          child: state.instanceSearchData.fold(
            () {
              if (state.isLoading) return const _SearchLoadingStateWidget();

              return const _SearchHistoryWidget();
            },
            (a) => a.fold((l) => Container(), (r) {
              if (r.results.isEmpty()) return const _SearchEmptyState();

              return _SearchResultWidget(
                showSuggestions: widget.showTextField,
                onSuggestionTap: onSuggestionTap,
              );
            }),
          ),
        ),
        if (widget.showTextField)
          SearchTextField(
            onSubmit: (query) =>
                context.pushRoute(InstanceSearchRoute(bloc: bloc)),
            autofocus: true,
            onChanged: (query) =>
                bloc.add(InstanceSearchEvent.queryChanged(query)),
            padding: pad(16, 8, 16, 12),
            controller: controller,
          ),
      ],
    );
  }
}

class _SearchLoadingStateWidget extends StatelessWidget {
  const _SearchLoadingStateWidget();

  @override
  Widget build(BuildContext context) {
    final state = context.watch<InstanceSearchBloc>().state;
    return Column(
      children: [
        Padding(
          padding: pad(30, 12, 0, 0),
          child: Row(
            children: [
              const SBox(
                height: 25,
                width: 25,
                child: CircularProgressIndicator(
                  color: AppTheme.on_surface_variant_dark,
                ),
              ),
              const SBox(width: 12),
              SText(
                '${context.translate.searchingFor.capitalize()} "${state.searchQuery.fold(() => '', (a) => a)}"',
                style: TStyle.BodyLarge,
              )
            ],
          ),
        ),
      ],
    );
  }
}

class _SearchEmptyState extends StatelessWidget {
  const _SearchEmptyState();

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: pad(16, 16, 16, 16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              SText(context.translate.noResult, style: TStyle.BodyLarge),
            ],
          ),
        ],
      ),
    );
  }
}

class _SearchResultWidget extends StatelessWidget {
  final bool showSuggestions;
  final Function(SuggestionModel suggestion) onSuggestionTap;
  const _SearchResultWidget({
    required this.showSuggestions,
    required this.onSuggestionTap,
  });

  @override
  Widget build(BuildContext context) {
    final state = context.watch<InstanceSearchBloc>().state;
    final bloc = context.read<InstanceSearchBloc>();
    KtList<KtList<SearchResultUnion>> results = emptyList();

    results = results.plusElement(
      state.instanceSearchResults
          .filter((quote) => quote is QuoteSearchUnionCase),
    );
    results = results.plusElement(
      state.instanceSearchResults
          .filter((category) => category is CategorySearchUnionCase),
    );
    results = results.plusElement(
      state.instanceSearchResults
          .filter((author) => author is AuthorSearchUnionCase),
    );

    results = results.filter((p0) => p0.isNotEmpty());

    final suggestions = state.suggestions;

    return ListView(
      children: [
        ...results.map(
          (group) {
            return Container(
              margin: pad(16, 0, 0, 0),
              padding: pad(12, 9, 12, 9),
              decoration: BoxDecoration(
                border: Border(
                  bottom: BorderSide(
                    color: AppTheme.outline_dark.withOpacity(.4),
                  ),
                ),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: pad(0, 12, 0, 0),
                        child: ImageWidget(
                          image: group.first().map(
                                category: (e) => AppIcons.searchCategory,
                                author: (e) => AppIcons.searchAuthor,
                                quote: (e) => AppIcons.searchQuote,
                              ),
                          height: 32,
                          width: 32,
                        ),
                      ),
                      const SizedBox(width: 8),
                      Column(
                        children: group
                            .map((item) => SearchListItemWidget(item))
                            .asList(),
                      )
                    ],
                  ),
                  if (group.size > 3)
                    TextButton(
                      onPressed: () {
                        final query =
                            state.searchQuery.fold(() => '', (a) => a);
                        context.pushRoute(
                          FullSearchRoute(
                            query: query,
                            firstItem: group.first(),
                          ),
                        );
                      },
                      child: Row(
                        children: [
                          const ImageWidget(
                            image: AppIcons.plus,
                            height: 32,
                            width: 32,
                          ),
                          const SBox(width: 5),
                          SText(
                            context.translate.seeMore.toUpperCase(),
                            style: TStyle.LabelButton,
                          ),
                        ],
                      ),
                    )
                ],
              ),
            );
          },
        ).asList(),
        if (suggestions.isNotEmpty() && showSuggestions)
          Row(
            children: [
              Padding(
                padding: pad(32, 12, 0, 12),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SText(
                      context.translate.suggestion.capitalize(),
                      style: TStyle.BodyLarge,
                      color: AppTheme.outline_dark,
                    ),
                    const SBox(height: 10),
                    ...suggestions
                        .map(
                          (suggestion) => SBox(
                            height: 30,
                            child: Row(
                              children: [
                                MaterialButton(
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(20),
                                  ),
                                  onPressed: () {
                                    onSuggestionTap(suggestion);
                                    bloc.add(
                                      InstanceSearchEvent.queryChanged(
                                        suggestion.name,
                                      ),
                                    );
                                  },
                                  child: SText(
                                    '${suggestion.name} [${suggestion.count}]',
                                    style: TStyle.TitleMedium,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        )
                        .asList(),
                  ],
                ),
              ),
            ],
          ),
      ],
    );
  }
}

class _SearchHistoryWidget extends StatelessWidget {
  const _SearchHistoryWidget();

  @override
  Widget build(BuildContext context) {
    return Container(
      color: AppTheme.background_dark.withOpacity(.1),
    );
  }
}
