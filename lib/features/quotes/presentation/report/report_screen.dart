import 'package:motivation_flutter/common/presentation/buttons.dart';
import 'package:motivation_flutter/common/presentation/common_presentation.dart';

@RoutePage()
class ReportScreen extends StatefulWidget {
  final Function(String reportText) onSubmit;
  const ReportScreen({super.key, required this.onSubmit});

  @override
  State<ReportScreen> createState() => _ReportScreenState();
}

class _ReportScreenState extends State<ReportScreen> {
  final controller = TextEditingController();

  @override
  void initState() {
    super.initState();
    controller.addListener(() => setState(() {}));
  }

  @override
  Widget build(BuildContext context) {
    return CommonScaffoldWithAppbar(
      resizeToAvoidBottomInset: true,
      title: context.translate.report.capitalize(),
      floatingActionButton: CustomFilledButton(
        label: context.translate.submit,
        isEnable: controller.value.text.isNotEmpty,
        onTap: () async {
          await context.popRoute();
          widget.onSubmit(controller.value.text);
        },
      ),
      body: Column(
        children: [
          SContainer(
            height: 200,
            padding: pad(16, 16, 16, 16),
            decoration: const BoxDecoration(
              border: Border(
                top: BorderSide(width: 0.5, color: AppTheme.on_surface_dark),
                bottom: BorderSide(width: 0.5, color: AppTheme.on_surface_dark),
              ),
            ),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Flexible(
                  child: TextField(
                    maxLength: 500,
                    controller: controller,
                    autofocus: true,
                    decoration: const InputDecoration(
                      border: InputBorder.none,
                      counter: SizedBox(),
                    ),
                    maxLines: 10,
                  ),
                ),
                IconButton(
                  onPressed: () => controller.clear(),
                  icon: const ImageWidget(
                    image: AppIcons.close,
                    height: 24,
                    width: 24,
                    color: AppTheme.white,
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
