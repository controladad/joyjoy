import 'package:motivation_flutter/common/presentation/common_presentation.dart';
import 'package:motivation_flutter/common/presentation/sub_auth_listener_navigator.dart';
import 'package:motivation_flutter/features/force_update/presentation/force_update_dialog.dart';
import 'package:motivation_flutter/features/profile/application/profile_bloc/profile_cubit.dart';

@RoutePage()
class NavBuilderScreen extends StatefulWidget {
  @override
  State<NavBuilderScreen> createState() => _NavBuilderScreenState();
}

class _NavBuilderScreenState extends State<NavBuilderScreen> {
  @override
  void initState() {
    checkUpdate();
    super.initState();
  }

  Future<void> checkUpdate() async {
    final bloc = context.read<ProfileCubit>();
    final result = await bloc.checkUpdate;
    result.fold(
      () => null,
      (a) => {
        if (a.forceUpdate)
          {
            if (mounted) {context.pushAndRemoveAll(const ForceUpdateRoute())}
          }
        else if (a.updateAvailable)
          {
            if (mounted)
              {
                showDialog(
                  context: context,
                  builder: (c) {
                    return ForceUpdateDialog(changeLog: a.changeLog);
                  },
                )
              }
          }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return const SubAuthListenerNavigator(
      child: CommonBackgroundContainer(
        child: SafeArea(
          child: AutoTabsScaffold(
            backgroundColor: AppTheme.background_dark,
            // key: navBuilderKey,
            extendBody: true,
            routes: [
              FeedsRoute(),
              // ExploreTab(),
              // ThemeTab(),
              // ProfileTab(),
            ],
            // bottomNavigationBuilder: _bottomNavigationBar,
            homeIndex: 0,
          ),
        ),
      ),
    );
  }
}

// ignore: unused_element
Widget _bottomNavigationBar(BuildContext context, TabsRouter router) {
  return Stack(
    alignment: Alignment.bottomCenter,
    clipBehavior: Clip.none,
    children: [
      SContainer(
        color: AppTheme.color1,
        height: 95,
        alignment: AlignmentDirectional.center,
        padding: pad(0, 0, 0, 25),
        child: Center(
          child: SContainer(
            maxWidth: 400,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                _BottomNavigationItemWidget(
                  label: context.translate.feeds,
                  inActiveIcon: AppIcons.feedsInActive,
                  tabsRouter: router,
                  activeIcon: AppIcons.feedsActive,
                  index: 0,
                ),
                _BottomNavigationItemWidget(
                  label: context.translate.explore,
                  tabsRouter: router,
                  index: 1,
                  inActiveIcon: AppIcons.exploreInActive,
                  activeIcon: AppIcons.exploreActive,
                ),
                const SizedBox(width: 32),
                _BottomNavigationItemWidget(
                  label: context.translate.theme,
                  index: 2,
                  inActiveIcon: AppIcons.themeInActive,
                  tabsRouter: router,
                  activeIcon: AppIcons.themeActive,
                ),
                _BottomNavigationItemWidget(
                  label: context.translate.profile,
                  index: 3,
                  tabsRouter: router,
                  inActiveIcon: AppIcons.profileInActive,
                  activeIcon: AppIcons.profileInActive,
                ),
              ],
            ),
          ),
        ),
      ),
      Positioned(
        bottom: 32,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            const SBox(height: 4),
            SText(context.translate.talk.capitalize(), style: TStyle.LabelSmall)
          ],
        ),
      ),
    ],
  );
}

class _BottomNavigationItemWidget extends StatelessWidget {
  final String label;
  final String? activeIcon;
  final String? inActiveIcon;
  final int index;
  final TabsRouter tabsRouter;

  const _BottomNavigationItemWidget({
    required this.label,
    this.inActiveIcon,
    this.activeIcon,
    required this.index,
    required this.tabsRouter,
  });

  @override
  Widget build(BuildContext context) {
    final bool isActive = tabsRouter.activeIndex == index;
    Color textAndIconColor() {
      if (isActive) {
        return AppTheme.primary_dark;
      }
      return AppTheme.white;
    }

    String? getIcon() {
      if (isActive) {
        return activeIcon;
      }

      return inActiveIcon;
    }

    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: () {
        tabsRouter.setActiveIndex(index);
      },
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          if (getIcon() != null)
            ImageWidget(
              image: getIcon(),
              height: 32,
              width: 32,
              color: textAndIconColor(),
            )
          else
            const SizedBox(
              height: 32,
            ),
          SizedBox(
            height: 7.5.h,
          ),
          SText(
            label.capitalize(),
            style: TStyle.LabelSmall,
            color: textAndIconColor(),
          )
        ],
      ),
    );
  }
}
