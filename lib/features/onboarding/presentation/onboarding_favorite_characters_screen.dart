import 'package:kt_dart/kt.dart';
import 'package:motivation_flutter/common/presentation/buttons.dart';
import 'package:motivation_flutter/common/presentation/common_presentation.dart';
import 'package:motivation_flutter/common/presentation/utils/failure_handler_widget.dart';
import 'package:motivation_flutter/features/onboarding/application/onboarding_bloc/onboarding_bloc.dart';
import 'package:motivation_flutter/features/onboarding/domain/models/onboarding_model.dart';
import 'package:motivation_flutter/features/onboarding/presentation/components/favorite_character_component.dart';
import 'package:motivation_flutter/features/quotes/application/quotes_bloc/quotes_bloc.dart';

class OnboardingFavoriteCharactersScreen extends StatelessWidget {
  final Function() onNext;
  const OnboardingFavoriteCharactersScreen({super.key, required this.onNext});

  @override
  Widget build(BuildContext context) {
    return Builder(
      builder: (context) {
        final state = context.watch<OnboardingBloc>().state;
        return Scaffold(
          backgroundColor: Colors.transparent,
          appBar: AppBar(
            backgroundColor: Colors.transparent,
            leadingWidth: double.infinity,
            leading: ListTile(
              leading: const ImageWidget(
                image: AppIcons.favoriteCharacter,
                height: 43,
                width: 38,
                color: AppTheme.on_surface_variant_dark,
              ),
              title: SText(
                context.translate.favoriteCharacters,
                style: TStyle.HeadlineSmall,
                color: AppTheme.on_surface_dark,
              ),
              subtitle: SText(
                context.translate.favoriteCharactersStatement,
                style: TStyle.LabelSmall,
                color: AppTheme.on_surface_dark,
              ),
            ),
          ),
          body: SafeArea(
            child: FailureHandlerWidget<OnboardingModel>(
              data: state.onboardingData,
              child: (r) => _Body(r),
              onRetry: () {},
            ),
          ),
          floatingActionButtonLocation:
              FloatingActionButtonLocation.centerDocked,
          floatingActionButton: state.onboardingData.fold(
            () => null,
            (a) => Container(
              width: double.infinity,
              decoration: BoxDecoration(
                color: AppTheme.surface1_dark.withOpacity(.05),
                borderRadius: const BorderRadius.only(
                  topLeft: Radius.circular(25),
                  topRight: Radius.circular(25),
                ),
              ),
              child: CustomFilledButton(
                label: state.selectedCharacterIds.isEmpty()
                    ? context.translate.skip
                    : context.translate.next,
                padding: pad(16, 12, 16, 32),
                onTap: () {
                  forceBlocToStart<QuotesBloc>(context);
                  onNext();
                },
              ),
            ),
          ),
        );
      },
    );
  }
}

class _Body extends StatelessWidget {
  final OnboardingModel data;
  const _Body(this.data);

  @override
  Widget build(BuildContext context) {
    final bloc = context.read<OnboardingBloc>();
    final state = context.watch<OnboardingBloc>().state;
    return Column(
      children: [
        Expanded(
          child: ListView(
            padding: pad(0, 10, 0, 100),
            children: data.authors
                .map(
                  (p0) => FavoriteCharacterComponent(
                    character: p0,
                    isSelected: state.selectedCharacterIds.contains(p0.id),
                    onTap: () =>
                        bloc.add(OnboardingEvent.characterClick(p0.id)),
                  ),
                )
                .asList(),
          ),
        ),
      ],
    );
  }
}

// class FavoriteCharactersPhoneView extends StatefulWidget {
//   const FavoriteCharactersPhoneView({super.key});

//   @override
//   State<FavoriteCharactersPhoneView> createState() =>
//       _FavoriteCharactersPhoneViewState();
// }

// class _FavoriteCharactersPhoneViewState
//     extends State<FavoriteCharactersPhoneView> {

//   @override
//   void dispose() {
//     refreshController.dispose();
//     super.dispose();
//   }

//   @override
//   Widget build(BuildContext context) {
//     // use FavoriteCharactersBloc Just for search on loading data
//     final FavoriteCharactersBloc favoriteCharactersBloc =
//         context.read<FavoriteCharactersBloc>();
//     final FavoriteCharactersState favoriteCharactersState =
//         context.watch<FavoriteCharactersBloc>().state;

//     // use onboardingBloc to select characters
//     final onboardingBloc = context.read<OnboardingBloc>();
//     final onboardingState = context.watch<OnboardingBloc>().state;

//     return BlocListener<FavoriteCharactersBloc, FavoriteCharactersState>(
//       listenWhen: (previous, current) =>
//           previous.isSucceed != current.isSucceed,
//       listener: (context, state) {
//         state.isSucceed.fold(
//           () => null,
//           (a) => a.fold(
//             (l) => refreshController.loadFailed(),
//             (r) {
//               if (state.hasReachedMax) {
//                 refreshController.loadNoData();
//               } else {
//                 refreshController.loadFailed();
//               }
//             },
//           ),
//         );
//       },
//       child: Scaffold(
//         body: CommonBackgroundContainer(
//           child: SafeArea(
//             child: Column(
//               children: [
//                 const SizedBox(height: 10),
//                 if (isSearchMode == false)

//                 if (isSearchMode)
//                   Row(
//                     children: [
//                       const SizedBox(width: 33),
//                       GestureDetector(
//                         onTap: () {
//                           FocusScope.of(context).requestFocus(FocusNode());
//                           favoriteCharactersBloc.add(
//                             const FavoriteCharactersEvent.clearSearchQuery(),
//                           );
//                         },
//                         child: const ImageWidget(
//                           image: AppIcons.downChevron,
//                           height: 7,
//                           width: 14,
//                           color: AppTheme.on_surface_variant_dark,
//                         ),
//                       ),
//                       const SizedBox(width: 20),
//                       SText(
//                         context.translate.search,
//                         style: TStyle.HeadlineSmall,
//                       ),
//                     ],
//                   ),
//                 Expanded(
//                   child: favoriteCharactersState.data.fold(
//                     () => const CommonLoader(),
//                     (a) => a.fold(
//                       (l) => Container(),
//                       (r) {
//                         final KtList<FavoriteCharacterModel> targetList =
//                             isSearchMode
//                                 ? favoriteCharactersState
//                                     .searchQueryResultCharacterList
//                                 : favoriteCharactersState.charactersList;
//                         final hasReachMax = isSearchMode
//                             ? favoriteCharactersState
//                                 .hasReachedMaxOfSearchQueryResult
//                             : favoriteCharactersState.hasReachedMax;
//                         final bool lazyLoadIsEnable = hasReachMax == false &&
//                             favoriteCharactersState.isLoading == false;
//                         return SmartRefresher(
//                           onLoading: () {
//                             if (hasReachMax == false &&
//                                 favoriteCharactersState.isLoading == false) {
//                               favoriteCharactersBloc.add(
//                                 const FavoriteCharactersEvent.loadData(
//                                   isLazyLoad: true,
//                                 ),
//                               );
//                             }
//                           },
//                           controller: refreshController,
//                           enablePullUp: lazyLoadIsEnable,
//                           enablePullDown: false,
//                           child: SingleChildScrollView(
//                             child: Column(
//                               children: [
//                                 for (final character in targetList.asList())
//                                   FavoriteCharacterComponent(
//                                     character: character,
//                                     isSelected: onboardingState
//                                         .selectedCharacter
//                                         .any((c) => c.id == character.id),
//                                     onTap: () => onboardingBloc.add(
//                                       OnboardingEvent.characterClick(character),
//                                     ),
//                                   ),
//                                 if (favoriteCharactersState.isLoading)
//                                   const CommonLoader(),
//                                 const SizedBox(height: 20),
//                               ],
//                             ),
//                           ),
//                         );
//                       },
//                     ),
//                   ),
//                 ),
//                 AnimatedContainer(
//                   duration: const Duration(milliseconds: 100),
//                   width: double.infinity,
//                   color: keyboardIsVisible(context)
//                       ? Colors.transparent
//                       : AppTheme.color1,
//                   child: Column(
//                     mainAxisSize: MainAxisSize.min,
//                     children: [
//                       const SizedBox(height: 11),
//                       CustomTextField(
//                         onChanged: (value) => favoriteCharactersBloc.add(
//                           FavoriteCharactersEvent.searchStringChanged(value),
//                         ),
//                         onFocusedChanged: (hasFocus) {
//                           setState(() {
//                             isSearchMode = hasFocus;
//                           });
//                         },
//                       ),
//                       const SizedBox(height: 16),
//                       OnKeyboardUpDisappearWidget(
//                         child: Column(
//                           children: [
//                             CustomFilledButton(
//                               label: context.translate.next,
//                               onTap: () {
//                                 context.pushRoute(
//                                   HowDoYouFeelRoute(
//                                     onboardingBloc: onboardingBloc,
//                                   ),
//                                 );
//                               },
//                               height: 52,
//                             ),
//                             const SizedBox(height: 32),
//                           ],
//                         ),
//                       ),
//                     ],
//                   ),
//                 )
//               ],
//             ),
//           ),
//         ),
//       ),
//     );
//   }
// }
