part of 'my_collection_bloc.dart';

@freezed
class MyCollectionEvent with _$MyCollectionEvent {
  const factory MyCollectionEvent.getCollections() = _GetCollections;
  const factory MyCollectionEvent.addNewCollection(
    String name, {
    bool? addToChangeList,
  }) = _AddNewCollection;

  const factory MyCollectionEvent.toggleQuoteInChangeList({
    required QuoteModel quote,
    required String collectionId,
  }) = _ToggleQuoteInChangeList;

  const factory MyCollectionEvent.saveChanges(String quoteId) = _SaveChanges;

  const factory MyCollectionEvent.renameCollection({
    required String name,
    required String collectionId,
  }) = _RenameCollection;

  const factory MyCollectionEvent.removeCollection(String id) =
      _RemoveCollection;

  const factory MyCollectionEvent.cancelChanges() = _CancelChanges;
}
