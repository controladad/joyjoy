import 'package:motivation_flutter/common/presentation/common_presentation.dart';
import 'package:motivation_flutter/features/onboarding/application/onboarding_bloc/onboarding_bloc.dart';
import 'package:motivation_flutter/features/onboarding/presentation/onboarding_users_name_screen.dart';

@RoutePage()
class OnboardingScreen extends StatelessWidget {
  const OnboardingScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) =>
          getIt<OnboardingBloc>()..add(const OnboardingEvent.getData()),
      child: Builder(
        builder: (context) {
          final state = context.watch<OnboardingBloc>().state;
          final int initialPage = state.onboardingCacheData.fold(
            () => 0,
            (a) => a.fold(
              (l) => 0,
              (r) {
                return 0;
                // if (r.userName.isNone()) return 0;
                // if (r.selectedFavoriteCategories.isEmpty()) return 1;
                // if (r.selectedFavoriteAuthors.isEmpty()) return 2;
                // if (r.didTryChat == false) return 3;
                // return 4;
              },
            ),
          );

          return state.onboardingCacheData
              .fold(() => Container(), (a) => _OnboardingFlow(initialPage));
        },
      ),
    );
  }
}

class _OnboardingFlow extends StatefulWidget {
  final int initialPage;
  const _OnboardingFlow(this.initialPage);

  @override
  State<_OnboardingFlow> createState() => _OnboardingFlowState();
}

class _OnboardingFlowState extends State<_OnboardingFlow> {
  late PageController controller;
  late VoidCallback next;
  late VoidCallback previous;

  @override
  void initState() {
    super.initState();
    controller = PageController(initialPage: widget.initialPage);

    next = () => controller.nextPage(
          duration: const Duration(milliseconds: 600),
          curve: Curves.ease,
        );
    previous = () => controller.previousPage(
          duration: const Duration(milliseconds: 250),
          curve: Curves.ease,
        );
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        previous();
        return false;
      },
      child: CommonBackgroundContainer(
        child: PageView(
          physics: const NeverScrollableScrollPhysics(),
          controller: controller,
          children: [
            OnboardingUsersNameScreen(
              onNext: () => context.pushAndRemoveAll(const NavBuilderRoute()),
            )
            // OnboardingUsersNameScreen(onNext: next),
            // OnboardingFavoriteCategoriesScreen(onNext: next),
            // OnboardingFavoriteCharactersScreen(onNext: next),
            // HowDoYouFeelScreen(
            //   onNext: () => Future.delayed(const Duration(milliseconds: 250))
            //       .then((_) => next()),
            // ),
            // OnboardingReviewsScreen(
            //   onSkip: () async {
            //     context.pushAndRemoveAll(
            //       PaywallRoute(
            //         onClose: (ctx) =>
            //             ctx.pushAndRemoveAll(const NavBuilderRoute()),
            //       ),
            //     );
            //   },
            // ),
          ],
        ),
      ),
    );
  }
}
