import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:kt_dart/kt.dart';
import 'package:motivation_flutter/features/sub_auth/domain/models/review_model.dart';

part 'paywall_model.freezed.dart';

@freezed
class PaywallModel with _$PaywallModel {
  const factory PaywallModel({
    required String userName,
    required PaywallTrialScheduleModel schedule,
    required KtList<PayWallSubscriptionPlanModel> plans,
    required KtList<PayWallOptionDefinitionModel> definitions,
    required KtList<PayWallComparisonTableItemModel> tableItems,
    required KtList<PaywallReviewModel> reviews,
    required KtList<QuestionAndAnswerModel> questions,
  }) = _PayWallModel;
}

@freezed
class PaywallTrialScheduleModel with _$PaywallTrialScheduleModel {
  const factory PaywallTrialScheduleModel({
    required KtList<PayWallTrialScheduleItemModel> items,
    required int daysToNotify,
    required int trialDays,
  }) = _PaywallTrialScheduleModel;
}

@freezed
class PayWallTrialScheduleItemModel with _$PayWallTrialScheduleItemModel {
  const factory PayWallTrialScheduleItemModel({
    required String id,
    required String icon,
    required String title,
    required String subtitle,
  }) = _PayWallTrialScheduleItemModel;
}

@freezed
class PayWallSubscriptionPlanModel with _$PayWallSubscriptionPlanModel {
  const factory PayWallSubscriptionPlanModel({
    required String id,
    required String title,
    required int duration,
    required String durationType,
    double? discountedPrice,
    double? discount,
    String? subTitle,
    required double price,
    PlanTrialModel? trialModel,
    required String productId,
    required String identifier,
  }) = _PayWallSubscriptionPlanModel;
}

@freezed
class PlanTrialModel with _$PlanTrialModel {
  const factory PlanTrialModel({
    required int duration,
    required String unit,
  }) = _PlanTrialModel;
}

@freezed
class PayWallOptionDefinitionModel with _$PayWallOptionDefinitionModel {
  const factory PayWallOptionDefinitionModel({
    required String icon,
    required String title,
    required String subtitle,
  }) = _PayWallOptionDefinitionModel;
}

@freezed
class PayWallComparisonTableItemModel with _$PayWallComparisonTableItemModel {
  const factory PayWallComparisonTableItemModel({
    required String id,
    required String label,
    required OptionAvailabilityEnum availabilityOnBasic,
    required OptionAvailabilityEnum availabilityOnPremium,
  }) = _PayWallComparisonTableItemModel;
}

enum OptionAvailabilityEnum {
  Lock,
  Ad,
  Open,
}

@freezed
class QuestionAndAnswerModel with _$QuestionAndAnswerModel {
  const factory QuestionAndAnswerModel({
    required String id,
    required String question,
    required String answer,
  }) = _QuestionAndAnswerModel;
}
