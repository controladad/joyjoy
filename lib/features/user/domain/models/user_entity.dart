import 'package:hive/hive.dart';

part 'user_entity.g.dart';

@HiveType(typeId: 0)
class UserEntity {
  @HiveField(0)
  String? name;
  @HiveField(1)
  String? family;
  @HiveField(2)
  String? token;
  @HiveField(3)
  String? email;
  @HiveField(4)
  DateTime? timerForVerification;

  UserEntity({
    this.name,
    this.family,
    this.token,
    this.email,
    this.timerForVerification,
  });

  UserEntity copyWith({
    String? name,
    String? family,
    String? token,
    String? email,
    DateTime? timerForVerification,
  }) {
    return UserEntity(
      name: name ?? this.name,
      timerForVerification: timerForVerification ?? this.timerForVerification,
      family: family ?? this.family,
      token: token ?? this.token,
      email: email ?? this.email,
    );
  }
}
