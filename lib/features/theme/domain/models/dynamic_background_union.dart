import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:motivation_flutter/common/presentation/common_presentation.dart';

part 'dynamic_background_union.freezed.dart';

@freezed
class DynamicBackgroundUnion with _$DynamicBackgroundUnion {
  const factory DynamicBackgroundUnion.color(Color color) = _Color;
  const factory DynamicBackgroundUnion.gradient(List<Color> colors) = _Gradient;
  const factory DynamicBackgroundUnion.image(Uint8List image) = _Image;
}
