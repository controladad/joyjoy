class EnvConstants {
  EnvConstants._();

  static const String admob_chat_unit_id =
      'ca-app-pub-9373648843858942/6318591563';

  static const String admob_global_unit_id =
      'ca-app-pub-9373648843858942/2536346302';

  static const String revenueCatIosApiKey =
      String.fromEnvironment('revenueCatIosApiKey');
  static const String revenueCatIosAppId =
      String.fromEnvironment('revenueCatIosAppId');
}
