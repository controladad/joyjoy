class AppIcons {
  AppIcons._();

  static const String seeAll = 'assets/icons/see_all.svg';
  static const String creamCheck = 'assets/icons/cream_check.png';
  static const String setting = 'assets/icons/gear_setting.png';
  static const String credit = 'assets/icons/credit.png';
  static const String feedsFilter = 'assets/icons/feeds_filter.png';
  static const String filter = 'assets/icons/filter.png';
  static const String chat = 'assets/icons/chat.png';
  static const String dislike = 'assets/icons/dislike.png';
  static const String dislikeFilled = 'assets/icons/dislike_filled.png';
  static const String like = 'assets/icons/like.png';
  static const String copy = 'assets/icons/copy.png';
  static const String translate = 'assets/icons/translate.png';
  static const String likeFilled = 'assets/icons/like_filled.png';
  static const String threeDot = 'assets/icons/three_dot.png';
  static const String showPass = 'assets/icons/show_password.svg';
  static const String hidePass = 'assets/icons/hide_password.svg';
  static const String logout = 'assets/icons/logout.png';
  static const String burger = 'assets/icons/burger.png';
  static const String disabledDiamond = 'assets/icons/disabled_diamond.png';
  static const String user = 'assets/icons/user.png';
  static const String google = 'assets/icons/google.png';
  static const String apple = 'assets/icons/apple.png';
  static const String feedsInActive = 'assets/icons/feeds_inactive.png';
  static const String privacy = 'assets/icons/privacy.png';
  static const String feedsActive = 'assets/icons/feeds_active.png';
  static const String talk = 'assets/icons/talk.png';
  static const String profileInActive = 'assets/icons/profile_inactive.png';
  static const String themeInActive = 'assets/icons/theme_inactive.png';
  static const String themeActive = 'assets/icons/theme_active.png';
  static const String exploreInActive = 'assets/icons/explore_inactive.png';
  static const String exploreActive = 'assets/icons/explore_active.png';
  static const String dropdown = 'assets/icons/dropdown.svg';
  static const String search = 'assets/icons/search.svg';
  static const String searchExplore = 'assets/icons/search_explore.png';
  static const String plusBold = 'assets/icons/plus_bold.svg';
  static const String close = 'assets/icons/close.svg';
  static const String backArrow = 'assets/icons/arrow_back.svg';
  static const String rightChevron = 'assets/icons/right_chevron.svg';
  static const String unSelectedHomeIcon = 'assets/icons/unselected_home.svg';
  static const String selectedProfileIcon = 'assets/icons/selected_profile.svg';
  static const String play = 'assets/icons/play.svg';
  static const String pause = 'assets/icons/pause.svg';
  static const String selectedHomeIcon = 'assets/icons/selected_home.svg';
  static const String unSelectedProfileIcon =
      'assets/icons/unselected_profile.svg';
  static const String emojisHappy = 'assets/images/happy.png';
  static const String emojisSad = 'assets/images/sad.png';
  static const String emojisAngry = 'assets/images/angry.png';
  static const String emojisAmazed = 'assets/images/amazed.png';
  static const String person = 'assets/icons/person.png';
  static const String unlock = 'assets/icons/unlock.png';
  static const String cloud = 'assets/icons/cloud.png';
  static const String cross = 'assets/icons/cross.png';
  static const String ring = 'assets/icons/ring.png';
  static const String star = 'assets/icons/star.png';
  static const String lock = 'assets/icons/lock.png';
  static const String ad = 'assets/icons/ad.png';
  static const String emptyStar = 'assets/icons/empty_star.png';
  static const String fullStar = 'assets/icons/full_star.png';
  static const String checkMark = 'assets/icons/checkboxes-dark.png';
  static const String downChevron = 'assets/icons/down_chevron.png';
  static const String gradientLock = 'assets/icons/gradient_lock.png';
  static const String favoriteCharacter = 'assets/icons/favorite_character.svg';
  static const String checkbox_dark = 'assets/icons/checkboxes_dark.png';
  static const String diamond = 'assets/icons/diamond.png';
  static const String share = 'assets/icons/share.svg';
  static const String heart = 'assets/icons/heart.svg';
  static const String bigAd = 'assets/icons/big_ad.png';
  static const String twitter = 'assets/icons/twitter.png';
  static const String instagram = 'assets/icons/instagram.png';
  static const String messages = 'assets/icons/messages.png';
  static const String facebook = 'assets/icons/facebook.png';
  static const String whatsapp = 'assets/icons/whatsapp.png';
  static const String saveImage = 'assets/icons/save_image.png';
  static const String addToCollection = 'assets/icons/add_to_collection.png';
  static const String reportQuote = 'assets/icons/report_quote.png';
  static const String copyText = 'assets/icons/copy_text.png';
  static const String filledHeart = 'assets/icons/filled_heart.svg';
  static const String categorySub = 'assets/icons/category_sub.png';
  static const String searchCategory = 'assets/icons/search_category_icon.png';
  static const String searchAuthor = 'assets/icons/search_author_icon.png';
  static const String searchQuote = 'assets/icons/search_quote_icon.png';
  static const String whiteLock = 'assets/icons/white_lock.png';
  static const String profileAbout = 'assets/icons/profile_about.png';
  static const String plus = 'assets/icons/plus.svg';
  static const String minus = 'assets/icons/minus.svg';
  static const String profileCustomerServices =
      'assets/icons/profile_customer_services.png';
  static const String profileCustomizeWidgets =
      'assets/icons/profile_customize_widget.png';
  static const String profileFAQ = 'assets/icons/profile_faq.png';
  static const String profileFavoriteAuthors =
      'assets/icons/profile_favorite_authors.png';
  static const String profileManageAccount =
      'assets/icons/profile_manage_account.png';
  static const String profileManageLikes =
      'assets/icons/profile_manage_likes.png';
  static const String profileManageSubscription =
      'assets/icons/profile_manage_subscription.png';
  static const String profileMyCollection =
      'assets/icons/profile_my_collection.png';
  static const String profilePreferredTopics =
      'assets/icons/profile_preferred_topics.png';
  static const String termsOfUse = 'assets/icons/profile_privacy_policy.png';
  static const String profileRateUs = 'assets/icons/profile_rate_us.png';
  static const String trash = 'assets/icons/trash.png';
  static const String share_white = 'assets/icons/share_white.png';
  static const String profileReferFriends =
      'assets/icons/profile_refer_friends.png';
  static const String profileReminders = 'assets/icons/profile_reminders.png';
  static const String dragHandler = 'assets/icons/drag_handler.png';
  static const String smallDiamond = 'assets/icons/small_diamond.png';
  static const String pinkCheck = 'assets/icons/pink_check.png';
  static const String themeIcon = 'assets/icons/theme_icon.png';
  static const String unselectedPinkCheck =
      'assets/icons/unselected_pink_check.png';
  static const String sampleReminderIcon =
      'assets/icons/sample_reminder_icon.png';
  static const String profileRestorePurchase =
      'assets/icons/profile_restore_purchase.png';
}
