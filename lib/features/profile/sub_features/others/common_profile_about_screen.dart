import 'package:motivation_flutter/common/presentation/common_presentation.dart';

enum ProfileAboutType { AboutUs, privacyPolicy, TermsOfUse }

@RoutePage()
class CommonProfileAboutScreen extends StatelessWidget {
  final String title;
  final String content;
  const CommonProfileAboutScreen({
    super.key,
    required this.title,
    required this.content,
  });

  @override
  Widget build(BuildContext context) {
    return CommonScaffoldWithAppbar(
      title: title,
      body: SingleChildScrollView(
        padding: const EdgeInsets.all(12.0),
        child: Column(
          children: [
            SText(content, style: TStyle.BodyMedium),
          ],
        ),
      ),
    );
  }
}
