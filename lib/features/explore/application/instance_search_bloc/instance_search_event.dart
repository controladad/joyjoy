part of 'instance_search_bloc.dart';

@freezed
class InstanceSearchEvent with _$InstanceSearchEvent {
  const factory InstanceSearchEvent.init() = _Init;
  const factory InstanceSearchEvent.queryChanged(String query) = _QueryChanged;
  const factory InstanceSearchEvent.submitQuery(String query) = _SubmitQuery;
  const factory InstanceSearchEvent.clearHistory() = _ClearHistory;
  const factory InstanceSearchEvent.onSuggestionTap(String suggestion) =
      _OnSuggestionTap;
}
