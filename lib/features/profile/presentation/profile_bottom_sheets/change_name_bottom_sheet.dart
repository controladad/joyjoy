import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:motivation_flutter/common/presentation/buttons.dart';
import 'package:motivation_flutter/common/presentation/common_presentation.dart';
import 'package:motivation_flutter/common/presentation/utils/custom_bottom_sheets.dart';
import 'package:motivation_flutter/features/profile/application/profile_bloc/profile_cubit.dart';

void showChangeNameBottomSheet(BuildContext context, String name) {
  showCustomBottomSheet(
    context: context,
    onClose: () {},
    child: _ChangeNameBottomSheet(name),
  );
}

class _ChangeNameBottomSheet extends HookWidget {
  final String defaultName;
  const _ChangeNameBottomSheet(this.defaultName);

  @override
  Widget build(BuildContext context) {
    final controller = useTextEditingController(text: defaultName);

    final profileCubit = context.read<ProfileCubit>();

    return Padding(
      padding: pad(16, 10, 16, 12),
      child: Column(
        children: [
          SText(
            context.translate.whatShouldICallYou,
            style: TStyle.TitleLarge,
          ),
          const SBox(height: 24),
          CustomTextField(
            controller: controller,
            onChanged: (userName) {},
            autofocus: true,
            icon: Padding(
              padding: const EdgeInsets.all(4),
              child: CustomFilledButton(
                label: context.translate.save,
                onTap: () async {
                  await profileCubit.changeUserName(controller.text);
                  if (context.mounted) {
                    Navigator.pop(context);
                  }
                },
                isEnable: controller.text.isNotEmpty,
                maxWidth: 73,
                width: 73,
              ),
            ),
          ),
          const SBox(height: 20),
        ],
      ),
    );
  }
}
