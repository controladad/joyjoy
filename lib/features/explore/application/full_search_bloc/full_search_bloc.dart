import 'package:dartz/dartz.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:kt_dart/kt.dart';
import 'package:motivation_flutter/common/common_models.dart';
import 'package:motivation_flutter/common/presentation/common_imports.dart';
import 'package:motivation_flutter/core/failures.dart';

import 'package:motivation_flutter/features/explore/domain/explore_repository.dart';
import 'package:motivation_flutter/features/explore/domain/models/search_models.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

part 'full_search_bloc.freezed.dart';
part 'full_search_event.dart';
part 'full_search_state.dart';

@injectable
class FullSearchBloc extends Bloc<FullSearchEvent, FullSearchState> {
  final IExploreRepository repository;
  FullSearchBloc(this.repository, FullSearchState init) : super(init) {
    on<FullSearchEvent>(_bloc);
  }

  int page = 1;
  late String query;
  late SearchResultUnion firsItem;
  bool hasReachEnd = false;

  Future<void> _bloc(FullSearchEvent event, Emitter emit) {
    return event.map(
      getFirstPage: (e) async {
        query = e.query;
        firsItem = e.firstItem;
        emit(state.copyWith(isLoading: true));
        final result = await repository.fullSearchQuery(
          query: e.query,
          firstItem: e.firstItem,
          page: 1,
        );
        emit(
          state.copyWith(isLoading: false, fullSearchResultsData: some(result)),
        );

        result.fold((l) => null, (r) {
          page += 1;
          emit(
            state.copyWith(
              results: r.results,
              total: some(r.total),
            ),
          );
          hasReachEnd =
              state.results.size >= state.total.fold(() => 0, (a) => a);
        });
      },
      lazyLoad: (e) async {
        if (hasReachEnd == true) {
          state.controller.loadNoData();
          return;
        }

        emit(state.copyWith(isLoading: true));
        final result = await repository.fullSearchQuery(
          query: query,
          page: page,
          firstItem: firsItem,
        );
        emit(
          state.copyWith(
            isLoading: false,
            fullSearchResultsData: some(result),
          ),
        );
        result.fold((l) {}, (r) {
          page++;
          emit(
            state.copyWith(
              results: state.results.plus(r.results),
            ),
          );
          hasReachEnd =
              state.results.size >= state.total.fold(() => 0, (a) => a);
        });

        result.fold((l) {
          state.controller.loadFailed();
        }, (r) {
          if (state.results.size >= state.total.fold(() => 0, (a) => a)) {
            state.controller.loadNoData();
          }
          state.controller.loadComplete();
        });
      },
      refresh: (e) async {
        emit(state.copyWith(isLoading: true));
        page = 1;
        final result = await repository.fullSearchQuery(
          page: page,
          firstItem: firsItem,
          query: query,
        );
        emit(
          state.copyWith(
            isLoading: false,
            fullSearchResultsData: some(result),
          ),
        );
        result.fold((l) {}, (r) {
          page += 1;
          emit(
            state.copyWith(
              results: r.results,
              total: some(r.total),
            ),
          );
          hasReachEnd =
              state.results.size >= state.total.fold(() => 0, (a) => a);

          result.fold((l) {
            state.controller.refreshFailed();
          }, (r) {
            state.controller.refreshCompleted();
          });
        });
      },
    );
  }
}
