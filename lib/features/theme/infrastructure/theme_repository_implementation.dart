import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:kt_dart/kt.dart';
import 'package:motivation_flutter/common/presentation/common_imports.dart';
import 'package:motivation_flutter/core/failures.dart';

import 'package:motivation_flutter/features/theme/domain/theme_repository.dart';
import 'package:motivation_flutter/features/theme/domain/models/theme_models.dart';
import 'package:motivation_flutter/shared/infrastructure/shared_data_source.dart';

@Injectable(as: IThemeRepository)
class ThemeRepositoryImplementation implements IThemeRepository {
  final ISharedDataSource sharedDataSource;

  ThemeRepositoryImplementation(this.sharedDataSource);

  @override
  Future<Either<GeneralFailure, KtList<ThemeModel>>> get getThemes =>
      tryCacheBlock(() => sharedDataSource.getThemes);

  @override
  Future<Either<GeneralFailure, ThemeModel>> selectTheme(String themeId) =>
      tryCacheBlock(() => sharedDataSource.selectTheme(themeId));

  @override
  Future<Either<GeneralFailure, ThemeModel>> get getSelectedTheme =>
      tryCacheBlock(() => sharedDataSource.getSelectedTheme);
}
