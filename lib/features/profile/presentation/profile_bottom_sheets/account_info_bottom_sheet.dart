import 'package:motivation_flutter/common/application/ad_cubit.dart';
import 'package:motivation_flutter/common/presentation/buttons.dart';
import 'package:motivation_flutter/common/presentation/common_presentation.dart';
import 'package:motivation_flutter/common/presentation/utils/custom_bottom_sheets.dart';
import 'package:motivation_flutter/features/profile/application/profile_bloc/profile_cubit.dart';
import 'package:motivation_flutter/features/profile/presentation/profile_bottom_sheets/change_name_bottom_sheet.dart';
import 'package:motivation_flutter/features/profile/presentation/profile_bottom_sheets/status_bottom_sheets.dart';
import 'package:motivation_flutter/features/sub_auth/application/sub_auth_bloc/sub_auth_bloc.dart';

void showAccountInfoBottomSheet(BuildContext context) {
  showCustomBottomSheet(
    context: context,
    onClose: () {},
    child: _AccountInfoBottomSheet(),
  );
}

class _AccountInfoBottomSheet extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final profileState = context.watch<ProfileCubit>().state;

    final accountInfo = profileState.offlineData.fold(
      () => null,
      (a) => a.fold(
        (l) => null,
        (r) => r.accountInfo,
      ),
    );

    final credit = accountInfo.toOption().fold(
          () => 0,
          (a) => a.map(
            loggedIn: (user) => user.credits,
            guest: (_) => 0,
          ),
        );

    final userName = accountInfo?.name.fold(() => '', (a) => a) ?? '';

    return Padding(
      padding: pad(16, 0, 16, 0),
      child: Column(
        children: [
          Padding(
            padding: pad(0, 8, 0, 26),
            child: Row(
              children: [
                const CommonBackArrowButton(),
                SText(
                  context.translate.account.capitalize(),
                  style: TStyle.TitleLarge,
                ),
              ],
            ),
          ),
          _ItemWidget(
            action: _WatchAdButton(),
            onTap: () {},
            icon: AppIcons.credit,
            title: context.translate.creditBalance,
            value: credit.toString(),
          ),
          _ItemWidget(
            action: const ImageWidget(
              image: AppIcons.rightChevron,
              height: 12,
              width: 12,
              color: AppTheme.white,
            ),
            onTap: () {
              showStatusBottomSheet(context);
            },
            icon: AppIcons.setting,
            title: context.translate.status,
            value: context.translate.active,
          ),
          _ItemWidget(
            action: const ImageWidget(
              image: AppIcons.rightChevron,
              height: 12,
              width: 12,
              color: AppTheme.white,
            ),
            onTap: () => showChangeNameBottomSheet(
              context,
              userName,
            ),
            icon: AppIcons.profileManageAccount,
            title: context.translate.nickname,
            value: userName,
          ),
          const SBox(height: 32),
          _LogoutButton(),
          const SBox(height: 40),
        ],
      ),
    );
  }
}

class _WatchAdButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final adCubit = context.read<AdCubit>();
    return GestureDetector(
      onTap: () => adCubit.showRewardedAd(true),
      child: Row(
        children: [
          SText(
            context.translate.addCredit.toUpperCase(),
            style: TStyle.LabelButton,
            color: AppTheme.primary_dark,
          ),
          const SBox(width: 5),
          const ImageWidget(
            image: AppIcons.bigAd,
            height: 24,
            width: 24,
          ),
        ],
      ),
    );
  }
}

class _LogoutButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final authBloc = context.read<SubAuthCubit>();
    final authState = context.watch<SubAuthCubit>().state;
    return CustomOutlinedButton(
      label: context.translate.logout,
      isLoading: authState.isLoading,
      onTap: () async {
        await authBloc.logout();
        if (context.mounted) {
          context.pushAndRemoveAll(const SplashRoute());
        }
      },
      leftIcon: const ImageWidget(
        image: AppIcons.logout,
        height: 18,
        width: 18,
      ),
    );
  }
}

class _ItemWidget extends StatelessWidget {
  final Widget action;
  final Function() onTap;
  final String icon;
  final String title;
  final String value;
  const _ItemWidget({
    required this.action,
    required this.onTap,
    required this.icon,
    required this.title,
    required this.value,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: pad(0, 3, 0, 3),
      child: MaterialButton(
        onPressed: () => onTap(),
        padding: pad(9, 16, 9, 16),
        height: 70,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15),
          side: const BorderSide(
            width: .25,
            color: AppTheme.outline_variant_dark,
          ),
        ),
        child: Row(
          children: [
            ImageWidget(image: icon, height: 32, width: 32),
            const SBox(width: 12),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SText(
                  title.toTitleCase(),
                  style: TStyle.LabelSmall,
                ),
                SText(
                  value.capitalize(),
                  style: TStyle.TitleMedium,
                ),
              ],
            ),
            const Spacer(),
            action,
          ],
        ),
      ),
    );
  }
}
