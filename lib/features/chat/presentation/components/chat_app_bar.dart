import 'package:motivation_flutter/common/presentation/common_imports.dart';

AppBar chatAppBar(BuildContext context) {
  return AppBar(
    backgroundColor: AppTheme.color1,
    actions: [
      IconButton(
        padding: pad(10, 0, 10, 0),
        onPressed: () => context.popRoute(),
        icon: const ImageWidget(
          image: AppIcons.close,
          color: AppTheme.white,
          height: 25,
          width: 25,
        ),
      )
    ],
    title: SizedBox(
      height: 52,
      child: Stack(
        alignment: Alignment.center,
        children: [
          SText(
            context.translate.imHereToHelpYou,
            style: TStyle.TitleLarge,
          ),
          Positioned(
            bottom: 6,
            left: 0,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                //!this widget works as a responsive spacer
                SText(
                  context.translate.imHereTo,
                  style: TStyle.TitleLarge,
                  color: Colors.transparent,
                ),
                const ImageWidget(
                  image: Images.underline2,
                  height: 7,
                  width: 50,
                )
              ],
            ),
          ),
        ],
      ),
    ),
    leadingWidth: 80,
    leading: Padding(
      padding: pad(16, 0, 16, 0),
      child: const ImageWidget(
        height: 51,
        width: 64,
        image: Images.chatAppBarLeading,
      ),
    ),
    centerTitle: true,
  );
}
