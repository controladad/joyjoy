import 'package:injectable/injectable.dart';
import 'package:kt_dart/kt.dart';
import 'package:motivation_flutter/common/common_models.dart';
import 'package:motivation_flutter/features/explore/domain/models/search_models.dart';
import 'package:motivation_flutter/features/explore/infrastructure/dtos/explore_dto.dart';
import 'package:motivation_flutter/features/explore/infrastructure/dtos/instant_search_dto.dart';

import 'package:motivation_flutter/features/explore/infrastructure/explore_api_services.dart';
import 'package:motivation_flutter/features/explore/infrastructure/explore_local_storage_services.dart';
import 'package:motivation_flutter/shared/infrastructure/dtos/author_dto.dart';
import 'package:motivation_flutter/shared/infrastructure/dtos/quotes_bundle_dto.dart';
import 'package:motivation_flutter/shared/infrastructure/dtos/sub_category_dto.dart';
import 'package:motivation_flutter/shared/infrastructure/shared_local_storage_services.dart';

abstract class IExploreApiDataSource {
  Future<ExploreDto> get getExplore;
  Future<SearchModel> instantSearch(String searchQuery);
  Future<LazyLoadResponseModel<SearchResultUnion>> fullSearch({
    required String searchQuery,
    required SearchResultUnion firstItem,
    required int page,
  });

  Future<KtList<String>> get getSearchHistories;
  Future<KtList<String>> addNewSearchHistory(String query);
  Future<KtList<String>> removeSearchHIstories();
}

@Injectable(as: IExploreApiDataSource)
class ExploreApiDataSourceImplementation implements IExploreApiDataSource {
  final IExploreApiService _apiService;
  final ISharedLocalStorageService sharedLocalStorage;
  final IExploreLocalStorageService exploreLocalStorage;

  ExploreApiDataSourceImplementation(
    this._apiService,
    this.sharedLocalStorage,
    this.exploreLocalStorage,
  );

  @override
  Future<KtList<String>> addNewSearchHistory(String query) {
    return exploreLocalStorage.addNewSearchHistory(query);
  }

  @override
  Future<KtList<String>> get getSearchHistories {
    return exploreLocalStorage.getSearchHistories;
  }

  @override
  Future<KtList<String>> removeSearchHIstories() {
    return exploreLocalStorage.removeSearchHIstories();
  }

  @override
  Future<ExploreDto> get getExplore async {
    final response = await _apiService.getExplore();
    final data = response.data as Map<String, dynamic>;
    return ExploreDto.fromJson(data);
  }

  @override
  Future<LazyLoadResponseModel<SearchResultUnion>> fullSearch({
    required String searchQuery,
    required SearchResultUnion firstItem,
    required int page,
  }) async {
    final response = await _apiService.fullSearch(
      searchQuery: searchQuery,
      firstItem: firstItem,
      page: page,
    );

    final data = response.data as Map<String, dynamic>;
    return firstItem.map(
      category: (e) {
        final rawResults = data['data'] as List<dynamic>;
        final results = rawResults.map(
          (e) => SearchResultUnion.category(
            SubCategoryDto.fromJson(e as Map<String, dynamic>).toDomain(),
          ),
        );
        return LazyLoadResponseModel(
          results: results.toList().kt,
          total: results.length,
        );
      },
      author: (e) {
        final total = (data['meta'] as Map<String, dynamic>)['total'] as int;
        final rawResults = data['data'] as List<dynamic>;
        final results = rawResults.map(
          (e) => SearchResultUnion.author(
            AuthorDto.fromJson(e as Map<String, dynamic>).toDomain(),
          ),
        );
        return LazyLoadResponseModel(
          results: results.toList().kt,
          total: total,
        );
      },
      quote: (e) {
        final bundle = QuotesBundleDto.fromJson(data);
        final quotes = bundle.toQuotes();
        return LazyLoadResponseModel(
          results: quotes.map((p0) => SearchResultUnion.quote(p0)),
          total: quotes.size,
        );
      },
    );
  }

  @override
  Future<SearchModel> instantSearch(String searchQuery) async {
    final response = await _apiService.instanceSearch(searchQuery);
    final data = response.data as Map<String, dynamic>;

    return InstantSearchDto.fromJson(data).toDomain();
  }
}
