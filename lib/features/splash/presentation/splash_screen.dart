import 'package:motivation_flutter/common/presentation/common_imports.dart';
import 'package:motivation_flutter/features/sub_auth/application/sub_auth_bloc/sub_auth_bloc.dart';

@RoutePage()
class SplashScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final state = context.watch<SubAuthCubit>().state;

    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      if (state.hasCompleteData.isSome()
          // state.isAuthenticated.isSome() &&
          // state.subscription.isSome()
          ) {
        state.hasCompleteData.fold(() => null, (completed) {
          if (completed == false) {
            context.pushAndRemoveAll(const OnboardingRoute());
            return;
          }
          // state.subscription.fold(() => null, (subscription) {
          //   final hasSubscription = subscription.isPremium;
          //   if (hasSubscription == false) {
          //     context.pushAndRemoveAll(
          //       PaywallRoute(
          //         onClose: (ctx) {
          //           ctx.pushAndRemoveAll(const NavBuilderRoute());
          //         },
          //       ),
          //     );
          //     return;
          //   }
          //   state.isAuthenticated.fold(() => null, (isAuth) {
          //     if (isAuth == false) {
          //       context.pushAndRemoveAll(
          //         LoginRoute(nextRoute: const NavBuilderRoute()),
          //       );
          //       return;
          //     }
          context.pushAndRemoveAll(const NavBuilderRoute());
          return;
          //   });
          // });
        });
      }
    });

    return const Scaffold(
      extendBodyBehindAppBar: true,
      body: ImageWidget(
        image: Images.splash,
        height: double.infinity,
        width: double.infinity,
      ),
    );
  }
}
