import 'package:motivation_flutter/common/presentation/buttons.dart';
import 'package:motivation_flutter/common/presentation/common_imports.dart';
import 'package:motivation_flutter/features/onboarding/application/onboarding_bloc/onboarding_bloc.dart';

class HowDoYouFeelScreen extends StatelessWidget {
  final Function() onNext;
  const HowDoYouFeelScreen({super.key, required this.onNext});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: SafeArea(child: _Body(onNext)),
    );
  }
}

class _Body extends StatelessWidget {
  final Function() onNext;
  const _Body(this.onNext);

  @override
  Widget build(BuildContext context) {
    final bloc = context.read<OnboardingBloc>();
    final state = context.watch<OnboardingBloc>().state;

    void onSubmit() {
      bloc.add(const OnboardingEvent.passedFeeling());
      // context
      //     .pushRoute(
      //       ChatRoute(
      //         onBoardingArg: some(
      //           ChatOnboardingArgument(
      //             onTestFinished: (ctx) {},
      //             onboardingBloc: bloc,
      //             feeling: state.selectedFeeling.fold(
      //               () => state.wroteFeeling.fold(() => '', (a) => a),
      //               (a) => a.label,
      //             ),
      //             userName: state.userName.fold(() => '', (a) => a),
      //           ),
      //         ),
      //       ),
      //     )
      //     .then((value) => onNext());
    }

    return Padding(
      padding: pad(22, 16, 22, 32),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Flexible(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                // const Spacer(),
                OnKeyboardUpDisappearWidget(
                  child: SText(
                    '${context.translate.hi.capitalize()} ${state.userName.fold(() => '', (a) => a.capitalize())} ${context.translate.imYourOnlinePalTtq}',
                    style: TStyle.TitleLarge,
                  ),
                ),
                const Flexible(
                  flex: 10,
                  child: Center(
                    child: ImageWidget(
                      image: Images.howDoYouFeel,
                      height: 280,
                      width: null,
                    ),
                  ),
                ),
              ],
            ),
          ),
          Column(
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  SText(
                    context.translate.howDoYouFill,
                    style: TStyle.TitleLarge,
                  ),
                  const ImageWidget(
                    image: Images.questionMark,
                    height: 52,
                    width: 32,
                  ),
                ],
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  //this is works as spacer for underLine of feel
                  SText(
                    context.translate.howDoYou,
                    style: TStyle.TitleLarge,
                    color: Colors.transparent,
                  ),
                  const ImageWidget(
                    image: Images.underline1,
                    height: 6,
                    width: 35,
                  ),
                ],
              ),
              OnKeyboardUpDisappearWidget(
                child: Column(
                  children: [
                    _FeelingRail(
                      onFeelingChanged: (feeling) => bloc.add(
                        OnboardingEvent.selectFeelingItem(feeling),
                      ),
                    ),
                    Padding(
                      padding: pad(0, 10, 0, 10),
                      child: SText(
                        context.translate.or.capitalize(),
                        style: TStyle.TitleMedium,
                      ),
                    ),
                  ],
                ),
              ),
              CustomTextField(
                leading: Container(
                  padding: pad(15, 3, 5, 0),
                  alignment: Alignment.centerLeft,
                  child: SText(
                    context.translate.iFeel.capitalize(),
                    style: TStyle.BodyMedium,
                  ),
                ),
                onChanged: (value) =>
                    bloc.add(OnboardingEvent.writeFeeling(value)),
                focusedBorderColor: AppTheme.primary_50,
                unfocusedBorderColor:
                    state.wroteFeeling.isSome() ? AppTheme.secondary_40 : null,
                icon: keyboardIsVisible(context)
                    ? Padding(
                        padding: const EdgeInsets.all(4),
                        child: CustomFilledButton(
                          maxWidth: 100,
                          label: context.translate.letsTalk,
                          onTap: () => onSubmit(),
                          isEnable: state.wroteFeeling
                              .fold(() => false, (a) => a.isNotEmpty),
                        ),
                      )
                    : null,
              ),
              const OnKeyboardUpDisappearWidget(child: SizedBox(height: 10)),
              if (!keyboardIsVisible(context))
                CustomFilledButton(
                  label: context.translate.letsTalkOkay,
                  onTap: () => onSubmit(),
                  isEnable: state.selectedFeeling.fold(
                    () => state.wroteFeeling
                        .fold(() => false, (a) => a.isNotEmpty),
                    (a) => true,
                  ),
                ),
            ],
          ),
        ],
      ),
    );
  }
}

class _FeelingRail extends StatelessWidget {
  final Function(FeelingClass feeling) onFeelingChanged;
  const _FeelingRail({required this.onFeelingChanged});

  @override
  Widget build(BuildContext context) {
    final state = context.watch<OnboardingBloc>().state;

    final feelings = [
      FeelingClass(
        id: '0',
        icon: AppIcons.emojisHappy,
        label: context.translate.happy,
      ),
      FeelingClass(
        id: '1',
        icon: AppIcons.emojisAngry,
        label: context.translate.angry,
      ),
      FeelingClass(
        id: '2',
        icon: AppIcons.emojisSad,
        label: context.translate.sad,
      ),
      FeelingClass(
        id: '3',
        icon: AppIcons.emojisAmazed,
        label: context.translate.amazed,
      ),
    ];
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: feelings
          .map(
            (e) => Flexible(
              child: Padding(
                padding: pad(3, 0, 3, 0),
                child: _FeelingWidget(
                  feeling: e,
                  isSelected: state.selectedFeeling
                      .fold(() => false, (a) => a.id == e.id),
                  onTap: (FeelingClass feeling) => onFeelingChanged(feeling),
                ),
              ),
            ),
          )
          .toList(),
    );
  }
}

class _FeelingWidget extends StatelessWidget {
  final FeelingClass feeling;
  final bool isSelected;
  final Function(FeelingClass feeling) onTap;
  const _FeelingWidget({
    required this.feeling,
    required this.isSelected,
    required this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    final borderRadius = BorderRadius.circular(25);
    return GestureDetector(
      onTap: () => onTap(feeling),
      child: SContainer(
        padding: pad(0, 5, 0, 5),
        maxWidth: 100,
        decoration: BoxDecoration(
          color: isSelected ? null : AppTheme.on_outline_dark.withOpacity(.3),
          gradient: isSelected ? AppGradients.secondary : null,
          borderRadius: borderRadius,
          border: Border.all(
            color: AppTheme.on_outline_dark,
          ),
        ),
        child: Row(
          children: [
            const Spacer(),
            Flexible(
              flex: 6,
              child: SContainer(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    ImageWidget(
                      image: feeling.icon,
                      height: null,
                      width: null,
                      color: AppTheme.white,
                    ),
                    SText(
                      feeling.label.toUpperCase(),
                      style: TStyle.LabelButton,
                      maxLines: 1,
                      scaleDown: true,
                    ),
                    const SizedBox(height: 5)
                  ],
                ),
              ),
            ),
            const Spacer(),
          ],
        ),
      ),
    );
  }
}

class FeelingClass {
  final String id;
  final String icon;
  final String label;

  FeelingClass({required this.id, required this.icon, required this.label});
}
