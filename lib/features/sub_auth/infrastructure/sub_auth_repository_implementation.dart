import 'package:google_sign_in/google_sign_in.dart';
import 'package:injectable/injectable.dart';
import 'package:kt_dart/kt.dart';
import 'package:motivation_flutter/common/presentation/common_imports.dart';
import 'package:motivation_flutter/core/firebase/push_notification_services.dart';
import 'package:motivation_flutter/features/profile/domain/models/account_info_model.dart';
import 'package:motivation_flutter/features/sub_auth/domain/models/sub_auth_model.dart';
import 'package:motivation_flutter/features/sub_auth/domain/models/paywall_model.dart';
import 'package:motivation_flutter/features/profile/domain/models/user_subscription_model.dart';
import 'package:motivation_flutter/core/failures.dart';
import 'package:dartz/dartz.dart';
import 'package:motivation_flutter/features/sub_auth/domain/sub_auth_repository.dart';
import 'package:motivation_flutter/features/sub_auth/domain/subscription_service.dart';
import 'package:motivation_flutter/features/sub_auth/infrastructure/dtos/auth_dto.dart';
import 'package:motivation_flutter/features/sub_auth/infrastructure/sub_auth_data_source.dart';
import 'package:motivation_flutter/features/user/infrastructure/user_local_data.dart';
import 'package:motivation_flutter/shared/infrastructure/shared_data_source.dart';
import 'package:sign_in_with_apple/sign_in_with_apple.dart';

@Injectable(as: ISubAuthRepository)
class SubAuthRepositoryImplementation implements ISubAuthRepository {
  final ISubAuthApiDataSource dataSource;
  final ISharedDataSource sharedDataSource;
  final GoogleSignIn _googleSignIn;
  final PushNotificationService _pushNotificationService;
  final IUserData _iUserData;
  final String _deviceInfoService;
  final String _device;
  final ISubscriptionService _subscriptionService;

  SubAuthRepositoryImplementation(
    this.dataSource,
    this.sharedDataSource,
    this._googleSignIn,
    this._pushNotificationService,
    this._iUserData,
    this._subscriptionService,
    @Named('deviceId') this._deviceInfoService,
    @Named('DEVICE') this._device,
  );

  @override
  Future<void> initialize() async {
    await tryCacheBlock(() => _subscriptionService.initialize());
  }

  Future<String?> get _fcmToken async => _pushNotificationService.getFcmToken();

  @override
  Future<bool> isAuthenticated() {
    return _iUserData.hasToken;
  }

  @override
  Future<Either<GeneralFailure, UserSubscription>> getSubscription() {
    return tryCacheBlock(() async => _subscriptionService.subscription);
  }

  @override
  Stream<UserSubscription> get subscriptionUpdated {
    return _subscriptionService.hasSubscription;
  }

  @override
  Stream<void> get cacheUpdated {
    return sharedDataSource.cacheUpdated;
  }

  @override
  Future<Either<GeneralFailure, bool>> signInWithGoogle() async {
    return tryCacheBlock(() async {
      final googleUser = await _googleSignIn.signIn();
      if (googleUser == null) {
        return false;
      }
      final userAuth = await googleUser.authentication;
      if (userAuth.accessToken != null) {
        final dto = await dataSource.loginWithGoogle(
          accessToken: userAuth.accessToken!,
          fcm: await _fcmToken ?? '',
          uuid: _deviceInfoService,
          device: _device,
        );

        if (dto.token != null) {
          final username = (await sharedDataSource.getAccountInfo()).name;

          final account = await sharedDataSource.updateAccountInfo(
            dto.toAccountInfo(name: username.toNullable()),
          );
          print(account);
          await _iUserData.storeToken(token: dto.token!);
          await _subscriptionService.login(dto.data!.id.toString());
          await sharedDataSource.syncCache();
          return true;
        } else {
          _iUserData.clearToken();
          return false;
        }
      }
      return false;
    });
  }

  @override
  Future<Either<GeneralFailure, bool>> signInWithApple() async {
    return tryCacheBlock(() async {
      final credential = await SignInWithApple.getAppleIDCredential(
        scopes: [
          AppleIDAuthorizationScopes.email,
          AppleIDAuthorizationScopes.fullName,
        ],
      );

      if (credential.identityToken == null) {
        return false;
      }

      final AuthResponseDto response = await dataSource.loginWithApple(
        accessToken: credential.identityToken!,
        fcm: await _fcmToken ?? '',
        uuid: _deviceInfoService,
      );
      if (response.token != null) {
        final username = (await sharedDataSource.getAccountInfo()).name;
        await sharedDataSource.updateAccountInfo(
          response.toAccountInfo(
            name: username.toNullable(),
          ),
        );
        await _iUserData.storeToken(token: response.token!);
        return true;
      } else {
        _iUserData.clearToken();
        return false;
      }
    });
  }

  @override
  Future<Either<GeneralFailure, bool>> signOut() {
    return tryCacheBlock(() async {
      dataSource.logout(
        fcm: await _fcmToken ?? '',
        uuid: _deviceInfoService,
      );
      await _googleSignIn.signOut();
      await _iUserData.logout;
      await sharedDataSource.logout();

      return true;
    });
  }

  @override
  Future<Either<GeneralFailure, bool>> deleteAccount() {
    return tryCacheBlock(() async {
      await dataSource.deleteAccount();

      await _googleSignIn.signOut();
      await _iUserData.logout;
      await sharedDataSource.logout();



      return true;
    });
  }

  @override
  Future<Either<GeneralFailure, bool>> userHasToken() async {
    return tryCacheBlock(() async {
      final bool hasToken = await _iUserData.hasToken;
      return hasToken;
    });
  }

  @override
  Future<Either<GeneralFailure, SubAuthModel>> get getData async {
    throw UnimplementedError();
  }

  @override
  Future<Either<GeneralFailure, PaywallModel>> get getPaywall async {
    return tryCacheBlock(() async {
      final cacheData = await sharedDataSource.getCacheData;
      final result = await dataSource.getPaywall;
      final firstPlanWithTrial = result.plans.kt
          .firstOrNull((element) => element.trialDurationValue != null);
      String? trialEndDate;
      if (firstPlanWithTrial != null) {
        final nextDateTime = DateTime.now()
            .add(Duration(days: firstPlanWithTrial.trialDurationValue!));
        trialEndDate = nextDateTime.mmmmDOrdinal();
      }
      return result.toDomain(
        userName: cacheData.account.name.toNullable() ?? '',
        schedule: PaywallTrialScheduleModel(
          items: firstPlanWithTrial == null
              ? emptyList()
              : KtList.from([
                  const PayWallTrialScheduleItemModel(
                    id: '0',
                    title: 'Today',
                    subtitle:
                        'Experience the life-altering effects right away by gaining access.',
                    icon: AppIcons.gradientLock,
                  ),
                  const PayWallTrialScheduleItemModel(
                    id: '1',
                    title: 'Tomorrow',
                    subtitle:
                        "We'll remind you with an email and notification that your trial is ending.",
                    icon: AppIcons.ring,
                  ),
                  PayWallTrialScheduleItemModel(
                    id: '2',
                    title: 'In ${firstPlanWithTrial.trialDurationValue} Days',
                    subtitle:
                        "On $trialEndDate, you'll be charged unless you cancel before then.",
                    icon: AppIcons.star,
                  ),
                ]),
          daysToNotify: 5,
          trialDays: 7,
        ),
        definitions: KtList.from([
          const PayWallOptionDefinitionModel(
            icon: AppIcons.cloud,
            title: 'Cloud backup & recovery',
            subtitle:
                'Maintains consistency between locally stored and cloud-based files.',
          ),
          const PayWallOptionDefinitionModel(
            icon: AppIcons.person,
            title: 'AI Friend',
            subtitle:
                'Artificial intelligence that can talk to you in natural language, and answer almost any questions you might have, is here.',
          ),
          const PayWallOptionDefinitionModel(
            icon: AppIcons.unlock,
            title: 'Unlock all features',
            subtitle:
                'keeps files in different locations up to date through the cloud.',
          ),
        ]),
        tableItems: KtList.from([
          const PayWallComparisonTableItemModel(
            id: '0',
            label: 'unlimitedTTQ',
            availabilityOnBasic: OptionAvailabilityEnum.Ad,
            availabilityOnPremium: OptionAvailabilityEnum.Open,
          ),
          const PayWallComparisonTableItemModel(
            id: '1',
            label: 'Backup & Recovery',
            availabilityOnBasic: OptionAvailabilityEnum.Lock,
            availabilityOnPremium: OptionAvailabilityEnum.Open,
          ),
          const PayWallComparisonTableItemModel(
            id: '2',
            label: 'no Ads',
            availabilityOnBasic: OptionAvailabilityEnum.Lock,
            availabilityOnPremium: OptionAvailabilityEnum.Open,
          ),
          const PayWallComparisonTableItemModel(
            id: '3',
            label: 'Sharing Without Watermark',
            availabilityOnBasic: OptionAvailabilityEnum.Lock,
            availabilityOnPremium: OptionAvailabilityEnum.Open,
          ),
          const PayWallComparisonTableItemModel(
            id: '4',
            label: 'Any Situation Categories',
            availabilityOnBasic: OptionAvailabilityEnum.Lock,
            availabilityOnPremium: OptionAvailabilityEnum.Open,
          ),
          const PayWallComparisonTableItemModel(
            id: '5',
            label: 'Free Customization',
            availabilityOnBasic: OptionAvailabilityEnum.Ad,
            availabilityOnPremium: OptionAvailabilityEnum.Open,
          ),
          const PayWallComparisonTableItemModel(
            id: '6',
            label: 'Free Customization',
            availabilityOnBasic: OptionAvailabilityEnum.Open,
            availabilityOnPremium: OptionAvailabilityEnum.Open,
          ),
        ]),
      );
    });
  }

  @override
  Stream<void> get userUpdated => _iUserData.userUpdated;

  @override
  Future<Either<GeneralFailure, UserSubscription>> changeSubscription(
    PayWallSubscriptionPlanModel plan,
  ) async {
    try {
      final result = await _subscriptionService.changeSubscription(plan);
      return right(result);
    } catch (e) {
      return left(const GeneralFailure.unexpected());
    }
  }

  @override
  Future<Either<GeneralFailure, UserSubscription>> restoreSubscription() async {
    try {
      final result = await _subscriptionService.restoreSubscription();
      return right(result);
    } catch (e) {
      return left(const GeneralFailure.unexpected());
    }
  }

  @override
  Future<bool> isDataCompleted() async {
    final cache = await sharedDataSource.getCacheData;
    return cache.account.name.isSome();
  }

  @override
  Future<Either<GeneralFailure, Unit>> userRewarded(double rewardValue) {
    return tryCacheBlock<Unit>(() async {
      await sharedDataSource.userRewarded(rewardValue);
      return unit;
    });
  }

  @override
  Future<Either<GeneralFailure, AccountInfoModel>> accountInfo() {
    return tryCacheBlock(
      () async => (await sharedDataSource.getCacheData).account,
    );
  }
}
