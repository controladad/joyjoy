import 'package:hive/hive.dart';
import 'package:injectable/injectable.dart';
import 'package:kt_dart/kt.dart';
import 'package:motivation_flutter/constants/db_constants.dart';

abstract class IExploreLocalStorageService {
  Future<KtList<String>> get getSearchHistories;
  Future<KtList<String>> addNewSearchHistory(String query);
  Future<KtList<String>> removeSearchHIstories();
}

@Injectable(as: IExploreLocalStorageService)
class ExploreLocalStorageServicesImplementation
    implements IExploreLocalStorageService {
  final Box<String> searchHistoryBox;

  ExploreLocalStorageServicesImplementation(
    @Named(DBConstants.SEARCH_HISTORY) this.searchHistoryBox,
  );

  @override
  Future<KtList<String>> addNewSearchHistory(String query) async {
    final values = searchHistoryBox.values.toList();
    if (values.contains(query)) {
      final index = values.toList().indexOf(query);
      await searchHistoryBox.deleteAt(index);
    }
    await searchHistoryBox.add(query);
    return getSearchHistories;
  }

  @override
  Future<KtList<String>> get getSearchHistories async {
    return searchHistoryBox.values.toList().kt.reversed();
  }

  @override
  Future<KtList<String>> removeSearchHIstories() async {
    await searchHistoryBox.clear();
    return getSearchHistories;
  }
}
