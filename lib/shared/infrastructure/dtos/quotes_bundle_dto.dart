import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:kt_dart/kt.dart';
import 'package:motivation_flutter/features/quotes/domain/models/quotes_models.dart';
import 'package:motivation_flutter/shared/infrastructure/dtos/author_dto.dart';
import 'package:motivation_flutter/shared/infrastructure/dtos/quote_dto.dart';

part 'quotes_bundle_dto.g.dart';
part 'quotes_bundle_dto.freezed.dart';

@freezed
class QuotesBundleDto with _$QuotesBundleDto {
  factory QuotesBundleDto({
    @JsonKey(name: 'quotes') required List<QuoteDto> quotes,
    @JsonKey(name: 'authors') required List<AuthorDto> authors,
  }) = _QuotesBundleDto;
  const QuotesBundleDto._();
  factory QuotesBundleDto.fromJson(Map<String, dynamic> json) =>
      _$QuotesBundleDtoFromJson(json);
  KtList<QuoteModel> toQuotes() {
    KtList<QuoteModel> quotesModels = emptyList();
    for (final quote in quotes) {
      final author =
          authors.firstWhere((element) => element.id == quote.authorId);
      quotesModels =
          quotesModels.plusElement(quote.toDomain(author.toDomain()));
    }
    return quotesModels;
  }
}
