part of 'theme_bloc.dart';

@injectable
@freezed
class ThemeState with _$ThemeState {
  const factory ThemeState({
    required Option<Either<GeneralFailure, KtList<ThemeModel>>> themeData,
    required Option<Either<GeneralFailure, ThemeModel>> selectedThemeData,
    required Option<ThemeModel> selectedTheme,
  }) = _ThemeState;
  @factoryMethod
  factory ThemeState.init() => ThemeState(
        selectedTheme: none(),
        themeData: none(),
        selectedThemeData: none(),
      );
}
