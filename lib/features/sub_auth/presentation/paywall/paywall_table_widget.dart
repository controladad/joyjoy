import 'package:kt_dart/kt.dart';
import 'package:motivation_flutter/common/presentation/common_imports.dart';
import 'package:motivation_flutter/features/sub_auth/domain/models/paywall_model.dart';

class PaywallComparisonTableWidget extends StatelessWidget {
  final KtList<PayWallComparisonTableItemModel> items;
  const PaywallComparisonTableWidget(this.items);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [
            _TableRowWidget(
              isRight: false,
              isFirst: true,
              height: 110,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const SText('🙂', style: TStyle.DisplaySmall),
                      SText(
                        context.translate.basic,
                        style: TStyle.TitleSmall,
                        maxLines: 1,
                      ),
                    ],
                  ),
                ],
              ),
            ),
            _TableRowWidget(
              isRight: true,
              isFirst: true,
              height: 110,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const SText('🤩', style: TStyle.DisplaySmall),
                  SText(
                    context.translate.premium,
                    style: TStyle.TitleSmall,
                    maxLines: 1,
                    scaleDown: true,
                  ),
                ],
              ),
            ),
          ],
        ),
        ...items.mapIndexed((index, item) {
          final isLast = index == items.size - 1;
          return Row(
            children: [
              _TableRowWidget(
                isRight: false,
                isLast: isLast,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Flexible(
                      flex: 5,
                      child: SText(
                        item.label.toTitleCase(),
                        style: TStyle.TitleSmall,
                        maxLines: 2,
                      ),
                    ),
                    const Spacer(),
                    ImageWidget(
                      image: getIconWithOptionAvailability(
                        item.availabilityOnBasic,
                      ),
                      height: item.availabilityOnBasic ==
                              OptionAvailabilityEnum.Lock
                          ? 30
                          : 50,
                      width: 40,
                    ),
                  ],
                ),
              ),
              _TableRowWidget(
                isLast: isLast,
                isRight: true,
                child: ImageWidget(
                  image:
                      getIconWithOptionAvailability(item.availabilityOnPremium),
                  height: 40,
                  width: 40,
                ),
              ),
            ],
          );
        }).asList(),
      ],
    );
  }
}

class _TableRowWidget extends StatelessWidget {
  final bool isFirst;
  final bool isLast;
  final bool isRight;
  final double height;
  final Widget child;

  const _TableRowWidget({
    required this.isRight,
    this.isLast = false,
    required this.child,
    this.isFirst = false,
    this.height = 54,
  });

  @override
  Widget build(BuildContext context) {
    BorderRadius? borderRadius;
    if (isFirst) {
      borderRadius = BorderRadius.only(
        topRight: isRight ? const Radius.circular(20) : Radius.zero,
        topLeft: !isRight ? const Radius.circular(20) : Radius.zero,
      );
    } else if (isLast) {
      borderRadius = BorderRadius.only(
        bottomRight: isRight ? const Radius.circular(20) : Radius.zero,
        bottomLeft: !isRight ? const Radius.circular(20) : Radius.zero,
      );
    }

    return Flexible(
      flex: isRight ? 3 : 5,
      child: SContainer(
        padding: pad(25, 0, 32, 0),
        margin: pad(!isRight ? 20 : 0, 0, isRight ? 20 : 0, 0),
        decoration: BoxDecoration(
          border: Border.all(
            width: isRight ? 1.5 : 0.5,
            color: isRight ? AppTheme.primary_80 : AppTheme.on_background_dark,
          ),
          borderRadius: borderRadius,
          color: isRight
              ? AppTheme.on_surface_variant_dark.withOpacity(.16)
              : null,
        ),
        height: height,
        child: Center(
          child: child,
        ),
      ),
    );
  }
}
