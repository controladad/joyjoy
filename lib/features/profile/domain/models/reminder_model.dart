import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:kt_dart/kt.dart';
import 'package:motivation_flutter/common/presentation/utils/common_functions.dart';
import 'package:motivation_flutter/features/quotes/domain/models/quotes_models.dart';
import 'package:uuid/uuid.dart';

part 'reminder_model.freezed.dart';

@freezed
class ReminderModel with _$ReminderModel {
  const factory ReminderModel({
    required String id,
    required KtList<SubCategoryModel> categories,
    required DateTime startTime,
    required DateTime endTime,
    required int countPerDuration,
    required String alarmSoundId,
    required KtList<DaysOfWeek> daysOfWeek,
    required bool isActive,
  }) = _ReminderModel;

  const ReminderModel._();

  factory ReminderModel.empty() => ReminderModel(
        id: const Uuid().v4(),
        categories: emptyList(),
        startTime: DateTime.now().copyWith(minute: 0, second: 0),
        endTime: DateTime.now()
            .copyWith(hour: DateTime.now().hour + 1, minute: 0, second: 0),
        countPerDuration: 1,
        alarmSoundId: 'default',
        daysOfWeek: emptyList(),
        isActive: true,
      );
}
