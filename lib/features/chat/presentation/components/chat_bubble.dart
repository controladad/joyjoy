import 'package:kt_dart/kt.dart';
import 'package:loading_animation_widget/loading_animation_widget.dart';
import 'package:motivation_flutter/common/presentation/common_imports.dart';
import 'package:flutter_chat_types/flutter_chat_types.dart' as types;
import 'package:motivation_flutter/features/chat/application/chat_bloc/chat_bloc.dart';
import 'package:motivation_flutter/features/chat/domain/models/chat_model.dart';
import 'package:motivation_flutter/features/chat/presentation/chat_screens/chat_phone_view.dart';
import 'package:motivation_flutter/features/chat/presentation/components/custom_focused_menu.dart';

Widget customBubbleBuilder(
  Widget textWidget, {
  required types.Message message,
  required bool nextMessageInGroup,
}) {
  final textMessage = message as types.TextMessage;
  final bool isLoading = textMessage.text == '@loading1234';

  final isSender = message.author.id == Sender.Client.name;
  final BuildContext context = chatScreenKey.currentContext!;

  return _Bubble(
    context: context,
    isSender: isSender,
    isLoading: isLoading,
    textMessage: textMessage,
  );
}

class _Bubble extends StatefulWidget {
  const _Bubble({
    required this.context,
    required this.isSender,
    required this.isLoading,
    required this.textMessage,
  });

  final BuildContext context;
  final bool isSender;
  final bool isLoading;
  final types.TextMessage textMessage;

  @override
  State<_Bubble> createState() => _BubbleState();
}

class _BubbleState extends State<_Bubble> {
  bool showLikeRow = true;
  @override
  void initState() {
    super.initState();
  }

  Future<void> onTapCopy() async {
    await Clipboard.setData(
      ClipboardData(text: widget.textMessage.text),
    );
    // ignore: use_build_context_synchronously
    context.popRoute();
  }

  @override
  Widget build(BuildContext context) {
    return FocusedMenuHolder(
      callBackOnOpenOrCloseDialog: (show) {
        setState(() {
          showLikeRow = show;
        });
      },
      onPressed: () {},
      menuItems: [
        FocusedMenuItem(
          backgroundColor: Colors.transparent,
          title: Container(
            width: 240,
            alignment: Alignment.center,
            decoration: BoxDecoration(
              gradient: AppGradients.surface3Dark,
              borderRadius: BorderRadius.circular(10),
            ),
            child: Column(
              children: [
                const SBox(
                  height: 20,
                ),
                Padding(
                  padding: pad(12, 0, 12, 0),
                  child: GestureDetector(
                    behavior: HitTestBehavior.translucent,
                    onTap: onTapCopy,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        SText(
                          context.translate.copy.capitalize(),
                          style: TStyle.LabelLarge,
                          color: AppTheme.white,
                        ),
                        const ImageWidget(
                          image: AppIcons.copy,
                          height: 24,
                          width: 24,
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          onPressed: () {},
        ),
      ],
      menuBoxDecoration: const BoxDecoration(
        color: Colors.transparent,
        borderRadius: BorderRadius.all(Radius.circular(25.0)),
      ),
      duration: const Duration(milliseconds: 100),
      animateMenuItems: false,
      blurBackgroundColor: Colors.black12,
      menuOffset: 20,
      menuItemExtent: 82,
      bottomOffsetHeight: 80.0,
      targetChild: ConstrainedBox(
        constraints: const BoxConstraints(maxWidth: 330),
        child: Row(
          mainAxisAlignment:
              widget.isSender ? MainAxisAlignment.end : MainAxisAlignment.start,
          children: [
            ConstrainedBox(
              constraints: const BoxConstraints(maxWidth: 230),
              child: Container(
                padding: pad(24, 12, 16, 12),
                decoration: BoxDecoration(
                  color: AppTheme.surface_variant_dark,
                  borderRadius: BorderRadius.only(
                    topLeft: const Radius.circular(25),
                    topRight: const Radius.circular(25),
                    bottomLeft: widget.isSender
                        ? const Radius.circular(25)
                        : const Radius.circular(10),
                    bottomRight: !widget.isSender
                        ? const Radius.circular(25)
                        : const Radius.circular(10),
                  ),
                  gradient: widget.isSender ? AppGradients.chatBubble : null,
                ),
                child: widget.isLoading
                    ? SBox(
                        height: 20,
                        child: LoadingAnimationWidget.horizontalRotatingDots(
                          color: AppTheme.white,
                          size: 30,
                        ),
                      )
                    : SText(
                        widget.textMessage.text,
                        style: TStyle.BodyLarge,
                      ),
              ),
            ),
            if (!widget.isSender && !widget.isLoading && showLikeRow)
              _LikeOrDislikeWidget(
                id: widget.textMessage.id,
              )
          ],
        ),
      ),
    );
  }
}

class _LikeOrDislikeWidget extends StatelessWidget {
  final String id;

  const _LikeOrDislikeWidget({required this.id});
  @override
  Widget build(BuildContext context) {
    final BuildContext context = chatScreenKey.currentContext!;
    final ChatBloc bloc = context.read<ChatBloc>();
    final ChatState state = context.watch<ChatBloc>().state;
    final MessageModel messageModel = state.messages.first((m) => m.id == id);
    void likeMessage() {
      if (!(messageModel.isLiked ?? false)) {
        bloc.add(ChatEvent.messageLiked(id));
      }
    }

    void disLikeMessage() {
      if (messageModel.isLiked ?? true) {
        bloc.add(ChatEvent.messageDisliked(id));
      }
    }

    return Padding(
      padding: pad(12, 0, 0, 0),
      child: Row(
        children: [
          GestureDetector(
            onTap: likeMessage,
            child: ImageWidget(
              image: (messageModel.isLiked ?? false)
                  ? AppIcons.likeFilled
                  : AppIcons.like,
              height: 24,
              width: 24,
            ),
          ),
          const SBox(
            width: 15,
          ),
          GestureDetector(
            onTap: disLikeMessage,
            child: ImageWidget(
              image: (messageModel.isLiked ?? true)
                  ? AppIcons.dislike
                  : AppIcons.dislikeFilled,
              height: 24,
              width: 24,
            ),
          ),
        ],
      ),
    );
  }
}
