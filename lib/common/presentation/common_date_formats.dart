import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

extension DateTimeFormatX on DateTime {
  String mmmmDdYyyyFormat(BuildContext context, {bool trueLocal = true}) {
    return DateFormat(
      'MMMM dd, yyyy',
      trueLocal ? Localizations.localeOf(context).languageCode : 'en',
    ).format(this);
  }

  String mmmmDOrdinal() {
    return DateFormat("MMMM d'${getOrdinalSuffix(day)}'").format(this);
  }

  String ddMmmmYyyy(BuildContext context, {bool trueLocal = true}) {
    return DateFormat(
      'dd MMMM yyyy',
      trueLocal ? Localizations.localeOf(context).languageCode : 'en',
    ).format(this);
  }

  String yMdFormat(BuildContext context, {bool trueLocal = true}) => DateFormat(
        'yMd',
        trueLocal ? Localizations.localeOf(context).languageCode : 'en',
      ).format(this);
  String md(BuildContext context, {bool trueLocal = true}) => DateFormat(
        'MMMd',
        trueLocal ? Localizations.localeOf(context).languageCode : 'en',
      ).format(this);
}

String getOrdinalSuffix(int day) {
  if (day >= 11 && day <= 13) {
    return 'th';
  }
  switch (day % 10) {
    case 1:
      return 'st';
    case 2:
      return 'nd';
    case 3:
      return 'rd';
    default:
      return 'th';
  }
}
