import 'dart:async';

import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:kt_dart/kt.dart';
import 'package:motivation_flutter/common/presentation/common_imports.dart';
import 'package:motivation_flutter/features/profile/domain/models/account_info_model.dart';
import 'package:motivation_flutter/features/profile/domain/models/collection_model.dart';
import 'package:motivation_flutter/features/profile/domain/models/reminder_model.dart';
import 'package:motivation_flutter/features/profile/domain/models/user_subscription_model.dart';

import 'package:motivation_flutter/features/quotes/domain/models/quotes_model.dart';
import 'package:motivation_flutter/features/quotes/domain/models/quotes_models.dart';
import 'package:motivation_flutter/features/quotes/domain/models/saved_quote_union.dart';
import 'package:motivation_flutter/features/sub_auth/domain/subscription_service.dart';
import 'package:motivation_flutter/features/theme/domain/models/theme_model.dart';
import 'package:motivation_flutter/shared/domain/shared_cache_props_model.dart';
import 'package:motivation_flutter/shared/infrastructure/dtos/quotes_bundle_dto.dart';
import 'package:motivation_flutter/shared/infrastructure/shared_api_services.dart';
import 'package:motivation_flutter/shared/infrastructure/shared_data_source.dart';
import 'package:motivation_flutter/shared/infrastructure/shared_local_storage_services.dart';

@Injectable(as: ISharedDataSource)
class SharedDataSourceImplementation implements ISharedDataSource {
  final ISharedApiService apiService;
  final ISharedLocalStorageService localStorageService;
  final ISubscriptionService subscriptionService;

  SharedDataSourceImplementation(
    this.apiService,
    this.localStorageService,
    this.subscriptionService,
  );

  @override
  Future<void> logout() async {
    await localStorageService.clearAllCache();
  }

  @override
  Future<QuotesBundleDto> getAndAddNewBundleToNotifications({
    required List<String> categories,
    required int count,
  }) async {
    final QuotesBundleDto bundle = await apiService.getNewBundle(
      categoryIds: categories,
      authorIds: [],
      count: count,
    );

    await localStorageService.addNewBundleToNotifications(
      authors: bundle.authors.kt.map((p0) => p0.toEntity()),
      quotes: bundle.quotes.kt.map((p0) => p0.toEntity()),
    );
    return bundle;
  }

  @override
  Future<QuotesBundleDto> getAndAddNewBundle({
    required List<String> categories,
    required List<String> authors,
  }) async {
    final QuotesBundleDto bundle = await apiService.getNewBundle(
      categoryIds: categories,
      authorIds: authors,
      count: Constants.chunkSize,
    );

    await localStorageService.addNewBundle(
      authors: bundle.authors.kt.map((p0) => p0.toEntity()),
      quotes: bundle.quotes.kt.map((p0) => p0.toEntity()),
    );
    return bundle;
  }

  @override
  Stream<SharedCachePropsModel?> get cacheUpdated {
    return localStorageService.cacheUpdated;
  }

  @override
  Future<void> deleteQuoteFromNotificationQuotes(String quoteId) =>
      localStorageService.deleteQuoteFromNotificationQuotes(quoteId);

  @override
  Future<KtList<QuoteModel>> getNotificationQuotes({
    required KtList<String> categoryIds,
    required int count,
  }) async {
    final notificationQuoteCount =
        await localStorageService.notificationsQuotesCount;
    final subscription = await subscriptionService.subscription;

    final hasSubscription =
        subscription.map(premium: (e) => true, free: (_) => false);

    if (notificationQuoteCount == 0 || hasSubscription) {
      await getAndAddNewBundleToNotifications(
        categories: categoryIds.asList(),
        count: count,
      );

      return localStorageService.getNotificationQuotesByCategoryIds(
        categoryIds: categoryIds,
      );
    } else {
      return localStorageService.getQuotes(newQuote: false, count: count);
    }
  }

  @override
  Future<QuoteModel> getRandomQuote() async {
    final quotes =
        await localStorageService.getQuotes(newQuote: false, count: 1);
    return quotes.first();
  }

  @override
  Future<KtList<QuoteModel>> getNewQuotesByFavorites() async {
    final newQuotesCount = await localStorageService.newQuotesCount;
    const hasSubscription = true;

    final cacheData = (await localStorageService.getCacheProps).toDomain;
    final favoriteAuthors = cacheData.favoriteAuthorsIds;
    final favoriteCategories = cacheData.favoriteCategoriesIds;

    if (hasSubscription &&
        newQuotesCount < Constants.minimumCountOfNewQuotesInView) {
      try {
        if (favoriteCategories.isNotEmpty() || favoriteAuthors.isNotEmpty()) {
          await getAndAddNewBundle(
            categories: favoriteCategories.asList(),
            authors: favoriteAuthors.asList(),
          );
        } else {
          await getAndAddNewBundle(categories: [], authors: []);
        }

        final q = localStorageService.getQuotes(newQuote: true);
        return q;
      } catch (e) {
        return localStorageService.getQuotes(newQuote: false);
      }
    }
    return localStorageService.getQuotes(newQuote: true);
  }

  @override
  Future<KtList<QuoteModel>> getSavedQuotes(SavedQuoteType type) async {
    final cacheResult = await localStorageService.getSavedQuotes(type);

    if (cacheResult.value2.isEmpty()) return cacheResult.value1;

    //value2 is QuotesIds that not found in cache
    final bundle = await apiService.getQuotesByIds(
      quotesIds: cacheResult.value2.asList(),
    );
    await localStorageService.addNewBundle(
      authors: bundle.authors.kt.map((p0) => p0.toEntity()),
      quotes: bundle.quotes.kt.map((p0) => p0.toEntity()),
    );

    final newResult = await localStorageService.getSavedQuotes(type);
    return newResult.value1;
  }

  @override
  Future<KtList<QuoteModel>> getQuotesByCategoryOrAuthor({
    required Option<String> authorId,
    required Option<String> categoryId,
  }) async {
    final bundle = await getAndAddNewBundle(
      categories: categoryId.fold(() => [], (a) => [a]),
      authors: authorId.fold(() => [], (a) => [a]),
    );
    final quotes = localStorageService.quoteEntitiesToDomain(
      bundle.quotes.map((e) => e.toEntity()).toList().kt,
    );
    return quotes;
  }

  @override
  Future<KtList<QuoteModel>> toggleSavedQuote({
    required SavedQuoteType type,
    required String quoteId,
  }) async {
    final cacheProps = (await localStorageService.getCacheProps).toDomain;
    late SharedCachePropsModel newCacheProps;

    type.map(
      history: (e) {
        newCacheProps = cacheProps.copyWith(
          quotesHistoryIds: cacheProps.quotesHistoryIds.plusElement(quoteId),
        );
      },
      liked: (e) {
        if (cacheProps.favoriteQuotesIds.contains(quoteId)) {
          newCacheProps = cacheProps.copyWith(
            favoriteQuotesIds:
                cacheProps.favoriteQuotesIds.minusElement(quoteId),
          );
        } else {
          newCacheProps = cacheProps.copyWith(
            favoriteQuotesIds:
                cacheProps.favoriteQuotesIds.plusElement(quoteId),
          );
        }
      },
      collection: (e) {
        final collection =
            cacheProps.collections.find((p0) => p0.id == e.collection.id);

        if (collection == null) throw Exception('Unknown Collection');
        late CollectionModel newCollection;

        if (collection.quotesIds.contains(quoteId)) {
          newCollection = collection.copyWith(
            quotesIds: collection.quotesIds.minusElement(quoteId),
          );
        } else {
          newCollection = collection.copyWith(
            quotesIds: collection.quotesIds.plusElement(quoteId),
          );
        }
        newCacheProps = cacheProps.copyWith(
          collections: cacheProps.collections
              .map((cl) => cl.id == e.collection.id ? newCollection : cl),
        );
      },
    );
    await localStorageService.updateCacheProps(newCacheProps);
    return getSavedQuotes(type);
  }

  @override
  Future<KtList<CollectionModel>> addOrUpdateCollections(
    CollectionModel collection,
  ) async {
    final cacheProps = (await localStorageService.getCacheProps).toDomain;
    late SharedCachePropsModel newCacheProps;
    if (cacheProps.collections.any((element) => element.id == collection.id)) {
      newCacheProps = cacheProps.copyWith(
        collections: cacheProps.collections
            .map((p0) => p0.id == collection.id ? collection : p0),
      );
    } else {
      newCacheProps = cacheProps.copyWith(
        collections: cacheProps.collections.plusElement(collection),
      );
    }
    final result = await localStorageService.updateCacheProps(newCacheProps);
    return result.collections;
  }

  @override
  Future<KtList<CollectionModel>> removeCollection(
    String collectionId,
  ) async {
    final cacheProps = (await localStorageService.getCacheProps).toDomain;
    late SharedCachePropsModel newCacheProps;
    final collection =
        cacheProps.collections.find((element) => element.id == collectionId);
    if (collection != null) {
      newCacheProps = cacheProps.copyWith(
        collections: cacheProps.collections.minusElement(collection),
      );
    }
    final result = await localStorageService.updateCacheProps(newCacheProps);
    return result.collections;
  }

  @override
  Future<KtList<ReminderModel>> addOrUpdateReminders(
    ReminderModel reminder,
  ) async {
    final cacheProps = (await localStorageService.getCacheProps).toDomain;
    late SharedCachePropsModel newCacheProps;
    if (cacheProps.reminders.any((element) => element.id == reminder.id)) {
      newCacheProps = cacheProps.copyWith(
        reminders: cacheProps.reminders
            .map((p0) => p0.id == reminder.id ? reminder : p0),
      );
    } else {
      newCacheProps = cacheProps.copyWith(
        reminders: cacheProps.reminders.plusElement(reminder),
      );
    }

    final result = await localStorageService.updateCacheProps(newCacheProps);
    return result.reminders;
  }

  @override
  Future<KtList<ReminderModel>> removeReminder(
    String reminderId,
  ) async {
    final cacheProps = (await localStorageService.getCacheProps).toDomain;
    late SharedCachePropsModel newCacheProps;
    if (cacheProps.reminders.any((element) => element.id == reminderId)) {
      newCacheProps = cacheProps.copyWith(
        reminders: cacheProps.reminders.filterNot((p0) => p0.id == reminderId),
      );
    }
    final result = await localStorageService.updateCacheProps(newCacheProps);
    return result.reminders;
  }

  @override
  Future<KtList<CollectionModel>> get getCollections async {
    final cacheProps = (await localStorageService.getCacheProps).toDomain;
    return cacheProps.collections;
  }

  @override
  Future<AccountInfoModel> getAccountInfo() async {
    final cacheProps = (await localStorageService.getCacheProps).toDomain;
    return cacheProps.account;
  }

  @override
  Future<AccountInfoModel> updateAccountInfo(AccountInfoModel account) async {
    final cacheProps = await getCacheData;
    final result = await localStorageService
        .updateCacheProps(cacheProps.copyWith(account: account));
    return result.account;
  }

  @override
  Future<KtList<ThemeModel>> addOrUpdateThemes(
    ThemeModel theme,
  ) async {
    final cacheProps = (await localStorageService.getCacheProps).toDomain;
    late SharedCachePropsModel newCacheProps;
    if (cacheProps.allThemes
        .any((element) => element.themeId == theme.themeId)) {
      newCacheProps = cacheProps.copyWith(
        allThemes: cacheProps.allThemes
            .map((p0) => p0.themeId == theme.themeId ? theme : p0),
      );
    } else {
      newCacheProps = cacheProps.copyWith(
        allThemes: cacheProps.allThemes.plusElement(theme),
      );
    }
    final result = await localStorageService.updateCacheProps(newCacheProps);
    return result.allThemes;
  }

  @override
  Future<KtList<ThemeModel>> get getThemes async {
    // final cacheProps = (await localStorageService.getCacheProps).toDomain;
    // final newCacheProps = await localStorageService.updateCacheProps(
    //   cacheProps.copyWith(allThemes: emptyList()),
    // );
    // return newCacheProps.allThemes;
    return initialThemes();
  }

  @override
  Future<ThemeModel> get getSelectedTheme async {
    final cacheProps = (await localStorageService.getCacheProps).toDomain;
    final selectedTheme = cacheProps.allThemes
        .find((p0) => p0.themeId == cacheProps.selectedThemeId);
    if (selectedTheme == null) throw Exception('Theme not found');

    return selectedTheme;
  }

  @override
  Future<ThemeModel> selectTheme(String themeId) async {
    final cacheProps = (await localStorageService.getCacheProps).toDomain;
    await localStorageService
        .updateCacheProps(cacheProps.copyWith(selectedThemeId: themeId));

    return getSelectedTheme;
  }

  @override
  Future<KtList<ReminderModel>> get getReminders async {
    final cacheProps = (await localStorageService.getCacheProps).toDomain;
    return cacheProps.reminders;
  }

  @override
  Future<UserSubscription> get getSubscription async =>
      subscriptionService.subscription;

  @override
  Future<void> markQuotesAsSeen(List<String> quotesIds) async {
    final cacheProps = (await localStorageService.getCacheProps).toDomain;
    localStorageService.updateCacheProps(
      cacheProps.copyWith(
        quotesHistoryIds: cacheProps.quotesHistoryIds.plus(quotesIds.kt),
      ),
    );
  }

  @override
  Future<void> deleteQuote(String quoteId) async {
    localStorageService.deleteQuote(quoteId);
  }

  @override
  Future<KtList<String>> toggleFavoriteAuthor(String authorId) async {
    final cacheProps = (await localStorageService.getCacheProps).toDomain;
    final newCacheProps = await localStorageService.updateCacheProps(
      cacheProps.copyWith(
        favoriteAuthorsIds: cacheProps.favoriteAuthorsIds.contains(authorId)
            ? cacheProps.favoriteAuthorsIds.minusElement(authorId)
            : cacheProps.favoriteAuthorsIds.plusElement(authorId),
      ),
    );

    // apiService
    //     .getNewBundle(
    //   categoryIds: newCacheProps.favoriteCategoriesIds.asList(),
    //   authorIds: newCacheProps.favoriteAuthorsIds.asList(),
    //   count: Constants.chunkSize,
    // )
    //     .then((newBundle) {
    //   localStorageService.addNewBundle(
    //     authors: newBundle.authors.kt.map((p0) => p0.toEntity()),
    //     quotes: newBundle.quotes.kt.map((p0) => p0.toEntity()),
    //   );
    // });

    return newCacheProps.favoriteAuthorsIds;
  }

  @override
  Future<KtList<String>> toggleFavoriteCategory(String categoryId) async {
    final cacheProps = (await localStorageService.getCacheProps).toDomain;
    final newCacheProps = await localStorageService.updateCacheProps(
      cacheProps.copyWith(
        favoriteCategoriesIds:
            cacheProps.favoriteCategoriesIds.contains(categoryId)
                ? cacheProps.favoriteCategoriesIds.minusElement(categoryId)
                : cacheProps.favoriteCategoriesIds.plusElement(categoryId),
      ),
    );

    // apiService
    //     .getNewBundle(
    //   categoryIds: newCacheProps.favoriteCategoriesIds.asList(),
    //   authorIds: newCacheProps.favoriteAuthorsIds.asList(),
    //   count: Constants.chunkSize,
    // )
    //     .then((newBundle) {
    //   localStorageService.addNewBundle(
    //     authors: newBundle.authors.kt.map((p0) => p0.toEntity()),
    //     quotes: newBundle.quotes.kt.map((p0) => p0.toEntity()),
    //   );
    // });

    return newCacheProps.favoriteCategoriesIds;
  }

  @override
  Future<void> updateFavorites({
    required Option<KtList<String>> authors,
    required Option<KtList<String>> categories,
  }) async {
    final cacheProps = (await localStorageService.getCacheProps).toDomain;

    await localStorageService.updateCacheProps(
      cacheProps.copyWith(
        favoriteAuthorsIds:
            authors.toNullable() ?? cacheProps.favoriteAuthorsIds,
        favoriteCategoriesIds:
            categories.toNullable() ?? cacheProps.favoriteCategoriesIds,
      ),
    );

    return;

    // apiService
    //     .getNewBundle(
    //   categoryIds: newCacheProps.favoriteCategoriesIds.asList(),
    //   authorIds: newCacheProps.favoriteAuthorsIds.asList(),
    //   count: Constants.chunkSize,
    // )
    //     .then((newBundle) {
    //   localStorageService.addNewBundle(
    //     authors: newBundle.authors.kt.map((p0) => p0.toEntity()),
    //     quotes: newBundle.quotes.kt.map((p0) => p0.toEntity()),
    //   );
    // });
  }

  @override
  Future<void> syncCache() async {
    // final cacheProps = await localStorageService.getCacheProps;
    // if (cacheProps.isFirst == true) {
    //   final cacheFromApi = (await apiService.getUserData()).toDomain();
    //   await localStorageService.updateCacheProps(
    //     cacheFromApi,
    //     clearPrevious: true,
    //   );
    // } else {
    //   final cacheChanges = localStorageService.getCacheChanges;
    //   final cacheFromApi =
    //       (await apiService.getUserData(listOfCacheProps: cacheChanges))
    //           .toDomain();
    //   await localStorageService.updateCacheProps(
    //     cacheFromApi,
    //     clearPrevious: true,
    //   );
    // }
  }

  @override
  Future<SharedCachePropsModel> get getCacheData async {
    final cache = (await localStorageService.getCacheProps).toDomain;
    return cache;
  }

  @override
  Future<void> get userDidTryChat async {
    final cacheProps = (await localStorageService.getCacheProps).toDomain;
    await localStorageService.updateCacheProps(
      cacheProps.copyWith(didTryChat: true),
    );
  }

  @override
  Future<void> get userPassedOnboarding async {
    final cacheProps = (await localStorageService.getCacheProps).toDomain;
    await localStorageService.updateCacheProps(
      cacheProps.copyWith(shouldShowOnboarding: false),
    );
  }

  @override
  Future<bool> get hasQuotesInCache async =>
      (await localStorageService.allQuotesCount) != 0;

  @override
  Future<void> deleteAllQuotes() async {
    return localStorageService.deleteNewQuotes();
  }

  @override
  Future<void> userRewarded(double rewardValue) async {
    final cacheProps = (await localStorageService.getCacheProps).toDomain;
    AccountInfoModel account = cacheProps.account;

    if (account is GuestUser) return;
    account = account as LoggedInUser;
    account = account.copyWith(credits: account.credits + rewardValue);

    await localStorageService.updateCacheProps(
      cacheProps.copyWith(account: account),
    );
  }

  @override
  Future<void> updateChatCredit(double credit) async {
    final cacheProps = (await localStorageService.getCacheProps).toDomain;
    AccountInfoModel account = cacheProps.account;

    if (account is GuestUser) return;
    account = account as LoggedInUser;
    account = account.copyWith(credits: credit);

    await localStorageService.updateCacheProps(
      cacheProps.copyWith(account: account),
    );
  }
}
