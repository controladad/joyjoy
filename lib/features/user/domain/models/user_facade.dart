import 'package:dartz/dartz.dart';

import 'package:motivation_flutter/features/language/domain/models/language_model.dart';
import 'package:motivation_flutter/features/quotes/application/watch_ads_data/watch_ads_data_bloc.dart';

abstract class IUserFacade {
  Option<LanguageModel> get currentLanguage;
  Future<void> storeUserLocale({required LanguageModel languageModel});
  DateTime? get getRemainTimeForPhone;
  Future<WhatShouldDoAfterSeenSomeQuoteType>get getWatchAdsData;
  Future<WhatShouldDoAfterSeenSomeQuoteType> persistWatchAdsData({
    bool? seenInAppReview,
    bool? anotherQuoteSeen,
  });
}
