import 'package:motivation_flutter/common/presentation/buttons.dart';
import 'package:motivation_flutter/common/presentation/common_presentation.dart';
import 'package:motivation_flutter/common/presentation/utils/custom_bottom_sheets.dart';
import 'package:motivation_flutter/features/quotes/domain/models/author_overview_model.dart';
import 'package:motivation_flutter/features/quotes/domain/models/quotes_screen_type.dart';
import 'package:motivation_flutter/features/sub_auth/application/sub_auth_bloc/sub_auth_bloc.dart';

Future<void> showAuthorInfoBottomSheets({
  required BuildContext context,
  required AuthorModel author,
  required SubAuthCubit subAuthCubit,
}) {
  return showCustomBottomSheet(
    context: context,
    onClose: () {},
    child: _Widget(
      author: author,
      authBloc: subAuthCubit,
    ),
  );
}

class _Widget extends StatelessWidget {
  final AuthorModel author;
  final SubAuthCubit authBloc;
  const _Widget({required this.author, required this.authBloc});

  @override
  Widget build(BuildContext context) {
    return BlocProvider.value(
      value: authBloc,
      child: Row(
        children: [
          Padding(
            padding: pad(16, 0, 12, 32),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: pad(8, 0, 0, 0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      RichText(
                        text: TextSpan(
                          text: author.name.toTitleCase(),
                          style: getTextStyle(
                            style: TStyle.TitleLarge,
                            context: context,
                          ),
                          children: [
                            TextSpan(
                              text: ' (${author.gender.capitalize()}) ',
                              style: getTextStyle(
                                style: TStyle.TitleSmall,
                                context: context,
                              ),
                            ),
                          ],
                        ),
                      ),
                      SText(author.job, style: TStyle.TitleMedium),
                      SText(
                        '${author.birthYear} - ${author.deathYear}',
                        style: TStyle.TitleMedium,
                      ),
                      const SBox(height: 12),
                      SText(
                        '${context.translate.country.capitalize()}:',
                        style: TStyle.TitleMedium,
                      ),
                      SText(
                        author.country.capitalize(),
                        style: TStyle.TitleMedium,
                        maxLines: 2,
                      ),
                      const SBox(height: 12),
                      SText(
                        '${context.translate.shortDescription.capitalize()}:',
                        style: TStyle.TitleMedium,
                      ),
                      SText(
                        author.shortDescription.capitalize(),
                        style: TStyle.TitleMedium,
                        maxLines: 2,
                        maxWidth: 330,
                      ),
                    ],
                  ),
                ),
                const SBox(height: 24),
                Builder(
                  builder: (context) {
                    return CustomOutlinedButton(
                      label: context.translate.viewAllQuotes,
                      onTap: () => context.pushRoute(
                        QuotesRoute(
                          type: QuotesScreenType.author(author.id, author.name),
                        ),
                      ),
                    );
                  },
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
