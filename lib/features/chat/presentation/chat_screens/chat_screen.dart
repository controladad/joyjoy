import 'package:motivation_flutter/common/presentation/common_imports.dart';
import 'package:motivation_flutter/features/chat/application/chat_bloc/chat_bloc.dart';
import 'package:motivation_flutter/utils/view/layout_builder.dart';
import 'package:motivation_flutter/features/chat/presentation/chat_screens/chat_phone_view.dart';

@RoutePage()
class ChatScreen extends StatelessWidget {
  const ChatScreen({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => getIt<ChatBloc>()..add(const ChatEvent.initial()),
      child: ResponsiveLayoutBuilder(
        phone: (context, child) {
          return const ChatPhoneView();
        },
      ),
    );
  }
}
