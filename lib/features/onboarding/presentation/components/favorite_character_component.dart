// import 'dart:ui';

import 'package:motivation_flutter/common/presentation/common_imports.dart';
import 'package:motivation_flutter/features/quotes/domain/models/author_overview_model.dart';

class FavoriteCharacterComponent extends StatelessWidget {
  final AuthorModel character;
  final bool isSelected;
  final VoidCallback onTap;
  const FavoriteCharacterComponent({
    super.key,
    required this.character,
    required this.isSelected,
    required this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: pad(0, 2, 0, 2),
      child: MaterialButton(
        animationDuration: const Duration(milliseconds: 20),
        padding: EdgeInsets.zero,
        elevation: 0,
        color: isSelected ? const Color(0x24FBAAFF) : Colors.transparent,
        splashColor: const Color(0x24FBAAFF).withOpacity(.2),
        highlightColor: const Color(0x24FBAAFF).withOpacity(.1),
        onPressed: onTap,
        child: Row(
          children: [
            const SizedBox(width: 18),
            Flexible(
              flex: 4,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  const SizedBox(
                    height: 9,
                    width: double.infinity,
                  ),
                  SText(
                    character.name,
                    style: TStyle.BodyLarge,
                    color: AppTheme.on_surface_dark,
                  ),
                  SText(
                    character.shortDescription,
                    // character.description,
                    style: TStyle.LabelSmall,
                    color: AppTheme.outline_dark,
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                  ),
                  const SizedBox(height: 9),
                ],
              ),
            ),
            const Spacer(),
            if (isSelected)
              const ImageWidget(
                image: AppIcons.checkbox_dark,
                height: 37,
                width: 37,
              )
            else
              const SizedBox(height: 37, width: 37),
            const SizedBox(width: 18),
          ],
        ),
      ),
    );
  }
}
