import 'dart:async';

import 'package:hive/hive.dart';
import 'package:injectable/injectable.dart';
import 'package:motivation_flutter/constants/db_constants.dart';
import 'package:motivation_flutter/features/profile/domain/models/user_subscription_entity.dart';
import 'package:motivation_flutter/features/profile/domain/models/user_subscription_model.dart';
import 'package:motivation_flutter/features/sub_auth/domain/models/paywall_model.dart';
//

abstract class ISubscriptionService {
  Future<void> initialize();
  Future<void> login(String userId);
  Future<void> logout();
  Future<UserSubscription> get subscription;
  Stream<UserSubscription> get hasSubscription;
  Future<UserSubscription> changeSubscription(
    PayWallSubscriptionPlanModel plan,
  );
  Future<UserSubscription> restoreSubscription();
}

// @iosEnv
// @Singleton(as: ISubscriptionService)
// class RevenueCatSubscriptionService implements ISubscriptionService {
//   final Box<UserSubscriptionEntity> subscriptionBox;
//   RevenueCatSubscriptionService(
//     @Named(DBConstants.SUBSCRIPTION) this.subscriptionBox,
//   );

//   @override
//   Future<void> initialize() async {
//     print('+++++++++++++++++++++++');
//     print(EnvConstants.revenueCatIosApiKey);
//     await Purchases.setLogLevel(LogLevel.debug);
//     final PurchasesConfiguration configuration =
//         PurchasesConfiguration(EnvConstants.revenueCatIosApiKey)
//           ..store = Store.appStore;

//     await Purchases.configure(configuration);
//     await getProducts();
//     final customerInfo =await Purchases.getCustomerInfo();
//     print(customerInfo);
//     if(customerInfo.activeSubscriptions.isEmpty){

//     }

//   }

//   @override
//   Future<void> login(String userId) => Purchases.logIn(userId);

//   @override
//   Future<void> logout() => Purchases.logOut();

//   Future<List<Package>> getProducts() async {
//     final Offerings offerings = await Purchases.getOfferings();
//     if (offerings.current != null) {
//       return offerings.current!.availablePackages;
//     } else {
//       return [];
//     }
//   }

//   @override
//   Future<UserSubscription> changeSubscription(
//     PayWallSubscriptionPlanModel plan,
//   ) async {
//     final packages = await getProducts();
//     final Package package =
//         packages.firstWhere((element) => element.identifier == plan.identifier);
//     final CustomerInfo customerInfo = await Purchases.purchasePackage(package);
//     final ac = customerInfo.entitlements.all['premium'];
//     if (ac?.isActive ?? false) {
//       return UserSubscription.premium(
//         name: ac!.identifier,
//         hasPermissionToSeeAllQuotes: true,
//         hasPermissionChat: true,
//         shouldShowAds: false,
//         startDate: DateTime.parse(ac.originalPurchaseDate),
//         nextBillingDate: DateTime.parse(ac.expirationDate ?? ''),
//         shouldRenew: ac.willRenew,
//       );
//     } else {
//       return UserSubscription.free(
//         lastPlanStartDate: none(),
//         lastPlanEndDate: none(),
//       );
//     }
//   }

//   @override
//   Stream<bool> get hasSubscription {
//     return subscriptionBox.watch().map((event) {
//       return (event.value as UserSubscriptionEntity).toDomain().isPremium;
//     });
//   }

//   @override
//   Future<UserSubscription> restoreSubscription() {
//     // TODO: implement restoreSubscription
//     throw UnimplementedError();
//   }

//   @override
//   Future<UserSubscription> get subscription async {
//     final subscriptions = subscriptionBox.values;
//     if(subscriptions.isNotEmpty){
//     return subscriptionBox.values.last.toDomain();
//     }else{
//       return UserSubscription.empty();
//     }
//   }
// }

// @androidEnv
@Singleton(as: ISubscriptionService)
class FakeSubscriptionService implements ISubscriptionService {
  final Box<UserSubscriptionEntity> subscriptionBox;
  FakeSubscriptionService(
    @Named(DBConstants.SUBSCRIPTION) this.subscriptionBox,
  );

  @override
  Future<void> initialize() async {
    // if (subscriptionBox.isEmpty) {
    //   subscriptionBox
    //       .add(UserSubscriptionEntity.fromDomain(UserSubscription.empty()));
    // }
  }

  @override
  Stream<UserSubscription> get hasSubscription {
    throw UnimplementedError();

    // return subscriptionBox.watch().map((event) {
    //   return (event.value as UserSubscriptionEntity).toDomain().isPremium;
    // });
  }

  @override
  Future<UserSubscription> get subscription async {
    return UserSubscription.premium(
      name: 'Premium',
      hasPermissionToSeeAllQuotes: true,
      hasPermissionChat: true,
      shouldShowAds: true,
      startDate: DateTime.now(),
      nextBillingDate: DateTime(2050),
      shouldRenew: false,
    );
  }

  @override
  Future<UserSubscription> changeSubscription(
    PayWallSubscriptionPlanModel plan,
  ) async {
    return UserSubscription.premium(
      name: 'Premium',
      hasPermissionToSeeAllQuotes: true,
      hasPermissionChat: true,
      shouldShowAds: true,
      startDate: DateTime.now(),
      nextBillingDate: DateTime(2050),
      shouldRenew: false,
    );
  }

  @override
  Future<UserSubscription> restoreSubscription() async {
    return UserSubscription.premium(
      name: 'Premium',
      hasPermissionToSeeAllQuotes: true,
      hasPermissionChat: true,
      shouldShowAds: true,
      startDate: DateTime.now(),
      nextBillingDate: DateTime(2050),
      shouldRenew: false,
    );
  }

  @override
  Future<void> login(String userId) async {}

  @override
  Future<void> logout() async {}
}
