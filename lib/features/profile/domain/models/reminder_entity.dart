import 'package:hive/hive.dart';
import 'package:kt_dart/kt.dart';
import 'package:motivation_flutter/common/presentation/utils/common_functions.dart';
import 'package:motivation_flutter/features/profile/domain/models/reminder_model.dart';
import 'package:motivation_flutter/features/quotes/domain/models/sub_category_entity.dart';

part 'reminder_entity.g.dart';

@HiveType(typeId: 7)
class ReminderEntity extends HiveObject {
  @HiveField(0)
  String id;
  @HiveField(1)
  List<SubCategoryEntity> categories;
  @HiveField(2)
  DateTime startTime;
  @HiveField(3)
  DateTime endTime;
  @HiveField(4)
  int countPerDuration;
  @HiveField(5)
  String alarmSoundId;
  @HiveField(6)
  List<int> daysOfWeek;
  @HiveField(7)
  bool isActive;

  ReminderEntity({
    required this.id,
    required this.categories,
    required this.startTime,
    required this.endTime,
    required this.countPerDuration,
    required this.alarmSoundId,
    required this.daysOfWeek,
    required this.isActive,
  });

  ReminderEntity copyWith({
    String? id,
    List<SubCategoryEntity>? categories,
    DateTime? startTime,
    DateTime? endTime,
    int? countPerDuration,
    String? alarmSoundId,
    List<int>? daysOfWeek,
    bool? isActive,
  }) {
    return ReminderEntity(
      id: id ?? this.id,
      categories: categories ?? this.categories,
      startTime: startTime ?? this.startTime,
      endTime: endTime ?? this.endTime,
      countPerDuration: countPerDuration ?? this.countPerDuration,
      alarmSoundId: alarmSoundId ?? this.alarmSoundId,
      daysOfWeek: daysOfWeek ?? this.daysOfWeek,
      isActive: isActive ?? this.isActive,
    );
  }

  factory ReminderEntity.fromDomain(ReminderModel model) {
    return ReminderEntity(
      id: model.id,
      categories:
          model.categories.map((e) => SubCategoryEntity.fromDomain(e)).asList(),
      startTime: model.startTime,
      endTime: model.endTime,
      countPerDuration: model.countPerDuration,
      alarmSoundId: model.alarmSoundId,
      daysOfWeek: model.daysOfWeek
          .map((day) => DaysOfWeek.values.indexOf(day))
          .asList(),
      isActive: model.isActive,
    );
  }

  ReminderModel toDomain() {
    return ReminderModel(
      id: id,
      categories: categories.kt.map((p0) => p0.toDomain()),
      startTime: startTime,
      endTime: endTime,
      countPerDuration: countPerDuration,
      alarmSoundId: alarmSoundId,
      daysOfWeek: daysOfWeek.kt.map((p0) => DaysOfWeek.values[p0]),
      isActive: isActive,
    );
  }
}
