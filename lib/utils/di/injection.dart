import 'dart:io';

import 'package:get_it/get_it.dart';
import 'package:injectable/injectable.dart';
import 'package:motivation_flutter/utils/di/injection.config.dart';

final getIt = GetIt.instance;

final environment = Platform.isAndroid ? 'Android' : 'Ios';
const iosEnv = Environment('Ios');
const androidEnv = Environment('Android');

@injectableInit
Future<void> configureInjection() => getIt.init(environment: environment);
