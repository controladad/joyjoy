// import 'package:bloc/bloc.dart';
// import 'package:dartz/dartz.dart';
// import 'package:freezed_annotation/freezed_annotation.dart';
// import 'package:injectable/injectable.dart';
// import 'package:kt_dart/collection.dart';
// import 'package:motivation_flutter/common/presentation/utils/constants.dart';
// import 'package:motivation_flutter/core/failures.dart';
// import 'package:motivation_flutter/features/onboarding/domain/models/favorite_character_model.dart';
// import 'package:motivation_flutter/features/onboarding/domain/models/list_of_characters_model.dart';
// import 'package:motivation_flutter/features/onboarding/domain/onboarding_repository.dart';

// part 'favorite_characters_event.dart';
// part 'favorite_characters_state.dart';
// part 'favorite_characters_bloc.freezed.dart';

// @injectable
// class FavoriteCharactersBloc
//     extends Bloc<FavoriteCharactersEvent, FavoriteCharactersState> {
//   final IOnboardingRepository _onboardingRepository;
//   FavoriteCharactersBloc(super.initState, this._onboardingRepository) {
//     on<FavoriteCharactersEvent>(_onFavoriteCharactersEvent);
//   }

//   int allDataPage = 1;
//   int pageOfResult = 1;

//   Future<void> _onFavoriteCharactersEvent(
//     FavoriteCharactersEvent event,
//     Emitter emit,
//   ) {
//     return event.map(
//       clearSearchQuery: (e) async {
//         emit(
//           state.copyWith(
//             searchString: none(),
//             searchQueryResultCharacterList: emptyList(),
//             isSucceed: none(),
//           ),
//         );
//       },
//       loadData: (event) async {
//         emit(
//           state.copyWith(
//             data: event.isLazyLoad ? none() : state.data,
//             isLoading: true,
//             isSucceed: none(),
//           ),
//         );
//         late int page;
//         late KtList<FavoriteCharacterModel> targetList;
//         state.searchString.fold(() {
//           if (event.isLazyLoad) {
//             page = allDataPage;
//           } else {
//             page = 1;
//             allDataPage = 1;
//             targetList = emptyList();
//           }
//           targetList = state.charactersList;
//         }, (a) {
//           if (event.isLazyLoad) {
//             page = pageOfResult;
//             targetList = state.searchQueryResultCharacterList;
//           } else {
//             pageOfResult = 1;
//             page = 1;
//             targetList = emptyList();
//           }
//         });
//         final charactersData =
//             await _onboardingRepository.getFavoriteCharacters(
//           page: page,
//           searchQuery: state.searchString.toNullable(),
//           limit: Constants.paginationLimit,
//         );
//         charactersData.fold(
//           (l) => emit(state.copyWith(isSucceed: some(left(l)))),
//           (r) {
//             state.searchString.fold(() {
//               allDataPage++;
//               emit(
//                 state.copyWith(
//                   hasReachedMax:
//                       r.totalCount == targetList.plus(r.characters.kt).size,
//                   data: some(charactersData),
//                   charactersList: targetList.plus(r.characters.kt),
//                   isLoading: false,
//                   isSucceed: some(right(unit)),
//                 ),
//               );
//             }, (a) {
//               pageOfResult++;
//               emit(
//                 state.copyWith(
//                   hasReachedMax:
//                       r.totalCount == targetList.plus(r.characters.kt).size,
//                   data: some(charactersData),
//                   searchQueryResultCharacterList:
//                       targetList.plus(r.characters.kt),
//                   isLoading: false,
//                   isSucceed: some(right(unit)),
//                 ),
//               );
//             });
//           },
//         );
//       },
//       searchStringChanged: (e) async {
//         emit(state.copyWith(searchString: some(e.value)));
//         add(const FavoriteCharactersEvent.loadData(isLazyLoad: true));
//       },
//     );
//   }
// }
