import 'package:hive/hive.dart';
import 'package:kt_dart/kt.dart';
import 'package:motivation_flutter/features/quotes/domain/models/author_overview_model.dart';
import 'package:motivation_flutter/features/quotes/domain/models/quotes_model.dart';

part 'quote_entity.g.dart';

@HiveType(typeId: 2)
class QuoteEntity {
  @HiveField(0)
  String id;
  @HiveField(1)
  String quote;
  @HiveField(2)
  List<String> categoryIds;
  @HiveField(3)
  String? authorId;

  QuoteEntity({
    required this.id,
    required this.quote,
    required this.categoryIds,
    this.authorId,
  });

  QuoteEntity copyWith({
    String? id,
    String? quote,
    List<String>? categoryIds,
    String? authorId,
    bool? isFavorite,
    bool? isInCollection,
  }) {
    return QuoteEntity(
      id: id ?? this.id,
      quote: quote ?? this.quote,
      categoryIds: categoryIds ?? this.categoryIds,
      authorId: authorId ?? this.authorId,
    );
  }

  factory QuoteEntity.fromDomain(QuoteModel model) {
    return QuoteEntity(
      id: model.id,
      quote: model.quote,
      categoryIds: model.categoryIds.asList(),
      authorId: model.author?.id,
    );
  }

  QuoteModel toDomain({
    required AuthorModel author,
  }) {
    return QuoteModel(
      id: id,
      quote: quote,
      author: author,
      categoryIds: categoryIds.kt,
    );
  }
}
