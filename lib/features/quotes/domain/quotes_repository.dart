import 'package:dartz/dartz.dart';
import 'package:kt_dart/kt.dart';
import 'package:motivation_flutter/core/failures.dart';
import 'package:motivation_flutter/features/profile/domain/models/collection_model.dart';

import 'package:motivation_flutter/features/quotes/domain/models/quotes_models.dart';
import 'package:motivation_flutter/features/quotes/domain/models/quotes_screen_type.dart';
import 'package:motivation_flutter/shared/domain/shared_cache_props_model.dart';

abstract class IQuotesRepository {
  Future<Either<GeneralFailure, String>> get getUserName;
  Stream<SharedCachePropsModel?> get cacheUpdated;
  Future<Either<GeneralFailure, KtList<QuoteModel>>> getQuotes(
    QuotesScreenType type,
  );
  Future<void> deleteAllQuotes();
  Future<void> changeFontRandomly();

  Future<Either<GeneralFailure, SharedCachePropsModel>> getCacheData();

  Future<Either<GeneralFailure, Unit>> addQuoteToCollection({
    required QuoteModel quote,
    required CollectionModel collection,
  });

  Future<Either<GeneralFailure, Unit>> toggleQuoteLike(QuoteModel quote);
  Future<Either<GeneralFailure, Unit>> deleteQuote(String quoteId);
  Future<Either<GeneralFailure, Unit>> reportQuote({
    required String quoteId,
    required String reportText,
  });
}
