import 'package:auto_route/auto_route.dart';
import 'package:flutter/widgets.dart';

import 'package:motivation_flutter/utils/view/layout_builder.dart';
import 'package:motivation_flutter/features/profile/presentation/profile_screens/profile_phone_view.dart';

@RoutePage()
class ProfileScreen extends StatelessWidget {
  const ProfileScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return ResponsiveLayoutBuilder(
      phone: (context, child) {
        return const ProfilePhoneView();
      },
    );
  }
}
