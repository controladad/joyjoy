import 'package:motivation_flutter/common/presentation/common_imports.dart';

import 'package:motivation_flutter/utils/view/layout_builder.dart';
import 'package:motivation_flutter/features/theme/presentation/theme_screens/theme_phone_view.dart';
import 'package:motivation_flutter/features/theme/application/theme_bloc/theme_bloc.dart';

@RoutePage()
class ThemeScreen extends StatelessWidget {
  const ThemeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) =>
          getIt<ThemeBloc>()..add(const ThemeEvent.getThemes()),
      child: ResponsiveLayoutBuilder(
        phone: (context, child) {
          return const ThemePhoneView();
        },
      ),
    );
  }
}
