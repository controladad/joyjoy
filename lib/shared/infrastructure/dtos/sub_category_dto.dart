import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:motivation_flutter/features/quotes/domain/models/quotes_models.dart';

part 'sub_category_dto.g.dart';
part 'sub_category_dto.freezed.dart';

@freezed
class SubCategoryDto with _$SubCategoryDto {
  factory SubCategoryDto({
    @JsonKey(name: 'id') required int id,
    @JsonKey(name: 'name') required String name,
    @JsonKey(name: 'image') required String icon,
    @JsonKey(name: 'tags') required List<String> tags,
    @JsonKey(name: 'type') required String status,
  }) = _SubCategoryDto;
  const SubCategoryDto._();
  factory SubCategoryDto.fromJson(Map<String, dynamic> json) =>
      _$SubCategoryDtoFromJson(json);

  factory SubCategoryDto.fromEntity(SubCategoryEntity entity) => SubCategoryDto(
        id: int.parse(entity.id),
        name: entity.name,
        icon: entity.icon,
        tags: entity.tags,
        status: '',
      );

  SubCategoryModel toDomain() {
    return SubCategoryModel(
      id: id.toString(),
      name: name,
      icon: icon,
      description: tags.join(', '),
      tags: tags,
      status: status == 'free'
          ? AccessabilityStatus.Free
          : AccessabilityStatus.Premium,
    );
  }

  SubCategoryEntity toEntity() {
    return SubCategoryEntity.fromDomain(toDomain());
  }
}
