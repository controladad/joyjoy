import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:kt_dart/kt.dart';
import 'package:motivation_flutter/features/quotes/domain/models/sub_category_model.dart';
part 'select_author_category_args.freezed.dart';

enum AuthorOrCategoryEnum { Category, Author }

@freezed
class SelectAuthorCategoryScreenType with _$SelectAuthorCategoryScreenType {
  const factory SelectAuthorCategoryScreenType.preferred(
    AuthorOrCategoryEnum type,
  ) = _Preferred;
  const factory SelectAuthorCategoryScreenType.reminderTopic({
    required Function(KtList<SubCategoryModel> categories) onSubmit,
    required KtList<String> selectedCategoriesIds,
  }) = _ReminderTopic;
}
