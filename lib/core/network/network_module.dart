import 'dart:io';

import 'package:dio/dio.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:injectable/injectable.dart';
import 'package:motivation_flutter/common/presentation/utils/app_widget.dart';

import 'package:motivation_flutter/constants/end_points.dart';
import 'package:motivation_flutter/core/network/api_error_handler.dart';
import 'package:motivation_flutter/features/user/infrastructure/user_local_data.dart';

@module
abstract class NetworkModule {
  @factoryMethod
  Dio provideDio(
    IUserData userData,
  ) {
    final dio = Dio();

    dio
      ..options.baseUrl = EndPoints.baseUrl
      ..options.connectTimeout = EndPoints.connectionTimeout
      ..options.receiveTimeout = EndPoints.receiveTimeout
      ..options.headers = {'Content-Type': 'application/json'}
      ..options.followRedirects = true
      ..interceptors.add(tokenInterceptor(userData))
      ..interceptors.add(updateInfoInterceptor)
      ..interceptors.add(LogInterceptor())
      ..interceptors.add(retryInterceptor(dio))
      ..interceptors.add(apiErrorHandler);

    return dio;
  }
}

@module
abstract class SignInInjectableModules {
  @injectable
  GoogleSignIn get googleSignIn => GoogleSignIn();
}

InterceptorsWrapper tokenInterceptor(IUserData userData) {
  return InterceptorsWrapper(
    onRequest: (
      RequestOptions options,
      RequestInterceptorHandler handler,
    ) async {
      final String? token = userData.user.toNullable()?.token;

      if (token != null) {
        options.headers.putIfAbsent('Authorization', () => 'Bearer $token');
      }
      print('token -> $token');

      return handler.next(options);
    },
  );
}

InterceptorsWrapper get updateInfoInterceptor {
  return InterceptorsWrapper(
    onRequest: (
      RequestOptions options,
      RequestInterceptorHandler handler,
    ) async {
      final String appVersion = globalPackageInfo?.version ?? '1.1.3';
      final String platform = Platform.isAndroid ? 'android' : 'ios';

      options.headers
          .putIfAbsent('X-App-Version', () => '$platform@$appVersion');

      return handler.next(options);
    },
  );
}
