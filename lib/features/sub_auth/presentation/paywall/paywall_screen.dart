import 'package:kt_dart/collection.dart';
import 'package:motivation_flutter/common/presentation/buttons.dart';
import 'package:motivation_flutter/common/presentation/common_imports.dart';
import 'package:motivation_flutter/common/presentation/utils/failure_handler_widget.dart';
import 'package:motivation_flutter/features/sub_auth/application/paywall_bloc/paywall_cubit.dart';
import 'package:motivation_flutter/features/sub_auth/application/sub_auth_bloc/sub_auth_bloc.dart';
import 'package:motivation_flutter/features/sub_auth/domain/models/paywall_model.dart';
import 'package:motivation_flutter/features/sub_auth/presentation/paywall/choose_subscription_bottom_sheets.dart';
import 'package:motivation_flutter/features/sub_auth/presentation/paywall/paywall_detentions_widget.dart';
import 'package:motivation_flutter/features/sub_auth/presentation/paywall/paywall_frequently_asked_questions_widget.dart';
import 'package:motivation_flutter/features/sub_auth/presentation/paywall/paywall_product_feedbacks_widget.dart';
import 'package:motivation_flutter/features/sub_auth/presentation/paywall/paywall_subscription_plans_widget.dart';
import 'package:motivation_flutter/features/sub_auth/presentation/paywall/paywall_table_widget.dart';
import 'package:motivation_flutter/features/sub_auth/presentation/paywall/paywall_top_widget.dart';
import 'package:motivation_flutter/features/sub_auth/presentation/paywall/paywall_trial_schedule_widget.dart';

@RoutePage()
class PaywallScreen extends StatelessWidget {
  final Function(BuildContext ctx) onClose;
  const PaywallScreen({
    super.key,
    required this.onClose,
  });

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => getIt<PaywallCubit>()..initialize(),
      child: Builder(
        builder: (context) {
          final state = context.watch<PaywallCubit>().state;
          final bloc = context.read<PaywallCubit>();

          return Scaffold(
            body: CommonBackgroundContainer(
              child: SafeArea(
                child: FailureHandlerWidget<PaywallModel>(
                  child: (r) => _Body(
                    model: r,
                    onClose: () => onClose(context),
                    onBuy: (plan) {
                      showBuySubscriptionBottomSheets(
                        context: context,
                        plan: plan,
                        bloc: bloc,
                        onDone: () => onClose(context),
                      );
                    },
                  ),
                  onRetry: () => bloc.initialize(),
                  data: state.data,
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}

class _Body extends StatelessWidget {
  final PaywallModel model;
  final Function() _onClose;
  final Function(PayWallSubscriptionPlanModel plan) onBuy;
  const _Body({
    required this.model,
    required dynamic Function() onClose,
    required this.onBuy,
  }) : _onClose = onClose;

  @override
  Widget build(BuildContext context) {
    final state = context.watch<PaywallCubit>().state;
    final bloc = context.read<PaywallCubit>();

    final plan = state.selectedSubscription.toNullable();
    return SingleChildScrollView(
      child: Column(
        children: [
          PaywallTopBarWidget(
            name: model.userName,
            onClose: _onClose,
            onRestore: () {
              context.read<SubAuthCubit>().restoreSubscription();
              context.popRoute();
            },
          ),
          PaywallTrialScheduleWidget(model.schedule.items),
          const SBox(height: 35),
          PaywallSubscriptionPlansWidget(
            plans: model.plans,
            onPlanSelection: (plan) {
              bloc.selectPlan(plan);
              onBuy(plan);
            },
            selectedPlan:
                state.selectedSubscription.getOrElse(() => model.plans.first()),
          ),
          const SBox(height: 35),
          PaywallDefinitionsWidget(model.definitions),
          const SBox(height: 10),
          if (model.tableItems.isNotEmpty())
            Padding(
              padding: pad(0, 0, 0, 20),
              child: PaywallComparisonTableWidget(model.tableItems),
            ),
          if (model.reviews.isNotEmpty())
            Padding(
              padding: pad(0, 0, 0, 33),
              child: PaywallProductFeedbacksWidget(model.reviews),
            ),
          if (model.questions.isNotEmpty())
            Padding(
              padding: pad(0, 0, 0, 20),
              child: PaywallFrequentlyAskedQuestions(model.questions),
            ),
          CustomFilledButton(
            label: context.translate.goPremium,
            leftIcon: const ImageWidget(
              image: AppIcons.smallDiamond,
              height: 15,
              width: 15,
            ),
            isEnable: plan != null,
            onTap: () => onBuy(plan!),
          ),
          const SBox(height: 32),
        ],
      ),
    );
  }
}
