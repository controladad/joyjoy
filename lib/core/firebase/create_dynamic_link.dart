// import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
// import 'package:motivation_flutter/common/presentation/common_presentation.dart';
// import 'package:package_info_plus/package_info_plus.dart';

// Future<String> createDynamicLink({
//   required bool short,
//   required String path,
//   required String id,
//   String? name,
//   String? imageUrl,
// }) async {
//   final FirebaseDynamicLinks firebaseDynamicLinks =
//       FirebaseDynamicLinks.instance;
//   final PackageInfo packageInfo = await PackageInfo.fromPlatform();

//   final DynamicLinkParameters parameters = DynamicLinkParameters(
//     uriPrefix: Constants.uri_prefix,
//     link: Uri.parse('https://ttq.ecommerce.com/$path/$id'),
//     androidParameters: AndroidParameters(
//       packageName: packageInfo.packageName,
//     ),
//     socialMetaTagParameters: SocialMetaTagParameters(
//       imageUrl: Uri.tryParse(imageUrl ?? ''),
//       title: name,
//     ),
//     iosParameters: IOSParameters(
//       bundleId: packageInfo.packageName,
//     ),
//   );

//   Uri url;
//   if (short) {
//     final ShortDynamicLink shortLink =
//         await firebaseDynamicLinks.buildShortLink(parameters);

//     url = shortLink.shortUrl;
//   } else {
//     url = await firebaseDynamicLinks.buildLink(parameters);
//   }

//   return url.path;
// }
