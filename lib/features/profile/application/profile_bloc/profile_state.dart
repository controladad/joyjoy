part of 'profile_cubit.dart';

@injectable
@freezed
class ProfileState with _$ProfileState {
  const factory ProfileState({
    required Option<Either<GeneralFailure, ProfileContentsModel>> onlineData,
    required Option<Either<GeneralFailure, ProfileModel>> offlineData,
    required KtList<ProfileBannerModel> banners,
  }) = _ProfileState;

  @factoryMethod
  factory ProfileState.init() => ProfileState(
        offlineData: none(),
        onlineData: none(),
        banners: emptyList(),
      );
}
