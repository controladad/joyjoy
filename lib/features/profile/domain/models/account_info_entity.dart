import 'package:hive/hive.dart';
import 'package:motivation_flutter/common/presentation/utils/common_functions.dart';
// import 'package:motivation_flutter/common/presentation/utils/common_functions.dart';
import 'package:motivation_flutter/features/profile/domain/models/account_info_model.dart';

part 'account_info_entity.g.dart';

@HiveType(typeId: 15)
class AccountInfoEntity extends HiveObject {
  @HiveField(0)
  String? name;
  @HiveField(1)
  String? accountStatus;
  @HiveField(2)
  String? email;
  @HiveField(3)
  DateTime? signupDate;
  @HiveField(4)
  String? premiumPlan;
  @HiveField(5)
  bool isGuest;
  @HiveField(6)
  double? credits;
  @HiveField(7)
  String? userId;

  AccountInfoEntity({
    this.name,
    this.userId,
    this.accountStatus,
    this.email,
    this.premiumPlan,
    this.signupDate,
    required this.isGuest,
    this.credits,
  });

  factory AccountInfoEntity.fromDomain(AccountInfoModel account) {
    return account.map(
      loggedIn: (loggedInUser) => AccountInfoEntity(
        userId: loggedInUser.userId,
        isGuest: false,
        name: loggedInUser.name.toNullable(),
        accountStatus: loggedInUser.accountStatus,
        email: loggedInUser.email,
        premiumPlan: loggedInUser.premiumPlan,
        signupDate: loggedInUser.signupDate,
        credits: loggedInUser.credits,
      ),
      guest: (guest) =>
          AccountInfoEntity(isGuest: true, name: guest.name.toNullable()),
    );
  }

  AccountInfoModel toDomain() {
    if (isGuest) {
      return AccountInfoModel.guest(name: name.toOption());
    } else {
      return AccountInfoModel.loggedIn(
        userId: userId ?? '',
        accountStatus: accountStatus ?? '',
        name: name.toOption(),
        signupDate: signupDate ?? DateTime.now(),
        email: email ?? '',
        premiumPlan: premiumPlan ?? '',
        credits: credits ?? 0,
      );
    }
  }
}
