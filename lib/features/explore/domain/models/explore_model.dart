import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:kt_dart/kt.dart';
import 'package:motivation_flutter/features/quotes/domain/models/category_model.dart';

part 'explore_model.freezed.dart';

@freezed
class ExploreModel with _$ExploreModel {
  const factory ExploreModel({
    required KtList<CategoryModel> categories,
  }) = _ExploreModel;
}
