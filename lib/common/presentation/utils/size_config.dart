import 'dart:math';

import 'package:flutter/material.dart';

class SizeConfig {
  static const Size designSize = Size(390, 844);
  static late double screenWidth;
  static late double screenHeight;

  static late double textMultiplier;
  static late double imageSizeMultiplier;
  static late double heightMultiplier;
  static late double widthMultiplier;
  static bool isPortrait = true;

  void init(BoxConstraints constraints, Orientation orientation) {
    if (orientation == Orientation.portrait) {
      screenWidth = constraints.maxWidth;
      screenHeight = constraints.maxHeight;
      isPortrait = true;
    } else {
      screenWidth = constraints.maxHeight;
      screenHeight = constraints.maxWidth;
      isPortrait = false;
    }

    widthMultiplier = sqrt(screenWidth / designSize.width);
textMultiplier=1;
    heightMultiplier = widthMultiplier;
  }
}

extension SizeConfigCalculatorIntX on num {
  double get w {
    return double.parse(
      (this * SizeConfig.widthMultiplier).toStringAsFixed(2),
    );
  }

  double get h => double.parse(
        (this * (SizeConfig.heightMultiplier)).toStringAsFixed(2),
      );

  double get n {
    if (SizeConfig.screenWidth < SizeConfig.designSize.width) {
      return this * (SizeConfig.widthMultiplier);
    } else {
      return this * 1;
    }
  }
}

EdgeInsetsGeometry pad(
  double? left,
  double? top,
  double? right,
  double? bottom,
) =>
    EdgeInsetsDirectional.fromSTEB(
      left?.w ?? 0,
      top?.h ?? 0,
      right?.w ?? 0,
      bottom?.h ?? 0,
    );
