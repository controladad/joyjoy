import 'package:kt_dart/kt.dart';
import 'package:motivation_flutter/common/presentation/common_imports.dart';
import 'package:motivation_flutter/features/explore/application/full_search_bloc/full_search_bloc.dart';
import 'package:motivation_flutter/features/explore/domain/models/search_models.dart';
import 'package:motivation_flutter/features/explore/presentation/components/search_components.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

@RoutePage()
class FullSearchScreen extends StatelessWidget {
  final String query;
  final SearchResultUnion firstItem;

  const FullSearchScreen({required this.query, required this.firstItem});

  @override
  Widget build(BuildContext context) {
    final String pageName = firstItem.map(
      category: (e) => context.translate.category,
      author: (e) => context.translate.author,
      quote: (e) => context.translate.quote,
    );
    return BlocProvider(
      create: (context) => getIt<FullSearchBloc>()
        ..add(FullSearchEvent.getFirstPage(query: query, firstItem: firstItem)),
      child: CommonBackgroundContainer(
        child: Scaffold(
          backgroundColor: Colors.transparent,
          appBar: AppBar(
            backgroundColor: Colors.transparent,
            title: SText(
              '${pageName.capitalize()} ${context.translate.as} "$query"',
              style: TStyle.HeadlineSmall,
            ),
            leading: const CommonBackArrowButton(),
          ),
          body: SafeArea(child: _Body(firstItem)),
        ),
      ),
    );
  }
}

class _Body extends StatelessWidget {
  final SearchResultUnion firstItem;
  const _Body(this.firstItem);

  @override
  Widget build(BuildContext context) {
    final state = context.watch<FullSearchBloc>().state;

    return _RefreshWidget(
      child: ListView(
        padding: pad(30, 0, 0, 0),
        children: state.results
            .mapIndexed(
              (index, item) => Row(
                children: [
                  if (index == 0)
                    ImageWidget(
                      image: firstItem.map(
                        category: (e) => AppIcons.searchCategory,
                        author: (e) => AppIcons.searchAuthor,
                        quote: (e) => AppIcons.searchQuote,
                      ),
                      height: 32,
                      width: 32,
                    )
                  else
                    const SBox(width: 32),
                  SearchListItemWidget(item),
                ],
              ),
            )
            .asList(),
      ),
    );
  }
}

class _RefreshWidget extends StatelessWidget {
  final Widget child;
  const _RefreshWidget({required this.child});

  @override
  Widget build(BuildContext context) {
    final bloc = context.read<FullSearchBloc>();
    final state = context.watch<FullSearchBloc>().state;
    return SmartRefresher(
      controller: state.controller,
      onLoading: () {
        bloc.add(const FullSearchEvent.lazyLoad());
      },
      onRefresh: () {
        bloc.add(const FullSearchEvent.refresh());
      },
      enablePullUp: state.fullSearchResultsData.fold(() => false, (a) => true),
      enablePullDown:
          state.fullSearchResultsData.fold(() => false, (a) => true),
      child: child,
    );
  }
}
