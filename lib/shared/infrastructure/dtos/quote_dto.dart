import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:kt_dart/kt.dart';
import 'package:motivation_flutter/features/quotes/domain/models/quotes_models.dart';

part 'quote_dto.g.dart';
part 'quote_dto.freezed.dart';

@freezed
class QuoteDto with _$QuoteDto {
  factory QuoteDto({
    @JsonKey(name: 'id') required int id,
    @JsonKey(name: 'quote') required String quote,
    @JsonKey(name: 'author_id') int? authorId,
    @JsonKey(name: 'category_ids') required List<int> categoryIds,
  }) = _QuoteDto;
  const QuoteDto._();
  factory QuoteDto.fromJson(Map<String, dynamic> json) =>
      _$QuoteDtoFromJson(json);

  QuoteEntity toEntity() {
    return QuoteEntity(
      id: id.toString(),
      quote: quote,
      categoryIds: categoryIds.map((e) => e.toString()).toList(),
      authorId: authorId.toString(),
    );
  }

  QuoteModel toDomain(AuthorModel author) {
    return QuoteModel(
      id: id.toString(),
      quote: quote,
      categoryIds: categoryIds.map((e) => e.toString()).toList().kt,
      author: author,
    );
  }
}
