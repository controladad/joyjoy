import 'package:dartz/dartz.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'account_info_model.freezed.dart';

@freezed
class AccountInfoModel with _$AccountInfoModel {
  const factory AccountInfoModel.loggedIn({
    required String userId,
    required String accountStatus,
    required Option<String> name,
    required DateTime signupDate,
    required String email,
    required String premiumPlan,
    required double credits,
  }) = LoggedInUser;
  const factory AccountInfoModel.guest({
    required Option<String> name,
  }) = GuestUser;

  const AccountInfoModel._();

  bool get isLoggedIn {
    return this is LoggedInUser;
  }
}

enum AccountStatus {
  Active,
  Deactivate,
}
