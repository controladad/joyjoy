import 'package:dartz/dartz.dart';
import 'package:motivation_flutter/core/failures.dart';
import 'package:motivation_flutter/features/force_update/domain/update_info_model.dart';

abstract class UpdateInfoRepository {
  Future<Either<GeneralFailure, UpdateInfoModel>> getUpdateInfo(String version);
}
