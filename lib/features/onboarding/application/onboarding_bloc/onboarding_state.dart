part of 'onboarding_bloc.dart';

@injectable
@freezed
class OnboardingState with _$OnboardingState {
  const factory OnboardingState({
    required Option<Either<GeneralFailure, OnboardingModel>> onboardingData,
    required Option<Either<GeneralFailure, OnboardingCacheDataModel>>
        onboardingCacheData,
    required Option<FeelingClass> selectedFeeling,
    required Option<String> wroteFeeling,
    required Option<String> userName,
    required Option<List<PaywallReviewModel>> reviews,
    required KtList<String> selectedFavoriteCategoriesIds,
    required KtList<String> selectedCharacterIds,
  }) = _OnboardingState;
  @factoryMethod
  factory OnboardingState.init() => OnboardingState(
        selectedCharacterIds: const KtList.empty(),
        selectedFavoriteCategoriesIds: const KtList.empty(),
        reviews: none(),
        selectedFeeling: none(),
        onboardingData: none(),
        onboardingCacheData: none(),
        wroteFeeling: none(),
        userName: none(),
      );
}
