import 'package:dartz/dartz.dart';
import 'package:kt_dart/kt.dart';
import 'package:motivation_flutter/features/profile/domain/models/account_info_model.dart';
import 'package:motivation_flutter/features/profile/domain/models/collection_model.dart';
import 'package:motivation_flutter/features/profile/domain/models/reminder_model.dart';
import 'package:motivation_flutter/features/profile/domain/models/user_subscription_model.dart';
import 'package:motivation_flutter/features/quotes/domain/models/saved_quote_union.dart';

import 'package:motivation_flutter/features/quotes/domain/models/quotes_models.dart';
import 'package:motivation_flutter/features/theme/domain/models/theme_model.dart';
import 'package:motivation_flutter/shared/domain/shared_cache_props_model.dart';
import 'package:motivation_flutter/shared/infrastructure/dtos/quotes_bundle_dto.dart';

abstract class ISharedDataSource {
  Stream<SharedCachePropsModel?> get cacheUpdated;

  Future<void> logout();
  Future<bool> get hasQuotesInCache;
  Future<void> deleteAllQuotes();

  Future<AccountInfoModel> getAccountInfo();
  Future<AccountInfoModel> updateAccountInfo(
    AccountInfoModel account,
  );

  Future<QuotesBundleDto> getAndAddNewBundle({
    required List<String> categories,
    required List<String> authors,
  });
  Future<KtList<QuoteModel>> getNewQuotesByFavorites();

  Future<void> deleteQuoteFromNotificationQuotes(String quoteId);
  Future<QuotesBundleDto> getAndAddNewBundleToNotifications({
    required List<String> categories,
    required int count,
  });
  Future<KtList<QuoteModel>> getNotificationQuotes({
    required KtList<String> categoryIds,
    required int count,
  });

  Future<QuoteModel> getRandomQuote();

  Future<KtList<QuoteModel>> getSavedQuotes(SavedQuoteType type);
  Future<KtList<QuoteModel>> getQuotesByCategoryOrAuthor({
    required Option<String> authorId,
    required Option<String> categoryId,
  });

  Future<KtList<String>> toggleFavoriteCategory(String categoryId);
  Future<KtList<String>> toggleFavoriteAuthor(String authorId);

  Future<void> updateFavorites({
    required Option<KtList<String>> authors,
    required Option<KtList<String>> categories,
  });

  Future<KtList<QuoteModel>> toggleSavedQuote({
    required SavedQuoteType type,
    required String quoteId,
  });

  Future<void> markQuotesAsSeen(List<String> quotesIds);
  Future<void> deleteQuote(String quoteId);

  Future<KtList<ThemeModel>> get getThemes;

  Future<ThemeModel> get getSelectedTheme;
  Future<ThemeModel> selectTheme(String themeId);
  Future<KtList<ThemeModel>> addOrUpdateThemes(
    ThemeModel theme,
  );

  Future<KtList<CollectionModel>> get getCollections;
  Future<KtList<CollectionModel>> addOrUpdateCollections(
    CollectionModel collection,
  );
  Future<KtList<CollectionModel>> removeCollection(
    String collectionId,
  );

  Future<KtList<ReminderModel>> get getReminders;
  Future<KtList<ReminderModel>> removeReminder(String reminderId);
  Future<KtList<ReminderModel>> addOrUpdateReminders(ReminderModel reminder);

  Future<UserSubscription> get getSubscription;

  Future<SharedCachePropsModel> get getCacheData;

  Future<void> syncCache();

  Future<void> userRewarded(double rewardValue);
  Future<void> updateChatCredit(double credit);

  Future<void> get userDidTryChat;
  Future<void> get userPassedOnboarding;
}
