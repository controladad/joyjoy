import 'package:motivation_flutter/features/language/domain/models/language_model.dart';
import 'package:hive/hive.dart';

part 'language_entity.g.dart';

@HiveType(typeId: 1)
class LanguageEntity {
  @HiveField(0)
  String? code;
  @HiveField(1)
  String? locale;
  @HiveField(2)
  String? language;

  LanguageEntity({
    this.code,
    this.locale,
    this.language,
  });

  LanguageEntity copyWith({
    int? id,
    String? code,
    String? locale,
    String? language,
  }) {
    return LanguageEntity(
      code: code ?? this.code,
      locale: locale ?? this.locale,
      language: language ?? this.language,
    );
  }

  factory LanguageEntity.fromDomain(
    LanguageModel languageModel,
  ) {
    return LanguageEntity(
      code: languageModel.code,
      language: languageModel.language,
      locale: languageModel.locale,
    );
  }

  LanguageModel toDomain() {
    return LanguageModel(
      code: code,
      language: language,
      locale: locale,
    );
  }

  @override
  String toString() =>
      'LanguageEntity(code: $code, locale: $locale, language: $language)';
}
