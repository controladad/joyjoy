import 'package:motivation_flutter/common/presentation/common_imports.dart';
import 'package:motivation_flutter/features/profile/presentation/profile_screens/profile_phone_view.dart';

class ProfileListItemWidget extends StatelessWidget {
  final ProfileItemModel item;
  const ProfileListItemWidget({required this.item});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: pad(0, 7, 0, 7),
      child: MaterialButton(
        height: 60.w,
        onPressed: () => item.onTap(),
        padding: EdgeInsets.zero,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15),
        ),
        child: SContainer(
          height: 63,
          maxWidth: 366,
          padding: pad(12, 9, 12, 9),
          decoration: BoxDecoration(
            border: Border.all(
              width: .15,
              color: AppTheme.outline_variant_dark,
            ),
            borderRadius: BorderRadius.circular(15),
          ),
          child: Row(
            children: [
              ImageWidget(
                image: item.icon,
                height: 38,
                width: 38,
              ),
              const SBox(width: 16),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: item.subTitle == null
                    ? MainAxisAlignment.center
                    : MainAxisAlignment.spaceBetween,
                children: [
                  SText(
                    item.label.toTitleCase(),
                    style: TStyle.TitleMedium,
                  ),
                  if (item.subTitle != null)
                    SText(
                      item.subTitle!.toTitleCase(),
                      style: TStyle.LabelSmall,
                    ),
                ],
              ),
              const Spacer(),
              const ImageWidget(
                image: AppIcons.rightChevron,
                color: AppTheme.white,
                height: 32,
                width: 32,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
