import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:kt_dart/kt.dart';
import 'package:motivation_flutter/core/failures.dart';
import 'package:motivation_flutter/features/profile/domain/models/collection_model.dart';

import 'package:motivation_flutter/features/profile/domain/profile_repository.dart';
import 'package:motivation_flutter/features/quotes/domain/models/quotes_model.dart';

part 'my_collection_bloc.freezed.dart';
part 'my_collection_event.dart';
part 'my_collection_state.dart';

@injectable
class MyCollectionBloc extends Bloc<MyCollectionEvent, MyCollectionState> {
  final IProfileRepository repository;
  late StreamSubscription<void>? cacheUpdatesSubscription;
  MyCollectionBloc(this.repository, MyCollectionState init) : super(init) {
    on<MyCollectionEvent>(_bloc);
    cacheUpdatesSubscription = repository.cacheUpdated.listen(
      (event) => add(
        const MyCollectionEvent.getCollections(),
      ),
    );
  }

  @override
  Future<void> close() {
    cacheUpdatesSubscription?.cancel();
    return super.close();
  }

  Future<void> _bloc(MyCollectionEvent event, Emitter emit) {
    return event.map(
      getCollections: (e) async {
        emit(
          state.copyWith(
            isLoading: true,
            data: none(),
            collections: emptyList(),
          ),
        );
        final result = await repository.getCollections;
        emit(state.copyWith(isLoading: false));

        result.fold(
          (l) => emit(state.copyWith(data: some(left(l)))),
          (r) => emit(
            state.copyWith(
              collections: r,
              data: some(right(r)),
            ),
          ),
        );
      },
      addNewCollection: (e) async {
        final newCollection = CollectionModel.empty(e.name);
        final collections =
            await repository.addOrUpdateCollection(newCollection);
        emit(state.copyWith(data: some(collections)));

        collections.fold(
          (l) {},
          (r) => emit(state.copyWith(collections: r)),
        );

        if (e.addToChangeList == true) {
          emit(
            state.copyWith(
              changeList: state.changeList.plusElement(newCollection.id),
            ),
          );
        }
      },
      removeCollection: (e) async {
        final collections = await repository.deleteCollection(e.id);
        emit(state.copyWith(data: some(collections)));

        collections.fold(
          (l) {},
          (r) => emit(state.copyWith(collections: r)),
        );
      },
      renameCollection: (e) async {
        final collection = state.collections
            .find((p0) => p0.id == e.collectionId)
            ?.copyWith(name: e.name);

        if (collection == null) throw Exception('collection not found');

        final collections = await repository.addOrUpdateCollection(collection);
        emit(state.copyWith(data: some(collections)));

        collections.fold(
          (l) {},
          (r) => emit(state.copyWith(collections: r)),
        );
      },
      toggleQuoteInChangeList: (e) async {
        emit(
          state.copyWith(
            changeList: state.changeList.contains(e.collectionId)
                ? state.changeList.minusElement(e.collectionId)
                : state.changeList.plusElement(e.collectionId),
          ),
        );
      },
      saveChanges: (e) async {
        for (final collectionId in state.changeList.asList()) {
          final result = await toggleQuoteInCollection(collectionId, e.quoteId);
          emit(state.copyWith(data: some(result)));
          result.fold((l) => null, (r) {
            emit(state.copyWith(collections: r));
          });
        }
        emit(state.copyWith(changeList: emptyList()));
      },
      cancelChanges: (e) async {
        emit(state.copyWith(changeList: emptyList()));
      },
    );
  }

  Future<Either<GeneralFailure, KtList<CollectionModel>>>
      toggleQuoteInCollection(String collectionId, String quoteId) async {
    CollectionModel? collection =
        state.collections.find((p0) => p0.id == collectionId);

    if (collection == null) {
      return left(
        const GeneralFailure.unexpected(message: 'collection not found'),
      );
    }

    collection = collection.copyWith(
      quotesIds: collection.quotesIds.contains(quoteId)
          ? collection.quotesIds.minusElement(quoteId)
          : collection.quotesIds.plusElement(quoteId),
    );

    final collections = await repository.addOrUpdateCollection(collection);
    return collections;
  }
}
