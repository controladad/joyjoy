import 'dart:io';

import 'package:dio/dio.dart';
import 'package:dio_smart_retry/dio_smart_retry.dart';
import 'package:motivation_flutter/core/failures.dart';

InterceptorsWrapper get apiErrorHandler {
  return InterceptorsWrapper(
    onError: (error, handler) {
      late GeneralFailure failure;

      if (error.response != null) {
        final statusCode = error.response!.statusCode!;

        if (statusCode == 401) {
          failure = const GeneralFailure.authentication();
        } else if (statusCode == 503) {
          failure = const GeneralFailure.underMaintenance();
        } else if (statusCode == 426) {
          failure = const GeneralFailure.forceUpdate('should be link');
        } else {
          failure = GeneralFailure.unexpected(message: error.message);
        }
      } else {
        failure = const GeneralFailure.noNetwork();
      }

      throw ApiException(failure);
    },
  );
}

RetryInterceptor retryInterceptor(Dio dio) {
  return RetryInterceptor(
    dio: dio,
    logPrint: print,
    retries: 0,
    retryDelays: [
      const Duration(seconds: 1),
      const Duration(seconds: 5),
    ],
  );
}

String? getStoreLinkIfForceUpdate(Response? response) {
  final data = response?.data as Map<String, dynamic>?;
  if (data == null) return null;

  final error = data['error'] as Map<String, dynamic>?;
  if (error == null) return null;

  final message = error['message'] as String?;
  if (message == null || message != 'Application update is available.') {
    return null;
  }

  final errorData = error[data] as Map<String, dynamic>;
  final storeLink =
      errorData[Platform.isIOS ? 'app_store_link' : 'google_play_link']
          as String;
  return storeLink;

  // if (((error.response?.data as Map<String, dynamic>)['error']
  // as Map<String, dynamic>)['message'] ==
  // 'Application update is available.') {
  // failure = GeneralFailure.forceUpdate()
  // }
}
