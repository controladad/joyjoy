import 'package:motivation_flutter/common/presentation/common_presentation.dart';

class SContainer extends StatelessWidget {
  final AlignmentGeometry? alignment;
  final AlignmentGeometry? transformAlignment;
  final Widget? child;
  final Clip clip;
  final Color? color;
  final BoxDecoration? decoration;
  final double? height;
  final double maxHeight;
  final double? width;
  final double maxWidth;
  final Decoration? foregroundDecoration;
  final EdgeInsetsGeometry? margin;
  final EdgeInsetsGeometry? padding;
  final Matrix4? transform;
  final Color? topperColor;

  const SContainer({
    super.key,
    this.alignment,
    this.transformAlignment,
    this.child,
    this.clip = Clip.none,
    this.color,
    this.decoration,
    this.width,
    this.height,
    this.foregroundDecoration,
    this.margin,
    this.padding,
    this.transform,
    this.maxHeight = double.infinity,
    this.maxWidth = double.infinity,
    this.topperColor,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: alignment,
      clipBehavior: clip,
      color: color,
      constraints: BoxConstraints(maxHeight: maxHeight.h, maxWidth: maxWidth.w),
      decoration: decoration,
      foregroundDecoration: foregroundDecoration,
      height: height?.h,
      width: width?.w,
      key: key,
      margin: margin,
      padding: padding,
      transform: transform,
      transformAlignment: transformAlignment,
      child: Container(
        height: height,
        width: width,
        decoration: BoxDecoration(
          borderRadius: decoration?.borderRadius,
          color: topperColor,
        ),
        child: child,
      ),
    );
  }
}

class SBox extends StatelessWidget {
  final Widget? child;
  final double? height;
  final double? width;
  final EdgeInsetsGeometry? padding;

  const SBox({
    super.key,
    this.child,
    this.height,
    this.width,
    this.padding,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: padding ?? pad(0, 0, 0, 0),
      child: SizedBox(
        height: height?.h,
        width: width?.w,
        key: key,
        child: child,
      ),
    );
  }
}

extension StringX on String {
  Widget widget({required TStyle style, Color? color, TextAlign? textAlign}) {
    return SText(
      this,
      style: style,
      color: color,
      textAlign: textAlign,
    );
  }
}

class SText extends StatelessWidget {
  final String text;
  final double? maxWidth;
  final Locale? locale;
  final int? maxLines;
  final bool scaleDown;
  final TextOverflow? overflow;
  final bool upscale;
  final TStyle style;
  final Color? color;
  final double? letterSpacing;
  final double? height;
  final TextAlign? textAlign;
  final TextDecoration? decoration;
  final Color? decorationColor;
  final String? fontFamily;
  final List<Shadow>? shadows;
  const SText(
    this.text, {
    required this.style,
    this.upscale = true,
    this.locale,
    this.shadows,
    super.key,
    this.maxLines,
    this.overflow,
    this.color,
    this.letterSpacing,
    this.textAlign,
    this.decoration,
    this.fontFamily,
    this.height,
    this.maxWidth,
    this.scaleDown = false,
    this.decorationColor,
  });

  @override
  Widget build(BuildContext context) {
    final widget = Text(
      text,
      locale: locale,
      maxLines: maxLines,
      overflow: overflow,
      textAlign: textAlign,
      style: getTextStyle(context: context, style: style)?.copyWith(
        color: color,
        shadows: shadows,
        letterSpacing: letterSpacing,
        decoration: decoration,
        fontFamily: fontFamily,
        height: height,
        decorationColor: decorationColor,
      ),
    );

    if (maxWidth != null) {
      return SBox(
        width: maxWidth,
        child: widget,
      );
    } else if (scaleDown) {
      return FittedBox(fit: BoxFit.scaleDown, child: widget);
    } else {
      return widget;
    }
  }
}
