import 'package:injectable/injectable.dart';

import 'package:motivation_flutter/features/feeds/infrastructure/dtos/feeds_dto.dart';

abstract class IFeedsApiDataSource {
  Future<FeedsDto> get getData;
}

@Injectable(as: IFeedsApiDataSource)
class FeedsApiDataSourceImplementation implements IFeedsApiDataSource {
  FeedsApiDataSourceImplementation();

  @override
  Future<FeedsDto> get getData async {
    throw UnimplementedError();
  }
}
