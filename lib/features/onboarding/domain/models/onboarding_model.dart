import 'package:dartz/dartz.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:kt_dart/kt.dart';
import 'package:motivation_flutter/features/profile/domain/models/account_info_model.dart';
import 'package:motivation_flutter/features/quotes/domain/models/author_overview_model.dart';
import 'package:motivation_flutter/features/quotes/domain/models/sub_category_model.dart';
import 'package:motivation_flutter/features/sub_auth/domain/models/review_model.dart';

part 'onboarding_model.freezed.dart';

@freezed
class OnboardingModel with _$OnboardingModel {
  const factory OnboardingModel({
    required String id,
    required KtList<SubCategoryModel> categories,
    required KtList<AuthorModel> authors,
    required KtList<PaywallReviewModel> reviews,
    required Option<AccountInfoModel> account,
  }) = _OnboardingModel;
}

@freezed
class OnboardingCacheDataModel with _$OnboardingCacheDataModel {
  const factory OnboardingCacheDataModel({
    required KtList<String> selectedFavoriteCategories,
    required KtList<String> selectedFavoriteAuthors,
    required Option<String> userName,
    required bool didTryChat,
    required bool shouldShowOnboarding,
    required bool loggedIn,
  }) = _OnboardingCacheDataModel;
}
