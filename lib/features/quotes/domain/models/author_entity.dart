import 'package:hive/hive.dart';
import 'package:motivation_flutter/features/quotes/domain/models/author_overview_model.dart';
import 'package:motivation_flutter/features/quotes/domain/models/sub_category_model.dart';

part 'author_entity.g.dart';

@HiveType(typeId: 4)
class AuthorEntity extends HiveObject {
  @HiveField(0)
  String id;
  @HiveField(1)
  String name;
  @HiveField(2)
  String shortDescription;
  @HiveField(3)
  int birthYear;
  @HiveField(4)
  int deathYear;
  @HiveField(5)
  String job;
  @HiveField(6)
  String country;
  @HiveField(7)
  String gender;
  @HiveField(8)
  int accessabilityStatus;

  AuthorEntity({
    required this.id,
    required this.name,
    required this.shortDescription,
    required this.birthYear,
    required this.deathYear,
    required this.job,
    required this.country,
    required this.gender,
    required this.accessabilityStatus,
  });

  AuthorEntity copyWith({
    String? id,
    String? name,
    String? shortDescription,
    int? birthYear,
    int? deathYear,
    String? job,
    String? country,
    String? gender,
    int? accessabilityStatus,
  }) {
    return AuthorEntity(
      id: id ?? this.id,
      name: name ?? this.name,
      shortDescription: shortDescription ?? this.shortDescription,
      birthYear: birthYear ?? this.birthYear,
      deathYear: deathYear ?? this.deathYear,
      job: job ?? this.job,
      country: country ?? this.country,
      gender: gender ?? this.gender,
      accessabilityStatus: accessabilityStatus ?? this.accessabilityStatus,
    );
  }

  factory AuthorEntity.fromDomain(AuthorModel model) {
    return AuthorEntity(
      id: model.id,
      name: model.name,
      shortDescription: model.shortDescription,
      birthYear: model.birthYear,
      deathYear: model.deathYear,
      job: model.job,
      country: model.country,
      gender: model.gender,
      accessabilityStatus: model.status.index,
    );
  }

  AuthorModel toDomain() {
    return AuthorModel(
      id: id,
      name: name,
      shortDescription: shortDescription,
      birthYear: birthYear,
      deathYear: deathYear,
      job: job,
      country: country,
      gender: gender,
      status: AccessabilityStatus.values[accessabilityStatus],
    );
  }
}
