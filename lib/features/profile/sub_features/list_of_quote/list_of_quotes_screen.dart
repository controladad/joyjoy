import 'package:flutter_share/flutter_share.dart';
import 'package:kt_dart/kt.dart';
import 'package:motivation_flutter/common/presentation/common_presentation.dart';
import 'package:motivation_flutter/common/presentation/utils/failure_handler_widget.dart';
import 'package:motivation_flutter/features/onboarding/presentation/components/profile_empty_state_widget.dart';
import 'package:motivation_flutter/features/profile/sub_features/list_of_quote/list_of_quote_bloc/list_of_quote_bloc.dart';
import 'package:motivation_flutter/features/profile/sub_features/my_collection/add_to_collection_bottom_sheets.dart';

import 'package:motivation_flutter/features/quotes/domain/models/quotes_models.dart';
import 'package:motivation_flutter/features/quotes/domain/models/saved_quote_union.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

@RoutePage()
class ListOfQuoteScreen extends StatelessWidget {
  final SavedQuoteType type;
  const ListOfQuoteScreen({super.key, required this.type});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) =>
          getIt<ListOfQuoteBloc>()..add(ListOfQuoteEvent.getFirstPage(type)),
      child: Builder(
        builder: (context) {
          final state = context.watch<ListOfQuoteBloc>().state;
          final bloc = context.read<ListOfQuoteBloc>();
          return CommonScaffoldWithAppbar(
            title: type
                .map(
                  liked: (value) => context.translate.likes,
                  collection: (value) => value.collection.name,
                  history: (value) => context.translate.history,
                )
                .capitalize(),
            floatingActionButtonLocation:
                FloatingActionButtonLocation.centerDocked,
            floatingActionButton: state.listOfQuoteResultsData.fold(
              () => null,
              (a) => a.fold(
                (l) => null,
                (r) {
                  if (r.isEmpty()) return null;
                  return SContainer(
                    width: double.infinity,
                    // topperColor: AppTheme.surface1_dark.withOpacity(.05),
                    decoration: BoxDecoration(
                      gradient: AppGradients.surface3Dark,
                      borderRadius: const BorderRadius.only(
                        topLeft: Radius.circular(25),
                        topRight: Radius.circular(25),
                      ),
                    ),
                    child: Padding(
                      padding: pad(16, 12, 16, 32),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          CustomTextField(
                            onChanged: (searchQuery) =>
                                bloc.add(ListOfQuoteEvent.search(searchQuery)),
                            label: context.translate.searchQuote,
                            focusedLabel: '',
                            hasClearButton: true,
                            leading: Padding(
                              padding: pad(16, 12, 10, 12),
                              child: const ImageWidget(
                                image: AppIcons.searchExplore,
                                height: 32,
                                width: 32,
                              ),
                            ),
                          ),
                          const CommonBottomInsetsWidget(),
                        ],
                      ),
                    ),
                  );
                },
              ),
            ),
            body: FailureHandlerWidget(
              data: state.listOfQuoteResultsData,
              onRetry: () => bloc.add(ListOfQuoteEvent.getFirstPage(type)),
              child: (r) => _Body(type),
            ),
          );
        },
      ),
    );
  }
}

class _Body extends StatelessWidget {
  final SavedQuoteType type;
  const _Body(this.type);

  @override
  Widget build(BuildContext context) {
    final bool showAddToCollection = type.map(
      liked: (_) => true,
      collection: (_) => false,
      history: (_) => false,
    );
    final state = context.watch<ListOfQuoteBloc>().state;
    return state.results.isEmpty()
        ? state.searchQuery.fold(
            () => ProfileEmptyState(
              title: context.translate.empty.capitalize(),
              subtitle: '',
            ),
            (a) => Column(
              children: [
                Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(16),
                      child: SText(
                        context.translate.noResult,
                        style: TStyle.BodyLarge,
                      ),
                    ),
                  ],
                )
              ],
            ),
          )
        : _RefreshWidget(
            child: Column(
              children: [
                Expanded(
                  child: ListView(
                    padding: pad(24, 4, 14, 4),
                    children: [
                      ...state.results
                          .map(
                            (quote) => _ListItemWidget(
                              quote: quote,
                              showAddToCollection: showAddToCollection,
                            ),
                          )
                          .asList(),
                      const SBox(height: 100),
                    ],
                  ),
                ),
                const CommonBottomInsetsWidget(),
              ],
            ),
          );
  }
}

class _ListItemWidget extends StatelessWidget {
  final QuoteModel quote;
  final bool showAddToCollection;
  const _ListItemWidget({
    required this.quote,
    required this.showAddToCollection,
  });

  @override
  Widget build(BuildContext context) {
    final bloc = context.read<ListOfQuoteBloc>();

    final popupItems = [
      if (showAddToCollection)
        _ClassPopupItemModel(
          name: context.translate.addToCollection,
          icon: AppIcons.addToCollection,
          onTap: () =>
              showAddToCollectionBottomSheet(context: context, quote: quote),
        ),
      _ClassPopupItemModel(
        name: context.translate.share,
        icon: AppIcons.share_white,
        onTap: () async {
          await FlutterShare.share(
            title: 'Share Quote',
            text: getShareQuoteText(quote),
            chooserTitle: 'Share Quote via',
          );
        },
      ),
      _ClassPopupItemModel(
        name: context.translate.remove,
        icon: AppIcons.trash,
        onTap: () => bloc.add(ListOfQuoteEvent.removeQuote(quote.id)),
      ),
    ];
    return SContainer(
      child: Row(
        children: [
          Expanded(
            child: Padding(
              padding: pad(0, 10, 0, 0),
              child: SText(
                quote.quote,
                style: TStyle.BodyLarge,
              ),
            ),
          ),
          const SBox(width: 10),
          PopupMenuButton(
            padding: EdgeInsets.zero,
            surfaceTintColor: Colors.transparent,
            color: Colors.transparent,
            shadowColor: Colors.transparent,
            elevation: 0,
            itemBuilder: (context) {
              return popupItems.map((e) {
                final bool isLast = e == popupItems.last;
                final bool isFirst = e == popupItems.first;

                BorderRadius? borderRadius;

                if (isFirst) {
                  borderRadius = const BorderRadius.only(
                    topRight: Radius.circular(12),
                    topLeft: Radius.circular(12),
                  );
                } else if (isLast) {
                  borderRadius = const BorderRadius.only(
                    bottomLeft: Radius.circular(12),
                    bottomRight: Radius.circular(12),
                  );
                }

                return PopupMenuItem(
                  enabled: false,
                  height: 45,
                  child: Container(
                    height: 45,
                    width: 250,
                    decoration: BoxDecoration(
                      gradient: AppGradients.surface3Dark,
                      borderRadius: borderRadius,
                    ),
                    child: GestureDetector(
                      onTap: () {
                        Navigator.pop(context);
                        e.onTap();
                      },
                      child: Container(
                        padding: pad(33, 0, 11, 0),
                        decoration: BoxDecoration(
                          borderRadius: borderRadius,
                          color: AppTheme.surface3_dark.withOpacity(.11),
                          border: !isLast && !isFirst
                              ? const Border(
                                  bottom: BorderSide(
                                    width: .5,
                                    color: AppTheme.outline_variant_dark,
                                  ),
                                  top: BorderSide(
                                    width: .5,
                                    color: AppTheme.outline_variant_dark,
                                  ),
                                )
                              : null,
                        ),
                        child: Row(
                          children: [
                            SText(
                              e.name.toTitleCase(),
                              style: TStyle.BodyMedium,
                            ),
                            const Spacer(),
                            ImageWidget(image: e.icon, height: 24, width: 24),
                          ],
                        ),
                      ),
                    ),
                  ),
                );
              }).toList();
            },
            icon: const ImageWidget(
              image: AppIcons.threeDot,
              height: 32,
              width: 32,
            ),
          ),
        ],
      ),
    );
  }
}

class _ClassPopupItemModel {
  final String name;
  final String icon;
  final Function() onTap;

  _ClassPopupItemModel({
    required this.name,
    required this.icon,
    required this.onTap,
  });
}

class _RefreshWidget extends StatelessWidget {
  final Widget child;
  const _RefreshWidget({required this.child});

  @override
  Widget build(BuildContext context) {
    final bloc = context.read<ListOfQuoteBloc>();
    final state = context.watch<ListOfQuoteBloc>().state;
    return SmartRefresher(
      controller: state.controller,
      onRefresh: () {
        bloc.add(const ListOfQuoteEvent.refresh());
      },
      enablePullDown:
          state.listOfQuoteResultsData.fold(() => false, (a) => true),
      child: child,
    );
  }
}
