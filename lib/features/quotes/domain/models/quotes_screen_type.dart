import 'package:freezed_annotation/freezed_annotation.dart';

part 'quotes_screen_type.freezed.dart';

@freezed
class QuotesScreenType with _$QuotesScreenType {
  const factory QuotesScreenType.feeds() = _Feeds;
  const factory QuotesScreenType.category(
    String categoryId,
    String categoryName,
  ) = _Category;
  const factory QuotesScreenType.author(String authorId, String authorName) =
      _Author;
}
