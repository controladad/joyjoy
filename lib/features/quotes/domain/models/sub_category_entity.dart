import 'package:hive/hive.dart';
import 'package:motivation_flutter/features/quotes/domain/models/sub_category_model.dart';

part 'sub_category_entity.g.dart';

@HiveType(typeId: 3)
class SubCategoryEntity extends HiveObject {
  @HiveField(0)
  String id;
  @HiveField(1)
  String name;
  @HiveField(2)
  String icon;
  @HiveField(3)
  String description;
  @HiveField(4)
  int status;
  @HiveField(5)
  List<String> tags;

  SubCategoryEntity({
    required this.id,
    required this.name,
    required this.icon,
    required this.status,
    required this.description,
    required this.tags,
  });

  SubCategoryEntity copyWith({
    String? id,
    String? name,
    String? icon,
    String? description,
    int? status,
    List<String>? tags,
  }) {
    return SubCategoryEntity(
      id: id ?? this.id,
      name: name ?? this.name,
      icon: icon ?? this.icon,
      status: status ?? this.status,
      description: description ?? this.description,
      tags: tags ?? this.tags,
    );
  }

  factory SubCategoryEntity.fromDomain(SubCategoryModel model) {
    return SubCategoryEntity(
      id: model.id,
      icon: model.icon,
      name: model.name,
      status: AccessabilityStatus.values.indexOf(model.status),
      description: model.description,
      tags: model.tags,
    );
  }

  SubCategoryModel toDomain() {
    return SubCategoryModel(
      id: id,
      icon: icon,
      name: name,
      status: AccessabilityStatus.values[status],
      description: description,
      tags: tags,
    );
  }
}
