part of 'chat_bloc.dart';

@injectable
@freezed
class ChatState with _$ChatState {
  const factory ChatState({
    required Option<Either<GeneralFailure, ChatModel>> data,
    required Option<Either<GeneralFailure, double>> creditCount,
    required KtList<MessageModel> messages,
    required bool isLoading,
    required Option<ChatStatus> chatStatus,
    required Option<Either<GeneralFailure, ChatNeededDataModel>> chatNeededData,
  }) = _ChatState;
  @factoryMethod
  factory ChatState.init() => ChatState(
        data: none(),
        creditCount: none(),
        messages: const KtList.empty(),
        isLoading: false,
        chatStatus: none(),
        chatNeededData: none(),
      );
}
