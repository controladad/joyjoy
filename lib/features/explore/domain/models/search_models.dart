import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:kt_dart/kt.dart';

import 'package:motivation_flutter/features/quotes/domain/models/quotes_models.dart';

part 'search_models.freezed.dart';

@freezed
class SearchModel with _$SearchModel {
  const factory SearchModel({
    required KtList<SearchResultUnion> results,
    required KtList<SuggestionModel> suggestions,
  }) = _SearchModel;
}

@freezed
class SearchResultUnion with _$SearchResultUnion {
  const factory SearchResultUnion.category(SubCategoryModel category) =
      CategorySearchUnionCase;
  const factory SearchResultUnion.author(AuthorModel author) =
      AuthorSearchUnionCase;
  const factory SearchResultUnion.quote(QuoteModel quote) =
      QuoteSearchUnionCase;
}

@freezed
class SuggestionModel with _$SuggestionModel {
  const factory SuggestionModel({
    required String name,
    required int count,
  }) = _SuggestionModel;
}
