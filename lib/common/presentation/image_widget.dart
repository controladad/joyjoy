import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:rive/rive.dart';

import 'package:motivation_flutter/common/presentation/common_imports.dart';

class ImageWidget extends StatelessWidget {
  final String? image;
  final String? placeholder;
  final BorderRadius borderRadius;
  final double? height;
  final double? width;
  final double maxHeight;
  final double maxWidth;
  final BoxFit fit;
  final Color? color;
  final Color placeholderColor;
  final Color placeholderBackgroundColor;
  final bool grayScale;
  final int quarterTurns;

  const ImageWidget({
    super.key,
    required this.image,
    required this.height,
    required this.width,
    this.placeholder,
    this.borderRadius = BorderRadius.zero,
    this.color = Colors.black,
    this.placeholderColor = Colors.white,
    this.placeholderBackgroundColor = Colors.black,
    this.grayScale = false,
    this.fit = BoxFit.scaleDown,
    this.maxHeight = double.infinity,
    this.maxWidth = double.infinity,
    this.quarterTurns = 0,
  });

  @override
  Widget build(BuildContext context) {
    return SContainer(
      height: height,
      width: width,
      maxHeight: maxHeight,
      maxWidth: maxWidth,
      child: RotatedBox(
        quarterTurns: quarterTurns,
        child: ClipRRect(
          borderRadius: borderRadius,
          child: ColorFiltered(
            colorFilter: grayScale
                ? const ColorFilter.mode(Colors.grey, BlendMode.saturation)
                : const ColorFilter.mode(
                    Colors.transparent,
                    BlendMode.multiply,
                  ),
            child: _imageWidget,
          ),
        ),
      ),
    );
  }

  Widget get _imageWidget {
    switch (type) {
      case _ImageOrAnimationType.Asset:
        {
          switch (format(image!)) {
            case _ImageOrAnimationFormat.Svg:
              return SvgPicture.asset(
                image!,
                placeholderBuilder: (_) => placeholderWidget,
                fit: fit,
                colorFilter: color != null
                    ? ColorFilter.mode(color!, BlendMode.srcIn)
                    : null,
              );
            case _ImageOrAnimationFormat.Riv:
              return RiveAnimation.asset(
                image!,
                placeHolder: placeholderWidget,
                fit: fit,
              );
            case _ImageOrAnimationFormat.Other:
              return Image.asset(
                image!,
                errorBuilder: (_, __, ___) => placeholderWidget,
                fit: fit,
              );
          }
        }

      case _ImageOrAnimationType.Network:
        {
          switch (format(image!)) {
            case _ImageOrAnimationFormat.Svg:
              return SvgPicture.network(
                image!,
                placeholderBuilder: (_) => placeholderWidget,
                fit: fit,
                colorFilter: color != null
                    ? ColorFilter.mode(color!, BlendMode.srcIn)
                    : null,
              );
            case _ImageOrAnimationFormat.Riv:
              return RiveAnimation.network(
                image!,
                placeHolder: placeholderWidget,
                fit: fit,
              );
            case _ImageOrAnimationFormat.Other:
              return CachedNetworkImage(
                imageUrl: image!,
                httpHeaders: const {'keep-Alive': 'timeout=30'},
                errorWidget: (context, url, error) => placeholderWidget,
                placeholder: (context, url) => placeholderWidget,
                fit: fit,
                height: height,
                width: width,
              );
          }
        }

      case _ImageOrAnimationType.File:
        {
          switch (format(image!)) {
            case _ImageOrAnimationFormat.Svg:
              return SvgPicture.file(
                File(image!),
                placeholderBuilder: (_) => placeholderWidget,
                fit: fit,
                colorFilter: color != null
                    ? ColorFilter.mode(color!, BlendMode.srcIn)
                    : null,
              );
            case _ImageOrAnimationFormat.Riv:
              return RiveAnimation.file(
                image!,
                placeHolder: placeholderWidget,
                fit: fit,
              );
            case _ImageOrAnimationFormat.Other:
              return Image.file(
                File(image!),
                errorBuilder: (_, __, ___) => placeholderWidget,
                fit: fit,
              );
          }
        }
      case _ImageOrAnimationType.InvalidType:
        return placeholderWidget;
    }
  }

  Widget get placeholderWidget {
    if (placeholder == null) return Container();
    late Widget widget;
    switch (format(placeholder!)) {
      case _ImageOrAnimationFormat.Svg:
        widget = ColoredBox(
          color: AppTheme.primary_dark,
          child: SvgPicture.asset(
            placeholder!,
            colorFilter: ColorFilter.mode(placeholderColor, BlendMode.srcIn),
            fit: fit,
            height: height,
            width: width,
          ),
        );
      case _ImageOrAnimationFormat.Riv:
        widget = RiveAnimation.asset(placeholder!, fit: fit);
      case _ImageOrAnimationFormat.Other:
        widget = Image.asset(
          placeholder!,
          fit: fit,
          width: width,
          height: height,
        );
    }
    return ColoredBox(
      color: placeholderBackgroundColor,
      child: widget,
    );
  }

  _ImageOrAnimationFormat format(String image) {
    if (image.contains('.svg')) {
      return _ImageOrAnimationFormat.Svg;
    } else if (image.contains('.riv')) {
      return _ImageOrAnimationFormat.Riv;
    } else {
      return _ImageOrAnimationFormat.Other;
    }
  }

  _ImageOrAnimationType get type {
    if (image?.contains('assets') ?? false) {
      return _ImageOrAnimationType.Asset;
    } else if (image?.contains('cache') ?? false) {
      return _ImageOrAnimationType.File;
    } else if (image?.startsWith('http') ?? false) {
      return _ImageOrAnimationType.Network;
    } else {
      return _ImageOrAnimationType.InvalidType;
    }
  }
}

enum _ImageOrAnimationType {
  Asset,
  Network,
  File,
  InvalidType,
}

enum _ImageOrAnimationFormat {
  Svg,
  Riv,
  Other,
}
