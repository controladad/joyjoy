// import 'package:motivation_flutter/common/presentation/common_presentation.dart';

// Future<void> showCustomModalBottomSheet({
//   required BuildContext context,
//   String? label,
//   required Widget? child,
//   Color? labelColor,
//   String? actionIcon,
//   required Function() onClose,
//   bool canDismiss = true,
//   bool? hasDivider,
// }) async {
//   await showModalBottomSheet(
//     elevation: 0,
//     isScrollControlled: true,
//     isDismissible: canDismiss,
//     backgroundColor: Colors.transparent,
//     context: context,
//     builder: (context) {
//       return SContainer(
//         width: double.infinity,
//         decoration: const BoxDecoration(
//           borderRadius: BorderRadius.vertical(top: Radius.circular(25)),
//           color: AppTheme.white,
//         ),
//         child: Column(
//           mainAxisSize: MainAxisSize.min,
//           children: [
//             const SBox(height: 11, width: double.infinity),
//             SContainer(
//               width: 50,
//               height: 5,
//               decoration: BoxDecoration(
//                 color: AppTheme.gray200,
//                 borderRadius: BorderRadius.circular(10),
//               ),
//             ),
//             const SBox(height: 16),
//             SText(
//               label ?? '',
//               style: TStyle.subtitle2,
//               color: labelColor ?? AppTheme.gray700,
//             ),
//             const SBox(height: 16),
//             if (hasDivider != false) const CommonDivider(),
//             SContainer(
//               width: double.infinity,
//               child: child,
//             ),
//           ],
//         ),
//       );
//     },
//   ).whenComplete(() {
//     onClose();
//   });
// }

// Future<void> showCustomModalBottomSheetNaked({
//   required BuildContext context,
//   required Widget? child,
//   double? letterSpacing,
//   String? label,
//   Color? labelColor,
//   String? actionIcon,
//   required Function() onClose,
//   bool? canDismiss,
// }) async {
//   await showModalBottomSheet(
//     elevation: 0,
//     isScrollControlled: true,
//     isDismissible: true,
//     backgroundColor: Colors.transparent,
//     context: context,
//     builder: (context) {
//       return SContainer(
//         child: child,
//       );
//     },
//   ).whenComplete(() {
//     onClose();
//   });
// }
