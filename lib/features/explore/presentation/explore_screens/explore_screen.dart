import 'package:auto_route/auto_route.dart';
import 'package:flutter/widgets.dart';
import 'package:motivation_flutter/utils/view/layout_builder.dart';
import 'package:motivation_flutter/features/explore/presentation/explore_screens/explore_phone_view.dart';

@RoutePage()
class ExploreScreen extends StatelessWidget {
  const ExploreScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return ResponsiveLayoutBuilder(
      phone: (context, child) {
        return const ExplorePhoneView();
      },
    );
  }
}
