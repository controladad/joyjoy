part of 'full_search_bloc.dart';

@freezed
class FullSearchEvent with _$FullSearchEvent {
  const factory FullSearchEvent.getFirstPage({
    required String query,
    required SearchResultUnion firstItem,
  }) = _FullSearch;
  const factory FullSearchEvent.lazyLoad() = _LazyLoad;
  const factory FullSearchEvent.refresh() = _Refresh;
}
