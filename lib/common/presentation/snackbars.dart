import 'package:flash/flash.dart';
import 'package:motivation_flutter/common/presentation/common_presentation.dart';
import 'package:motivation_flutter/core/failures.dart';

Future<void> showSnackbar(
  BuildContext context,
  String text, {
  double paddingFromBottom = 10,
  double? width = double.infinity,
  Color color = const Color(0xffE9E0E4),
  Color textColor = Colors.black,
}) async {
  ScaffoldMessenger.of(context).removeCurrentSnackBar();
  await showFlash(
    context: context,
    duration: const Duration(seconds: 2),
    builder: (context, controller) {
      return Container(
        alignment: Alignment.bottomCenter,
        child: Container(
          margin: pad(10, 10, 10, paddingFromBottom),
          padding: pad(16, 19, 16, 19),
          width: width,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: color,
          ),
          child: SText(
            text,
            style: TStyle.BodyMedium,
            color: textColor,
          ),
        ),
      );
    },
  );
}

Future<void> showFailureSnackBar(
  BuildContext context,
  GeneralFailure failure, {
  Function()? onRetry,
}) async {
  ScaffoldMessenger.of(context).removeCurrentSnackBar();
  await showFlash(
    context: context,
    duration: const Duration(seconds: 2),
    builder: (context, controller) {
      return Container(
        alignment: Alignment.bottomCenter,
        child: Container(
          margin: pad(10, 10, 10, 10),
          padding: pad(16, 19, 16, 19),
          width: double.infinity,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: const Color(0xffE9E0E4),
          ),
          child: Row(
            children: [
              Flexible(
                child: SText(
                  getFailureDescription(context, failure).toTitleCase(),
                  style: TStyle.BodyMedium,
                  color: Colors.black,
                ),
              ),
              Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  if (onRetry != null)
                    GestureDetector(
                      onTap: onRetry,
                      child: SText(
                        context.translate.retry.toUpperCase(),
                        style: TStyle.LabelButton,
                        color: const Color(0xff8E3D97),
                      ),
                    )
                ],
              )
            ],
          ),
        ),
      );
    },
  );
}

String getFailureDescription(BuildContext context, GeneralFailure failure) {
  return failure.map(
    noNetwork: (e) => context.translate.internetConnectionError,
    unexpected: (e) => context.translate.someThingIsWrong,
    authentication: (e) => context.translate.authenticationError,
    underMaintenance: (e) => context.translate.underMaintenance,
    localException: (e) => context.translate.someThingIsWrong,
    forceUpdate: (e) => 'Update Need',
  );
}
