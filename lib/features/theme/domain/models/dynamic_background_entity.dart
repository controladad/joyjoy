import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'dart:typed_data';

import 'package:motivation_flutter/features/theme/domain/models/dynamic_background_union.dart';

part 'dynamic_background_entity.g.dart';

@HiveType(typeId: 14)
class DynamicBackgroundEntity extends HiveObject {
  @HiveField(0)
  int? color;
  @HiveField(1)
  List<int>? gradient;
  @HiveField(2)
  List<int>? image;

  DynamicBackgroundEntity({
    this.color,
    this.gradient,
    this.image,
  }) : assert(
          (color != null && gradient == null && image == null) ||
              (gradient != null && color == null && image == null) ||
              (image != null && color == null && gradient == null),
          'Only one of color, gradient, or image should be defined.',
        );

  DynamicBackgroundEntity copyWith({
    int? color,
    List<int>? gradient,
    List<int>? image,
  }) {
    return DynamicBackgroundEntity(
      color: color ?? this.color,
      gradient: gradient ?? this.gradient,
      image: image ?? this.image,
    );
  }

  factory DynamicBackgroundEntity.fromDomain(DynamicBackgroundUnion model) {
    return model.map(
      color: (e) => DynamicBackgroundEntity(color: e.color.value),
      gradient: (e) => DynamicBackgroundEntity(
        gradient: e.colors.map((e) => e.value).toList(),
      ),
      image: (e) => DynamicBackgroundEntity(image: e.image),
    );
  }

  DynamicBackgroundUnion toDomain() {
    if (color != null) {
      return DynamicBackgroundUnion.color(Color(color!));
    } else if (gradient != null) {
      return DynamicBackgroundUnion.gradient(
        gradient!.map((e) => Color(e)).toList(),
      );
    } else if (image != null) {
      return DynamicBackgroundUnion.image(Uint8List.fromList(image!));
    } else {
      throw Exception();
    }
  }
}
