import 'package:freezed_annotation/freezed_annotation.dart';

import 'package:motivation_flutter/features/sub_auth/domain/models/sub_auth_models.dart';

part 'sub_auth_dto.freezed.dart';
part 'sub_auth_dto.g.dart';

@freezed
class SubAuthDto with _$SubAuthDto {
  factory SubAuthDto({
    @JsonKey(name: 'id') required String id,
  }) = _SubAuthDto;

  factory SubAuthDto.fromJson(
    Map<String, dynamic> json,
  ) =>
      _$SubAuthDtoFromJson(json);
  const SubAuthDto._();

  SubAuthModel toDomain() => const SubAuthModel(
        chatStatus: QuoteStatus.Ad,
        hasSubscription: false,
        hasTokenToChat: false,
        isAuth: true,
        quoteStatus: QuoteStatus.Ad,
      );
}
