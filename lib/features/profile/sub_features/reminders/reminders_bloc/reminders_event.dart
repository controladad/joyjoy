part of 'reminders_bloc.dart';

@freezed
class RemindersEvent with _$RemindersEvent {
  const factory RemindersEvent.getReminders() = _GetReminders;
  const factory RemindersEvent.addNewReminder(ReminderModel reminder) =
      _AddNewReminder;
  const factory RemindersEvent.editReminder(ReminderModel reminder) =
      _EditReminder;
  const factory RemindersEvent.toggleReminder(ReminderModel reminder) =
      _ToggleReminder;
  const factory RemindersEvent.removeReminder(String id) = _RemoveReminder;
}
