import 'package:motivation_flutter/common/presentation/buttons.dart';
import 'package:motivation_flutter/common/presentation/common_imports.dart';
import 'package:motivation_flutter/common/presentation/utils/custom_bottom_sheets.dart';
import 'package:motivation_flutter/features/chat/application/chat_bloc/chat_bloc.dart';
import 'package:motivation_flutter/features/chat/domain/chat_status.dart';
import 'package:motivation_flutter/features/chat/presentation/components/chat_body.dart';

class ChatBottomWidget extends StatefulWidget {
  final TextEditingController controller;

  const ChatBottomWidget({super.key, required this.controller});

  @override
  State<ChatBottomWidget> createState() => _ChatBottomWidgetState();
}

class _ChatBottomWidgetState extends State<ChatBottomWidget> {
  String? message;

  @override
  void initState() {
    super.initState();
    widget.controller.addListener(() {
      setState(() {
        message = widget.controller.text;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    final bloc = context.read<ChatBloc>();
    final state = context.watch<ChatBloc>().state;
    final int creditCount = state.creditCount
        .fold(() => 0, (a) => a.fold((l) => 0, (r) => r.round()));
    print(state.creditCount);
    void submitMessage() {
      if (creditCount == 0) {
        showCustomBottomSheet(
          context: context,
          onClose: () {},
          child: const RunOutCreditBottomSheet(),
        );
      } else {
        if (message?.isNotEmpty ?? false) {
          bloc.add(ChatEvent.sendMessage(message ?? ''));
          widget.controller.text = '';
        }
      }
    }

    return Padding(
      padding: pad(16, 0, 16, 16),
      child: state.chatStatus.fold(
        () => CustomTextField(
          controller: widget.controller,
          isEnable: false,
          maxWidth: double.infinity,
          onChanged: (value) {},
          icon: Padding(
            padding: const EdgeInsets.all(4),
            child: CustomFilledButton(
              label: context.translate.send,
              subtitle: '($creditCount)',
              onTap: () {},
              isEnable: false,
              maxWidth: 73,
              width: 73,
            ),
          ),
        ),
        (nextMessageStatus) => nextMessageStatus.maybeMap(
          limitReached: (e) => CustomFilledButton(
            label: 'Limit Reached',
            onTap: () => context.popRoute(),
          ),
          subscriptionEnd: (e) => CustomFilledButton(
            label: 'Your subscription Finished',
            onTap: () => context.popRoute(),
          ),
          orElse: () {
            final remainingCharacters = nextMessageStatus is LimitedChat
                ? nextMessageStatus.remainingCharacters
                : null;

            return CustomTextField(
              onSubmit: (value) => submitMessage(),
              controller: widget.controller,
              maxLength: remainingCharacters,
              maxWidth: double.infinity,
              onChanged: (value) {
                setState(() {
                  message = value;
                });
              },
              icon: Padding(
                padding: const EdgeInsets.all(2.0),
                child: CustomFilledButton(
                  label: context.translate.send,
                  padding: const EdgeInsets.all(2),
                  onTap: submitMessage,
                  subtitle: '($creditCount)',
                  isEnable: message?.isNotEmpty ?? false,
                  maxWidth: 73,
                  width: 73,
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
