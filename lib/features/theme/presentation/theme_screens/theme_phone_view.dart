import 'dart:math';

import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:kt_dart/kt.dart';
import 'package:motivation_flutter/common/application/ad_cubit.dart';
import 'package:motivation_flutter/common/presentation/buttons.dart';
import 'package:motivation_flutter/common/presentation/common_imports.dart';
import 'package:motivation_flutter/common/presentation/utils/failure_handler_widget.dart';
import 'package:motivation_flutter/features/quotes/presentation/dynamic_background.dart';
import 'package:motivation_flutter/features/theme/application/theme_bloc/theme_bloc.dart';
import 'package:motivation_flutter/features/theme/domain/models/theme_model.dart';

class ThemePhoneView extends StatelessWidget {
  const ThemePhoneView({super.key});

  @override
  Widget build(BuildContext context) {
    final state = context.watch<ThemeBloc>().state;
    final bloc = context.read<ThemeBloc>();
    return CommonBackgroundContainer(
      child: Scaffold(
        backgroundColor: Colors.transparent,
        appBar: AppBar(
          surfaceTintColor: Colors.transparent,
          backgroundColor: Colors.transparent,
          title: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SText(
                context.translate.theme.capitalize(),
                style: TStyle.TitleLarge,
              ),
              SText(
                context.translate.personalizeFeedsToSuitYourPreferences
                    .capitalize(),
                style: TStyle.TitleSmall,
              ),
            ],
          ),
        ),
        body: FailureHandlerWidget<KtList<ThemeModel>>(
          data: state.themeData,
          onRetry: () => bloc.add(const ThemeEvent.getThemes()),
          child: (r) => _Body(r),
        ),
      ),
    );
  }
}

class _Body extends StatelessWidget {
  final KtList<ThemeModel> themes;
  const _Body(this.themes);

  @override
  Widget build(BuildContext context) {
    final Map<String, KtList<ThemeModel>> themesByTag = {};
    for (final theme in themes.asList()) {
      themesByTag[theme.tag] =
          themesByTag[theme.tag]?.plusElement(theme) ?? [theme].kt;
    }

    final state = context.watch<ThemeBloc>().state;
    final bloc = context.read<ThemeBloc>();

    return GridView.extent(
      childAspectRatio: .55,
      maxCrossAxisExtent: 220.w,
      padding: pad(19, 22, 19, 22),
      crossAxisSpacing: 12,
      mainAxisSpacing: 24,
      children: themesByTag.entries.first.value
          .map(
            (theme) => _ThemeItemWidget(
              theme: theme,
              isSelected:
                  state.selectedTheme.toNullable()?.themeId == theme.themeId,
              onTap: () => showDialog(
                context: context,
                builder: (context) => Dialog(
                  child: _ChangeThemePreview(
                    theme,
                    () => bloc.add(ThemeEvent.selectTheme(theme.themeId)),
                  ),
                ),
              ),
            ),
          )
          .asList(),
    );
  }
}

class _ThemeItemWidget extends StatelessWidget {
  final ThemeModel theme;
  final bool isSelected;
  final Function() onTap;
  const _ThemeItemWidget({
    required this.theme,
    required this.isSelected,
    required this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    final BorderRadius borderRadius = BorderRadius.circular(30);
    return GestureDetector(
      onTap: onTap,
      child: SContainer(
        width: 170,
        height: 290,
        foregroundDecoration: BoxDecoration(
          color: AppTheme.primary_80.withOpacity(.14),
          borderRadius: borderRadius,
        ),
        padding: pad(16, 16, 0, 0),
        decoration: BoxDecoration(
          borderRadius: borderRadius,
          border: Border.all(
            color: isSelected ? AppTheme.primary_dark : Colors.transparent,
          ),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            SBox(
              height: 210,
              child: Row(
                children: [
                  Column(
                    children: theme.backGrounds
                        .map(
                          (p0) => Padding(
                            padding: pad(0, 0, 0, 4),
                            child: DynamicBackgroundWidget(
                              background: p0,
                              height: 18,
                              width: 18,
                              radius: 4,
                            ),
                          ),
                        )
                        .asList(),
                  ),
                  const Spacer(),
                  SizedBox(
                    width: 120,
                    child: ClipRect(
                      child: Stack(
                        alignment: Alignment.topRight,
                        children: theme.backGrounds
                            .mapIndexed(
                              (index, background) {
                                final rotation = (index - 8) * (pi / 180) * 2;
                                return Transform.translate(
                                  offset: Offset(20 + index * 13, index * -6),
                                  child: Transform.rotate(
                                    angle: rotation,
                                    child: Center(
                                      child: DynamicBackgroundWidget(
                                        background: background,
                                        height: 123,
                                        width: 113,
                                        radius: 2,
                                        child: Center(
                                          child: Transform.rotate(
                                            angle: -rotation,
                                            child: Text(
                                              'ABCD',
                                              style: TextStyle(
                                                fontFamily: theme.fontFamily,
                                                color: theme.textColor,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                );
                              },
                            )
                            .reversed()
                            .asList(),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              height: 40,
              margin: pad(0, 0, 16, 16),
              padding: pad(20, 0, 20, 0),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(14),
                color: AppTheme.primary_dark.withOpacity(.08),
              ),
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  AnimatedCrossFade(
                    firstChild: const SBox(width: 0),
                    secondChild: Padding(
                      padding: pad(0, 0, 12, 0),
                      child: const ImageWidget(
                        image: AppIcons.pinkCheck,
                        height: 24,
                        width: 24,
                      ),
                    ),
                    crossFadeState: isSelected
                        ? CrossFadeState.showSecond
                        : CrossFadeState.showFirst,
                    duration: const Duration(milliseconds: 150),
                  ),
                  Flexible(
                    child: SText(
                      theme.name.capitalize(),
                      style: TStyle.BodyMedium,
                      scaleDown: true,
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class _ChangeThemePreview extends HookWidget {
  final ThemeModel theme;
  final Function() onAgree;
  const _ChangeThemePreview(this.theme, this.onAgree);

  @override
  Widget build(BuildContext context) {
    final hasSeenAd = useState(false);

    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(28),
        gradient: AppGradients.surface3Dark,
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          const SBox(height: 32),
          SText(theme.name.toTitleCase(), style: TStyle.TitleLarge),
          const SBox(height: 60),
          Stack(
            alignment: Alignment.topRight,
            children: theme.backGrounds
                .mapIndexed(
                  (index, background) {
                    final rotation = (index - 8) * (pi / 180) * 2;
                    return Transform.translate(
                      offset: Offset(-40 + index * 13, index * -6),
                      child: Transform.rotate(
                        angle: rotation,
                        child: Center(
                          child: DynamicBackgroundWidget(
                            background: background,
                            height: 123,
                            width: 113,
                            radius: 2,
                            child: Center(
                              child: Transform.rotate(
                                angle: -rotation,
                                child: Text(
                                  'ABCD',
                                  style: TextStyle(
                                    fontFamily: theme.fontFamily,
                                    color: theme.textColor,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    );
                  },
                )
                .reversed()
                .asList(),
          ),
          const SBox(height: 50),
          Padding(
            padding: pad(24, 0, 24, 24),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                TextButton(
                  child: SText(
                    context.translate.close.toUpperCase(),
                    style: TStyle.LabelButton,
                    color: AppTheme.secondary_dark,
                  ),
                  onPressed: () {
                    context.popRoute();
                  },
                ),
                SBox(
                  width: 155,
                  child: CustomOutlinedButton(
                    label: context.translate.set_theme,
                    leftIcon: hasSeenAd.value
                        ? null
                        : const ImageWidget(
                            fit: BoxFit.fitWidth,
                            image: AppIcons.bigAd,
                            height: 17,
                            width: 22,
                          ),
                    onTap: () async {
                      if (hasSeenAd.value) {
                        onAgree();
                        context.popRoute();
                        hasSeenAd.value = false;
                      } else {
                        final result =
                            await context.read<AdCubit>().showRewardedAd(false);
                        hasSeenAd.value = result;
                      }
                    },
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
