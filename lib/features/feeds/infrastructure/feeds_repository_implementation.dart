import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:motivation_flutter/core/failures.dart';

import 'package:motivation_flutter/core/network/network_info.dart';

import 'package:motivation_flutter/features/feeds/infrastructure/dtos/feeds_dto.dart';
import 'package:motivation_flutter/features/feeds/infrastructure/feeds_data_source.dart';
import 'package:motivation_flutter/features/feeds/domain/feeds_repository.dart';
import 'package:motivation_flutter/features/feeds/domain/models/feeds_models.dart';

@Injectable(as: IFeedsRepository)
class FeedsRepositoryImplementation implements IFeedsRepository {
  final INetworkInfo _networkInfo;
  final IFeedsApiDataSource _dataSource;

  FeedsRepositoryImplementation(this._networkInfo, this._dataSource);

  @override
  Future<Either<GeneralFailure, FeedsModel>> get getData async {
    if (await _networkInfo.isConnected == false) {
      return left(const GeneralFailure.noNetwork());
    }
    try {
      final FeedsDto dto = await _dataSource.getData;
      final FeedsModel result = dto.toDomain();

      return right(result);
    } catch (e) {
      return left(const GeneralFailure.unexpected());
    }
  }
}
