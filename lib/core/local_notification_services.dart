import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:injectable/injectable.dart';
import 'package:kt_dart/kt.dart';
import 'package:timezone/timezone.dart' as tz;
import 'package:timezone/data/latest_all.dart' as tz;
// import 'package:flutter_native_timezone/flutter_native_timezone.dart' as nt;

@singleton
class LocalNotificationService {
  final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      FlutterLocalNotificationsPlugin();
  AndroidNotificationChannel channel = const AndroidNotificationChannel(
    'MCH001', // id
    'JoyJoy Notifications', // title
    importance: Importance.high,
  );

  Future initialize() async {
    await flutterLocalNotificationsPlugin.initialize(
      const InitializationSettings(
        android: AndroidInitializationSettings('background'),
        iOS: DarwinInitializationSettings(),
      ),
    );

    await flutterLocalNotificationsPlugin
        .resolvePlatformSpecificImplementation<
            AndroidFlutterLocalNotificationsPlugin>()
        ?.createNotificationChannel(channel);
    tz.initializeTimeZones();

    // final String currentTimezone =
    //     await nt.FlutterNativeTimezone.getLocalTimezone();
    // tz.setLocalLocation(tz.getLocation(currentTimezone));
  }

  Future<bool> deleteAllPendingNotifications() async {
    try {
      await flutterLocalNotificationsPlugin.cancelAll();
      return true;
    } catch (e) {
      return false;
    }
  }

  Future<int> addGroupOfNotifications(
    KtList<CustomNotification> notifications,
  ) async {
    for (final notification in notifications.asList()) {
      await addScheduledNotification(
        id: notification.id,
        scheduleDate: notification.scheduleDate,
        body: notification.body,
        title: notification.title,
      );
    }
    final totalNotifications =
        await flutterLocalNotificationsPlugin.pendingNotificationRequests();
    return totalNotifications.length;
  }

  Future<int> get pendingCount async {
    final result =
        await flutterLocalNotificationsPlugin.pendingNotificationRequests();
    return result.length;
  }

  Future<bool> addScheduledNotification({
    required int id,
    String? title,
    String? body,
    required DateTime scheduleDate,
  }) async {
    try {
      await flutterLocalNotificationsPlugin.zonedSchedule(
        id,
        title,
        body,
        tz.TZDateTime(
          tz.local,
          scheduleDate.year,
          scheduleDate.month,
          scheduleDate.day,
          scheduleDate.hour,
          scheduleDate.minute,
          scheduleDate.second,
        ),
        NotificationDetails(
          android: AndroidNotificationDetails(channel.id, channel.name),
          iOS: const DarwinNotificationDetails(),
        ),
        uiLocalNotificationDateInterpretation:
            UILocalNotificationDateInterpretation.absoluteTime,
        androidScheduleMode: AndroidScheduleMode.exactAllowWhileIdle,
      );
      return true;
    } catch (e) {
      return false;
    }
  }
}

class CustomNotification {
  final int id;
  final String title;
  final String body;
  final DateTime scheduleDate;

  CustomNotification({
    required this.id,
    required this.title,
    required this.body,
    required this.scheduleDate,
  });
}
