import 'package:kt_dart/collection.dart';
import 'package:motivation_flutter/common/presentation/common_imports.dart';
import 'package:motivation_flutter/common/presentation/utils/failure_handler_widget.dart';
import 'package:motivation_flutter/features/quotes/application/quotes_bloc/quotes_bloc.dart';
import 'package:motivation_flutter/features/quotes/application/watch_ads_data/watch_ads_data_bloc.dart';
import 'package:motivation_flutter/features/quotes/domain/models/quotes_screen_type.dart';
import 'package:motivation_flutter/features/quotes/presentation/quote_card_item_widget.dart';

class QuotesWidget extends StatelessWidget {
  final QuotesScreenType type;
  final QuotesBloc quotesBloc;
  const QuotesWidget({required this.type, required this.quotesBloc});

  @override
  Widget build(BuildContext context) {
    return BlocProvider.value(
      value: quotesBloc,
      child: const _QuotesWidget(),
    );
  }
}

class _QuotesWidget extends StatefulWidget {
  const _QuotesWidget();

  @override
  State<_QuotesWidget> createState() => __QuotesWidgetState();
}

class __QuotesWidgetState extends State<_QuotesWidget>
    with TickerProviderStateMixin {
  late AnimationController behindCardsController;
  late AnimationController frontCardController;

  final frontCardTransform = TransformClass(
    rotate: -7,
    translate: const Offset(50, 0),
    opacity: 1,
  );
  final frontCardEndTransform = TransformClass(
    rotate: 180,
    translate: const Offset(500, 500),
    opacity: 1,
  );
  late TransformClass frontCardActualTransform;
  late TransformClass frontCardActualEndTransform;

  @override
  void initState() {
    super.initState();

    behindCardsController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 400),
    );
    frontCardController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 300),
    );

    frontCardActualTransform = frontCardTransform;
    frontCardActualEndTransform = TransformClass(
      rotate: -8,
      translate: const Offset(500, 0),
      opacity: 0,
    );
  }

  @override
  Widget build(BuildContext context) {
    final bloc = context.read<QuotesBloc>();
    final state = context.watch<QuotesBloc>().state;
    final WatchAdsDataBloc watchAdsDataBloc = context.read<WatchAdsDataBloc>();

    int getActualIndex(int index) {
      if (state.visibleQuotes.size < 7) {
        return index + 7 - state.visibleQuotes.size;
      } else {
        return index;
      }
    }

    TransformClass getTransform({required int index, required bool end}) {
      final int e = getActualIndex(index);
      const double verticalScaler = .8;
      const double horizontalScaler = .9;

      final transforms = [
        TransformClass(
          rotate: -2,
          translate:
              const Offset(340 * horizontalScaler, -130 * verticalScaler),
          opacity: .35,
        ),
        TransformClass(
          rotate: -4,
          translate:
              const Offset(295 * horizontalScaler, -125 * verticalScaler),
          opacity: .6,
        ),
        TransformClass(
          rotate: -5,
          translate:
              const Offset(225 * horizontalScaler, -110 * verticalScaler),
          opacity: .75,
        ),
        TransformClass(
          rotate: -6,
          translate: const Offset(150 * horizontalScaler, -85 * verticalScaler),
          opacity: .85,
        ),
        TransformClass(
          rotate: -7,
          translate: const Offset(110 * horizontalScaler, -55 * verticalScaler),
          opacity: .94,
        ),
        TransformClass(
          rotate: -7,
          translate: const Offset(85 * horizontalScaler, -25 * verticalScaler),
          opacity: .98,
        ),
        frontCardTransform,
      ];

      if (end) {
        return transforms[e + 1];
      } else {
        return transforms[e];
      }
    }

    Future<void> next() async {
      watchAdsDataBloc.add(const WatchAdsDataEvent.anotherQuoteSeen());
      await behindCardsController.forward().then((value) {
        setState(() {
          frontCardActualTransform = frontCardTransform;
          bloc.add(const QuotesEvent.nextQuote());
          behindCardsController.reset();
        });
      });
    }

    void onDragStart(DragUpdateDetails details) {
      final delta = details.delta;
      setState(() {
        frontCardActualTransform = frontCardActualTransform.copyWith(
          translate: Offset(
            frontCardActualTransform.translate.dx + delta.dx,
            frontCardActualTransform.translate.dy + delta.dy,
          ),
        );
      });
    }

    void onDragEnd(DragEndDetails details) {
      final velocity = details.velocity.pixelsPerSecond.distance;
      final shouldGo = velocity > 1000;

      if (shouldGo == false) {
        setState(() {
          frontCardActualEndTransform = frontCardTransform;
        });
      } else {
        setState(() {
          frontCardActualEndTransform = TransformClass(
            rotate: 10,
            translate: Offset(
              details.velocity.pixelsPerSecond.dx * .5,
              details.velocity.pixelsPerSecond.dy * .5,
            ),
            opacity: .5,
          );
        });
      }

      if (shouldGo) {
        next();
      }
      frontCardController.forward().then((value) {
        if (shouldGo) {
          setState(() {
            frontCardActualTransform = TransformClass(
              rotate: -15,
              translate: details.velocity.pixelsPerSecond,
              opacity: 1,
            );
          });
        } else {
          setState(() {
            frontCardActualTransform = frontCardTransform;
          });
        }
        setState(() {
          frontCardController.reset();
        });
      });
    }

    return BlocListener<QuotesBloc, QuotesState>(
      listenWhen: (previous, current) => previous.data != current.data,
      listener: (context, state) async {
        state.data.fold(
          () => null,
          (a) => a.fold(
            (l) {
              showFailureSnackBar(context, l);
            },
            (r) => null,
          ),
        );
      },
      child: Builder(
        builder: (context) {
          if (state.remainingQuotes.isEmpty()) {
            return FailureHandlerWidget(
              data: state.data,
              child: (r) => Container(),
              onRetry: () {},
            );
          }
          return Stack(
            children: state.visibleQuotes.mapIndexed(
              (index, quote) {
                final e = getActualIndex(index);
                final quoteIndex = state.allQuotes.indexOf(quote);
                if (e == 6) {
                  return GestureDetector(
                    onPanUpdate: onDragStart,
                    onPanEnd: onDragEnd,
                    child: Center(
                      child: QuoteItemWidget(
                        onReport: next,
                        frontCard: true,
                        themeIndex: quoteIndex,
                        key: ValueKey(quote.id),
                        quote: quote,
                        transform: frontCardActualTransform,
                        endTransform: frontCardActualEndTransform,
                        controller: frontCardController,
                        onColorChanged: (_) {},
                      ),
                    ),
                  );
                }
                return Center(
                  child: QuoteItemWidget(
                    onReport: () async {},
                    frontCard: e >= 5,
                    themeIndex: quoteIndex,
                    key: ValueKey(quote.id),
                    quote: quote,
                    transform: getTransform(index: index, end: false),
                    endTransform: getTransform(index: index, end: true),
                    controller: behindCardsController,
                    onColorChanged: (_) {},
                  ),
                );
              },
            ).asList(),
          );
        },
      ),
    );
  }
}
