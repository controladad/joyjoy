part of 'explore_bloc.dart';

@injectable
@freezed
class ExploreState with _$ExploreState {
  const factory ExploreState({
    required Option<Either<GeneralFailure, ExploreModel>> categoriesData,
    required KtList<CategoryModel> categories,
  }) = _ExploreState;
  @factoryMethod
  factory ExploreState.init() => ExploreState(
        categories: emptyList(),
        categoriesData: none(),
      );
}
