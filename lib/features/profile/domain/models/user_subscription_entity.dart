import 'package:dartz/dartz.dart';
import 'package:hive/hive.dart';
import 'package:motivation_flutter/features/profile/domain/models/user_subscription_model.dart';

part 'user_subscription_entity.g.dart';

@HiveType(typeId: 9)
class UserSubscriptionEntity extends HiveObject {
  UserSubscriptionEntity();

  factory UserSubscriptionEntity.fromDomain(
    UserSubscription subscription,
  ) {
    if (subscription is PremiumCase) {
      return PremiumEntity.fromDomain(subscription);
    } else if (subscription is FreeCase) {
      return FreeEntity.fromDomain(subscription);
    } else {
      throw Exception('Unknown Subscription Plan to store in hive');
    }
  }

  UserSubscription toDomain() {
    if (this is PremiumEntity) {
      return (this as PremiumEntity).toDomain();
    } else if (this is FreeEntity) {
      return (this as FreeEntity).toDomain();
    } else {
      throw Exception('Unknown Subscription Plan to store in hive');
    }
  }
}

@HiveType(typeId: 10)
class PremiumEntity extends UserSubscriptionEntity {
  @HiveField(0)
  String name;
  @HiveField(1)
  bool hasPermissionToSeeAllQuotes;
  @HiveField(2)
  bool hasPermissionToChat;
  @HiveField(3)
  bool shouldShowAds;
  @HiveField(4)
  DateTime startDate;
  @HiveField(5)
  DateTime nextBillingDate;
  @HiveField(6)
  bool shouldRenew;
  @HiveField(7)
  DateTime lastUpdate;

  PremiumEntity({
    required this.name,
    required this.hasPermissionToChat,
    required this.hasPermissionToSeeAllQuotes,
    required this.shouldShowAds,
    required this.startDate,
    required this.nextBillingDate,
    required this.shouldRenew,
    required this.lastUpdate,
  });

  PremiumEntity copyWith({
    String? name,
    bool? hasPermissionToSeeAllQuotes,
    bool? hasPermissionToChat,
    bool? shouldShowAds,
    DateTime? startDate,
    DateTime? nextBillingDate,
    bool? shouldRenew,
    DateTime? lastUpdate,
  }) {
    return PremiumEntity(
      name: name ?? this.name,
      hasPermissionToSeeAllQuotes:
          hasPermissionToSeeAllQuotes ?? this.hasPermissionToSeeAllQuotes,
      hasPermissionToChat: hasPermissionToChat ?? this.hasPermissionToChat,
      shouldRenew: shouldRenew ?? this.shouldRenew,
      shouldShowAds: shouldShowAds ?? this.shouldShowAds,
      startDate: startDate ?? this.startDate,
      nextBillingDate: nextBillingDate ?? this.nextBillingDate,
      lastUpdate: lastUpdate ?? this.lastUpdate,
    );
  }

  factory PremiumEntity.fromDomain(
    PremiumCase subscription,
  ) {
    return PremiumEntity(
      name: subscription.name,
      hasPermissionToChat: subscription.hasPermissionChat,
      hasPermissionToSeeAllQuotes: subscription.hasPermissionToSeeAllQuotes,
      nextBillingDate: subscription.nextBillingDate,
      shouldRenew: subscription.shouldRenew,
      shouldShowAds: subscription.shouldShowAds,
      startDate: subscription.startDate,
      lastUpdate: DateTime.now(),
    );
  }

  @override
  PremiumCase toDomain() {
    return PremiumCase(
      name: name,
      hasPermissionToSeeAllQuotes: hasPermissionToSeeAllQuotes,
      hasPermissionChat: hasPermissionToChat,
      shouldShowAds: shouldShowAds,
      startDate: startDate,
      nextBillingDate: nextBillingDate,
      shouldRenew: shouldRenew,
    );
  }
}

@HiveType(typeId: 11)
class FreeEntity extends UserSubscriptionEntity {
  @HiveField(0)
  DateTime? lastPlanStartDate;
  @HiveField(1)
  DateTime? lastPlanEndDate;

  FreeEntity({
    required this.lastPlanEndDate,
    required this.lastPlanStartDate,
  });

  FreeEntity copyWith({
    DateTime? lastPlanStartDate,
    DateTime? lastPlanEndDate,
  }) {
    return FreeEntity(
      lastPlanEndDate: lastPlanEndDate ?? this.lastPlanEndDate,
      lastPlanStartDate: lastPlanStartDate ?? this.lastPlanStartDate,
    );
  }

  factory FreeEntity.fromDomain(
    FreeCase free,
  ) {
    return FreeEntity(
      lastPlanEndDate: free.lastPlanEndDate.toNullable(),
      lastPlanStartDate: free.lastPlanStartDate.toNullable(),
    );
  }

  @override
  FreeCase toDomain() {
    return FreeCase(
      lastPlanEndDate:
          lastPlanEndDate != null ? some(lastPlanEndDate!) : none(),
      lastPlanStartDate:
          lastPlanStartDate != null ? some(lastPlanStartDate!) : none(),
    );
  }
}
