import 'package:kt_dart/kt.dart';
import 'package:motivation_flutter/common/presentation/common_imports.dart';
import 'package:motivation_flutter/features/sub_auth/domain/models/review_model.dart';

class PaywallProductFeedbacksWidget extends StatelessWidget {
  final KtList<PaywallReviewModel> reviews;
  const PaywallProductFeedbacksWidget(this.reviews);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: pad(33, 0, 33, 0),
          child: SText(
            context.translate.productFeedbacks.toTitleCase(),
            style: TStyle.TitleLarge,
          ),
        ),
        const SBox(height: 12),
        SBox(
          height: 150,
          child: ListView.builder(
            padding: pad(15, 0, 15, 0),
            scrollDirection: Axis.horizontal,
            itemBuilder: (context, index) {
              final item = reviews[index];
              return Center(
                child: SContainer(
                  margin: pad(6, 0, 6, 0),
                  width: 320,
                  height: 150,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                  ),
                  child: Stack(
                    children: [
                      const ImageWidget(
                        image: Images.review,
                        width: double.infinity,
                        height: double.infinity,
                      ),
                      Padding(
                        padding: pad(20, 20, 20, 20),
                        child: Column(
                          children: [
                            Row(
                              children: [
                                SText(
                                  item.name.capitalize(),
                                  style: TStyle.BodyMedium,
                                ),
                                const Spacer(),
                                _StarWidget(stars: item.stars),
                              ],
                            ),
                            const SBox(height: 16),
                            SText(item.review, style: TStyle.BodySmall)
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              );
            },
            itemCount: reviews.size,
          ),
        ),
      ],
    );
  }
}

class _StarWidget extends StatelessWidget {
  final int stars;
  const _StarWidget({required this.stars});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: List.generate(
        5,
        (index) => Padding(
          padding: pad(2.5, 0, 2.5, 0),
          child: ImageWidget(
            image: index < stars ? AppIcons.fullStar : AppIcons.emptyStar,
            height: 20,
            width: 20,
          ),
        ),
      ).toList(),
    );
  }
}
