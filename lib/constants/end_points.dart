class EndPoints {
  EndPoints._();

  static const String baseUrl = 'https://api.joyjoy.chat/';

  static const Duration receiveTimeout = Duration(seconds: 15);

  static const Duration connectionTimeout = Duration(seconds: 30);

  static const String getPosts = '$baseUrl/posts';
}
