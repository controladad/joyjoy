import 'dart:async';
import 'package:dartz/dartz.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:kt_dart/kt.dart';
import 'package:motivation_flutter/common/presentation/common_imports.dart';
import 'package:motivation_flutter/core/failures.dart';
import 'package:motivation_flutter/features/chat/domain/chat_status.dart';
import 'package:motivation_flutter/features/chat/domain/chat_repository.dart';
import 'package:motivation_flutter/features/chat/domain/models/chat_models.dart';
import 'package:uuid/uuid.dart';

part 'chat_bloc.freezed.dart';
part 'chat_event.dart';
part 'chat_state.dart';

@injectable
class ChatBloc extends Bloc<ChatEvent, ChatState> {
  final IChatRepository repository;
  late StreamSubscription<void>? cacheUpdatesSubscription;
  ChatBloc(this.repository, ChatState init) : super(init) {
    on<ChatEvent>(_chatBloc);
    cacheUpdatesSubscription = repository.cacheUpdated.listen((newCache) async {
      add(const ChatEvent.updateCredit());
    });
  }
  @override
  Future<void> close() {
    cacheUpdatesSubscription?.cancel();
    return super.close();
  }

  Future<void> _chatBloc(ChatEvent event, Emitter emit) {
    return event.map(
      updateCredit: (e) async {
        final Either<GeneralFailure, double> result =
            await repository.chatCredit;
        emit(state.copyWith(creditCount: some(result)));
      },
      messageLiked: (e) async {
        final KtList<MessageModel> backUpList = state.messages;
        final KtList<MessageModel> newMessageList =
            state.messages.map((message) {
          if (message.id == e.id) {
            message = message.copyWith(isLiked: true);
          }
          return message;
        });

        emit(state.copyWith(messages: newMessageList));
        final Either<GeneralFailure, Unit> result =
            await repository.toggleResponseFeedback(id: e.id, isLike: true);
        result.fold(
            (l) => emit(state.copyWith(messages: backUpList)), (r) => null);
      },
      messageDisliked: (e) async {
        final KtList<MessageModel> backUpList = state.messages;
        final KtList<MessageModel> newMessageList =
            state.messages.map((message) {
          if (message.id == e.id) {
            message = message.copyWith(isLiked: false);
          }
          return message;
        });
        emit(state.copyWith(messages: newMessageList));

        final Either<GeneralFailure, Unit> result =
            await repository.toggleResponseFeedback(id: e.id, isLike: false);
        result.fold(
            (l) => emit(state.copyWith(messages: backUpList)), (r) => null);
        emit(state.copyWith(messages: newMessageList));
      },
      initial: (e) async {
        add(const ChatEvent.updateCredit());
        emit(state.copyWith(chatNeededData: none()));
        late Either<GeneralFailure, ChatNeededDataModel> result;
        result = await repository.chatNeededData;
        emit(
          state.copyWith(
            messages: const KtList.empty(),
            chatNeededData: some(result),
          ),
        );
        emit(
          state.copyWith(
            chatStatus: some(const ChatStatus.unlimitedChat()),
          ),
        );
      },
      sendMessage: (e) async {
        if (state.isLoading) return;

        emit(state.copyWith(isLoading: true));
        final MessageModel newMessage = MessageModel(
          id: const Uuid().v4(),
          text: e.message,
          dateTime: DateTime.now(),
          sender: Sender.Client,
        );
        emit(
          state.copyWith(
            messages: state.messages.plus(
              [newMessage, MessageModel.loading('${newMessage.id}#loading')].kt,
            ),
          ),
        );
        final result = await repository.sendMessage(
          message: newMessage,
          prevMessageList: state.messages.toNormalList(),
        );
        result.fold(
          (l) {
            emit(
              state.copyWith(
                isLoading: false,
                messages: state.messages.filterIndexed(
                  (index, p1) => index < state.messages.size - 2,
                ),
              ),
            );
          },
          (r) {
            emit(
              state.copyWith(
                isLoading: false,
                chatStatus: some(r.chatStatus),
                messages: state.messages.map((message) {
                  if (message.id == '${newMessage.id}#loading') {
                    return r.message;
                  } else {
                    return message;
                  }
                }),
              ),
            );
          },
        );
      },
      clearMessages: (e) async {},
    );
  }
}
