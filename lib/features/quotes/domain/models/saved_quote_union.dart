import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:motivation_flutter/features/profile/domain/models/collection_model.dart';

part 'saved_quote_union.freezed.dart';

@freezed
class SavedQuoteType with _$SavedQuoteType {
  const factory SavedQuoteType.history() = _History;
  const factory SavedQuoteType.liked() = _Liked;
  const factory SavedQuoteType.collection(CollectionModel collection) =
      _Collection;
}
