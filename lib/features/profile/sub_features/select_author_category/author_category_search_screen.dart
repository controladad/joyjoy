import 'package:dartz/dartz.dart';
import 'package:kt_dart/kt.dart';
import 'package:motivation_flutter/common/presentation/common_presentation.dart';
import 'package:motivation_flutter/common/presentation/loaders.dart';
import 'package:motivation_flutter/features/profile/sub_features/select_author_category/select_author_category_screen.dart';
import 'package:motivation_flutter/features/profile/sub_features/select_author_category/select_author_category_bloc/select_author_category_bloc.dart';
import 'package:motivation_flutter/features/profile/sub_features/select_author_category/select_author_category_args.dart';
import 'package:motivation_flutter/features/quotes/domain/models/quotes_models.dart';

@RoutePage()
class AuthorCategorySearchScreen extends StatelessWidget {
  final SelectAuthorCategoryBloc bloc;
  final SelectAuthorCategoryScreenType type;
  const AuthorCategorySearchScreen({
    super.key,
    required this.bloc,
    required this.type,
  });

  @override
  Widget build(BuildContext context) {
    final isPremium = hasSubscription(context);
    return BlocProvider.value(
      value: bloc,
      child: Builder(
        builder: (context) {
          final state = context.watch<SelectAuthorCategoryBloc>().state;
          final bloc = context.read<SelectAuthorCategoryBloc>();
          return WillPopScope(
            onWillPop: () async {
              bloc.add(SelectAuthorCategoryEvent.search(none()));
              return true;
            },
            child: CommonScaffoldWithAppbar(
              title: context.translate.search,
              body: Column(
                children: [
                  Expanded(
                    child: state.searchQuery.fold(
                      () => Container(),
                      (a) => state.searchData.fold(
                        () => const Center(child: CommonLoader()),
                        (a) => a.fold(
                          (l) => Container(),
                          (r) => ListView(
                            children: state.searchResults
                                .map(
                                  (item) => SelectAuthorCategoryItemWidget(
                                    item: item,
                                    isSearch: true,
                                    isPremiumUser: isPremium,
                                    isSelected: state.uncommittedSelects
                                        .map((p0) => p0.id)
                                        .contains(item.id),
                                    onTap: () {
                                      if (item.status ==
                                              AccessabilityStatus.Free ||
                                          isPremium) {
                                        bloc.add(
                                          SelectAuthorCategoryEvent.toggle(
                                            item,
                                          ),
                                        );
                                      } else {
                                        context.pushRoute(
                                          PaywallRoute(
                                            onClose: (ctx) => ctx.popRoute(),
                                          ),
                                        );
                                      }
                                    },
                                  ),
                                )
                                .asList(),
                          ),
                        ),
                      ),
                    ),
                  ),
                  SBox(
                    height: 70,
                    padding: pad(20, 20, 20, 20),
                    child: SearchTextField(
                      hintText: context.translate.category.capitalize(),
                      autofocus: true,
                      onChanged: (value) {
                        // if (value.isNotEmpty) {
                        bloc.add(
                          SelectAuthorCategoryEvent.search(some(value)),
                        );
                        // }
                      },
                    ),
                  ),
                  const CommonBottomInsetsWidget(),
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
