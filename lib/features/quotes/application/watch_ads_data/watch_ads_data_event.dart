part of 'watch_ads_data_bloc.dart';

@freezed
class WatchAdsDataEvent with _$WatchAdsDataEvent {
  const factory WatchAdsDataEvent.anotherQuoteSeen() = _AnotherQuoteSeen;
  const factory WatchAdsDataEvent.inAppReviewSeen() = _InAppReviewSeen;
  const factory WatchAdsDataEvent.getWatchAdsData() = _GetWatchAdsData;
}
