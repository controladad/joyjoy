class DBConstants {
  DBConstants._();

  static const String DB_NAME = 'hive';

  static const String MAIN_DB_PATH = 'hiveDb';

  // Put box names here:
  static const String USER = 'userBox';
  static const String LANGUAGE = 'languageBox';
  static const String SEARCH_HISTORY = 'searchHistory';
  static const String SUBSCRIPTION = 'subscription';
  static const String USER_WATCH_ADS = 'userWatchAds';

  static const String ALL_QUOTES = 'allQuotes';
  static const String NEW_QUOTES = 'newQuotes';
  static const String NOTIFICATION_QUOTES = 'notificationQuotes';

  static const String CATEGORIES = 'categories';
  static const String AUTHORS = 'authors';
  static const String QUOTES_CACHE_PROPS = 'quotesCacheProps';
}
