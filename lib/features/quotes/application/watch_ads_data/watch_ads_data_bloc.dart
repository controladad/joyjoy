import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:motivation_flutter/features/user/domain/models/user_facade.dart';

part 'watch_ads_data_event.dart';
part 'watch_ads_data_state.dart';
part 'watch_ads_data_bloc.freezed.dart';

@injectable
class WatchAdsDataBloc extends Bloc<WatchAdsDataEvent, WatchAdsDataState> {
  final IUserFacade iUserFacade;
  WatchAdsDataBloc(this.iUserFacade, WatchAdsDataState init) : super(init) {
    on<WatchAdsDataEvent>(_bloc);
  }
  Future<void> _bloc(WatchAdsDataEvent event, Emitter emit) {
    emit(state.copyWith(whatShouldDoAfterSeenQuote: none()));
    return event.map(
      anotherQuoteSeen: (e) async {
        final WhatShouldDoAfterSeenSomeQuoteType nextStuff =
            await iUserFacade.persistWatchAdsData(anotherQuoteSeen: true);
        emit(state.copyWith(whatShouldDoAfterSeenQuote: some(nextStuff)));
      },
      inAppReviewSeen: (e) async {
        final WhatShouldDoAfterSeenSomeQuoteType nextStuff =
            await iUserFacade.persistWatchAdsData(seenInAppReview: true);
        emit(state.copyWith(whatShouldDoAfterSeenQuote: some(nextStuff)));
      },
      getWatchAdsData: (e) async {
        final WhatShouldDoAfterSeenSomeQuoteType nextStuff =
            await iUserFacade.getWatchAdsData;
        emit(state.copyWith(whatShouldDoAfterSeenQuote: some(nextStuff)));
      },
    );
  }
}
