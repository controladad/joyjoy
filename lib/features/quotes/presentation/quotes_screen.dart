import 'package:motivation_flutter/common/presentation/common_presentation.dart';
import 'package:motivation_flutter/common/presentation/utils/failure_handler_widget.dart';
import 'package:motivation_flutter/features/quotes/application/quotes_bloc/quotes_bloc.dart';
import 'package:motivation_flutter/features/quotes/domain/models/quotes_screen_type.dart';
import 'package:motivation_flutter/features/quotes/presentation/quotes_widget.dart';

@RoutePage()
class QuotesScreen extends StatelessWidget {
  final QuotesScreenType type;
  const QuotesScreen({super.key, required this.type});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) =>
          getIt<QuotesBloc>()..add(QuotesEvent.fetchData(type)),
      child: CommonScaffoldWithAppbar(
        title: type.map(
          feeds: (e) => '',
          category: (e) => e.categoryName.capitalize(),
          author: (e) => e.authorName.capitalize(),
        ),
        body: Builder(
          builder: (context) {
            final bloc = context.read<QuotesBloc>();
            final state = context.watch<QuotesBloc>().state;
            return FailureHandlerWidget(
              data: state.data,
              onRetry: () => bloc.add(QuotesEvent.fetchData(type)),
              child: (r) => Column(
                children: [
                  const Spacer(),
                  QuotesWidget(
                    type: type,
                    quotesBloc: bloc,
                  ),
                  const Spacer(),
                ],
              ),
            );
          },
        ),
      ),
    );
  }
}
