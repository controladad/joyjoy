import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:motivation_flutter/core/failures.dart';
import 'package:motivation_flutter/features/profile/domain/models/user_subscription_model.dart';
import 'package:motivation_flutter/features/sub_auth/domain/models/paywall_model.dart';

import 'package:motivation_flutter/features/sub_auth/domain/sub_auth_repository.dart';

part 'sub_auth_bloc.freezed.dart';
part 'sub_auth_state.dart';

@injectable
class SubAuthCubit extends Cubit<SubAuthState> {
  final ISubAuthRepository repository;

  SubAuthCubit(
    this.repository,
    SubAuthState init,
  ) : super(init) {
    // repository.userUpdated.listen(
    //   (event) => add(const SubAuthEvent.checkAuthentication()),
    // );
    // repository.subscriptionUpdated.listen(
    //   (event) => add(const SubAuthEvent.checkSubscription()),
    // );
    repository.cacheUpdated.listen(
      (event) => checkDataCompletion(),
    );
  }
  Future<void> init() async {
    emit(
      state.copyWith(
        subscription: none(),
        isAuthenticated: none(),
        successOrFailure: none(),
      ),
    );
    // add(const SubAuthEvent.checkAuthentication());
    // add(const SubAuthEvent.checkSubscription());
    // add(const SubAuthEvent.checkDataCompletion());

    await checkDataCompletion();
  }

  Future<void> restoreSubscription() async {
    final lastSub = state.subscription;
    emit(state.copyWith(subscription: none()));
    final result = await repository.restoreSubscription();
    result.fold((l) {
      emit(state.copyWith(subscription: lastSub));
    }, (r) {
      emit(state.copyWith(subscription: some(r)));
    });
  }

  Future<void> changeSubscription(
    PayWallSubscriptionPlanModel plan,
  ) async {
    emit(state.copyWith(subscription: none()));
    final result = await repository.changeSubscription(plan);
    result.fold((l) => null, (r) {
      emit(state.copyWith(subscription: some(r)));
    });
  }

  Future<void> checkSubscription() async {
    final subscription = await repository.getSubscription();
    subscription.fold(
      (l) => emit(
        state.copyWith(subscription: some(UserSubscription.empty())),
      ),
      (r) => emit(state.copyWith(subscription: some(r))),
    );
  }

  Future<void> checkAuthentication() async {
    final isAuth = await repository.isAuthenticated();
    emit(state.copyWith(isAuthenticated: some(isAuth)));
  }

  Future<void> checkDataCompletion() async {
    final isDataCompleted = await repository.isDataCompleted();
    emit(state.copyWith(hasCompleteData: some(isDataCompleted)));
  }

  Future<void> signInWithGoogle() async {
    emit(
      state.copyWith(
        successOrFailure: none(),
        loginLoading: some(LoginMethod.Google),
      ),
    );
    final result = await repository.signInWithGoogle();
    emit(state.copyWith(successOrFailure: some(result), loginLoading: none()));
  }

  Future<void> signInWithApple() async {
    emit(
      state.copyWith(
        successOrFailure: none(),
        loginLoading: some(LoginMethod.Apple),
      ),
    );
    final result = await repository.signInWithApple();
    emit(state.copyWith(successOrFailure: some(result), loginLoading: none()));
  }

  Future<void> logout() async {
    emit(state.copyWith(successOrFailure: none(), isLoading: true));
    final result = await repository.signOut();
    emit(state.copyWith(successOrFailure: some(result), isLoading: false));
  }

  Future<bool> deleteAccount() async {
    emit(state.copyWith(successOrFailure: none(), isLoading: true));
    final result = await repository.deleteAccount();
    emit(state.copyWith(successOrFailure: some(result), isLoading: false));
    return result.fold((l) => false, (r) => r);
  }

  Future<void> cancelRenew() async {}
  Future<void> userWatchedAd(double rewardValue) async {
    repository.userRewarded(rewardValue);
  }
}
