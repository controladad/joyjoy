part of 'update_info_bloc.dart';

@injectable
@freezed
class UpdateInfoState with _$UpdateInfoState {
  const factory UpdateInfoState({
    required Option<Either<GeneralFailure, UpdateInfoModel>> updateInfoModel,
  }) = _UpdateInfoState;
  @factoryMethod
  factory UpdateInfoState.init() => UpdateInfoState(updateInfoModel: none());
}
