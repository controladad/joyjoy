import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:kt_dart/kt.dart';

abstract class IProfileApiService {
  Future<Response> get getCustomerServiceInfo;
  Future<Response> get getTermsOfUse;
  Future<Response> get getBanners;
  Future<Response> get getAboutUs;
  Future<Response> get getPrivacyPolicy;
  Future<Response> get appUpdateInfo;

  Future<Response> getCategories({
    required int page,
    required Option<String> searchQuery,
    required Option<KtList<String>> ids,
    required int limit,
  });

  Future<Response> getAuthors({
    required int page,
    required Option<String> searchQuery,
    required Option<KtList<String>> ids,
    required int limit,
  });
}

@Injectable(as: IProfileApiService)
class ProfileApiServiceImplementation implements IProfileApiService {
  final Dio api;

  ProfileApiServiceImplementation(this.api);

  @override
  Future<Response> getCategories({
    required int page,
    required Option<String> searchQuery,
    required Option<KtList<String>> ids,
    required int limit,
  }) async {
    final response = await api.get(
      'api/V1/quote/category-list',
      queryParameters: {
        'page': page,
        'limit': limit,
        if (searchQuery.isSome()) 'search': searchQuery.toNullable(),
        if (ids.isSome()) 'ids': ids.toNullable()?.joinToString(separator: ',')
      },
    );
    return response;
  }

  @override
  Future<Response> getAuthors({
    required int page,
    required Option<String> searchQuery,
    required Option<KtList<String>> ids,
    required int limit,
  }) {
    return api.get(
      'api/V1/quote/author-list',
      queryParameters: {
        'page': page,
        'limit': limit,
        if (searchQuery.isSome()) 'search': searchQuery.toNullable(),
        if (ids.isSome()) 'ids': ids.toNullable()?.joinToString(separator: ',')
      },
    );
  }

  @override
  Future<Response> get getAboutUs => api.get('api/V1/core/about-us');

  @override
  Future<Response> get getCustomerServiceInfo =>
      api.get('api/V1/core/customer-service');

  @override
  Future<Response> get getPrivacyPolicy =>
      api.get('api/V1/core/privacy-policy');

  @override
  Future<Response> get getTermsOfUse => api.get('api/V1/core/terms-of-service');

  @override
  Future<Response> get getBanners => api.get('api/V1/core/home');

  @override
  Future<Response> get appUpdateInfo => api.get('api/V1/core/app-update');
}
