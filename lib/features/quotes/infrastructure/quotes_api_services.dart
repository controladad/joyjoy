import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';

abstract class IQuotesApiServices {
  Future<Response> reportQuote({
    required String quoteId,
    required String reportText,
  });
}

@Injectable(as: IQuotesApiServices)
class QuotesApiServicesImplementation implements IQuotesApiServices {
  final Dio api;

  QuotesApiServicesImplementation(this.api);

  @override
  Future<Response> reportQuote({
    required String quoteId,
    required String reportText,
  }) async {
    final response = await api.post(
      'api/V1/quote/$quoteId/report',
      data: jsonEncode({'report_message': reportText}),
    );
    return response;
  }
}
