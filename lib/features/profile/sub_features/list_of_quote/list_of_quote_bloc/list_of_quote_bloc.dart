import 'package:dartz/dartz.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:kt_dart/kt.dart';
import 'package:motivation_flutter/common/presentation/common_imports.dart';
import 'package:motivation_flutter/core/failures.dart';

import 'package:motivation_flutter/features/profile/domain/profile_repository.dart';
import 'package:motivation_flutter/features/quotes/domain/models/quotes_model.dart';
import 'package:motivation_flutter/features/quotes/domain/models/saved_quote_union.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

part 'list_of_quote_bloc.freezed.dart';
part 'list_of_quote_event.dart';
part 'list_of_quote_state.dart';

@injectable
class ListOfQuoteBloc extends Bloc<ListOfQuoteEvent, ListOfQuoteState> {
  final IProfileRepository repository;
  ListOfQuoteBloc(this.repository, ListOfQuoteState init) : super(init) {
    on<ListOfQuoteEvent>(_bloc);
  }

  late SavedQuoteType destination;

  Future<void> _bloc(ListOfQuoteEvent event, Emitter emit) {
    return event.map(
      getFirstPage: (e) async {
        destination = e.destination;
        emit(state.copyWith(isLoading: true));

        final result =
            await repository.getQuotesByDestination(destination: e.destination);

        emit(
          state.copyWith(
            isLoading: false,
            listOfQuoteResultsData: some(result),
          ),
        );

        result.fold((l) => null, (r) {
          emit(state.copyWith(results: r));
        });
      },
      refresh: (e) async {
        emit(state.copyWith(isLoading: true));
        final result =
            await repository.getQuotesByDestination(destination: destination);

        emit(
          state.copyWith(
            isLoading: false,
            listOfQuoteResultsData: some(result),
          ),
        );
        result.fold((l) {}, (r) {
          emit(state.copyWith(results: r));

          result.fold((l) {
            state.controller.refreshFailed();
          }, (r) {
            state.controller.refreshCompleted();
          });
        });
      },
      removeQuote: (e) async {
        final result =
            await repository.removeQuote(type: destination, quoteId: e.id);
        result.fold((l) {}, (r) {
          emit(
            state.copyWith(
              results: r,
              listOfQuoteResultsData: some(result),
            ),
          );
        });
      },
      search: (e) async {
        emit(
          state.copyWith(
            searchQuery: e.searchQuery.isEmpty ? none() : some(e.searchQuery),
          ),
        );
        final KtList<QuoteModel> allQuotes = state.listOfQuoteResultsData.fold(
          () => emptyList(),
          (a) => a.fold(
            (l) => emptyList(),
            (r) => r,
          ),
        );
        if (e.searchQuery.isNotEmpty) {
          emit(
            state.copyWith(
              results: allQuotes.filter(
                (quote) => quote.quote
                    .toLowerCase()
                    .contains(e.searchQuery.toLowerCase()),
              ),
            ),
          );
        } else {
          emit(state.copyWith(results: allQuotes));
        }
      },
    );
  }
}
