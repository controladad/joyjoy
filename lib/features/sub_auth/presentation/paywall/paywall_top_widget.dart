import 'package:motivation_flutter/common/presentation/buttons.dart';
import 'package:motivation_flutter/common/presentation/common_imports.dart';

class PaywallTopBarWidget extends StatelessWidget {
  final String name;
  final Function() onClose;
  final Function() onRestore;
  const PaywallTopBarWidget({
    required this.name,
    required this.onClose,
    required this.onRestore,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: pad(30, 12, 30, 0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              CustomOutlinedButton(
                maxWidth: 100,
                label: context.translate.restore,
                onTap: onRestore,
              ),
              IconButton(
                onPressed: onClose,
                icon: const ImageWidget(
                  height: 20,
                  width: 20,
                  image: AppIcons.cross,
                ),
              )
            ],
          ),
          const SBox(height: 20),
          SText(
            context.translate.raiseYourSpirits.capitalize(),
            style: TStyle.TitleLarge,
          ),
          SText(
            '${context.translate.letsMakeYou} $name 🤩',
            style: TStyle.TitleLarge,
          ),
          const SBox(height: 20),
          const Center(
            child: ImageWidget(
              image: Images.paywallImage,
              height: 191,
              width: 286,
            ),
          ),
        ],
      ),
    );
  }
}
