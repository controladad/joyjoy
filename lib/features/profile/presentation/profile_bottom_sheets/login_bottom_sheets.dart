import 'dart:io';

import 'package:motivation_flutter/common/presentation/common_imports.dart';
import 'package:motivation_flutter/common/presentation/utils/custom_bottom_sheets.dart';
import 'package:motivation_flutter/core/failures.dart';
import 'package:motivation_flutter/features/sub_auth/application/sub_auth_bloc/sub_auth_bloc.dart';

void showLoginBottomSheet(
  BuildContext context,
  void Function(bool res) onClose,
) {
  showCustomBottomSheet(
    context: context,
    onClose: () {},
    child: _LoginBottomSheets(onClose),
  );
}

class _LoginBottomSheets extends StatelessWidget {
  final void Function(bool result) onDone;

  const _LoginBottomSheets(this.onDone);

  @override
  Widget build(BuildContext context) {
    final bloc = context.read<SubAuthCubit>();
    final state = context.watch<SubAuthCubit>().state;

    return BlocListener<SubAuthCubit, SubAuthState>(
      listenWhen: (previous, current) {
        return previous.successOrFailure.isSome() !=
            current.successOrFailure.isSome();
      },
      listener: (context, state) {
        state.successOrFailure.fold(
          () => null,
          (a) => a.fold(
            (l) {
              return showFailureSnackBar(context, l);
            },
            (r) {
              onDone(r);
              if (r) {
                Navigator.pop(context);
              } else {
                print('hello');
                return showFailureSnackBar(
                  context,
                  const GeneralFailure.unexpected(),
                );
              }
            },
          ),
        );
      },
      child: Column(
        children: [
          SText(context.translate.signUpLogin, style: TStyle.TitleLarge),
          const SBox(height: 10),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8),
            child: SText(
              context.translate.join_joyjoy_now_message,
              style: TStyle.TitleMedium,
              textAlign: TextAlign.center,
            ),
          ),
          Padding(
            padding: pad(16, 25, 16, 32),
            child: Column(
              children: [
                if (Platform.isIOS)
                  SizedBox(
                    height: 48,
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        backgroundColor: AppTheme.white,
                      ),
                      onPressed: () => bloc.signInWithApple(),
                      child: Row(
                        children: [
                          if (state.loginLoading
                              .fold(() => false, (a) => a == LoginMethod.Apple))
                            const _Loader(AppTheme.black)
                          else
                            const ImageWidget(
                              image: AppIcons.apple,
                              height: 20,
                              width: 20,
                            ),
                          const SBox(width: 16),
                          Text(
                            context.translate.continueWithApple,
                            style: const TextStyle(
                              fontFamily: 'sf_pro',
                              color: AppTheme.black,
                              fontWeight: FontWeight.w500,
                              fontSize: 19,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                const SBox(height: 10),
                SizedBox(
                  height: 48,
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        backgroundColor: AppTheme.blue),
                    onPressed: () => bloc.signInWithGoogle(),
                    child: Row(
                      children: [
                        if (state.loginLoading
                            .fold(() => false, (a) => a == LoginMethod.Google))
                          const _Loader(AppTheme.white)
                        else
                          const ImageWidget(
                            image: AppIcons.google,
                            height: 20,
                            width: 20,
                          ),
                        const SBox(width: 16),
                        Text(
                          context.translate.continueWithGoogle,
                          style: const TextStyle(
                            fontFamily: 'roboto',
                            color: AppTheme.white,
                            fontWeight: FontWeight.w500,
                            fontSize: 19,
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class _Loader extends StatelessWidget {
  final Color color;
  const _Loader(this.color);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        SizedBox.square(
          dimension: 18,
          child: Center(
            child: CircularProgressIndicator(
              strokeWidth: 3,
              color: color,
            ),
          ),
        ),
      ],
    );
  }
}
