import 'dart:io';

import 'package:flutter_bloc/flutter_bloc.dart';
// ignore: depend_on_referenced_packages
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:kt_dart/kt.dart';

import 'package:motivation_flutter/features/language/domain/models/language_model.dart';
import 'package:motivation_flutter/features/user/domain/models/user_facade.dart';

part 'language_cubit.freezed.dart';
part 'language_state.dart';

@injectable
class LanguageCubit extends Cubit<LanguageState> {
  final IUserFacade _userFacade;
  LanguageCubit(super.initState, this._userFacade) {
    _init();
  }

  List<LanguageModel> supportedLanguages = [];

  void _init() {
    final currentLanguage = _userFacade.currentLanguage;

    final languageState = currentLanguage.fold(
      () {
        final locale = Platform.localeName.substring(0, 2);
        final code = Platform.localeName.substring(3, 5);
        final languageModel = LanguageModel(
          locale: locale,
          code: code,
        );
        return state.copyWith(currentLanguage: languageModel);
      },
      (curLanguage) => state.copyWith(
        currentLanguage: curLanguage,
        isSelected: true,
      ),
    );
    emit(languageState);
  }

  Future<void> changeLanguage(LanguageModel languageModel) async {
    await _userFacade.storeUserLocale(languageModel: languageModel);
    final stateToEmit = state.copyWith(
      currentLanguage: languageModel,
      isSelected: true,
    );
    emit(stateToEmit);
  }
}
