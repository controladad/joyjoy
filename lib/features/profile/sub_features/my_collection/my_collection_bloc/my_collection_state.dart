part of 'my_collection_bloc.dart';

@injectable
@freezed
class MyCollectionState with _$MyCollectionState {
  const factory MyCollectionState({
    required Option<Either<GeneralFailure, KtList<CollectionModel>>> data,
    required bool isLoading,
    required KtList<CollectionModel> collections,
    required KtList<String> changeList,
  }) = _MyCollectionState;

  @factoryMethod
  factory MyCollectionState.init() => MyCollectionState(
        data: none(),
        collections: emptyList(),
        isLoading: true,
        changeList: emptyList(),
      );
}
