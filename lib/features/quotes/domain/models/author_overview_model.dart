import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:motivation_flutter/features/quotes/domain/models/quotes_models.dart';

part 'author_overview_model.freezed.dart';

@freezed
class AuthorModel with _$AuthorModel {
  const factory AuthorModel({
    required String id,
    required String name,
    required String shortDescription,
    required int birthYear,
    required int deathYear,
    required String job,
    required String country,
    required String gender,
    required AccessabilityStatus status,
  }) = _AuthorModel;
}
