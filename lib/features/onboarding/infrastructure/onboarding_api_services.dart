import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';

abstract class IOnboardingApiService {
  Future<Response> getCategories();
  Future<Response> getAuthors();
  Future<Response> getReviews();
}

@Injectable(as: IOnboardingApiService)
class OnboardingApiServiceImplementation implements IOnboardingApiService {
  final Dio api;

  OnboardingApiServiceImplementation(this.api);

  @override
  Future<Response> getCategories() {
    return api.get(
      'api/V1/quote/category-list',
      queryParameters: {
        'page': 1,
        'limit': 10,
      },
    );
  }

  @override
  Future<Response> getAuthors() {
    return api.get(
      'api/V1/quote/author-list',
      queryParameters: {
        'page': 1,
        'limit': 15,
      },
    );
  }

  @override
  Future<Response> getReviews() {
    throw UnimplementedError();
  }
}
